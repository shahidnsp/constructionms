<?php namespace App;

/**
 * @property integer $bank_id
 * @property string  $type
 * @property string $iban
 */
use Illuminate\Database\Eloquent\Model;

class Account extends Model {


    protected $fillable = ['bank_id', 'number', 'type', 'name', 'iban', 'issueDate', 'balance' ];

//    protected $dateFormat = 'Y-m-d\TH:i:sP'; //W3C date formatting


    public function account_particulars()
    {
        return $this->hasMany('App\Account_particular');
    }

    public function checkque()
    {
        return $this->hasMany('App\Checkque');

    }

    public function card()
    {
        return $this->hasMany('App\Card');

    }

    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }

    public function loan()
    {
        return $this->hasMany('App\Loan');
    }

    //Cascade delete
    public static function boot()
    {
        parent::boot();


        static::created(function ($account) {
            if($account->type!='lo') {
                $bank = \App\Bank::find($account->bank_id);
                if ($bank != null) {
                    \App\Ledger::create([
                        'name' => $account->name,
                        'unders_id' => '29',
                        'openingBalance' => $account->balance,
                        'crOrDr' => 'Dr',
                        'bankAccountNumber' => $account->number,
                        'branchName' => $bank->branch,
                        'branchCode' => $bank->branchcode,
                        'isedit' => 1,
                        'bankaccount_id' => $account->id
                    ]);
                }
            }
        });

        static::updated(function ($account) {
            $bank=\App\Bank::find($account->bank_id);
            if($bank!=null) {
                $ledger = \App\Ledger::where('bankaccount_id', '=', $account->id)->get()->first();
                $ledger->name = $account->name;
                $ledger->bankAccountNumber=$account->number;
                $ledger->branchName=$bank->branch;
                $ledger->branchCode=$bank->branchcode;
                $ledger->save();
            }
        });


        static::deleted(function($account){
            $account->loan()->delete();
            $account->card()->delete();
            $account->checkque()->delete();
            $account->account_particulars()->delete();
            \App\Ledger::where('bankaccount_id','=',$account->id)->delete();
        });
    }

}
