<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Account_particular extends Model {

	//
    protected $fillable = ['account_id','payDate','description','chequeNo','withdrawal','deposit','balance' ];

    public function account()
    {
        return $this->belongsTo('App\Account');

    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function($particular){
            $account=\App\Account::find($particular->account_id);

            if($particular->withdrawal!=null)
                $account->balance+=$particular->withdrawal;

            if($particular->deposit!=null)
                $account->balance-=$particular->deposit;

            $account->update();

            \App\Journal::where('bankpayment_id','=',$particular->id)->delete();
        });

        static::created(function($particular){
            $account=\App\Account::find($particular->account_id);

            if($account!=null) {
                if ($particular->withdrawal != null)
                    $account->balance -= $particular->withdrawal;

                if ($particular->deposit != null)
                    $account->balance += $particular->deposit;

                $account->update();
            }

            $ledger=\App\Ledger::where('bankaccount_id',$particular->account_id)->get()->first();

            if($ledger!=null) {
                if ($particular->deposit != null || $particular->deposit != 0) {
                    $journal = new \App\Journal();
                    $journal->drledgers_id = $ledger->id;
                    $journal->crledgers_id = 1;
                    $journal->dramount = $particular->deposit;
                    $journal->cramount = $particular->deposit;
                    $journal->note = $particular->description;
                    $journal->vouchertype = "Bank Deposit";
                    $journal->date = $particular->payDate;
                    $journal->bankpayment_id = $particular->id;
                    $journal->save();
                }

                if ($particular->withdrawal != null || $particular->withdrawal != 0) {
                    $journal = new \App\Journal();
                    $journal->drledgers_id = 1;
                    $journal->crledgers_id = $ledger->id;
                    $journal->dramount = $particular->withdrawal;
                    $journal->cramount = $particular->withdrawal;
                    $journal->note = $particular->description;
                    $journal->vouchertype = "Bank Withdrawal";
                    $journal->date = $particular->payDate;
                    $journal->bankpayment_id = $particular->id;
                    $journal->save();
                }
            }

        });

        static::updating(function($particular){

            /*$account=\App\Account::find($particular->account_id);

            if($particular->withdrawal!=null)
                $account->balance+=$particular->withdrawal;

            if($particular->deposit!=null)
                $account->balance-=$particular->deposit;

            return $account->save();*/
        });

        static::updated(function($particular){
            $account=\App\Account::find($particular->account_id);

            if($account!=null) {
                if ($particular->withdrawal != null)
                    $account->balance -= $particular->withdrawal;

                if ($particular->deposit != null)
                    $account->balance += $particular->deposit;

                $account->update();
            }

            //Delete before Updating Journal Entry
            \App\Journal::where('bankpayment_id','=',$particular->id)->delete();

            $ledger=\App\Ledger::where('bankaccount_id',$particular->account_id)->get()->first();
            if($ledger!=null) {
                if ($particular->deposit != null || $particular->deposit != 0) {
                    $journal = new \App\Journal();
                    $journal->drledgers_id = $ledger->id;
                    $journal->crledgers_id = 1;
                    $journal->dramount = $particular->deposit;
                    $journal->cramount = $particular->deposit;
                    $journal->note = $particular->description;
                    $journal->vouchertype = "Bank Deposit";
                    $journal->date = $particular->payDate;
                    $journal->bankpayment_id = $particular->id;
                    $journal->save();
                }

                if ($particular->withdrawal != null || $particular->withdrawal != 0) {
                    $journal = new \App\Journal();
                    $journal->drledgers_id = 1;
                    $journal->crledgers_id = $ledger->id;
                    $journal->dramount = $particular->withdrawal;
                    $journal->cramount = $particular->withdrawal;
                    $journal->note = $particular->description;
                    $journal->vouchertype = "Bank Withdrawal";
                    $journal->date = $particular->payDate;
                    $journal->bankpayment_id = $particular->id;
                    $journal->save();
                }
            }
        });
    }


}
