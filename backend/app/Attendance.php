<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = ['date','employees_id','attendance_groups_id','wage','hour','total','status'];


    public function employees()
    {
        return $this->belongsTo('App\Employee','employees_id');
    }
    public function attendancegroup()
    {
        return $this->belongsTo('App\AttendanceGroup','attendance_groups_id');
    }
}
