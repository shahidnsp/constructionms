<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceGroup extends Model
{

    protected $fillable = ['date','leaders_id','projects_id'];
    /*protected $appends = ['month'];

    public function getMonthAttribute()
    {
        return \Carbon\Carbon::createFromTimestamp(strtotime($this->attributes['date']))->format('M-Y');
    }*/

    public function leaders()
    {
        return $this->belongsTo('App\Leader','leaders_id');
    }
    public function projects()
    {
        return $this->belongsTo('App\Project','projects_id');
    }
    public function attendances()
    {
        return $this->hasMany('App\Attendance','attendance_groups_id');
    }
}
