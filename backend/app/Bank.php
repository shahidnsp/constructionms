<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model {

    protected $fillable = ['name', 'branch', 'address', 'ifsc', 'branchcode', 'micrcode', 'contact', 'swiftcode'];

    public function account()
    {
        return $this->hasMany('App\Account');

    }

    public function loan()
    {
        return $this->hasMany('App\Loan');
    }

    //Cascade delete
    public static function boot()
    {
        parent::boot();

        static::deleted(function ($bank) {
            $bank->loan()->delete();
        });

        static::deleted(function ($bank) {
            $bank->account()->delete();
        });
    }
}
