<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model {

    protected $fillable = ['number','name','description','payDate','amount','balance','paid','file_id'];
    public function photo()
    {
        return $this->hasOne('App\File');
    }

}
