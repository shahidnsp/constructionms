<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cash extends Model
{
    //
    protected $table = 'cash';
    protected $dates = ['created_at', 'updated_at', 'date'];
    protected $fillable = ['date','totaldebit','totalcredit'];
    protected $appends = ['month'];

    public function getMonthAttribute()
    {
        return \Carbon\Carbon::createFromTimestamp(strtotime($this->attributes['date']))->format('M-Y');
    }
}
