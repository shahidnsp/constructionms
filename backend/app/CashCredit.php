<?php

namespace App;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class CashCredit extends Model
{
    //
    protected  $table = 'cashcredit';
    protected $fillable = ['amount', 'description', 'date' ];

    public static function boot()
    {
        parent::boot();

        //TODO month wise sum test

        static::updating(function($credit){
//            $cash = \App\Cash::find(1);

            $closingMonth =Carbon::createFromTimestamp(strtotime($credit->date));
            //Try to find record for closing month if not make one
            $cash = \App\Cash::firstOrNew(['date'=>substr($closingMonth->endOfMonth(),0,10)]);

            $cash->totalcredit -= $credit->amount;

            if($cash->totalcredit<0)
                $cash->totalcredit = 0;

            $cash->save();
        });

        static::saved(function($credit){

//            $cash = \App\Cash::find(1);
            $closingMonth =Carbon::createFromTimestamp(strtotime($credit->date));
            //Try to find record for closing month if not make one
            $cash = \App\Cash::firstOrNew(['date'=>substr($closingMonth->endOfMonth(),0,10)]);

            $cash->totalcredit += $credit->amount;

            $cash->save();
        });

        static::deleted(function($credit){
//            $cash = \App\Cash::find(1);

            $closingMonth =Carbon::createFromTimestamp(strtotime($credit->date));
            //Try to find record for closing month if not make one
            $cash = \App\Cash::firstOrNew(['date'=>substr($closingMonth->endOfMonth(),0,10)]);

            $cash->totalcredit -= $credit->amount;

            if($cash->totalcredit<0)
                $cash->totalcredit =0;

            $cash->save();
        });
    }
}
