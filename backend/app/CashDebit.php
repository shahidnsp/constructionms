<?php

namespace App;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class CashDebit extends Model
{
    //
    protected  $table = 'cashdebit';
    protected $fillable = ['amount', 'description', 'date' ];

    public static function boot()
    {
        parent::boot();

        static::updating(function($debit){

            $closingMonth = Carbon::createFromTimestamp(strtotime($debit->date));
            //Try to find record for new month if not make one
            $cash = \App\Cash::firstOrNew(['date'=>substr($closingMonth->endOfMonth(),0,10)]);

            $cash->totaldebit -= $debit->amount;

            if($cash->totaldebit<0)
                $cash->totaldebit = 0;

            return $cash->save();
        });

        static::saved(function($debit){

            $closingMonth = Carbon::createFromTimestamp(strtotime($debit->date));
            //Try to find record for new month if not make one
            $cash = \App\Cash::firstOrNew(['date'=>substr($closingMonth->endOfMonth(),0,10)]);

            $cash->totaldebit += $debit->amount;

            $cash->save();
        });


        static::deleted(function($debit){

            $closingMonth = Carbon::createFromTimestamp(strtotime($debit->date));

            //Try to find record for new month if not make one
            $cash = \App\Cash::firstOrNew(['date'=>substr($closingMonth->endOfMonth(),0,10)]);

            $cash->totaldebit -= $debit->amount;

            if($cash->totaldebit<0)
                $cash->totaldebit = 0;

            $cash->save();
        });




    }
}
