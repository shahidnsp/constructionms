<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkque extends Model {

	//

    protected  $table = 'cheques';

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function checkque_record()
    {
        return $this->hasMany('App\Checkque_record');
    }
   //Cascade delete
    public static function boot()
    {
        parent::boot();

        static::deleted(function ($checkque) {
            $checkque->checkque_record()->detach();
        });
    }
}
