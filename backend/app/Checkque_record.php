<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkque_record extends Model {

	//

    protected $table='cheque_records';


    public function checkque()
    {
        return $this->belongsTo('App\Checkque');

    }
}
