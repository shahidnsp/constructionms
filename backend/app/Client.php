<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name', 'address', 'phone','mobile','email','place', 'district', 'description', 'visited', 'firstvisiting', 'reminderdate','openBal','crOrDr'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($client) {
            \App\Ledger::create([
                'name'=>$client->name,
                'unders_id'=>'27',
                'address'=>$client->address,
                'phone'=>$client->phone,
                'email'=>$client->email,
                'mobile'=>$client->mobile,
                'openingBalance'=>$client->openBal,
                'crOrDr'=>$client->crOrDr,
                'isedit'=>1,
                'clients_id'=>$client->id
            ]);
        });

        static::updated(function ($client) {
            $ledger=\App\Ledger::where('clients_id','=',$client->id)->get()->first();
            $ledger->name=$client->name;
            $ledger->save();
        });

        static::deleted(function ($client) {
            \App\Ledger::where('clients_id','=',$client->id)->delete();
        });
    }
}
