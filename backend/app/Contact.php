<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

	//
    protected  $fillable = ['title', 'name', 'lastname','address', 'email1', 'email2', 'website','photo'];

    protected $hidden = ['created_at','updated_at'];


    public function user()
    {
       return $this->belongsTo('App\User');
    }

    public function file()
    {
        return $this->belongsTo('App\File');
    }

   public  function phone()
   {
       return $this->hasMany('App\Phone');//->select(['number','id']);
   }
    public function insurance()
    {
        return $this->hasOne('App\Insurance');
    }
   /* public function  employee()
    {
        return $this->hasOne('App\Employee');
    }*/


    //Cascade delete
    public static function boot()
    {
        parent::boot();

        static::deleted(function($contact){
//            TODO cascade delete all related models
           $contact->phone()->delete();
        });



        static::deleted(function($contact){
            $contact->insurance()->delete();
        });
    }

}
