<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model {

    protected $fillable = ['name','description','projects_id','date'];
    public  function project()
    {
        return $this->belongsTo('App\Project','projects_id');
    }

    public  function files()
    {
        return $this->hasMany('App\File','documents_id');
    }

}
