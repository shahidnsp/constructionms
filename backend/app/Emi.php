<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Emi extends Model {

    protected  $table = 'emi';

    protected $fillable = ['loan_id', 'amount', 'description', 'date'];

    public static function boot()
    {
        parent::boot();

        static::updating(function($emi){

            /*$loan=\App\Loan::find($emi->loan_id);

            $loan->paid += $emi->amount;
            $loan->balance -= $emi->amount;

            $loan->save();*/
        });


        static::deleted(function($emi){
            $loan=\App\Loan::find($emi->loan_id);

            if($loan!=null) {
                $loan->paid -= $emi->amount;
                $loan->balance += $emi->amount;

                $loan->update();
            }
            \App\Journal::where('loanpayment_id','=',$emi->id)->delete();

        });

        static::created(function($emi){
            $loan=\App\Loan::find($emi->loan_id);

            if($loan!=null) {
                $loan->paid += $emi->amount;
                $loan->balance -= $emi->amount;

                $loan->update();
            }

            $ledger=\App\Ledger::where('loan_id',$emi->loan_id)->get()->first();
            if($ledger!=null) {
                $journal = new \App\Journal();
                $journal->drledgers_id = $ledger->id;
                $journal->crledgers_id = 3;
                $journal->dramount = $emi->amount;
                $journal->cramount = $emi->amount;
                $journal->note = "Emi Paid to " . $ledger->name;
                $journal->vouchertype = "Loan Payment";
                $journal->date = $emi->date;
                $journal->loanpayment_id = $emi->id;
                $journal->save();
            }
        });

        static::updated(function($emi){
            $loan=\App\Loan::find($emi->loan_id);

            if($loan!=null) {
                $loan->paid += $emi->amount;
                $loan->balance -= $emi->amount;

                $loan->update();
            }

            \App\Journal::where('loanpayment_id','=',$emi->id)->delete();

            $ledger=\App\Ledger::where('loan_id',$emi->loan_id)->get()->first();
            if($ledger!=null) {
                $journal = new \App\Journal();
                $journal->drledgers_id = $ledger->id;
                $journal->crledgers_id = 3;
                $journal->dramount = $emi->amount;
                $journal->cramount = $emi->amount;
                $journal->note = "Emi Paid to " . $ledger->name;
                $journal->vouchertype = "Loan Payment";
                $journal->date = $emi->date;
                $journal->loanpayment_id = $emi->id;
                $journal->save();
            }
        });
    }

    public function loan()
    {
        return $this->belongsTo('App\Loan');
    }
}
