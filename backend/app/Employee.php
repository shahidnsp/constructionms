<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model {

    protected $fillable = [ 'name', 'address', 'phone', 'mobile', 'place', 'basic', 'ta', 'da', 'hra', 'medical', 'total', 'joinDate', 'releavingDate', 'employeeType', 'active','photo'];


    public function attendances()
    {
        return $this->hasMany('App\Attendance', 'employees_id');
    }
    public function wage()
    {
        return $this->hasMany('App\Wage','employees_id');
    }
    
   /* public function contact()
    {
        return $this->belongsTo('App\Contact');
    }*/

    public function employeeSalary()
    {
        return $this->hasMany('App\EmployeeSalary');
    }

    public function staffAttendance()
    {
        return $this->hasMany('App\StaffAttendance','employees_id');
    }

    public function employeeAdvance()
    {
        return $this->hasMany('App\EmployeeAdvance');
    }

//    public static function boot()
//    {
//        parent::boot();
//
//        static::created(function ($employee) {
//            \App\Ledger::create([
//                'name'=>$employee->name. '\'s Account',
//                'unders_id'=>'8',
//                'type'=>'Other',
//                'isedit'=>1,
//                'date'=>$employee->joinDate,
//                'employees_id'=>$employee->id
//            ]);
//
//            /*if($supplier->openBal!=null) {
//                $currentLedger = \App\Ledger::where('suppliers_id', $supplier->id)->get()->first();
//
//                if ($supplier->paymentmode === 'Debit') {
//                    \App\Journal::create([
//                        'drledgers_id' => $currentLedger->id,
//                        'crledgers_id' => '3',
//                        'dramount' => $supplier->openBal,
//                        'cramount' => $supplier->openBal,
//                        'note' => 'Opening balance of' . $supplier->name,
//                        'vouchertype' => 'Journal',
//                        'date' => $supplier->date
//                    ]);
//                } else {
//                    \App\Journal::create([
//                        'drledgers_id' => '3',
//                        'crledgers_id' => $currentLedger->id,
//                        'dramount' => $supplier->openBal,
//                        'cramount' => $supplier->openBal,
//                        'note' => 'Opening balance of' . $supplier->name,
//                        'vouchertype' => 'Journal',
//                        'date' => $supplier->date
//                    ]);
//                }
//            }*/
//
//
//        });
//
//        static::updated(function ($employee) {
//            $ledger=\App\Ledger::where('employees_id','=',$employee->id)->get()->first();
//            $ledger->name=$employee->name.'\'s Account';
//            $ledger->save();
//        });
//
//        static::deleted(function ($employee) {
//            \App\Ledger::where('employees_id','=',$employee->id)->delete();
//
//            $employee->employeeAdvance()->delete();
//            $employee->employeeSalary()->delete();
//        });
//    }
}
