<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeSalary extends Model
{
    protected $fillable = ['date', 'employees_id', 'basic', 'ta', 'da', 'hra', 'medical', 'total', 'paid', 'balance', 'advance','amount','prebalance', 'description','ledgers_id'];
    protected $table = 'employeeSalaries';

    public function employee()
    {
        return $this->belongsTo('App\Employee','employees_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($salary) {

            $emp=Employee::find($salary->employees_id);

            if($emp!=null) {
                if($salary->paid!=0 || $salary->paid > 0) {
                    $journal = new \App\Journal();
                    $journal->drledgers_id = 4;
                    $journal->crledgers_id = $salary->ledgers_id;
                    $journal->dramount = $salary->paid;
                    $journal->cramount = $salary->paid;
                    $journal->note = "Salary Paid to " . $emp->name;
                    $journal->vouchertype = "Salary";
                    $journal->date = $salary->date;
                    $journal->salary_id = $salary->id;
                    $journal->save();
                }
            }

            $advance=\App\SalaryAdvace::where('employees_id',$salary->employees_id)->get()->last();
            if($advance!=null) {
                $advance->balance = $advance->balance - $salary->advance;
                $advance->save();
            }


            /*$prebalance = \App\EmployeeSalary::where('employees_id', $salary->employees_id)->get()->last();
            if($prebalance!=null) {
                $prebalance->prebalance -=  $salary->prebalance;
                $prebalance->save();
            }*/

        });
        static::updating(function ($salary) {
            $advance= \App\WageAdvance::where('employees_id',$salary->employees_id)->get()->last();
            $advance->balance=$advance->balance+$salary->advance;
            $advance->update();
        });

        static::updated(function ($salary) {

            \App\Journal::where('salary_id','=',$salary->id)->delete();


            $emp=Employee::find($salary->employees_id);

            if($emp!=null) {
                if($salary->paid!=0 || $salary->paid > 0) {
                    $journal = new \App\Journal();
                    $journal->drledgers_id = 4;
                    $journal->crledgers_id = $salary->ledgers_id;
                    $journal->dramount = $salary->paid;
                    $journal->cramount = $salary->paid;
                    $journal->note = "Salary Paid to " . $emp->name;
                    $journal->vouchertype = "Salary";
                    $journal->date = $salary->date;
                    $journal->salary_id = $salary->id;
                    $journal->save();
                }
            }

            $advance=\App\SalaryAdvace::where('employees_id',$salary->employees_id)->get()->last();
            if($advance!=null) {
                $advance->balance = $advance->balance - $salary->advance;
                $advance->save();
            }
        });

        static::deleted(function ($salary) {
            $advance=\App\SalaryAdvace::where('employees_id',$salary->employees_id)->get()->last();
            if($advance!=null) {
                $advance->balance = $advance->balance + $salary->advance;
                $advance->save();
            }
            \App\Journal::where('salary_id','=',$salary->id)->delete();
        });
    }
}
