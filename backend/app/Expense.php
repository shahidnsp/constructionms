<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model {

    protected $fillable = ['name','description','amount','payDate','projects_id'];

    public function projects()
    {
        return $this->belongsTo('App\Project','projects_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($expense) {

            $ledger=\App\Ledger::where('projects_id',$expense->projects_id)->get()->first();

            $project=\App\Project::find($expense->projects_id);

            $journal = new \App\Journal();
            $journal->drledgers_id = $ledger->id;
            $journal->crledgers_id = 3;
            $journal->dramount = $expense->amount;
            $journal->cramount = $expense->amount;
            $journal->note = "Cash Paid to ".$expense->name.' for '.$project->name;
            $journal->vouchertype = "Expense";
            $journal->date = $expense->payDate;
            $journal->expenses_id = $expense->id;
            $journal->save();

        });

        static::updated(function ($expense) {


            \App\Journal::where('expenses_id','=',$expense->id)->delete();

            $ledger=\App\Ledger::where('projects_id',$expense->projects_id)->get()->first();
            $project=\App\Project::find($expense->projects_id);

            $journal = new \App\Journal();
            $journal->drledgers_id = $ledger->id;
            $journal->crledgers_id = 3;
            $journal->dramount = $expense->amount;
            $journal->cramount = $expense->amount;
            $journal->note = "Cash Paid to ".$expense->name.' for '.$project->name;
            $journal->vouchertype = "Expense";
            $journal->date = $expense->payDate;
            $journal->expenses_id = $expense->id;
            $journal->save();

        });

        static::deleted(function ($expense) {
            \App\Journal::where('expenses_id','=',$expense->id)->delete();
        });
    }
}
