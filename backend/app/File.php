<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class File extends Model {

	//

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function contact()
    {
        return $this->hasMany('App\Contact');
    }

    public function bill()
    {
        return $this->belongsTo('App\Bill');
    }

    public function insurance()
    {
        return $this->belongsTo('App\Insurance');
    }

    public function dp()
    {
        return $this->hasOne('App\Personaldetail');
    }

    public function pd()
    {
        return $this->hasMany('App\Personaldetail');
    }


}
