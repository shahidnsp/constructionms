<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected  $fillable=['leader_id','employee_id'];

    public function leaders()
    {
        return $this->belongsTo('App\Leader','leader_id');
    }

    public function employees()
    {
        return $this->belongsTo('App\Employee','employee_id');
    }
}
