<?php

/**
 * Account controller excepts loan account
 * for loan account please check LoanController
 */

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request ;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;
use Auth;
use App\User;
use App\Account;
use App\Bank;

class AccountController extends Controller
{

    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {

        // Loan account is not validated


        return Validator::make($data,[
            'bank_id'  => 'required',
            'number'   => 'required|max:255',
            'type'     => 'required|in:sb,cr,o,lo',
            'name'     => 'required|max:255',
            'issueDate'=> 'required|date',
            'balance'  => 'required|numeric|min:0|max:999999999999999999',
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $bank_id= $request->get('bank_id');
//        $bank_id = '2';
        if($bank_id !== NULL)
            return Bank::find($bank_id)->account;
        else
        return Account::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $account = new Account($request->all());
        $user=User::findOrFail(Auth::id());
        $account->user=$user->username;
        if($account->save()){
            return $account;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Account::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        //

        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $account = Account::find($id);
        /*$account->fill($request->all());
        $user=User::findOrFail(Auth::id());
        $account->user=$user->username;*/
        if($account->update($request->all())){
            return $account;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO Account model for cascade delete

        if(Account::destroy($id))
            return Response::json(array('msg'=>'Banks deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
