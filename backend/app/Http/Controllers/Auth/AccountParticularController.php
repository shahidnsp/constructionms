<?php

namespace App\Http\Controllers\Auth;



use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Auth;
use App\User;
use App\Account;
use App\Account_particular;


class AccountParticularController extends Controller
{

    /**
     * Validates given data for account particular
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'account_id'  =>'required',
            'payDate'  =>'required|date',
            'description'  =>'required|max:255',
            //'deposit'   =>'required|numeric|max:999999999999999999',
           // 'balance'  =>'required|numeric|max:999999999999999999',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $account_id=$request->get('account_id');
        if($account_id !== NULL)
            return Account::find($account_id)->account_particulars;
        else
            return Account_particular::all();
    }

    /**
     * Function to remove particular amount from Account
     * @param $particular
     */
    protected function remAmout($particular)
    {
        $account=\App\Account::find($particular->account_id);

        if($particular->withdrawal != null && $particular->deposit == null)
            $account->balance += $particular->withdrawal;

        if($particular->deposit != null && $particular->withdrawal == null)
            $account->balance -= $particular->deposit;

        return $account->save();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $particular=new Account_particular($request->all());
        $user=User::findOrFail(Auth::id());
        $particular->user=$user->username;
        if($particular->save()) {
            return $particular;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Account_particular::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $particular=Account_particular::find($id);

        /*$particular->fill($request->all());
        $user=User::findOrFail(Auth::id());
        $particular->user=$user->username;*/
        if($this->remAmout($particular) && $particular->update($request->all())){
            return $particular;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO delete particular balance change
        if(Account_particular::destroy($id))
            return Response::json(array('msg'=>'Account Particular deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}