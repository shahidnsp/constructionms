<?php

namespace App\Http\Controllers\Auth;

use App\Attendance;
use App\AttendanceGroup;
use App\Employee;

use Illuminate\Http\Request as Request ;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class AttendanceController extends Controller
{
    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {
        return Validator::make($data,[
//            'attendance_groups_id'  => 'required',
            'employees_id'   => 'required',
//            'wage'   => 'required',
//            'hour'   => 'required',
//            'total'   => 'required',
            'status'   => 'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $attendance= Attendance::with('employees','attendancegroup')->get();
    }


    /***
     * get attendance report
     * @param $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAttendanceReport(Request $request)
    {

        $month = $request->month;
//        $attendance = Attendance::with('attendancegroup')->with('employees')->get();
        $attendance = Employee::with(['attendances.attendancegroup' =>function($query){
            $query->where('month', 'May 2016')->orderBy('date');
        }])->with('attendances.attendancegroup.leaders')->where('employeeType','Non Office Staff')->get();
        return $attendance;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $attendance = new Attendance($request->all());

        if($attendance->save()){
            return $attendance;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $attendance= Attendance::with('employees','attendancegroup.leaders.employee')->where('attendance_groups_id',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $attendance = Attendance::find($id);
        if($attendance->update($request->all())){
            return $attendance;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO Attendance model for cascade delete

        if(Attendance::where('attendance_groups_id','=',$id)->delete())
            return Response::json(array('msg'=>'Attendance deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
