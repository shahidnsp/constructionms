<?php

namespace App\Http\Controllers\Auth;

use App\Attendance;
use App\AttendanceGroup;
use App\Group;
use Carbon\Carbon;
use Illuminate\Http\Request as Request ;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class AttendanceGroupController extends Controller
{
    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {
        return Validator::make($data,[
            'date'  => 'required',
            'leaders_id'   => 'required',
            'projects_id'   => 'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }

        return $attendance= AttendanceGroup::with('leaders.employee','projects')->where('date','>=',$from)->where('date','<=',$to)->get();
    }



    public function getAttendanceList(Request $request)
    {
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }

        $groups= AttendanceGroup::with('leaders.employee','projects','attendances.employees')->get();//->where('date','>=',$from)->where('date','<=',$to)->get();

        $lists=[];
        $i=0;

        foreach($groups as $group){
            //$list['employee']=$attendance->attendances->employees->name;
            $list['date']=$group->date;
            $list[$group->date]=$group->leaders->employee->name;
            $lists[$i]=$list;
            $i++;
        }

        return $lists;
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAttendanceDateList()
    {
        return AttendanceGroup::whereNotNull('date')
            ->orderBy('date','desc')
            ->get();
    }

    public  function getWage(Request $request)
    {
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }
        $wages=[];
        $i=0;
        $groups=AttendanceGroup::where('leaders_id',$request->leaders_id)->where('date','>=',$from)->where('date','<=',$to)->get();
        $employees=Group::where('leader_id',$request->leaders_id)->with('employees')->get();
        foreach($employees as $employee) {
            $present=0;
            $absent=0;
            $name=$employee->employees->name;
            $total=0;
            foreach($groups as $group){
                $attendance=Attendance::where('attendance_groups_id',$group->id)->where('employees_id',$employee->employee_id)->get()->first();
                if($attendance!=null) {
                    if ($attendance->status === 'P') {
                        $present++;
                        $total += $attendance->total;
                    } else {
                        $absent++;
                    }
                }

            }
            $wage['name']=$name;
            $wage['present']=$present;
            $wage['absent']=$absent;
            $wage['total']=$total;
            $wages[$i]=$wage;
            $i++;
        }

        return $wages;



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $attendance = new AttendanceGroup($request->all());
        //$attendance->month=\Carbon\Carbon::createFromTimestamp(strtotime($this->$request->date)->format('M-Y'));
        if($attendance->save()){
            return $attendance;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $attendance = AttendanceGroup::find($id);
        $attendance->month=\Carbon\Carbon::createFromTimestamp(strtotime($this->$request->date)->format('M-Y'));
        if($attendance->update($request->all())){
            return $attendance;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO Attendance Group model for cascade delete

        if(AttendanceGroup::destroy($id))
            return Response::json(array('msg'=>'Attendance Group deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
