<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use Validator;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
Use App\Bank;
Use App\User;
class BankController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'  => 'required|max:60',
            'branch'    => 'required|max:60',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return Bank::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $validator = $this->validator($request->all());
        if($validator->fails()){
             return Response::json( $validator->errors()
                 ,400);
        }

        $bank = new Bank($request->all());
        $user=User::findOrFail(Auth::id());
        $bank->user=$user->username;
        if($bank->save()){
            return $bank;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Bank::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        //

        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $bank = Bank::find($id);
        $bank->fill($request->all());
        $user=User::findOrFail(Auth::id());
        $bank->user=$user->username;
        if($bank->save()){
            return $bank;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO Bank model for cascade delete

        if(Bank::destroy($id))
            return Response::json(array('msg'=>'Banks deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
