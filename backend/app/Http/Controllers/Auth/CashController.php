<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cash;

class CashController extends Controller
{
    public function getCashReport()
    {
        return Cash::whereNotNull('date')
            ->orderBy('date','desc')
            ->get();
    }
}
