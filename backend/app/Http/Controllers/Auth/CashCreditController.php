<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Validator;
use Response;
use Auth;
use App\User;
use App\CashCredit;

class CashCreditController extends Controller
{

    /**
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'amount'      =>'required|numeric|max:999999999999999999',
            'description' =>'required|alpha_text',
            'date'        =>'required|date'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return CashCredit::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors(),
                400);
        }

        $credit = new CashCredit($request->all());
        $user=User::findOrFail(Auth::id());
        $credit->user=$user->username;

        if($credit->save()){
            return $credit;
        }

        return Response::json(['error'=>'Server is down'],500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        return CashCredit::findOrfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        //
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $credit = CashCredit::find($id);
//        $credit->fill($request->all());

        if($credit->update($request->all())){
            return $credit;
        }

        return Response::json(['error'=>'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        if(CashCredit::destroy($id))
            return Response::json(array('msg'=>'Cash credit record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
