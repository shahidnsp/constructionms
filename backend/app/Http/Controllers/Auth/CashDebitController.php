<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use Response;
use Auth;
use App\User;
use App\CashDebit;

class CashDebitController extends Controller
{
    /**
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'amount'      =>'required|numeric|max:999999999999999999',
            'description' =>'required|alpha_text',
            'date'        =>'required|date'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return CashDebit::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return CashDebit|\Illuminate\Http\JsonResponse*
     */

    public function store(Request $request)
    {
        //
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors(),
                400);
        }

        $debit = new CashDebit($request->all());
        $user=User::findOrFail(Auth::id());
        $debit->user=$user->username;
        if($debit->save()){
            return $debit;
        }

        return Response::json(['error'=>'Server is down'],500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        return CashDebit::findOrfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param integer $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id,Request $request)
    {
        //
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $debit = CashDebit::find($id);
//        $debit->fill($request->all());

        if($debit->update($request->all())){
            return $debit;
        }

        return Response::json(['error'=>'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        if(CashDebit::destroy($id))
            return Response::json(array('msg'=>'Cash debit record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
