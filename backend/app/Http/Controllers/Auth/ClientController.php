<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Client;

use Response;
use Validator;

class ClientController extends Controller
{

    /**
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
//            'place' => 'required',
            'firstvisiting' => 'required',
            'reminderdate' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return Client::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(),400);
        }

        $clients = new Client($request->all());
        if ($clients->save()) {
            return $clients;
        } else {
            return Response::json(['error' => 'Server Down'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Client::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(),400);
        }
        $client = Client::find($id);
        if ($client->update($request->all())) {
            return $client;
        }

        return Response::json(['error' => 'server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Client::destroy($id)) {
            return Response::json(['msg' => 'Record deleted from clints']);
        } else {
            return Response::json(['error' => 'Record not found']);
        }
    }
}
