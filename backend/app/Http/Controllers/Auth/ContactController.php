<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use Auth;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;
use Validator;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Contact;
use App\Phone;
use App\Employee;

class ContactController extends Controller
{
    protected function validator(array $data)
    {

        //TODO photo field decision
        return Validator::make($data,
            [
                'title'          => 'required|max:5',
                'name'           => 'required|max:45',
                //'lastname'       => 'required|max:45',
                //'address'        => 'required|max:250',
               // 'email1'         => 'required|max:254|email',
               // 'email2'         => 'max:254|email',
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        return Contact::with('phone','user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //

    }

    protected function getUserId()
    {
        //TODO change here to get user id
//        return 1;/
        return Auth::id();

    }

    protected function getDefaultPhoto(){
        //TODO change here to get Photo id
        return 1;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if($request->id==null) {
            //
            $validator = $this->validator($request->all());
            if ($validator->fails()) {
                return Response::json($validator->errors()
                    , 400);
            }


            $contact = new Contact($request->all());
            $contact->user_id = $this->getUserId();
            $contact->file_id = $this->getDefaultPhoto();

            if ($request->hasFile('photo')) {
                $files = $request->file('photo');
                //foreach($files as $file) {
                $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg');
                $validator = Validator::make(array('photo'=> $files), $rules);
                if($validator->passes()){
                    $contact->photo = $this->uploadImage($files);
                }
                //}
            }

            if ($contact->save()) {
                //Get all phone record
                $phones = $request->only('phone')['phone'];

                if($phones != null) {
                    //Save each phone number with new contact Id
                    foreach ($phones as $phone) {

                        $validNumber = Validator::make($phone, array('number' => 'required|min:5'));

                        if ($validNumber->passes()) {

                            $phoneObj = new Phone($phone);
                            $contact->phone()->save($phoneObj);
                        }
                    }
                }


                return Contact::with('phone', 'user')->find($contact->id); //TODO optimize
            }
        }else{
            $validator = $this->validator($request->all());
            if($validator->fails()){
                return Response::json($validator->errors()
                    ,400);
            }

            $contact = Contact::find($request->id);
            $contact->fill($request->all());
            /*if ($request->hasFile('photo')) {
                $files = $request->file('photo');
                //foreach($files as $file) {
                $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg');
                $validator = Validator::make(array('photo'=> $files), $rules);
                if($validator->passes()){
                    $contact->photo = $this->uploadImage($files);
                }
                //}
            }*/
            if($contact->save()){
                //Get all phone records
                $phones = $request->only('phone')['phone'];

                //Update each phone number or create if not exists
                foreach($phones as $phone){
                    $validNumber = Validator::make($phone,array('number'=>'required|min:5'));

                    if($validNumber->passes()){

                        if(isset($phone['id'])){

                            $phoneObj = Phone::find($phone['id']);
                            $phoneObj->number = $phone['number'];
                            $phoneObj->save();
                        }else{
                            $phoneObj = new Phone($phone);
                            $contact->phone()->save($phoneObj);
                        }


                    }
                }
                return Contact::with('phone','user')->find($contact->id);
            }
        }

        return Response::json( ['error' => 'Server is down']
        ,500);
    }

    /**
     * Upload file into server
     * @param $file
     * @return string
     */
    public  function uploadImage($file){
        $storedFileName="";
        if(!empty($file)){

            $extension=$file->getClientOriginalExtension();
            $fileName = rand(11111,99999).'.'.$extension;
            /* $imageRealPath 	= 	$file->getRealPath();
             $img = Image::make($imageRealPath); // use this if you want facade style code
             $img->resize(intval('500'), null, function($constraint) {
                 $constraint->aspectRatio();
             });*/
            $storedFileName=$fileName;
            //$storedFileName=$fileName;
            /* $file->move(
                 public_path().'/img/upload/', $fileName
             );*/

            //$img->save(public_path().$storedFileName);
            /*$path = public_path().'img/upload/'.$fileName;*/
            //$image = Image::make($file->getRealPath())->resize(200, 200);//->save($path);

            Storage::disk('local')->put($fileName,file_get_contents($file->getRealPath()));

        }
        return $storedFileName;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {

        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $contact = Contact::find($id);
        $contact->fill($request->all());
        if($contact->save()){
            //Get all phone records
            $phones = $request->only('phone')['phone'];

            //Update each phone number or create if not exists
            foreach($phones as $phone){
                $validNumber = Validator::make($phone,array('number'=>'required|min:5'));

                if($validNumber->passes()){

                    if(isset($phone['id'])){

                        $phoneObj = Phone::find($phone['id']);
                        $phoneObj->number = $phone['number'];
                        $phoneObj->save();
                    }else{
                        $phoneObj = new Phone($phone);
                        $contact->phone()->save($phoneObj);
                    }


                }
            }
            return Contact::with('phone','user')->find($contact->id);
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Contact::with('phone','user')->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        return Contact::with('phone','user')->findOrFail($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */

    public function destroy($id)
    {
        $contact=Contact::find($id);
        if($contact->photo != 'profile.jpg') {
            $exist=Storage::disk('local')->exists($contact->photo);
            if($exist)
                Storage::delete($contact->photo);
        }

        $count = Contact::destroy($id);

        if($count)
            return Response::json(array('msg'=>$count.' records deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}