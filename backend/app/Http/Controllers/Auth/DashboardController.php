<?php

namespace App\Http\Controllers\Auth;

use App\Journal;
use App\Ledger;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    /*function __construct(){
        $this->middleware('auth');
    }*/

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //        return \Auth::check()?'User logged in':'User not logged in';
//        return \Auth::getUser();

        return view('app.index');
    }


    public function getStatus()
    {
        $lists=[];
            //Get Cash Balance
            $ledgers = Ledger::where('unders_id',28)->get();
            $cashBalance=0;
            if($ledgers!=null) {
                foreach ($ledgers as $account) {
                    $debit = 0;
                    $credit = 0;
                    $journals = Journal::where('drledgers_id', $account->id)->get();
                    foreach ($journals as $journal) {
                        $debit += $journal->dramount;
                    }
                    $journals = Journal::where('crledgers_id', $account->id)->get();
                    foreach ($journals as $journal) {
                        $credit += $journal->cramount;
                    }
                    $openBal = $account->openingBalance;
                    if ($account->crOrDr == 'Dr') {
                        $debit = $debit + $openBal;
                    } elseif ($account->crOrDr == 'Cr') {
                        $credit = $credit + $openBal;
                    }
                    if ($credit > $debit) {
                        $bal = $credit - $debit;
                        $cashBalance -= $bal;
                    } else {
                        $bal = $debit - $credit;
                        $cashBalance += $bal;
                    }
                }
            }
        if($cashBalance<0){
            $cashBalance=-1*$cashBalance;
            $lists['cashBalance']=$cashBalance.' Cr';
        }else{
            $lists['cashBalance']=$cashBalance.' Dr';
        }

        //Get Sundry Debtors Balance
        $ledgers = Ledger::where('unders_id',27)->get();
        if($ledgers!=null) {
            $sundryDebtorBalance = 0;
            foreach ($ledgers as $account) {
                $debit = 0;
                $credit = 0;
                $journals = Journal::where('drledgers_id', $account->id)->get();
                foreach ($journals as $journal) {
                    $debit += $journal->dramount;
                }
                $journals = Journal::where('crledgers_id', $account->id)->get();
                foreach ($journals as $journal) {
                    $credit += $journal->cramount;
                }
                $openBal = $account->openingBalance;
                if ($account->crOrDr == 'Dr') {
                    $debit = $debit + $openBal;
                } elseif ($account->crOrDr == 'Cr') {
                    $credit = $credit + $openBal;
                }
                if ($credit > $debit) {
                    $bal = $credit - $debit;
                    $sundryDebtorBalance -= $bal;
                } else {
                    $bal = $debit - $credit;
                    $sundryDebtorBalance += $bal;
                }
            }
            if($sundryDebtorBalance<0){
                $sundryDebtorBalance=-1*$sundryDebtorBalance;
                $lists['sundryDebtor']=$sundryDebtorBalance.' Cr';
            }else{
                $lists['sundryDebtor']=$sundryDebtorBalance.' Dr';
            }
        }

        //Get Sundry Debtors Balance
        $ledgers = Ledger::where('unders_id',23)->get();
        if($ledgers!=null) {
            $sundryCreditorBalance = 0;
            foreach ($ledgers as $account) {
                $debit = 0;
                $credit = 0;
                $journals = Journal::where('drledgers_id', $account->id)->get();
                foreach ($journals as $journal) {
                    $debit += $journal->dramount;
                }
                $journals = Journal::where('crledgers_id', $account->id)->get();
                foreach ($journals as $journal) {
                    $credit += $journal->cramount;
                }
                $openBal = $account->openingBalance;
                if ($account->crOrDr == 'Dr') {
                    $debit = $debit + $openBal;
                } elseif ($account->crOrDr == 'Cr') {
                    $credit = $credit + $openBal;
                }
                if ($credit > $debit) {
                    $bal = $credit - $debit;
                    $sundryCreditorBalance -= $bal;
                } else {
                    $bal = $debit - $credit;
                    $sundryCreditorBalance += $bal;
                }
            }
            if($sundryCreditorBalance<0){
                $sundryDebtorBalance=-1*$sundryCreditorBalance;
                $lists['sundryCreditor']=$sundryCreditorBalance.' Cr';
            }else{
                $lists['sundryCreditor']=$sundryCreditorBalance.' Dr';
            }
        }

        //Get indirectExpense Balance
        $ledgers = Ledger::where('unders_id',16)->get();
        if($ledgers!=null) {
            $indirectExpense = 0;
            foreach ($ledgers as $account) {
                $debit = 0;
                $credit = 0;
                $journals = Journal::where('drledgers_id', $account->id)->get();
                foreach ($journals as $journal) {
                    $debit += $journal->dramount;
                }
                $journals = Journal::where('crledgers_id', $account->id)->get();
                foreach ($journals as $journal) {
                    $credit += $journal->cramount;
                }
                $openBal = $account->openingBalance;
                if ($account->crOrDr == 'Dr') {
                    $debit = $debit + $openBal;
                } elseif ($account->crOrDr == 'Cr') {
                    $credit = $credit + $openBal;
                }
                if ($credit > $debit) {
                    $bal = $credit - $debit;
                    $indirectExpense -= $bal;
                } else {
                    $bal = $debit - $credit;
                    $indirectExpense += $bal;
                }
            }
            if($indirectExpense<0){
                $sundryDebtorBalance=-1*$indirectExpense;
                $lists['indirectExpense']=$indirectExpense.' Cr';
            }else{
                $lists['indirectExpense']=$indirectExpense.' Dr';
            }
        }

        //Get indirect Income Balance
        $ledgers = Ledger::where('unders_id',15)->get();
        if($ledgers!=null) {
            $indirectIncome = 0;
            foreach ($ledgers as $account) {
                $debit = 0;
                $credit = 0;
                $journals = Journal::where('drledgers_id', $account->id)->get();
                foreach ($journals as $journal) {
                    $debit += $journal->dramount;
                }
                $journals = Journal::where('crledgers_id', $account->id)->get();
                foreach ($journals as $journal) {
                    $credit += $journal->cramount;
                }
                $openBal = $account->openingBalance;
                if ($account->crOrDr == 'Dr') {
                    $debit = $debit + $openBal;
                } elseif ($account->crOrDr == 'Cr') {
                    $credit = $credit + $openBal;
                }
                if ($credit > $debit) {
                    $bal = $credit - $debit;
                    $indirectIncome -= $bal;
                } else {
                    $bal = $debit - $credit;
                    $indirectIncome += $bal;
                }
            }
            if($indirectIncome<0){
                $sundryDebtorBalance=-1*$indirectIncome;
                $lists['indirectIncome']=$indirectIncome.' Cr';
            }else{
                $lists['indirectIncome']=$indirectIncome.' Dr';
            }
        }

        $userscount=User::count();
        $lists['usercount']=$userscount;

        return $lists;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
