<?php

namespace App\Http\Controllers\Auth;

use App\Document;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;
use Response;
use Auth;

class DocumentController extends Controller
{
    /**
     * Validates given data for Rent
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name' =>'required|max:255',
            'projects_id'   =>'required|numeric|max:999999999999999999',
            'date' =>'required|date',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Document::with('project')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        /*$validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $doc=new Document($request->all());



        if($doc->save()){

            if ($request->hasFile('photo')) {
                $files = $request->file('photo');
                foreach($files as $file) {
                    $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg,pdf,doc,docx');
                    $validator = Validator::make(array('photo'=> $file), $rules);
                    if($validator->passes()){
                        $docFile=new File();
                        $docFile->name=$this->uploadImage($file);
                        $docFile->extension=$file->getClientOriginalExtension();
                        $docFile->documents_id=$doc->id;
                        $docFile->save();
                    }
                }

            }

            return $doc;
        }*/
        if ($request->hasFile('photo')) {
            $files = $request->file('photo');
            foreach($files as $file) {
                $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg,pdf,doc,docx');
                $validator = Validator::make(array('photo'=> $file), $rules);
                if($validator->passes()){
                    $docFile=new File();
                    $docFile->name=$this->uploadImage($file);
                    $docFile->extension=$file->getClientOriginalExtension();
                    //$docFile->documents_id=$doc->id;
                    $docFile->save();
                }
            }
            return 'ssss';

        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Upload file into server
     * @param $file
     * @return string
     */
    public  function uploadImage($file){
        $storedFileName="";
        if(!empty($file)){

            $extension=$file->getClientOriginalExtension();
            $fileName = 'Doc'.rand(11111,99999).'.'.$extension;
            $imageRealPath 	= 	$file->getRealPath();
            $img =Image::make($imageRealPath); // use this if you want facade style code
            $img->resize(intval('500'), null, function($constraint) {
                $constraint->aspectRatio();
            });
            $storedFileName=$fileName;
            //$storedFileName=$fileName;
            /* $file->move(
                 public_path().'/img/upload/', $fileName
             );*/

            //$img->save(public_path().$storedFileName);
            /*$path = public_path().'img/upload/'.$fileName;*/
            $image = Image::make($file->getRealPath())->resize(200, 200);//->save($path);

            Storage::disk('local')->put($fileName,$image->stream());//file_get_contents($file->getRealPath()));

        }
        return $storedFileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $doc= Document::find($id);

        $doc->fill($request->all());

      /*  $user=User::findOrFail(Auth::id());
        $rent->user=$user->username;*/

        if($doc->save()){
            return $doc;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(Document::destroy($id))
            return Response::json(array('msg'=>'Document deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
