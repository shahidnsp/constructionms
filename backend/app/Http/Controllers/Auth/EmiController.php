<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Auth;
use App\User;
use App\Account;
use App\Loan;
use App\Emi;

class EmiController extends Controller
{

    /**
     * New data validation
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'loan_id'     =>'required|numeric',
            'amount'      =>'required|numeric|max:999999999999999999',
            //'description' =>'required|alpha_text|max:250',
            'date'        =>'required|date',
        ]);
    }

    /**
     * Function to reduce emi amount from loan
     * @param $emi
     */

    protected function removeEmi($emi)
    {
        $loan=\App\Loan::find($emi->loan_id);

        $loan->paid -= $emi->amount;
        $loan->balance += $emi->amount;
        return $loan->save();
    }

    /**
     * Function to add emi amount to loan
     * @param $emi
     */

    protected function addEmi($emi)
    {
        $loan=\App\Loan::find($emi->loan_id);

        $loan->paid += $emi->amount;
        $loan->balance -= $emi->amount;
        $loan->save();
    }


    /**
     * Display a listing of the Emi resource.
     * If loan id is not given, then returns all Emi resource.
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */

    public function index(Request $request)
    {
        $loan_id = $request->get('loan_id');

        //All Emi recipets for loan
        if($loan_id !==NULL)
            return Loan::find($loan_id)->emi;
        else
            return Emi::with('loan')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $emi = new Emi($request->all());
        $user=User::findOrFail(Auth::id());
        $emi->user=$user->username;
        if($emi->save()){
            return $emi;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        return Emi::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        //TODO Update loan  balance
        $emi = Emi::find($id);

        if($this->removeEmi($emi)&&$emi->update($request->all())){
            $this->addEmi($emi);
            return $emi;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO delete emi balance change in loan
        if(Emi::destroy($id))
            return Response::json(array('msg'=>'Emi deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }


}
