<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;
use Response;
use Auth;

class EmployeeController extends Controller
{
    /**
     * Validates given data for Employee
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name' =>'required|max:255',
            'joinDate'=>'required'
            //'contact_id'    =>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if($request->type==null){
            $employees = Employee::where('employeeType','Office Staff')->with('wage.employee')->with('staffAttendance')->get();
            foreach ($employees as $employee) {
                $employee['present'] = 1;
                $employee['note'] = '';
            }
            return $employees;

        }
        elseif($request->type==1)
            return Employee::with('wage.employee')->where('employeeType','Office Staff')->get();
        elseif($request->type==2)
            return Employee::with('wage.employee')->where('employeeType','Non Office Staff')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if($request->id==null) {
            $validator = $this->validator($request->all());
            if ($validator->fails()) {
                return Response::json($validator->errors()
                    , 400);
            }

            $employee = new Employee($request->all());
            $employee->active=1;
            if ($request->hasFile('photo')) {
                $files = $request->file('photo');
                //foreach($files as $file) {
                $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg');
                $validator = Validator::make(array('photo'=> $files), $rules);
                if($validator->passes()){
                    $employee->photo = $this->uploadImage($files);
                }
                //}
            }else{
                $employee->photo ='profile.jpg';
            }

            $user=User::findOrFail(Auth::id());
            $employee->user=$user->username;

            if ($employee->save()) {
                return $employee;
            }
        }else{
            $employee = Employee::find($request->id);

            if ($request->hasFile('photo')) {
                $files = $request->file('photo');
                $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg');
                $validator = Validator::make(array('photo'=> $files), $rules);
                if($validator->passes()){
                    $employee->photo = $this->uploadImage($files);
                }
            }

            if($request->name!=null)
                $employee->name=$request->name;
            if($request->address!=null)
                $employee->address=$request->address;
            if($request->phone!=null)
                $employee->phone=$request->phone;
            if($request->mobile!=null)
                $employee->mobile=$request->mobile;
            if($request->place!=null)
                $employee->place=$request->place;
            if($request->basic!=null)
                $employee->basic=$request->basic;
            if($request->ta!=null)
                $employee->ta=$request->ta;
            if($request->da!=null)
                $employee->da=$request->da;
            if($request->hra!=null)
                $employee->hra=$request->hra;
            if($request->medical!=null)
                $employee->medical=$request->medical;
            if($request->total!=null)
                $employee->total=$request->total;
            if($request->joinDate!=null)
                $employee->joinDate=$request->joinDate;
            if($request->employeeType!=null)
                $employee->employeeType=$request->employeeType;


            if ($employee->save()) {
                return $employee;
            }
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Upload file into server
     * @param $file
     * @return string
     */
    public  function uploadImage($file){
        $storedFileName="";
        if(!empty($file)){

            $extension=$file->getClientOriginalExtension();
            $fileName = 'Emp'.rand(11111,99999).'.'.$extension;
            $imageRealPath 	= 	$file->getRealPath();
            $img = Image::make($imageRealPath); // use this if you want facade style code
            $img->resize(intval('500'), null, function($constraint) {
                $constraint->aspectRatio();
            });
            $storedFileName=$fileName;
            //$storedFileName=$fileName;
            /* $file->move(
                 public_path().'/img/upload/', $fileName
             );*/

            //$img->save(public_path().$storedFileName);
            /*$path = public_path().'img/upload/'.$fileName;*/
            $image = Image::make($file->getRealPath())->resize(200, 200);//->save($path);

            Storage::disk('local')->put($fileName,$image->stream());//file_get_contents($file->getRealPath()));

        }
        return $storedFileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Employee::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return Employee::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $employee=Employee::find($id);
        $employee->fill($request->all());
        if($employee->save()) {
            return $employee;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $emp=Employee::find($id);

        if($emp->photo != 'profile.jpg') {
            $exist=Storage::disk('local')->exists($emp->photo);
            if($exist)
                Storage::delete($emp->photo);
        }
        //TODO Employee model for cascade delete
        if(Employee::destroy($id))
            return Response::json(array('msg'=>'Employee record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
