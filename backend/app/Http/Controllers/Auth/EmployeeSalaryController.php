<?php

namespace App\Http\Controllers\Auth;

use App\Employee;
use App\SalaryAdvace;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\EmployeeSalary;
use Response;
use Validator;
use Auth;

class EmployeeSalaryController extends Controller
{

    public function validator(array $data)
    {
        return Validator::make($data, [
            'date' => 'required',
            'employees_id' => 'required',
            'total' => 'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }
        if($request->employees_id==0)
            return EmployeeSalary::where('date','>=',$from)
                ->where('date','<=',$to)->with('employee')->get();
        else
            return EmployeeSalary::where('date','>=',$from)
                ->where('date','<=',$to)->where('employees_id',$request->employees_id)->with('employee')->get();

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public  function getPreBalance(Request $request){
        return EmployeeSalary::where('employees_id',$request->employees_id)->sum('balance');
    }

    /**
     * @return array
     */
    public function getSalaryBalanceList(){
        $lists=[];
        $i=0;
        $employees=Employee::where('employeeType','Office Staff')->get();
        foreach($employees as $employee){
            $adv=EmployeeSalary::where('employees_id',$employee->id)->sum('balance');
            if($adv!=0) {
                $list['employees_id'] = $employee->id;
                $list['name'] = $employee->name;
                $list['balance'] = $adv;
                $lists[$i++] = $list;
            }
        }
        return $lists;
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(),400);

        }

        $employee_salary = new EmployeeSalary($request->all());
        if($request->balance==0 || $request->paid==0)
            $employee_salary->balance=$request->balance-$request->prebalance;
        else
            $employee_salary->balance=$request->balance-$request->prebalance;

        $user=User::findOrFail(Auth::id());
        $employee_salary->user=$user->username;

        if ($employee_salary->save()) {
            return $employee_salary;
        }
        return Response::json(['error' => 'Server is down'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return EmployeeSalary::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return EmployeeSalary::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $employee_salary = EmployeeSalary::find($id);
        $employee_salary->fill($request->all());
        if ($employee_salary->save()) {
            return $employee_salary;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (EmployeeSalary::destroy($id)) {
            return Response::json(['msg' => 'Employee Salary record deleted']);
        }
        else
            return Response::json(['error' => 'Record not found'], 400);
    }
}
