<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;
use Auth;
use App\User;
use App\Expense;

class ExpenseController extends Controller
{
    /**
     * Validates given data for Expense
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required|max:255',
            'amount'     =>'required|numeric|max:999999999999999999',
            'payDate'    =>'required|date',
            'projects_id' =>'numeric'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $project_id = $request->input('projects_id');
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if ($project_id === null)
            return Expense::with('projects')->get();
        else
            return Expense::where('projects_id',$project_id)->with('projects')->where('payDate','>=',$from)->where('payDate','<=',$to)->get();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors()
                , 400);
        }

        $expense = new Expense($request->all());
        $user=User::findOrFail(Auth::id());
        $expense->user=$user->username;
        if ($expense->save()) {
            return $expense;
            /*$project_id = $request->input('project_id');
            if ($project_id === null)
                return $expense;
            else{
                $expense->projects()->attach($project_id);
                return Expense::with('projects')->find($expense->id);
            }*/
        }
        return Response::json(['error' => 'Server is down']
            , 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Expense::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return Expense::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors()
                , 400);
        }
        $expense = Expense::find($id);
        $expense->fill($request->all());
        $user=User::findOrFail(Auth::id());
        $expense->user=$user->username;
        if ($expense->save()) {
            return $expense;
        }
        return Response::json(['error' => 'Server is down']
            , 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO Employee model for cascade delete
        if (Expense::destroy($id))
            return Response::json(array('msg' => 'Expense record deleted'));
        else
            return Response::json(array('error' => 'Records not found'), 400);
    }
}
