<?php

namespace App\Http\Controllers\Auth;

use App\Group;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class GroupController extends Controller
{
    /**
     * Validates given data for Income
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'leader_id'       =>'required',
            'employee_id'     =>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return Group::with('leaders','employees')->where('leader_id','=',$request->leader_id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $g=Group::where('leader_id',$request->leader_id)->where('employee_id',$request->employee_id)->get()->first();
        if($g===null) {
            $group = new Group($request->all());

            if ($group->save()) {
                return $group;
            }
        }else{
            return 'Repeat';
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $group=Group::find($id);
        $group->fill($request->all());
        if($group->save()) {
            return $group;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(Group::destroy($id))
            return Response::json(array('msg'=>'Group  deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
