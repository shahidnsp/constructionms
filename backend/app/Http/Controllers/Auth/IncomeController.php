<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use Validator;
use Response;
use Auth;
use App\User;
use App\Income;

class IncomeController extends Controller
{
    /**
     * Validates given data for Income
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required|max:255',
            'amount'     =>'required|numeric|max:999999999999999999',
            'date'    =>'required|date',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $project_id=$request->get('projects_id');
        $from= $request->get('fromDate');
        $to=$request->get('toDate');

        if($project_id===null)
            return Income::with('projects')->get();
        else
            return Income::with('projects')->where('projects_id',$project_id)->where('date','>=',$from)->where('date','<=',$to)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $income=new Income($request->all());
        $user=User::findOrFail(Auth::id());
        $income->user=$user->username;
        if($income->save()){
            return $income;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Income::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return Income::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $income=Income::find($id);
        $user=User::findOrFail(Auth::id());
        $income->user=$user->username;
        $income->fill($request->all());
        if($income->save()) {
            return $income;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO cascade delete

        if(Income::destroy($id))
            return Response::json(array('msg'=>'Income record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }

}
