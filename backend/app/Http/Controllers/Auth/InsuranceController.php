<?php

namespace App\Http\Controllers\Auth;

use App\Insurance;
use Illuminate\Http\Request as Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class InsuranceController extends Controller
{
    /**
     * Validates given data for Insurance
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'       =>'required|max:255',
            'insuranceNumber'     => 'required|max:255',
            'term'    =>'required|numeric|max:999999999999999999',
            'amount'=>'required|numeric|max:999999999999999999',
            'nominee' =>'required|max:255',
            'premiumAmount'=>'required|numeric|max:999999999999999999',
            'takenDate'=>'required|date',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Insurance::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $insure=new Insurance($request->all());
        if($insure->save()){
            return $insure;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Insurance::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return Insurance::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $insure=Insurance::find($id);
        //$insure->fill($request->all());
        if($insure->update($request->all())) {
            return $insure;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(Insurance::destroy($id))
            return Response::json(array('msg'=>'Insurance record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
