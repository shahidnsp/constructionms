<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;
use App\Insurance_premium;
use App\Insurance;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class InsurancePremiumController extends Controller
{
    /**
     * Validates given data for Insurance_premium
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'insurance_id'       =>'required',
            'amount'     =>'required|numeric|max:999999999999999999',
            'payDate'    =>'required|date',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $insurance_id=$request->get('insurance_id');
        return Insurance::find($insurance_id)->insurance_premium;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }
        $premium=new Insurance_premium($request->all());
        if($premium->save())
        {
            return $premium;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Insurance_premium::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return Insurance_premium::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $premium=Insurance_premium::find($id);
        //$premium->fill($request->all());
        if($premium->update($request->all()))
            return $premium;
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(Insurance_premium::destroy($id))
            return Response::json(array('msg'=>'Premium deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
