<?php

namespace App\Http\Controllers\Auth;

use App\Journal;
use App\Ledger;
use App\Under;
use Carbon\Carbon;
use Illuminate\Http\Request as Request;

use Auth;
use Validator;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class JournalController extends Controller
{

    private $tradingBalance=0,$tradingStatus='';

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'drledgers_id' => 'required',
            'crledgers_id' => 'required',
            'dramount' => 'required',
            'cramount' => 'required',
            'date' => 'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }
        return Journal::where('date','>=',$from)
            ->where('date','<=',$to)->with('crledger','drledger')->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getAccountStatement(Request $request)
    {
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }
        return Journal::where('date','>=',$from)
            ->where('date','<=',$to)->where('drledgers_id','=',$request->ledgers_id)->orWhere('crledgers_id','=',$request->ledgers_id)->with('crledger','drledger')->get();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getJournalBetweenDate(Request $request)
    {
        $from=$request->get('fromDate');
        $to=$request->get('toDate');
        return Journal::where('date','>=',$from)
            ->where('date','<=',$to)->with('crledger','drledger')->get();
    }

    /**
     * @return array
     */
    public function getLedgerBalance(){
        $balance=[];

        $ledgers=Ledger::all();
        $i=0;
        foreach($ledgers as $account){

            $debit=0;
            $credit=0;
            $ledger['name']=$account->name;
            $journals=Journal::where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $ledger['debit']=$debit;
            $ledger['credit']=$credit;
            $openBal=$account->openingBalance;
            if($account->crOrDr=='Dr') {
                $debit = $debit + $openBal;
                $ledger['openingBalance']=$openBal.' Dr';
            }elseif($account->crOrDr=='Cr') {
                $credit = $credit + $openBal;
                $ledger['openingBalance']=$openBal.' Cr';
            }

            if($credit>$debit) {
                $bal = $credit-$debit;
                $ledger['balance']=$bal.' Cr';
            }else{
                $bal = $debit-$credit;

                $ledger['balance']=$bal.' Dr';
            }

            $balance[$i]=$ledger;
            $i++;
        }

        return $balance;

    }

    public function getSundryDebtorList(){
        $balance=[];

        $ledgers=Ledger::where('unders_id',27)->get();//To get Sundry Debtor's Account
        $i=0;
        foreach($ledgers as $account){

            $debit=0;
            $credit=0;
            $ledger['name']=$account->name;
            $journals=Journal::where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }

            $ledger['debit']=$debit;
            $ledger['credit']=$credit;

            $openBal=$account->openingBalance;
            if($account->crOrDr=='Dr'){
                $debit+=$openBal;
                $ledger['openingBalance']=$openBal.' Dr';
            }

            if($account->crOrDr=='Cr'){
                $credit+=$openBal;
                $ledger['openingBalance']=$openBal.' Cr';
            }

            if($credit>$debit) {
                $bal = $credit-$debit;
                $ledger['due']=$bal;
                $ledger['advance']='';

            }else{
                $bal = $debit-$credit;
                $ledger['due']='';
                $ledger['advance']=$bal;
            }

            $balance[$i]=$ledger;
            $i++;
        }

        return $balance;

    }

    public function getSundryCreditorList(){
        $balance=[];

        $ledgers=Ledger::where('unders_id',23)->get();//To get Sundry Debtor's Account
        $i=0;
        foreach($ledgers as $account){

            $debit=0;
            $credit=0;
            $ledger['name']=$account->name;
            $journals=Journal::where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }

            $ledger['debit']=$debit;
            $ledger['credit']=$credit;

            $openBal=$account->openingBalance;
            if($account->crOrDr=='Dr'){
                $debit+=$openBal;
                $ledger['openingBalance']=$openBal.' Dr';
            }

            if($account->crOrDr=='Cr'){
                $credit+=$openBal;
                $ledger['openingBalance']=$openBal.' Cr';
            }

            if($credit>$debit) {
                $bal = $credit-$debit;
                $ledger['due']='';
                $ledger['advance']=$bal;
            }else{
                $bal = $debit-$credit;
                $ledger['due']=$bal;
                $ledger['advance']='';
            }

            $balance[$i]=$ledger;
            $i++;
        }

        return $balance;

    }

    /**
     * @param Request $request
     * @return array
     */
    public function getTrialBalance(Request $request){

        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }
        $trail=[];

        $ledgers=Ledger::all();
        $i=0;
        foreach($ledgers as $account){

            $debit=0;
            $credit=0;
            $ledger['name']=$account->name;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

                if($balance != 0){
                    $ledger['credit']=$balance;
                    $ledger['debit']='';
                }
            }else{
                $balance = $debit-$credit;

                if($balance != 0){
                    $ledger['credit']='';
                    $ledger['debit']=$balance;
                }
            }

            if($balance!=0) {
                $trail[$i] = $ledger;
                $i++;
            }
        }

        return $trail;

    }

    public function getTradingAccount(Request $request){


        //START DEBIT SIDE OF TRADING ACCOUNT
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }
        $trail=[];


        //To GET PURCHASE AMOUNT
        $ledgers=Ledger::where('id',2)->get();
        $i=0;
        $debit=0;
        $credit=0;
        foreach($ledgers as $account){

            $ledger['name']=$account->name;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }else{
                $balance = $debit-$credit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }
            $ledger['type']='Purchase';
            $ledger['side']='Dr';

            if($balance!=0) {
                $trail[$i] = $ledger;
                $i++;
            }
        }



        //To GET PURCHASE RETURN AMOUNT
        $ledgers=Ledger::where('id',5)->get();
        //$i=0;
        $debit=0;
        $credit=0;
        foreach($ledgers as $account){

            $ledger['name']=$account->name;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }else{
                $balance = $debit-$credit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }
            $ledger['type']='Purchase Return';
            $ledger['side']='Dr';

            if($balance!=0) {
                $trail[$i] = $ledger;
            }
        }



        //To GET ALL DIRECT EXPENSES AMOUNT
        $ledgers=Ledger::where('unders_id',10)->get();
        //$i=0;
        foreach($ledgers as $account){

            $debit=0;
            $credit=0;
            $ledger['name']=$account->name;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }else{
                $balance = $debit-$credit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }

            $ledger['type']='Other';
            $ledger['side']='Dr';
            if($balance!=0) {
                $trail[$i] = $ledger;
                $i++;
            }
        }
        //END DEBIT SIDE OF TRADING ACCOUNT


        //START CREDIT SIDE OF TRADING ACCOUNT


        //To GET SALES AMOUNT
        $ledgers=Ledger::where('id',1)->get();

        $debit=0;
        $credit=0;
        foreach($ledgers as $account){

            $ledger['name']=$account->name;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }else{
                $balance = $debit-$credit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }
            $ledger['type']='Sale';
            $ledger['side']='Cr';

            if($balance!=0) {
                $trail[$i] = $ledger;
                $i++;
            }
        }



        //To GET SALE RETURN AMOUNT
        $ledgers=Ledger::where('id',4)->get();
        //$i=0;
        $debit=0;
        $credit=0;
        foreach($ledgers as $account){

            $ledger['name']=$account->name;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }else{
                $balance = $debit-$credit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }
            $ledger['type']='Sale Return';
            $ledger['side']='Cr';

            if($balance!=0) {
                $trail[$i] = $ledger;
            }
        }



        //To GET ALL DIRECT INCOME AMOUNT
        $ledgers=Ledger::where('unders_id',11)->get();
        //$i=0;
        foreach($ledgers as $account){

            $debit=0;
            $credit=0;
            $ledger['name']=$account->name;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }else{
                $balance = $debit-$credit;

                if($balance != 0){
                    $ledger['balance']=$balance;
                }
            }

            $ledger['type']='Other';
            $ledger['side']='Cr';
            if($balance!=0) {
                $trail[$i] = $ledger;
                $i++;
            }
        }
        //END CREDIT SIDE OF TRADING ACCOUNT



        return $trail;

    }


    /**
     * To Get Trading Account Balance..........................For Profit and Loss Account
     * @param $from
     * @param $to
     */
    private function getTradingAmount($from,$to){
        //START DEBIT SIDE OF TRADING ACCOUNT

        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }
        $debitTotal=0;
        $creditTotal=0;
        $saleTotal=0;
        $salereturnTotal=0;
        $purchaseTotal=0;
        $purchasereturnTotal=0;



        //To GET PURCHASE AMOUNT
        $ledgers=Ledger::where('id',2)->get();
        $debit=0;
        $credit=0;
        foreach($ledgers as $account){

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

            }else{
                $balance = $debit-$credit;

            }

            if($balance!=0) {
               $purchaseTotal+=$balance;
            }
        }



        //To GET PURCHASE RETURN AMOUNT
        $ledgers=Ledger::where('id',5)->get();
        $debit=0;
        $credit=0;
        foreach($ledgers as $account){

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

            }else{
                $balance = $debit-$credit;

            }

            if($balance!=0) {
                $purchasereturnTotal+=$balance;
            }
        }

        $debitTotal+=($purchaseTotal-$purchasereturnTotal);



        //To GET ALL DIRECT EXPENSES AMOUNT
        $ledgers=Ledger::where('unders_id',10)->get();
        //$i=0;
        foreach($ledgers as $account){

            $debit=0;
            $credit=0;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

            }else{
                $balance = $debit-$credit;

            }

            if($balance!=0) {
               $debitTotal+=$balance;
            }
        }
        //END DEBIT SIDE OF TRADING ACCOUNT


        //START CREDIT SIDE OF TRADING ACCOUNT


        //To GET SALES AMOUNT
        $ledgers=Ledger::where('id',1)->get();

        $debit=0;
        $credit=0;
        foreach($ledgers as $account){

            $ledger['name']=$account->name;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

            }else{
                $balance = $debit-$credit;

            }

            if($balance!=0) {
               $saleTotal+=$balance;
            }
        }



        //To GET SALE RETURN AMOUNT
        $ledgers=Ledger::where('id',4)->get();
        //$i=0;
        $debit=0;
        $credit=0;
        foreach($ledgers as $account){

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

            }else{
                $balance = $debit-$credit;
            }

            if($balance!=0) {
                $salereturnTotal+=$balance;
            }
        }

        $creditTotal+=($saleTotal-$salereturnTotal);

        //To GET ALL DIRECT INCOME AMOUNT
        $ledgers=Ledger::where('unders_id',11)->get();

        foreach($ledgers as $account){

            $debit=0;
            $credit=0;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;
            }else{
                $balance = $debit-$credit;
            }

            if($balance!=0) {
                $creditTotal+=$balance;
            }
        }
        //END CREDIT SIDE OF TRADING ACCOUNT

        if($debitTotal>$creditTotal){
            $this->tradingBalance=$debitTotal-$creditTotal;
            $this->tradingStatus='Cr';
        }elseif ($creditTotal>$debitTotal){
            $this->tradingBalance=$creditTotal-$debitTotal;
            $this->tradingStatus='Dr';
        }

    }


    public function getProfitandLossAccount(Request $request){
        $profitAndLoss=[];
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }
        $this->getTradingAmount($from,$to);
        $i=0;

        $status=[];
        if($this->tradingStatus=='Dr'){
            $status['particular']='gross profit b/d';
            $status['balance']=$this->tradingBalance;
            $status['side']='Cr';
            $status['type']='Status';
        }elseif($this->tradingStatus=='Cr'){
            $status['particular']='gross profit b/d';
            $status['balance']=$this->tradingBalance;
            $status['side']='Dr';
            $status['type']='Status';
        }
        $profitAndLoss[$i]=$status;
        $i++;

        //To GET ALL INDIRECT EXPENSES AMOUNT
        $ledgers=Ledger::where('unders_id',12)->get();
        foreach($ledgers as $account){

            $debit=0;
            $credit=0;
            $status['particular']=$account->name;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

                if($balance != 0){
                    $status['balance']=$balance;
                }
            }else{
                $balance = $debit-$credit;

                if($balance != 0){
                    $status['balance']=$balance;
                }
            }

            $status['type']='Other';
            $status['side']='Dr';
            if($balance!=0) {
                $profitAndLoss[$i] = $status;
                $i++;
            }
        }


        //To GET ALL INDIRECT INCOME AMOUNT
        $ledgers=Ledger::where('unders_id',13)->get();
        foreach($ledgers as $account){

            $debit=0;
            $credit=0;
            $status['particular']=$account->name;
            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('drledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('date','>=',$from)
                ->where('date','<=',$to)->where('crledgers_id',$account->id)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $balance=0;
            if($credit>$debit) {
                $balance = $credit - $debit;

                if($balance != 0){
                    $status['balance']=$balance;
                }
            }else{
                $balance = $debit-$credit;

                if($balance != 0){
                    $status['balance']=$balance;
                }
            }

            $status['type']='Other';
            $status['side']='Cr';
            if($balance!=0) {
                $profitAndLoss[$i] = $status;
                $i++;
            }
        }


        return $profitAndLoss;
    }






    /**
     * @param Request $request
     * @return array
     */

    public function getCashbook(Request $request){

        $from= $request->fromDate;
        $to=$request->toDate;
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }

        $balance=[];

        $ledgers=Ledger::where('unders_id',28)->orWhere('unders_id',29)->with('unders')->get();//To get Leader with under Cash-in-hand or Bank Account

        $i=0;
        foreach($ledgers as $account){

            $debit=0;
            $credit=0;
            $ledger['name']=$account->name;
            $journals=Journal::where('drledgers_id',$account->id)->where('date','>=',$from)->where('date','<=',$to)->get();
            foreach($journals as $journal){
                $debit+=$journal->dramount;
            }

            $journals=Journal::where('crledgers_id',$account->id)->where('date','>=',$from)->where('date','<=',$to)->get();
            foreach($journals as $journal){
                $credit+=$journal->cramount;
            }
            $ledger['ledgers_id']=$account->id;
            $ledger['debit']=$debit;
            $ledger['credit']=$credit;
            $ledger['unders_id']=$account->unders_id;
            $openBal=$account->openingBalance;
            if($account->crOrDr=='Dr') {
                $debit = $debit + $openBal;
                $ledger['openingBalance']=$openBal;
                $ledger['openingDrorCr']='Dr';
            }elseif($account->crOrDr=='Cr') {
                $credit = $credit + $openBal;
                $ledger['openingBalance']=$openBal;
                $ledger['openingDrorCr']='Cr';
            }

            if($credit>$debit) {
                $bal = $credit-$debit;
                $ledger['balance']=$bal;
                $ledger['crOrDr']='Cr';
            }else{
                $bal = $debit-$credit;
                $ledger['balance']=$bal;
                $ledger['crOrDr']='Dr';
            }

            $balance[$i]=$ledger;
            $i++;
        }
        return $balance;
    }

    public function getLedgerDetailsReportForCashBook(Request $request){
        $ledger_id=$request->ledgers_id;
        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        $ledgerDetails=[];
        if($ledger_id!=null && $fromDate !=null && $toDate!=null) {
            $journals = Journal::where('drledgers_id', $ledger_id)
                ->orWhere('crledgers_id', $ledger_id)
                ->where('date', '>=', $fromDate)
                ->where('date', '<=', $toDate)->get();
            $balance=0;
            $details=[];
            //List Opening Balance
            $ledger=Ledger::find($ledger_id);
            if($ledger!=null) {
                if($ledger->openingBalance!=0) {
                    $details['voucherType'] = 'Opening Balance';
                    $details['date'] = $fromDate;
                    $openBal=$ledger->openingBalance;
                    if ($ledger->crOrDr == 'Cr') {
                        $details['debit'] = '0.00';
                        $details['credit'] = $openBal;
                        $balance -= $openBal;
                        $details['balance'] = $openBal . ' Cr';
                    }else{
                        $details['debit'] = $openBal;
                        $details['credit'] = '0.00';
                        $balance += $openBal;
                        $details['balance'] = $openBal . ' Dr';
                    }
                    array_push($ledgerDetails,$details);
                }
            }


            foreach($journals as $journal){

                $details['voucherType']=$journal->vouchertype;
                $details['date']=$journal->date;
                if($journal->drledgers_id==$ledger_id){
                    $balance+=$journal->dramount;
                    $details['debit']=$journal->dramount;
                    $details['credit']='0.00';
                    if($balance<0) {
                        $bal=$balance*-1;
                        $details['balance'] = $bal . ' Cr';
                    }
                    else {
                        $details['balance'] = $balance . ' Dr';
                    }
                }
                if($journal->crledgers_id==$ledger_id){
                    $balance-=$journal->cramount;
                    $details['debit']='0.00';
                    $details['credit']=$journal->cramount;
                    if($balance<0) {
                        $bal=$balance*-1;
                        $details['balance'] = $bal . ' Cr';
                    }
                    else {
                        $details['balance'] = $balance . ' Dr';
                    }
                }
                $details['purchases_id']=$journal->purchases_id;
                $details['incomes_id']=$journal->incomes_id;
                $details['expenses_id']=$journal->expenses_id;
                $details['wages_id']=$journal->wages_id;
                $details['advance_id']=$journal->advance_id;
                $details['salary_id']=$journal->salary_id;
                $details['wageadvance_id']=$journal->wageadvance_id;
                $details['bankpayment_id']=$journal->bankpayment_id;
                $details['loanpayment_id']=$journal->loanpayment_id;
                $details['loanexpense_id']=$journal->loanexpense_id;
                $details['transfer_id']=$journal->transfer_id;
                $details['materialreturn_id']=$journal->materialreturn_id;
                $details['machinery_id']=$journal->machinery_id;

                array_push($ledgerDetails,$details);
            }
        }

        return $ledgerDetails;
    }



    /**
     * @param Request $request
     * @return array
     */

    public function getDayBook(Request $request){

        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }

        $daybook=[];
        $journals=Journal::where('date','>=',$from)->where('date','<=',$to)->with('drledger')->with('crledger')->get();

        foreach($journals as $journal){
            $book=[];
            $book['id']=$journal->id;
            $book['date']=$journal->date;
            $book['voucherType']=$journal->vouchertype;
            $book['debitLedger']=$journal->drledger->name;
            $book['creditLedger']=$journal->crledger->name;
            $book['debit']=$journal->cramount;
            $book['credit']=$journal->dramount;

            $book['purchases_id']=$journal->purchases_id;
            $book['incomes_id']=$journal->incomes_id;
            $book['expenses_id']=$journal->expenses_id;
            $book['wages_id']=$journal->wages_id;
            $book['advance_id']=$journal->advance_id;
            $book['salary_id']=$journal->salary_id;
            $book['wageadvance_id']=$journal->wageadvance_id;
            $book['bankpayment_id']=$journal->bankpayment_id;
            $book['loanpayment_id']=$journal->loanpayment_id;
            $book['loanexpense_id']=$journal->loanexpense_id;
            $book['transfer_id']=$journal->transfer_id;
            $book['materialreturn_id']=$journal->materialreturn_id;
            $book['machinery_id']=$journal->machinery_id;
            array_push($daybook,$book);
        }

        return $daybook;
    }

    /**
     * @return array
     *
     */

    public function getLedgerBalanceDate(Request $request){

        $from=$request->get('fromDate');
        $to=$request->get('toDate');
        $group_id=$request->accountgroup_id;
        $ledger_id=$request->accountledger_id;
        $balance = [];

        if($group_id=='All' && $ledger_id=='All') {

            $ledgers = Ledger::all();
            $i = 0;
            foreach ($ledgers as $account) {

                $debit = 0;
                $credit = 0;
                $ledger['name'] = $account->name;
                $journals = Journal::where('drledgers_id', $account->id)->where('date', '>=', $from)->where('date', '<=', $to)->get();
                foreach ($journals as $journal) {
                    $debit += $journal->dramount;
                    //$credit += $journal->cramount;
                }

                $journals = Journal::where('crledgers_id', $account->id)->where('date', '>=', $from)->where('date', '<=', $to)->get();
                foreach ($journals as $journal) {
                    //$debit += $journal->dramount;
                    $credit += $journal->cramount;
                }

                $ledger['debit']=$debit;
                $ledger['credit']=$credit;
                $openBal=$account->openingBalance;
                if($account->crOrDr=='Dr') {
                    $debit = $debit + $openBal;
                    $ledger['openingBalance']=$openBal.' Dr';
                }elseif($account->crOrDr=='Cr') {
                    $credit = $credit + $openBal;
                    $ledger['openingBalance']=$openBal.' Cr';
                }

                if($credit>$debit) {
                    $bal = $credit-$debit;
                    $ledger['balance']=$bal.' Cr';
                }else{
                    $bal = $debit-$credit;

                    $ledger['balance']=$bal.' Dr';
                }
                $balance[$i] = $ledger;
                $i++;
            }
        }

        if($group_id !='All' && $ledger_id=='All') {
            $ledgers = Ledger::where('unders_id',$group_id)->get();
            $i = 0;
            foreach ($ledgers as $account) {

                $debit = 0;
                $credit = 0;
                $ledger['name'] = $account->name;
                $journals = Journal::where('drledgers_id', $account->id)->where('date', '>=', $from)->where('date', '<=', $to)->get();
                foreach ($journals as $journal) {
                    $debit += $journal->dramount;
                    //$credit += $journal->cramount;
                }

                $journals = Journal::where('crledgers_id', $account->id)->where('date', '>=', $from)->where('date', '<=', $to)->get();
                foreach ($journals as $journal) {
                    //$debit += $journal->dramount;
                    $credit += $journal->cramount;
                }

                $ledger['debit']=$debit;
                $ledger['credit']=$credit;
                $openBal=$account->openingBalance;
                if($account->crOrDr=='Dr') {
                    $debit = $debit + $openBal;
                    $ledger['openingBalance']=$openBal.' Dr';
                }elseif($account->crOrDr=='Cr') {
                    $credit = $credit + $openBal;
                    $ledger['openingBalance']=$openBal.' Cr';
                }

                if($credit>$debit) {
                    $bal = $credit-$debit;
                    $ledger['balance']=$bal.' Cr';
                }else{
                    $bal = $debit-$credit;

                    $ledger['balance']=$bal.' Dr';
                }
                $balance[$i] = $ledger;
                $i++;
            }
        }

        if($ledger_id !='All') {
            $account = Ledger::find($ledger_id);
            $i = 0;
            //foreach ($ledgers as $account) {

                $debit = 0;
                $credit = 0;
                $ledger['name'] = $account->name;
                $journals = Journal::where('drledgers_id', $account->id)->where('date', '>=', $from)->where('date', '<=', $to)->get();
                foreach ($journals as $journal) {
                    $debit += $journal->dramount;
                    //$credit += $journal->cramount;
                }

                $journals = Journal::where('crledgers_id', $account->id)->where('date', '>=', $from)->where('date', '<=', $to)->get();
                foreach ($journals as $journal) {
                    //$debit += $journal->dramount;
                    $credit += $journal->cramount;
                }

            $ledger['debit']=$debit;
            $ledger['credit']=$credit;
            $openBal=$account->openingBalance;
            if($account->crOrDr=='Dr') {
                $debit = $debit + $openBal;
                $ledger['openingBalance']=$openBal.' Dr';
            }elseif($account->crOrDr=='Cr') {
                $credit = $credit + $openBal;
                $ledger['openingBalance']=$openBal.' Cr';
            }

            if($credit>$debit) {
                $bal = $credit-$debit;
                $ledger['balance']=$bal.' Cr';
            }else{
                $bal = $debit-$credit;

                $ledger['balance']=$bal.' Dr';
            }
                $balance[$i] = $ledger;
                $i++;
            //}
        }

        return $balance;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 500);
        }

        $journal = new Journal($request->all());
        if ($journal->save()) {
            return $journal;
        }

        return Response::json(['error' => 'Server Down'], 500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Journal::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $journal = Journal::find($id);
        $journal->fill($request->all());
        if ($journal->save()) {
            return $journal;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Journal::destroy($id)) {
            return Response::json(['msg' => 'Journal record Deleted']);
        }
        else{
            return Response::json(['error' => 'Record Not found'], 400);
        }
    }
}
