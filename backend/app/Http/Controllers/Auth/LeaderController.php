<?php

namespace App\Http\Controllers\Auth;

use App\Group;
use App\Leader;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class LeaderController extends Controller
{

    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {
        return Validator::make($data,[
            'project_id'  => 'required',
            'employee_id'   => 'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if($request->project_id != null) {
            //return Leader::with('projects', 'employee', 'groups.employees')->get();
            $employees = [];
            $leaders = Leader::where('project_id', $request->project_id)->get();
            foreach($leaders as $leader){
                $groups = Group::where('leader_id', $leader->id)->with('employees')->get();
                foreach($groups as $group){
                    array_push($employees, $group->employees);
                }
            }
            return $employees;
        }
        else
            return Leader::with('projects','employee')->get();
    }

    public function getLeaderEmployeeID(Request $request)
    {
        return Leader::where('project_id','=',$request->project_id)->where('employee_id','=',$request->employee_id)->get()->first();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $leader = new Leader($request->all());

        if($leader->save()){
            return $leader;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Leader::with('employee')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $leader = Leader::find($id);
        if($leader->update($request->all())){
            return $leader;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $count = Leader::destroy($id);

        if($count)
            return Response::json(array('msg'=>$count.' records deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
