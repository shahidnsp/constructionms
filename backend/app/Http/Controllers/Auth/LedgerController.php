<?php

namespace App\Http\Controllers\Auth;

use App\Ledger;
use Illuminate\Http\Request as Request;

use Auth;
use Mockery\Mock;
use Validator;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LedgerController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'unders_id' => 'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $under_id=$request->under_id;
        if($under_id == null)
            return Ledger::with('unders')->get();
        else
            return Ledger::where('unders_id',$under_id)->with('unders')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store( Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $ledger = new Ledger($request->all());
        $ledger->isedit=1;
        if ($ledger->save()) {
            return $ledger;
        }

        return Response::json(['error' => 'Server down'],500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Ledger::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return Ledger::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);

        }

        $ledger = Ledger::find($id);
        $ledger->fill($request->all());
        if ($ledger->save()) {
            return $ledger;
        }

        return Response::json(['error' => 'Server Down'], 500);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Ledger::destroy($id)) {
            return Response::json(['msg' => 'Ledger table deleted']);
        }
        else
            return Response::json(['error' => 'Record not found'], 400);
    }
}
