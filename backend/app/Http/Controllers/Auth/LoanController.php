<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use App\User;
use Auth;

use App\Loan;
use App\Bank;
use App\Emi;
use App\Account;

class LoanController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data,[
            'bank_id'       => 'required|numeric',
            'account_id'    => 'numeric', //TODO decision on making account for loan
            'name'          => 'required|alpha_text',
            //'description'   => 'required|alpha_text',
            'amount'        => 'required|numeric',
            'paid'          => 'numeric',
            'balance'       => 'numeric',
            'date'          => 'required|date',
            'closedate'     => 'required|date',
            ]);
    }

    /**
     * Loan account validator
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    protected function accountValidator(array $data)
    {
        return Validator::make($data,[
            'bank_id'  => 'required',
            'number'   => 'required|max:255',
            'name'     => 'required|max:255',
            'iban'     => 'max:255',
            'issueDate'=> 'required|date',
            'balance'  => 'required|numeric|max:999999999999999999',

        ]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Loan::with('bank','account')->get();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Creates a loan account with validation
     * @param array $data
     * @return Response|Array
     */
    protected function createAccount(array $data)
    {
        $data['type'] = 'lo';

        $account = new Account($data);

        if($account->save()){
            return $account;
        }
        return false;
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        $loanData = $request->except(['bank','account']);

        $user=User::findOrFail(Auth::id());
        //Creating loan account data
        $account = $request->only('account')['account'];
        $account['bank_id'] = $loanData['bank_id'];
        $account['name'] = $loanData['name'];
        $account['issueDate'] = $loanData['date'];
        $account['balance'] = $loanData['balance'];
        $account['user']=$user->username;
        $accountValidator = $this->accountValidator($account);

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        if($accountValidator->fails()){
            return Response::json($accountValidator->errors()
                ,400);
        }

        $account = $this->createAccount($account);

        $loan = new Loan($request->all());
        $loan['account_id'] = $account->id;
        $loan['user']=$user->username;
        if($loan->save()){

            return Loan::with('account','bank')->find($loan->id);
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        try{

            return Loan::with('bank','account')->findOrFail($id);
        }
        catch(\Exception $e){
            return Response::json(['error'=>'Item not found'],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

        $validator = $this->validator($request->all());
        $loanData = $request->except(['bank','account']);
        //Getting loan account data

        $accountData = $request->only('account')['account'];
        $accountData['bank_id'] = $loanData['bank_id'];
        $accountData['name'] = $loanData['name'];
        $accountData['issueDate'] = $loanData['date'];
        $accountData['balance'] = $loanData['balance'];

        $accountValidator = $this->accountValidator($accountData);


        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        if($accountValidator->fails()){
            return Response::json($accountValidator->errors()
                ,400);
        }

        $loan = Loan::find($id);


        $account = Account::find($loan->account_id);

       // $account->fill($accountData); //Update account info
       // $loan->fill($request->all()); //Update loan info

        //Save all changes
        if($loan->update($accountData) && $account->update($request->all())){
            return Loan::with('account','bank')->find($id);
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO Loan model for cascade delete
        if(Loan::destroy($id))
            return Response::json(array('msg'=>'Loan deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
