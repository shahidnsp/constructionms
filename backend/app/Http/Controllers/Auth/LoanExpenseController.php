<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;
use App\LoanExpense;
use App\Loan;
use Auth;
use Validator;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LoanExpenseController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'loan_id' => 'required',
            'date' => 'required',
            'amount' => 'required'
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $loan_id = $request->get('loan_id');

//        all loan expense
        if ($loan_id !== null) {
            return Loan::find($loan_id)->loan_expense;
        }
        else
            return LoanExpense::with('loan')->get();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $loan_expense = new LoanExpense($request->all());

        if ($loan_expense->save()) {
            return $loan_expense;
        }

        return Response::json(['error' => 'Serever down'], 500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return LoanExpense::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return LoanExpense::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(),400);
        }

        $loan_expense = LoanExpense::find($id);
        $loan_expense->fill($request->all());
        if ($loan_expense->save()) {
            return $loan_expense;
        }

        return Response::json(['error' => 'Server down', 500]);
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (LoanExpense::destroy($id)) {
            return Response::json(['msg' => 'Loan Expense record deleted']);
        }
        else{
            return Response::json(['error' => 'Record not found'], 500);

        }
    }
}
