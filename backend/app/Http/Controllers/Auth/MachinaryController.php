<?php

namespace App\Http\Controllers\Auth;

use App\Machinary;
use Illuminate\Http\Request as Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Mockery\CountValidator\Exception;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Input;

class MachinaryController extends Controller
{
    /**
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'qty' => 'required',
            'wageperday' => 'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Machinary::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        if($request->id==null){
            $machinary = new Machinary($request->all());

            if ($request->hasFile('photo')) {
                $files = $request->file('photo');
                //foreach($files as $file) {
                $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg');
                $validator = Validator::make(array('photo'=> $files), $rules);
                if($validator->passes()){
                    $machinary->photo = $this->uploadImage($files);
                }
                //}
            }

            if ($machinary->save()) {
                return $machinary;
            }
        }else{
            $machinary = Machinary::find($request->id);


            if ($request->hasFile('photo')) {
                $files = $request->file('photo');
                $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg');
                $validator = Validator::make(array('photo'=> $files), $rules);
                if($validator->passes()){
                    $machinary->photo = $this->uploadImage($files);
                }
            }

            $machinary->name=$request->name;
            $machinary->description=$request->description;
            $machinary->wageperday=$request->wageperday;
            $machinary->qty=$request->qty;

            if ($machinary->save()) {
                return $machinary;
            }
        }

        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Upload file into server
     * @param $file
     * @return string
     */
    public  function uploadImage($file){
        $storedFileName="";
        if(!empty($file)){

            $extension=$file->getClientOriginalExtension();
            $fileName = rand(11111,99999).'.'.$extension;
            $imageRealPath 	= 	$file->getRealPath();
            $img = Image::make($imageRealPath); // use this if you want facade style code
            $img->resize(intval('500'), null, function($constraint) {
                $constraint->aspectRatio();
            });
            $storedFileName='/img/upload/'. $fileName;
            //$storedFileName=$fileName;
            /* $file->move(
                 public_path().'/img/upload/', $fileName
             );*/

            $img->save(public_path().$storedFileName);
            /*$path = public_path().'img/upload/'.$fileName;
            $image = Image::make($file->getRealPath())->resize(200, 200)->save($path);*/

            //Storage::disk('local')->put($fileName,  file_get_contents($file->getRealPath()));

        }
        return $storedFileName;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Machinary::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $machinary=Machinary::find($id);
        /*try{
            if($row->photo!=null)
                unlink(public_path($row->photo));
        }catch (Exception $e){}*/
        if($machinary->photo!=''){
            if (file_exists(public_path($machinary->photo))) {
                unlink(public_path($machinary->photo));
            }
        }

        if (Machinary::destroy($id)) {
            return Response::json(['msg' => 'Machinary deleted']);
        } else {
            return Response::json(['error' => 'Record not found'], 400);
        }
    }
}
