<?php

namespace App\Http\Controllers\Auth;


use App\MachineryTransferItem;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class MachineryTransferItemController extends Controller
{
    /**
     * Validates given data for Bill
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'machinaries_id' =>'required',
            'machinerytransfers_id'=>'required',
            'qty'         =>'required|numeric|max:999999999999999999',
            'rate'        =>'required|numeric|max:999999999999999999',
            'net'         =>'required|numeric|max:999999999999999999',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return MachineryTransferItem::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $item=new MachineryTransferItem($request->all());
        if($item->save()){
            return $item;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return MachineryTransferItem::with('machinerytransfer','machinery')->where('machinerytransfers_id','=',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $item=MachineryTransferItem::find($id);
        $item->fill($request->all());
        if($item->save()) {
            return $item;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(MachineryTransferItem::where('machinerytransfers_id','=',$id)->delete())
            return Response::json(array('msg'=>'Machinery Transfer Item record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
