<?php

namespace App\Http\Controllers\Auth;



use App\MeterialReturn;
use Carbon\Carbon;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class MaterialReturnController extends Controller
{
    /**
     * Validates given data for Bill
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'date'    =>'required|date',
            'projects_id'=>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }

        return MeterialReturn::where('date','>=',$from)->where('date','<=',$to)->with('project')->get();
        //return MeterialReturn::with('project')->get();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getMaterialReturnBetweenDate(Request $request)
    {
        $from=$request->get('fromDate');
        $to=$request->get('toDate');
        return MeterialReturn::where('date','>=',$from)
            ->where('date','<=',$to)->with('project')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $bill=new MeterialReturn($request->all());
        if($bill->save()){
            return $bill;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return MeterialReturn::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $bill=MeterialReturn::find($id);
        $bill->fill($request->all());
        if($bill->save()) {
            return $bill;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(MeterialReturn::destroy($id))
            return Response::json(array('msg'=>'MeterialReturn record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
