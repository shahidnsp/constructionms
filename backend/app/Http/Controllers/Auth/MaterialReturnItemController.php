<?php

namespace App\Http\Controllers\Auth;




use App\MeterialReturn_Item;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class MaterialReturnItemController extends Controller
{
    /**
     * Validates given data for Bill
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'products_id' =>'required',
            'materialreturns_id'=>'required',
            'qty'         =>'required|numeric|max:999999999999999999',
            'rate'        =>'required|numeric|max:999999999999999999',
            'units_id'    =>'required',
            'net'         =>'required|numeric|max:999999999999999999',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return MeterialReturn_Item::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $item=new MeterialReturn_Item($request->all());
        if($item->save()){
            return $item;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return MeterialReturn_Item::with('returns','product','unit')->where('materialreturns_id','=',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $item=MeterialReturn_Item::find($id);
        $item->fill($request->all());
        if($item->save()) {
            return $item;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(MeterialReturn_Item::where('materialreturns_id','=',$id)->delete())
            return Response::json(array('msg'=>'MeterialReturn Item record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
