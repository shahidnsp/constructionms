<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

use App\Note;

class NoteController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data,
            [
               'name' => 'required|max:255',
               'description' => 'required|max:16000'
            ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Note::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $note = new Note($request->all());
        if($note->save()){
            return $note;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        return Note::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        return Note::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        //

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $note = Note::find($id);

        $note->fill($request->all());
        if($note->save()){
            return $note;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        if(Note::destroy($id))
            return Response::json(array('msg'=>'record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
