<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use Auth;
use App\OfficeExpense;
use Validator;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class OfficeExpenseController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'date' => 'required',
            'amount' => 'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return OfficeExpense::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(),400);
        }

        $office = new OfficeExpense($request->all());
        if ($office->save()) {
            return $office;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return OfficeExpense::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }
        $office = OfficeExpense::find($id);
        $office->fill($request->all());
        if ($office->save()) {
            return $office;
        }

        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (OfficeExpense::destroy($id)) {
            return Response::json(['msg' => 'OfficeExpense record deleted']);
        } else {
            return Response::json(['error' => 'Record not found']);
        }
    }
}
