<?php

namespace App\Http\Controllers\Auth;

use App\Partner;
use App\User;
use Illuminate\Http\Request as Request;

use App\Http\Controllers\Controller;
use Auth;
use Mockery\Exception;
use Validator;
use Response;

use App\Contact;
use App\Phone;
use App\Employee;

class PartnerController extends Controller
{

    protected function validator(array $data)
    {

        //TODO photo field decision
        return Validator::make($data,
            [
                'firstname'      => 'required|max:45',
                'lastname'       => 'required|max:45',
                'address'        => 'required|max:250',
                'project_id'     =>'required',
            ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Partner::with('user','projects')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $partner = new Partner($request->all());
        $user=User::findOrFail(Auth::id());
        $partner->user=$user->username;
        if($partner->save()){
            return $partner;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $partner = Partner::find($id);
        if($partner->update($request->all())){
            return $partner;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $count = Partner::destroy($id);

        if($count)
            return Response::json(array('msg'=>$count.' records deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
