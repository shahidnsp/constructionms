<?php

namespace App\Http\Controllers\Auth;

use App\Product;
use Illuminate\Http\Request as Request ;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class ProductController extends Controller
{

    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {

        // Loan account is not validated


        return Validator::make($data,[
            'name'  => 'required',
            'sales_price'   => 'required',
            'unit_id'     => 'required',
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Product::with('units','suppliers')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $product = new Product($request->all());
      /*  $user=User::findOrFail(Auth::id());
        $account->user=$user->username;*/
        if($product->save()){
            return $product;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Product::with('units','suppliers')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $product = Product::find($id);
        /*$account->fill($request->all());
        $user=User::findOrFail(Auth::id());
        $account->user=$user->username;*/
        if($product->update($request->all())){
            return $product;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO Product model for cascade delete

        if(Product::destroy($id))
            return Response::json(array('msg'=>'Product deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
