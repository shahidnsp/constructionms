<?php

namespace App\Http\Controllers\Auth;

use App\Expense;
use App\Income;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Validator;
use Response;
use App\User;
use App\Project;
use App\Wage;
use App\Employee;

class ProjectController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'         => 'required|alpha_text|max:45',
            'description'  => 'alpha_text|max:500',
            'approxamount' => 'numeric|max:999999999999999999',
            'startdate'    => 'required|date',
            'enddate'      => 'date',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $active=$request->active;
        if($active==null)
            return Project::with('expenses','wages')->where('active',1)->get();
        else
            return Project::with('expenses','wages')->where('active',$active)->get();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function deactivateProject(Request $request){
        $id=$request->get('projects_id');

        $project=Project::find($id);
        $project->active=0;
        $project->save();
        return $project;
    }

    public function getProjectStatus(Request $request){
        $id=$request->get('projects_id');
        $status=[];
        $income=Income::where('projects_id',$id)->sum('amount');
        $status['income']=$income;

        $expense=Expense::where('projects_id',$id)->sum('amount');
        $wage=Wage::where('projects_id',$id)->sum('amount');
        $status['expense']=$expense+$wage;

        $profit=$income-($expense+$wage);
        $status['profit']=$profit;

        return $status;
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     *  Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $project = new Project($request->all());

        $user=User::findOrFail(Auth::id());
        $project->user=$user->username;

        if($project->save())
            return  Project::with('expenses','wages.employee')->find($project->id);

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        return  Project::with('expenses','wages.employee')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $project = Project::find($id);
        $project ->fill($request->all());

        $user=User::findOrFail(Auth::id());
        $project->user=$user->username;

        if($project ->save()){
            return  Project::with('expenses','wages.employee')->find($project->id);
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO project cascade delete

        if(Project::destroy($id))
            return Response::json(array('msg'=>'Project deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }

}
