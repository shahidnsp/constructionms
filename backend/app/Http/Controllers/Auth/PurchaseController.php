<?php

namespace App\Http\Controllers\Auth;

use App\Purchase;
use Carbon\Carbon;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class PurchaseController extends Controller
{
    /**
     * Validates given data for Bill
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'date'    =>'required|date',
            'total'   =>'required',
            'paid'   =>'required',
            'balance'   =>'required',
            'suppliers_id'   =>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }

        return Purchase::where('date','>=',$from)->where('date','<=',$to)->with('supplier')->get();
        //return Purchase::all();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getPurchaseBetweenDate(Request $request)
    {
        $from=$request->get('fromDate');
        $to=$request->get('toDate');
        return Purchase::where('date','>=',$from)
            ->where('date','<=',$to)->with('supplier')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }
        $mode='';
        $bill=new Purchase($request->all());
        if($request->paid==0)
            $mode='Credit';
        elseif($request->balance <= 0)
            $mode='Cash';
        else
            $mode='Cash Credit';

        $bill->mode=$mode;

        if($bill->save()){
            return $bill;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Purchase::with('supplier')->findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $bill=Purchase::find($id);
        $bill->fill($request->all());
        if($bill->save()) {
            return $bill;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(Purchase::destroy($id))
            return Response::json(array('msg'=>'Purchase record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
