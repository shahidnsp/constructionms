<?php

namespace App\Http\Controllers\Auth;

use App\Purchase_Items;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class PurchaseItemController extends Controller
{
    /**
     * Validates given data for Bill
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'products_id' =>'required',
            'purchases_id'=>'required',
            'qty'         =>'required|numeric|max:999999999999999999',
            'rate'        =>'required|numeric|max:999999999999999999',
            'units_id'    =>'required',
            'net'         =>'required|numeric|max:999999999999999999',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Purchase_Items::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $item=new Purchase_Items($request->all());
        if($item->save()){
            return $item;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Purchase_Items::with('purchase','product','unit')->where('purchases_id','=',$id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $item=Purchase_Items::find($id);
        $item->fill($request->all());
        if($item->save()) {
            return $item;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(Purchase_Items::where('purchases_id','=',$id)->delete())
            return Response::json(array('msg'=>'Purchase Items record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
