<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

use App\User;
use App\Reminder;


class ReminderController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data,
            [
                'name'          =>'required|max:255',
                'description'   =>'required|max:16000',
                'regDate'       =>'required|date',
                'remDate'       =>'required|date'
            ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return Reminder::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $reminder = new Reminder($request->all());
        $reminder->done='false';
        if($reminder->save())
            return $reminder;

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        return Reminder::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param Request $request
     * @return Response
     */
    public function update($id,Request $request)
    {
        //
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $reminder = Reminder::find($id);
        $reminder->fill($request->all());
        if($reminder->save()){
            return $reminder;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        if(Reminder::destroy($id))
            return Response::json(array('msg'=>' Reminder deleted'));
        else
            return Response::json(array('error'=>'Records not found'),404);
    }
}
