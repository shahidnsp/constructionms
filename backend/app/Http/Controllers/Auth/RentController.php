<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Auth;
use App\User;
use App\Rent;
use App\Expense;

class RentController extends Controller
{
    /**
     * Validates given data for Rent
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name' =>'required|max:255',
            'amount'   =>'required|numeric|max:999999999999999999',
            'payDate' =>'required|date',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        return Rent::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $rent=new Rent($request->all());

        $user=User::findOrFail(Auth::id());
        $rent->user=$user->username;

        if($rent->save()){
            return $rent;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Rent::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return Employee::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $rent= Rent::find($id);

        $rent->fill($request->all());

        $user=User::findOrFail(Auth::id());
        $rent->user=$user->username;

        if($rent->save()){
            return $rent;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(Rent::destroy($id))
            return Response::json(array('msg'=>'record deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
