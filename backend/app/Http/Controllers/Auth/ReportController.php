<?php

namespace App\Http\Controllers\Auth;

use App\Project;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    /**
     * Get total wage paid to specified employees.
     *
     */
    public function getTotalEmployeeWage($id)
    {
        $emp=\App\Employee::find($id);
        return $emp->totalAmount;
    }

    /**
     * Get total income with respect to given month.
     *
     */
    public function getTotalIncome($year,$month){

        $startDate = \Carbon\Carbon::create($year,$month,1);
        $endDate = $startDate->endOfMonth();

        $income=\App\Income::where('date','>=',$startDate)->where('date','<=',$endDate)->get();
        return $income->sum('amount');
    }

    /**
     * Get total project expense with respect to given month.
     *
     */
    public function getTotalProjectExpense($year,$month,$id){
        $startDate = \Carbon\Carbon::create($year,$month,1);
        $endDate = $startDate->endOfMonth();
        $project =  \App\Project::find($id);

        return $project->expenses->whereNotBetween('payDate',['1973-06-19','2008-06-17'])->sum('amount');
    }
}
