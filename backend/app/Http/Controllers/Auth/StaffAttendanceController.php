<?php

namespace App\Http\Controllers\Auth;

use App\Employee;
use App\StaffAttendance;
use Carbon\Carbon;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Response;
use Validator;

class StaffAttendanceController extends Controller
{
    /**
     * @param array $data
     * @return \Illuminate\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'date' => 'required',
            'present' => 'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->date) {
            return StaffAttendance::with('staffattendancelist')->with('employee')->where('date',$request->date)->get();
        } else {
            return StaffAttendance::with('staffattendancelist')->with('employee')->get();
        }
    }

    public function get_attendance(Request $request)
    {
        if ($request->date) {

            $employees = Employee::where('employeeType','Office Staff')->get();
            foreach ($employees as $value) {
                $value['attendance'] = '';
            }

            $time = strtotime($request->date);
            $month = date("m", $time);
            $year = date("Y", $time);
            $fromdate = $year . '-' . $month . '-1';
            $todate = $year . '-' . $month . '-31';

            foreach($employees as $employee) {
                $attendance = StaffAttendance::where('employees_id', $employee->id)->where('date', '>=', $fromdate)->where('date', '<=', $todate)->orderBy('date')->get();
                foreach ($attendance as $value) {
                    $date = strtotime($value->date);
                    $day = date("d", $date);
                    $value->day = (int)$day;
                }
                $attendance_len = sizeof($attendance);

                for ($i = $attendance_len; $i <= 31; $i++) {

                }
                $employee->attendance = $attendance;

            }
            return $employees;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(),400);
        }

        $attendance = new StaffAttendance($request->all());
        if ($attendance->save()) {
            return $attendance;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $attendance = StaffAttendance::find($id);
        $attendance->fill($request->all());
        if ($attendance->save()) {
            return $attendance;
        }

        return Response::json(['error' => 'Server Down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (StaffAttendance::destroy($id)) {
            return Response::json(['msg' => 'Staff Attendance record deleted']);
        }
        else
            return Response::json(['error' => 'Record not found'], 400);
    }
}
