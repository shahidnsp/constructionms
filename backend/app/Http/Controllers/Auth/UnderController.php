<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request as request;

use Auth;
use App\Under;
use Validator;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UnderController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Under::with('parent')->get();
    }

    /**
     * To Display Bank Account Information while it under Bank
     */
    public function accountLedgerForBank(Request $requests){

        $list=[];

        $visibleBank='false';
        $id=$requests->id;
        if($id=='18' || $id=='29') {
            $visibleBank = 'true';
        } else{
            $found=Under::where('id',$id)->whereIn('groupUnder',[18,29])->get();
            if(count($found)==0){
                $visibleBank='false';
            }else{
                $visibleBank='true';
            }
        }
        $list['bank']=$visibleBank;

        $visibleSec='false';
        if($id=='23' || $id=='27') {
            $visibleSec = 'true';
        } else{
            $found=Under::where('id',$id)->whereIn('groupUnder',[23,27])->get();
            if(count($found)==0){
                $visibleSec='false';
            }else{
                $visibleSec='true';
            }
        }
        $list['sec']=$visibleSec;

        return $list;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(),400);
        }
        $under = new Under($request->all());
        $under->isedit=1;
        if ($under->save()) {
            return $under;
        }

        return Response::json(['error' => 'server down']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Under::findOrfail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            Response::json($validator->errors(), 400);
        }

        $under = Under::find($id);
        $under->fill($request->all());
        if ($under->save()) {
            return $under;
        }

        return Response::json(['error' => 'Server Down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Under::destroy($id)) {
            return Response::json(['msg' => 'Under table deleted']);
        } else {
            return Response::json(['error' => 'Record not found']);
        }
    }
}
