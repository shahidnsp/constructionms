<?php

namespace App\Http\Controllers\Auth;

use App\Unit;
use Illuminate\Http\Request as Request ;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class UnitController extends Controller
{
    /**
     * Validates given data for account
     * @param array $data
     * @return Validator
     */

    protected function validator(array $data)
    {

        // Unit account is not validated

        return Validator::make($data,[
            'name'  => 'required',
            'isunit'   => 'required',
        ]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Unit::with('products')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $unit = new Unit($request->all());
        /*  $user=User::findOrFail(Auth::id());
          $account->user=$user->username;*/
        if($unit->save()){
            return $unit;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Unit::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $unit = Unit::find($id);
        /*$account->fill($request->all());
        $user=User::findOrFail(Auth::id());
        $account->user=$user->username;*/
        if($unit->update($request->all())){
            return $unit;
        }
        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO Unit model for cascade delete

        if(Unit::destroy($id))
            return Response::json(array('msg'=>'Unit deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
