<?php

namespace App\Http\Controllers\Auth;

use App\Personaldetail;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Mockery\Exception;
use Response;
use Validator;
use Auth;

use App\User;

class UserController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data,[
            //'email'     =>'required|email',
            'username'  =>'required',
            'name'      =>'required',
            'lastname'  =>'alpha_text'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //

        if(Auth::id() == 1)
            return User::where('id','!=',\Auth::id())->get();
        else
            return Response::json(['error'=>'Forbidden'],403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        if($request->id==null) {
            $validator = $this->validator($request->all());

            if ($validator->fails()) {
                return Response::json($validator->errors()
                    , 400);
            }

            $user = new User($request->all());

            $user->type = 'u';
            $user->password = 'admin';
            $user->permission = '111,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,111';
            if ($request->hasFile('photo')) {
                $files = $request->file('photo');
                //foreach($files as $file) {
                $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg');
                $validator = Validator::make(array('photo'=> $files), $rules);
                if($validator->passes()){
                    $user->photo = $this->uploadImage($files);
                }
                //}
            }else{
                $user->photo ='profile.jpg';
            }

            if ($user->save()) {
                $personal = new Personaldetail();
                $personal->user_id = $user->id;
                $personal->name = $user->name;
                $personal->save();
                return $user;
            }
        }else{
            $validator = $this->validator($request->all());

            if($validator->fails()){
                return Response::json($validator->errors()
                    ,400);
            }

            $user = User::find($request->id);
            $user->fill($request->all());

            if ($request->hasFile('photo')) {
                $files = $request->file('photo');
                //foreach($files as $file) {
                $rules = array('photo' => 'required|mimes:png,gif,jpeg,jpg');
                $validator = Validator::make(array('photo'=> $files), $rules);
                if($validator->passes()){
                    $user->photo = $this->uploadImage($files);
                }
                //}
            }


            if($user->save()){
                return $user;
            }
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Upload file into server
     * @param $file
     * @return string
     */
    public  function uploadImage($file){
        $storedFileName="";
        if(!empty($file)){

            $extension=$file->getClientOriginalExtension();
            $fileName = 'User'.rand(11111,99999).'.'.$extension;
             $imageRealPath 	= 	$file->getRealPath();
             $img = Image::make($imageRealPath); // use this if you want facade style code
             $img->resize(intval('500'), null, function($constraint) {
                 $constraint->aspectRatio();
             });
            $storedFileName=$fileName;
            //$storedFileName=$fileName;
            /* $file->move(
                 public_path().'/img/upload/', $fileName
             );*/

            //$img->save(public_path().$storedFileName);
            /*$path = public_path().'img/upload/'.$fileName;*/
            $image = Image::make($file->getRealPath())->resize(128, 128);//->save($path);

            Storage::disk('local')->put($fileName,$image->stream());//file_get_contents($file->getRealPath()));

        }
        return $storedFileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

    public function show($id)
    {
        $user = User::with('personaldetail')->findOrFail($id);
        $userData['id'] = $user->id;
        $userData['name'] = $user->name;
        $userData['lastname'] = $user->lastname;
        $userData['address']=$user->personaldetail->address;
        $userData['post']=$user->personaldetail->post;
        $userData['district']=$user->personaldetail->district;
        $userData['state']=$user->personaldetail->state;
        $userData['pin']=$user->personaldetail->pin;
        $userData['phone']=$user->personaldetail->phone;
        $userData['mobile1']=$user->personaldetail->mobile1;
        $userData['mobile2']=$user->personaldetail->mobile2;
        $userData['email1']=$user->personaldetail->email1;
        $userData['email2']=$user->personaldetail->email2;

        return $userData;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

        $validator = $this->validator($request->all());

        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }

        $user = User::find($id);
        $user->fill($request->all());

        if($user->save()){
            return $user;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

       $user = User::find($id);

       if ($user->photo != 'profile.jpg') {
           $exist=Storage::disk('local')->exists($user->photo);
           if($exist)
                Storage::delete($user->photo);
       }

        //TODO cascade

        if(User::destroy($id))
            return Response::json(array('msg'=>'User deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
