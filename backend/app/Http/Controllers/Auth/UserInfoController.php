<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use Validator;
use Response;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Helpers\UserHelper;
use App\User;
use App\Personaldetail;



class UserInfoController extends Controller
{

    const ALL_PERMISSION = true;
    const ONLY_PAGE = true;

    public function UserInfoController()
    {
        $userInfo['id']        = Auth::id();
        $userInfo['firstname'] = Auth::user()->name;
        $userInfo['lastname']  = Auth::user()->lastname;
        $userInfo['photo']     = Auth::user()->photo;;
        $userInfo['menu']      = UserHelper::pages(Auth::user()->permission);
        $userInfo['permissions'] = UserHelper::pages(Auth::user()->permission,false,true,true);
        $userInfo['isAdmin']   = Auth::user()->isAdmin();

        return $userInfo;
    }

    public function updateUserInfo($id,Request $request)
    {
        $userdata = $request->only('id','name','lastname');
        $personaldata = $request->except('id','name','lastname');

        $user = User::findOrFail($id);
        $user->update($userdata);

        $personal = Personaldetail::where('user_id',$id)->first();
        $personal->update($personaldata);
        return $this->getUserInfo($id);
    }

    protected function getUserInfo($id)
    {
        $user = User::with('personaldetail')->findOrFail($id);
        $userData['id'] = $user->id;
        $userData['name'] = $user->name;
        $userData['lastname'] = $user->lastname;
        $userData['address']=$user->personaldetail->address;
        $userData['post']=$user->personaldetail->post;
        $userData['district']=$user->personaldetail->district;
        $userData['state']=$user->personaldetail->state;
        $userData['pin']=$user->personaldetail->pin;
        $userData['phone']=$user->personaldetail->phone;
        $userData['mobile1']=$user->personaldetail->mobile1;
        $userData['mobile2']=$user->personaldetail->mobile2;
        $userData['email1']=$user->personaldetail->email1;
        $userData['email2']=$user->personaldetail->email2;

        return $userData;
    }

    protected function getUserDetails()
    {
        $user = User::with('personaldetail')->findOrFail(Auth::id());
        $userData['id'] = $user->id;
        $userData['photo'] = $user->photo;
        $userData['name'] = $user->name;
        $userData['lastname'] = $user->lastname;
        $userData['address']=$user->personaldetail->address;
        $userData['post']=$user->personaldetail->post;
        $userData['district']=$user->personaldetail->district;
        $userData['state']=$user->personaldetail->state;
        $userData['pin']=$user->personaldetail->pin;
        $userData['phone']=$user->personaldetail->phone;
        $userData['mobile1']=$user->personaldetail->mobile1;
        $userData['mobile2']=$user->personaldetail->mobile2;
        $userData['email1']=$user->personaldetail->email1;
        $userData['email2']=$user->personaldetail->email2;

        return $userData;
    }

    public function resetUserPassword(Request $request)
    {
        try{
            $id = $request->input('id');
            $validator = Validator::make(['id'=>$id],['id'=>'required|numeric']);

            if($validator->passes()){
                $user = User::findOrFail($id);
                //TODO add settings for reset password
                $user->password = 'admin';
                if($user->save())
                    return $user;
            }
            else
                return Response::json(['error'=>$validator->errors()],400);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(['error'=>'User not found'],404);
        }

    }

    public function setUserPassword(Request $request)
    {
        $data = $request->only('oldPassword','password');
        $validator = Validator::make($data,[
            'oldPassword'=>'required',
            'password'=>'required'
        ]);


        if($validator->passes()){
            try{

                $user = User::findOrFail(Auth::id());
            }
            catch(ModelNotFoundException $e)
            {
                return Response::json(['error'=>'Requested user not found'],404);
            }



            if($user->changePassword($data['oldPassword'],$data['password'])){
                if($user->save())
                    return $user;
            }
            else
                return Response::json(['error'=>'Old password does not match'],400);
        }
        else
            return Response::json(['errors'=>$validator->errors()],400);
    }

    public function getUserPermission(Request $request)
    {
        try{
            $id = $request->input('id');
            $validator = Validator::make(['id'=>$id],['id'=>'required|numeric']);

            if($validator->passes()){
                $user = User::findOrFail($id);

                $permission =
                    UserHelper::pages($user->permission,!self::ONLY_PAGE,self::ALL_PERMISSION);

                return Response::json($permission);
            }
            else
                return Response::json(['error'=>$validator->errors()],400);
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(['error'=>'User not found'],404);
        }
    }

    public function setUserPermission(Request $request)
    {
        if($request->only('id','permission'))
        {
            $permission= UserHelper::makePermission($request->input('permission'));

            $user = User::findOrFail($request->input('id'));
            $user->permission = $permission;
            if($user->save())
                return $user;
            return Response::json(['error'=>'Server is down'],500);
        }

        return Response::json(['error'=>'bad request'],400);
    }

    public function getAllPages($id)
    {
        return $userPages = UserHelper::notPages(User::find($id)->permission);
    }

}