<?php

namespace App\Http\Controllers\Auth;

use App\Employee;
use App\WageAdvance;
use Carbon\Carbon;
use Illuminate\Http\Request as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Redis\RedisServiceProvider;
use Validator;


class WageAdvanceController extends Controller
{
    public function validator(array $data)
    {
        return Validator::make($data, [
            'employees_id' => 'required',
            'date' => 'required',
            'amount' => 'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $from= $request->get('fromDate');
        $to=$request->get('toDate');
        if($from==null || $to==null) {
            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth();
        }
        return WageAdvance::where('date','>=',$from)
            ->where('date','<=',$to)->with('employee')->get();
    }

    public function getAdvance(Request $request){
        return WageAdvance::where('employees_id',$request->employees_id)->sum('balance');
    }

    public function getWageAdvanceList(){
        $lists=[];
        $i=0;
        $employees=Employee::where('employeeType','Non Office Staff')->get();
        foreach($employees as $employee){
            $adv=WageAdvance::where('employees_id',$employee->id)->sum('balance');
            if($adv!=0) {
                $list['employees_id'] = $employee->id;
                $list['name'] = $employee->name;
                $list['balance'] = $adv;
                $lists[$i++] = $list;
            }
        }
        return $lists;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(),400);
        }

        $salary_advance = new WageAdvance($request->all());
        $salary_advance->balance=$request->amount;
        if ($salary_advance->save()) {
            return $salary_advance;
        }
        return Response::json(['error' => 'Server Down'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return WageAdvance::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $salary_advance = WageAdvance::find($id);
        $salary_advance->fill($request->all());
        $salary_advance->balance=$request->amount;
        if ($salary_advance->save()) {
            return $salary_advance;
        }

        return Response::json(['error' => 'Server Down'], 500);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (WageAdvance::destroy($id)) {
            return Response::json(['msg' => 'Wage Advance record deleted']);
        }
        else
            return Response::json(['error' => 'Record not found'], 400);
    }
}
