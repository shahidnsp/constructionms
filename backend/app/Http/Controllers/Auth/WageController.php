<?php

namespace App\Http\Controllers\Auth;

use App\WageAdvance;
use Illuminate\Http\Request as Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

use App\Wage;
use App\Expense;
use App\Employee;

class WageController extends Controller
{
    protected  function remAmount($wage)
    {
        $emp=Employee::find($wage->employee_id);

        $emp->totalAmount -= $wage->amount;
        return $emp->save();
    }
    /**
     * Validates given data for Wage
     * @param array $data
     * @return Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'        =>'required|alpha_text|max:255',
            'employees_id' =>'numeric',
            'projects_id'  =>'numeric',
            'amount'      =>'required|numeric|max:999999999999999999',
            'payDate'        =>'required|date',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $projects_id=$request->get('projects_id');
        $from= $request->get('fromDate');
        $to=$request->get('toDate');

        if($projects_id==null)
            return Wage::with('employee')->with('projects')->get();
        else
            return Wage::with('employee')->where('projects_id',$projects_id)->where('payDate','>=',$from)->where('payDate','<=',$to)->get();
    }

    /**
     * @return array
     */
    public function getWageBalanceList(){
        $lists=[];
        $i=0;
        $employees=Employee::where('employeeType','Non Office Staff')->get();
        foreach($employees as $employee){
            $adv=Wage::where('employees_id',$employee->id)->sum('balance');
            if($adv!=0) {
                $list['employees_id'] = $employee->id;
                $list['name'] = $employee->name;
                $list['balance'] = $adv;
                $lists[$i++] = $list;
            }
        }
        return $lists;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getWageBetweenDate(Request $request)
    {
        return Wage::with('employee')->where('payDate','>=',$request->fromDate)
            ->where('payDate','<=',$request->toDate)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

//      dd($request->all());

        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $wage=new Wage($request->all());
        //$wage->employees_id = $request->input('employee')['name']['id'];
        if($wage->save()){
            /*$advance=WageAdvance::where('employees_id',$request->employees_id)->get()->last();
            if($advance!=null) {
                $advance->balance = $advance->balance - $request->advance;
                $advance->save();
            }*/
            return $wage;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Wage::with('employee')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json($validator->errors()
                ,400);
        }
        $wage=Wage::find($id);
        //$wage->fill($request->all());

        //TODO possible undefined index error
       // $wage->employees_id = $request->input('employee')['name']['id'];

        if($wage->update($request->all())) {
            $advance=WageAdvance::where('employees_id',$request->employees_id)->get()->last();
            $advance->balance=$advance->balance-$request->advance;
            $advance->save();
            return $wage;
        }

        return Response::json( ['error' => 'Server is down']
            ,500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //TODO Account model for cascade delete

        if(Wage::destroy($id))
            return Response::json(array('msg'=>'Wage deleted'));
        else
            return Response::json(array('error'=>'Records not found'),400);
    }
}
