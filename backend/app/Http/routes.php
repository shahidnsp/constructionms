<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', function () {
    //return view('welcome');
    return view('auth/login');
}]);

//TODO remove
/**
 * Add small testing code here
 */
Route::get('test',function(){
   /* function IncomeAndExpense()
    {
        $startDate = Carbon\Carbon::create('2000','04','1');

        $income =  App\Income::whereBetween('date',[$startDate,'2007-08-16'])
            ->get()
            ->sum('amount');

        $expense = App\Expense::whereBetween('payDate',[$startDate,'2009-08-01'])
            ->get()
            ->sum('amount');

        $expense += App\Rent::whereBetween('payDate',[$startDate,'2009-08-01'])
            ->get()
            ->sum('amount');

        $expense += App\Wage::whereBetween('payDate',[$startDate,'2009-08-01'])
            ->get()
            ->sum('amount');

        $report['start']        = $startDate;
        $report['end']          = $startDate->endOfMonth();
        $report['totalIncome']  = $income;
        $report['totalExpense'] = $expense;
        return $report;
    }*/

    /*$fromDate = Carbon\Carbon::create('1997','12','09');
    $toDate=Carbon\Carbon::create('2016','04','1');
        return App\Wage::with('employee')->where('payDate','>=',$fromDate)
            ->where('payDate','<=',$toDate)->get();*/


});

Route::get('images/{filename}', function ($filename)
{
    $file = \Illuminate\Support\Facades\Storage::get($filename);
    return response($file, 200)->header('Content-Type', 'image/jpeg');
});



//Route::controller('user','Auth\AuthController');

Route::group(['prefix' => 'user'], function () {

    Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
    Route::post('login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);

    Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);
});


// Loggedin page
Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'Auth\DashboardController@index']);

//TODO add middleware to authenticate
//Route::group(['middleware'=>'auth','prefix'=>'api'],function(){
Route::group(['prefix' => 'api'], function () {

    //User info APIs

    Route::get('userinfo',['as'=>'userinfo','uses'=>'Auth\UserInfoController@UserInfoController']);

    Route::put('userinfo/{id}',['as'=>'userinfo','uses'=>'Auth\UserInfoController@updateUserInfo']);

    Route::get('getUserDetails','Auth\UserInfoController@getUserDetails');

    Route::get('getAllPages/{id}',['as'=>'getAllPages','uses'=>'Auth\UserInfoController@getAllPages']);

    Route::get('projectReport/{year}/{month}/{id}',['as'=>'getTotalProjectExpense','uses'=>'Auth\ReportController@getTotalProjectExpense']);

    Route::get('totalWage/{id}',['as'=>'getTotalEmployeeWage','uses'=>'Auth\ReportController@getTotalEmployeeWage']);

    Route::get('totalIncome/{year}/{month}',['as'=>'getTotalIncome','uses'=>'Auth\ReportController@getTotalIncome']);

    Route::post('resetPassword',['as'=>'resetPass','uses'=>'Auth\UserInfoController@resetUserPassword']);

    //TODO use password broker
    Route::post('changePassword',['as'=>'changePass','uses'=>'Auth\UserInfoController@setUserPassword']);

    Route::get('getPermission',['as'=>'getPermission','uses'=>'Auth\UserInfoController@getUserPermission']);
    Route::post('changePermission',['as'=>'setPermission','uses'=>'Auth\UserInfoController@setUserPermission']);


    //End of User info APIs

    //Rest resources
    Route::resource('home', 'Auth\HomeController');
    Route::resource('contact', 'Auth\ContactController');
    Route::resource('note', 'Auth\NoteController');
    Route::resource('reminder', 'Auth\ReminderController'); //TODO resource controller
    Route::resource('bank', 'Auth\BankController');
    Route::resource('account', 'Auth\AccountController');
    Route::resource('accountParticular', 'Auth\AccountParticularController');
    Route::resource('emi', 'Auth\EmiController');
    Route::resource('loan', 'Auth\LoanController');
    Route::resource('card', 'Auth\CardController');
    Route::resource('partner', 'Auth\PartnerController');

    Route::get('cash','Auth\CashController@getCashReport');
    Route::resource('cashDebit', 'Auth\CashDebitController');
    Route::resource('cashCredit', 'Auth\CashCreditController');
    Route::resource('user', 'Auth\UserController');
    Route::get('wageBetweenDate','Auth\WageController@getWageBetweenDate');


    //Shahid
    Route::resource('project','Auth\ProjectController');
    Route::resource('employee', 'Auth\EmployeeController');
    Route::resource('wage', 'Auth\WageController');
    Route::resource('rent', 'Auth\RentController');
    Route::resource('expense', 'Auth\ExpenseController');
    Route::resource('bill', 'Auth\BillController');
    Route::resource('income', 'Auth\IncomeController');
    Route::resource('insurance', 'Auth\InsuranceController');
    Route::resource('premium', 'Auth\InsurancePremiumController');
    Route::resource('product', 'Auth\ProductController');
    Route::resource('unit', 'Auth\UnitController');
    Route::resource('supplier', 'Auth\SupplierController');
    Route::resource('attendance', 'Auth\AttendanceController');
    Route::resource('attendancegroup', 'Auth\AttendanceGroupController');
    Route::get('attendancelist', 'Auth\AttendanceGroupController@getAttendanceDateList');
    Route::get('attendancereport', 'Auth\AttendanceGroupController@getAttendanceList');
    Route::resource('leader', 'Auth\LeaderController');
    Route::resource('group', 'Auth\GroupController');
    Route::resource('purchase', 'Auth\PurchaseController');
    Route::resource('purchaseitem', 'Auth\PurchaseItemController');
    Route::resource('transfer', 'Auth\TransferController');
    Route::resource('transferitem', 'Auth\TransferItemController');
    Route::resource('materialreturn', 'Auth\MaterialReturnController');
    Route::resource('materialreturn_item', 'Auth\MaterialReturnItemController');
    Route::resource('machinery', 'Auth\MachinaryController');
    Route::resource('machinerytransfer', 'Auth\MachineryTransferController');
    Route::resource('machinerytransferitem', 'Auth\MachineryTransferItemController');
    Route::resource('document', 'Auth\DocumentController');
    Route::resource('wageadvance', 'Auth\WageAdvanceController');
    Route::resource('staffattendance', 'Auth\StaffAttendanceController');
    Route::resource('staffattendancedetail', 'Auth\StaffAttendanceDetailController');
    Route::get('getDashboardStatus', 'Auth\DashboardController@getStatus');

    Route::get('getledgerbalance', 'Auth\JournalController@getLedgerBalance');
    Route::get('getledgerbalancedate', 'Auth\JournalController@getLedgerBalanceDate');
    Route::post('deactivateproject', 'Auth\ProjectController@deactivateProject');
    Route::get('getAdvance', 'Auth\SalaryAdvanceController@getAdvance');
    Route::get('getWageAdvance', 'Auth\WageAdvanceController@getAdvance');
    Route::get('getleaderEmployeeID', 'Auth\LeaderController@getLeaderEmployeeID');
    Route::get('getSalaryBalance', 'Auth\EmployeeSalaryController@getPreBalance');
    Route::get('getSalaryAdvanceList', 'Auth\SalaryAdvanceController@getSalaryAdvanceList');
    Route::get('getWageAdvanceList', 'Auth\WageAdvanceController@getWageAdvanceList');
    Route::get('getSalaryBalanceList', 'Auth\EmployeeSalaryController@getSalaryBalanceList');
    Route::get('getWageBalanceList', 'Auth\WageController@getWageBalanceList');

    Route::get('accountLedgerForBank', 'Auth\UnderController@accountLedgerForBank');
    Route::get('getLedgerDetailsForCashBook', 'Auth\JournalController@getLedgerDetailsReportForCashBook');

    //Project Summazion
    Route::get('projectstatus', 'Auth\ProjectController@getProjectStatus');
    //Account Report
    Route::get('cashbook', 'Auth\JournalController@getCashbook');
    Route::get('daybook', 'Auth\JournalController@getDayBook');
    Route::get('accountstatement', 'Auth\JournalController@getAccountStatement');
    Route::get('trialbalance', 'Auth\JournalController@getTrialBalance');
    Route::get('tradingaccount', 'Auth\JournalController@getTradingAccount');
    Route::get('profitandloss', 'Auth\JournalController@getProfitandLossAccount');
    Route::get('getsundrydebtor', 'Auth\JournalController@getSundryDebtorList');
    Route::get('getsundrycreditor', 'Auth\JournalController@getSundryCreditorList');


    //Employee Wage
    Route::get('getwage', 'Auth\AttendanceGroupController@getWage');

    //Search Options
    Route::get('getpurchasewithdate', 'Auth\PurchaseController@getPurchaseBetweenDate');
    Route::get('gettransferwithdate', 'Auth\TransferController@getTransferBetweenDate');
    Route::get('getmaterialreturnwithdate', 'Auth\MaterialReturnController@getMaterialReturnBetweenDate');
    Route::get('getmachinerytransferwithdate', 'Auth\MachineryTransferController@getMachineryTransferBetweenDate');
    Route::get('getjournalwithdate', 'Auth\JournalController@getJournalBetweenDate');
    Route::get('getattendance', 'Auth\StaffAttendanceController@get_attendance');
    //End Rest resources


    //noushid
    Route::resource('ledger', 'Auth\LedgerController');
    Route::resource('under', 'Auth\UnderController');
    Route::resource('journal', 'Auth\JournalController');
    Route::resource('office', 'Auth\OfficeExpenseController');
    Route::resource('loanexp', 'Auth\LoanExpenseController');
    Route::resource('employeesalary', 'Auth\EmployeeSalaryController');
    Route::resource('salaryadvance', 'Auth\SalaryAdvanceController');
    Route::resource('clients', 'Auth\ClientController');
    Route::get('attendancereport', 'Auth\AttendanceController@getAttendanceReport');

});



//Load angular templates
//TODO user permission validation
Route::get('template/{name}', ['as' => 'template', function ($name) {
//    $permission = UserHelper::pages(Auth::user()->permission,false,true,true);
//    return view('app.' . $name,['permission'=>$permission]);
    return view('app.' . $name);
}]);

//TODO load temp test json files
Route::get('json/{name}', function ($name) {

    return Storage::get('json/' . $name);
});

// Catch all undefined routes and give to index.php to handle  .
Route::any('{undefinedRoute}', function ($undefinedRoute) {
    return view('app.index');
})->where('undefinedRoute', '([A-z\d-\/_.]+)?');


// Using different syntax for Blade to avoid conflicts with Jade.
// You are well-advised to go without any Blade at all.
Blade::setContentTags('[[', ']]'); // For variables and all things Blade.
Blade::setEscapedContentTags('[[-', '-]]'); // For escaped data.
