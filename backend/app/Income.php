<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model {

    protected $fillable = ['name','description','amount','date','projects_id'];

    public function projects()
    {
        return $this->belongsTo('App\Project','projects_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($income) {
            $ledger=\App\Ledger::where('projects_id',$income->projects_id)->get()->first();
            $project=\App\Project::find($income->projects_id);

            $journal = new \App\Journal();
            $journal->drledgers_id = 3;
            $journal->crledgers_id = $ledger->id;
            $journal->dramount = $income->amount;
            $journal->cramount = $income->amount;
            $journal->note = "Cash from ".$project->name;
            $journal->vouchertype = "Income";
            $journal->date = $income->date;
            $journal->incomes_id = $income->id;
            $journal->save();

        });

        static::updated(function ($income) {


            \App\Journal::where('incomes_id','=',$income->id)->delete();

            $ledger=\App\Ledger::where('projects_id',$income->projects_id)->get()->first();

            $project=\App\Project::find($income->projects_id);

            $journal = new \App\Journal();
            $journal->drledgers_id = 3;
            $journal->crledgers_id = $ledger->id;
            $journal->dramount = $income->amount;
            $journal->cramount = $income->amount;
            $journal->note = "Cash from ".$project->name;
            $journal->vouchertype = "Income";
            $journal->date = $income->date;
            $journal->incomes_id = $income->id;
            $journal->save();

        });

        static::deleted(function ($income) {
            \App\Journal::where('incomes_id','=',$income->id)->delete();
        });
    }
}
