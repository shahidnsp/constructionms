<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model {

	//
    protected $fillable = ['name','insuranceNumber','term','description ','amount','paidAmount','nominee','premiumAmount','takenDate','contact_id','file_id'];
    public function insurance_premium()
    {
        return $this->hasMany('App\Insurance_premium');
    }

    public function photo()
    {
        return $this->hasOne('App\File');
    }

    public function agent()
    {
        return $this->belongsTo('App\Contact');
    }

    public static function boot()
    {
        parent::boot();

        //Cascade delete for Insurance_premium relation
        static::deleted(function($insurance){
            $insurance->insurance_premium()->delete();
        });

        //Cascade delete for photo relation
        static::deleted(function($insurance){
            $insurance->photo()->delete();
        });
    }
}
