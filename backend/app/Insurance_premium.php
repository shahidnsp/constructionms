<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurance_premium extends Model {

    protected  $table = 'insurance_premium';
    protected $fillable = ['insurance_id','amount','payDate'];
    public function insurance()
    {
        return $this->belongsTo('App\Insurance');
    }
    public static function boot()
    {
        parent::boot();

//        static::created(function($premium){
//            $insurance=\App\Insurance::find($premium->insurance_id);
//
//            $insurance->paidAmount += $premium->amount;
//            $premium->save();
//        });
//
//        static::deleted(function($premium){
//            $insurance=\App\Insurance::find($premium->insurance_id);
//
//            $insurance->paidAmount -=$premium->amount;
//            $premium->save();
//        });
//        static::updating(function($premium){
//
//            $insurance=\App\Insurance::find($premium->insurance_id);
//
//            $insurance->paidAmount-=$premium->amount;
//            $premium->save();
//        });
//        static::updated(function($premium){
//            $insurance=\App\Insurance::find($premium->insurance_id);
//
//            $insurance->paidAmount += $premium->amount;
//            $premium->save();
//        });
    }

}
