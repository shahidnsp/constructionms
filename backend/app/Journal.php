<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    protected $fillable = ['drledgers_id', 'crledgers_id', 'dramount', 'cramount', 'note', 'date','vouchertype','purchases_id','incomes_id','expenses_id','wages_id','advance_id','salary_id','wageadvance_id','bankpayment_id','loanpayment_id','loanexpense_id','transfer_id','materialreturn_id','machinery_id'];

    public function drledger()
    {
        return $this->belongsTo('App\Ledger','drledgers_id');
    }

    public function crledger()
    {
        return $this->belongsTo('App\Ledger','crledgers_id');
    }
}
