<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leader extends Model
{
    protected $fillable=['project_id','employee_id'];

    public function projects()
    {
        return $this->belongsTo('App\Project','project_id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function groups()
    {
        return $this->hasMany('App\Group');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($leader) {

            \App\Group::create([
                'leader_id' => $leader->id,
                'employee_id' => $leader->employee_id,
            ]);

        });

        static::deleted(function ($leader) {
            \App\Group::where('leader_id','=',$leader->id)->delete();
        });
    }
}
