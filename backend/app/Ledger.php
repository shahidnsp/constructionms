<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ledger extends Model
{
    protected $fillable = ['name', 'unders_id', 'openingBalance','crOrDr','narration','mailingName','address','phone','mobile','email','creditPeriod','creditLimit','tin','cst','pan','bankAccountNumber','branchName','branchCode','isedit','bankaccount_id','projects_id','loan_id','clients_id'];

    public function unders()
    {
        return $this->belongsTo('App\Under','unders_id');
    }

   /* public static function boot()
    {
        parent::boot();

        static::created(function ($ledger) {

            if($ledger->openBal!=null) {
                if ($ledger->paymentmode === 'Debit') {
                    \App\Journal::create([
                        'drledgers_id' => $ledger->id,
                        'crledgers_id' => '3',
                        'dramount' => $ledger->openBal,
                        'cramount' => $ledger->openBal,
                        'note' => 'Opening balance of ' . $ledger->name,
                        'vouchertype' => 'Journal',
                        'date' => $ledger->date
                    ]);
                } else {
                    \App\Journal::create([
                        'drledgers_id' => '3',
                        'crledgers_id' => $ledger->id,
                        'dramount' => $ledger->openBal,
                        'cramount' => $ledger->openBal,
                        'note' => 'Opening balance of ' . $ledger->name,
                        'vouchertype' => 'Journal',
                        'date' => $ledger->date
                    ]);
                }
            }

        });

    }*/
}
