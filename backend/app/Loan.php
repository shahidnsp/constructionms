<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model {


    protected  $table = 'loan';
    protected  $fillable = ['bank_id', 'account_id', 'name', 'description', 'amount', 'paid', 'balance', 'date', 'closedate', 'interest'];
	//
    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }

    public function emi()
    {
        return $this->hasMany('App\Emi');

    }
    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function loan_expense()
    {
        return $this->hasMany('App\LoanExpense');
    }

    //Cascade delete
    public static function boot()
    {
        parent::boot();

        static::created(function ($loan) {
            \App\Ledger::create([
                'name' => $loan->name,
                'unders_id' => '3',
                'openingBalance' => $loan->paid,
                'crOrDr' => 'Dr',
                'isedit'=>1,
                'loan_id'=>$loan->id
            ]);
        });

        static::deleted(function ($loan) {
            $loan->emi()->delete();
            \App\Ledger::where('loan_id','=',$loan->id)->delete();
        });

        static::updated(function ($loan) {
            /*$account=\App\Account::find($loan->account_id);
            $account->balance=$loan->paid;
            $account->save();*/
        });
    }
}
