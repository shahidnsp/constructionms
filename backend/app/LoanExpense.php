<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanExpense extends Model
{
    protected $fillable = ['loan_id', 'name', 'description', 'date', 'amount'];
    protected $table = 'loanexpenses';

    public function loan()
    {
        return $this->belongsTo('App\Loan', 'loan_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($expense) {

            $journal = new \App\Journal();
            $journal->drledgers_id = 14;
            $journal->crledgers_id = 1;
            $journal->dramount = $expense->amount;
            $journal->cramount = $expense->amount;
            $journal->note = $expense->description;
            $journal->vouchertype = "Loan Expense";
            $journal->date = $expense->date;
            $journal->loanexpense_id = $expense->id;
            $journal->save();

        });

        static::updated(function ($expense) {
            \App\Journal::where('loanexpense_id','=',$expense->id)->delete();

            $journal = new \App\Journal();
            $journal->drledgers_id = 14;
            $journal->crledgers_id = 1;
            $journal->dramount = $expense->amount;
            $journal->cramount = $expense->amount;
            $journal->note = $expense->description;
            $journal->vouchertype = "Loan Expense";
            $journal->date = $expense->date;
            $journal->loanexpense_id = $expense->id;
            $journal->save();

        });

        static::deleted(function ($expense) {
            \App\Journal::where('loanexpense_id','=',$expense->id)->delete();
        });
    }
}
