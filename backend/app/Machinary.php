<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Machinary extends Model
{
    protected $fillable=['name','description','wageperday','qty','photo'];
}
