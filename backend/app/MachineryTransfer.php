<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineryTransfer extends Model
{
    protected $table='machinerytransfers';
    protected $fillable=['projects_id','date','description','from','status','amount'];

    public function project(){
        return $this->belongsTo('App\Project','projects_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($transfer) {

            $ledger=\App\Ledger::where('projects_id',$transfer->projects_id)->get()->first();

            if($ledger!=null) {
                $journal = new \App\Journal();
                $journal->drledgers_id = $ledger->id;
                $journal->crledgers_id = 5;
                $journal->dramount = $transfer->amount;
                $journal->cramount = $transfer->amount;
                $journal->note = $transfer->description;
                $journal->vouchertype = "Material Transfer";
                $journal->date = $transfer->date;
                $journal->machinery_id = $transfer->id;
                $journal->save();
            }
        });

        static::updated(function ($transfer) {


            \App\Journal::where('machinery_id','=',$transfer->id)->delete();

            $ledger=\App\Ledger::where('projects_id',$transfer->projects_id)->get()->first();
            if($ledger!=null) {
                $journal = new \App\Journal();
                $journal->drledgers_id = $ledger->id;
                $journal->crledgers_id = 5;
                $journal->dramount = $transfer->amount;
                $journal->cramount = $transfer->amount;
                $journal->note = $transfer->description;
                $journal->vouchertype = "Material Transfer";
                $journal->date = $transfer->date;
                $journal->machinery_id = $transfer->id;
                $journal->save();
            }
        });

        static::deleted(function ($transfer) {
            \App\Journal::where('machinery_id','=',$transfer->id)->delete();
        });
    }
}
