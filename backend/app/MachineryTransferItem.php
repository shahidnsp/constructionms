<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineryTransferItem extends Model
{
    protected $table='machinerytransfer_items';
    protected $fillable=['machinaries_id','machinerytransfers_id','qty','rate','net','days'];

    public function machinerytransfer(){
        return $this->belongsTo('App\MachineryTransfer','machinerytransfers_id');
    }

    public function machinery(){
        return $this->belongsTo('App\Machinary','machinaries_id');
    }
}
