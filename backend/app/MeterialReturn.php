<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeterialReturn extends Model
{
    protected $table='materialreturns';
    protected $fillable=['projects_id','date','description','amount'];

    public function return_item(){
        return $this->hasMany('App\MeterialReturn_Item');
    }

    public function project(){
        return $this->belongsTo('App\Project','projects_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($return) {

            $ledger=\App\Ledger::where('projects_id',$return->projects_id)->get()->first();

            if($ledger!=null) {
                $journal = new \App\Journal();
                $journal->drledgers_id = 0;
                $journal->crledgers_id = $ledger->id;
                $journal->dramount = $return->amount;
                $journal->cramount = $return->amount;
                $journal->note = $return->description;
                $journal->vouchertype = "Material Return";
                $journal->date = $return->date;
                $journal->materialreturn_id = $return->id;
                $journal->save();
            }

        });

        static::updated(function ($return) {
            \App\Journal::where('materialreturn_id','=',$return->id)->delete();

            $ledger=\App\Ledger::where('projects_id',$return->projects_id)->get()->first();

            if($ledger!=null) {
                $journal = new \App\Journal();
                $journal->drledgers_id = 0;
                $journal->crledgers_id = $ledger->id;
                $journal->dramount = $return->amount;
                $journal->cramount = $return->amount;
                $journal->note = $return->description;
                $journal->vouchertype = "Material Return";
                $journal->date = $return->date;
                $journal->materialreturn_id = $return->id;
                $journal->save();
            }
        });

        static::deleted(function ($return) {
            \App\Journal::where('materialreturn_id','=',$return->id)->delete();
        });
    }
}
