<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeterialReturn_Item extends Model
{
    //
    protected $table='materialreturns_items';
    protected $fillable=['products_id','materialreturns_id','qty','rate','units_id','net'];

    public function returns(){
        return $this->belongsTo('App\MeterialReturn','materialreturns_id');
    }

    public function unit(){
        return $this->belongsTo('App\Unit','units_id');
    }

    public function product(){
        return $this->belongsTo('App\Product','products_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($return) {
            $stock=new \App\StockPosting();
            $pur=\App\MeterialReturn::find($return->materialreturns_id);
            $stock->date=$pur->date;
            $stock->voucherType='Return';
            $stock->voucherId=$return->materialreturns_id;
            $stock->product_id=$return->products_id;
            $stock->inwardsQty=$return->qty;
            $stock->outwadsQty=0;
            $stock->rate=$return->rate;
            $stock->unit_id=$return->units_id;
            $stock->save();
        });

        static::updated(function ($return) {

        });

        static::deleted(function ($return) {
            \App\StockPosting::where('voucherId','=',$return->materialreturns_id)->where('voucherType','=','Return')->where('products_id','=',$return->products_id)->delete();
        });
    }
}
