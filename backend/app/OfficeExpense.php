<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeExpense extends Model
{
    protected $table = 'officeEexpenses';

    protected $fillable = ['date', 'amount', 'description'];
}
