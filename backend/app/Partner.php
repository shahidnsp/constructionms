<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Partner extends Model
{
    protected  $fillable = ['firstname', 'lastname','address', 'phone','mobile1','mobile2', 'email', 'amount','share','project_id','regDate'];

    public function projects()
    {
        return $this->belongsTo('App\Project','project_id');
    }

    public function attendances()
    {
        return $this->hasMany('App\Attendance');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
