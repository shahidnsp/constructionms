<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Personaldetail extends Model {

	//
    protected $fillable =['address', 'post', 'district', 'state', 'pin', 'phone', 'mobile1', 'mobile2', 'email1', 'email2'];

    //to refer hasMany relationship to File
    public function file()
    {
        return $this->belongsTo('App\File');
    }


}
