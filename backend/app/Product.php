<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','sales_price','purchase_price','qty','unit_id','supplier_id','stockvalue'];

    public function units()
    {
        return $this->belongsTo('App\Unit','unit_id');
    }

    public function suppliers()
    {
        return $this->belongsTo('App\Ledger','supplier_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($product) {
            $stock=new \App\StockPosting();
            $stock->date=Carbon::now();
            $stock->voucherType='Opening Stock';
            $stock->voucherId=$product->id;
            $stock->product_id=$product->id;
            $stock->inwardsQty=$product->qty;
            $stock->outwadsQty=0;
            $stock->rate=$product->purchase_price;
            $stock->unit_id=$product->unit_id;
            $stock->save();
        });

        static::updated(function ($product) {

        });

        static::deleted(function ($product) {
            \App\StockPosting::where('voucherId','=',$product->id)->where('voucherType','=','Opening Stock')->where('products_id','=',$product->id)->delete();
        });
    }
}
