<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

	//
    protected $fillable = [ 'name', 'description', 'approxamount', 'startdate', 'enddate','contactperson','address','phone','mobile','active'];

    public function wages()
    {
        return $this->hasMany('App\Wage','projects_id');
    }

    public  function expenses()
    {
        return $this->hasMany('App\Expense','projects_id');
    }


    public  function partner()
    {
        return $this->hasMany('App\Partner');
    }

    public function transfer(){
        return $this->hasMany('App\Transfer');
    }


    public static function boot()
    {
        parent::boot();

        static::created(function ($project) {
            \App\Ledger::create([
                'name'=>$project->name,
                'unders_id'=>'27',
                'openingBalance'=>'0.00',
                'crOrDr'=>'Dr',
                'isedit'=>1,
                'projects_id'=>$project->id
            ]);
        });

        static::updated(function ($project) {
            $ledger=\App\Ledger::where('projects_id','=',$project->id)->get()->first();
            $ledger->name=$project->name;
            $ledger->save();
        });

        static::deleted(function ($project) {
            \App\Ledger::where('projects_id','=',$project->id)->delete();
        });
    }
}
