<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectExpense extends Model
{
    //
    protected  $table = 'projectexpense';
    public function expense()
    {
        return $this->belongsTo('App\Expense');
    }

    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
