<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

use App\User;



class AppServiceProvider extends ServiceProvider
{



    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Validator::extend('alpha_text',function($attribute,$value){
            //Alpha dash with space,dot and newline
            return preg_match('/^[\n\. \pL\pM\pN_-]+$/u', $value);
        });

        /**
         * Add each every events for Model observers
         */
//
//        User::saved(function($user){
//            dd($user);
//        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
