<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable=['date','description','total','suppliers_id','balance','paid'];

    public function purchase_item(){
        return $this->hasMany('App\Purchase_Items');
    }

    public function supplier(){
        return $this->belongsTo('App\Ledger','suppliers_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($purchase) {

            $supplier=\App\Ledger::find($purchase->suppliers_id);

            if($purchase->paid >= $purchase->total) {
                $journal = new \App\Journal();
                $journal->drledgers_id = 11;
                $journal->crledgers_id = 1;
                $journal->dramount = $purchase->paid;
                $journal->cramount = $purchase->paid;
                $journal->note = "Cash Purchase from ".$supplier->name;
                $journal->vouchertype = "Purchase";
                $journal->date = $purchase->date;
                $journal->purchases_id = $purchase->id;
                $journal->save();
            }else if($purchase->paid < $purchase->total){
                //To get Supplier's Ledger Account

                    if ($purchase->paid == '0' || $purchase->paid == '0.00') {
                        $ledger=\App\Ledger::find($purchase->suppliers_id);
                        if($ledger != null) {
                            $journal = new \App\Journal();
                            $journal->drledgers_id = 11;
                            $journal->crledgers_id = $ledger->id;
                            $journal->dramount = $purchase->balance;
                            $journal->cramount = $purchase->balance;
                            $journal->note = "Credit Purchase from " . $supplier->name;
                            $journal->vouchertype = "Purchase";
                            $journal->date = $purchase->date;
                            $journal->purchases_id = $purchase->id;
                            $journal->save();
                        }

                    } else if ($purchase->paid != 0 && $purchase->balance != 0) {

                        $ledger1=\App\Ledger::find($purchase->suppliers_id);
                        if($ledger1 != null) {
                            //To store credit amount
                            $journal = new \App\Journal();
                            $journal->drledgers_id = 11;
                            $journal->crledgers_id = $ledger1->id;
                            $journal->dramount = $purchase->balance;
                            $journal->cramount = $purchase->balance;
                            $journal->note = "Credit Purchase from " . $supplier->name;
                            $journal->vouchertype = "Purchase";
                            $journal->date = $purchase->date;
                            $journal->purchases_id = $purchase->id;
                            $journal->save();
                        }

                        //To store paid amount
                        $journal = new \App\Journal();
                        $journal->drledgers_id = 11;
                        $journal->crledgers_id = 1;
                        $journal->dramount = $purchase->paid;
                        $journal->cramount = $purchase->paid;
                        $journal->note = "Cash Purchase from " . $supplier->name;
                        $journal->vouchertype = "Purchase";
                        $journal->date = $purchase->date;
                        $journal->purchases_id = $purchase->id;
                        $journal->save();
                    }
                }

        });

        static::updated(function ($purchase) {

            \App\Journal::where('purchases_id','=',$purchase->id)->delete();


            $supplier=\App\Ledger::find($purchase->suppliers_id);

            if($purchase->paid >= $purchase->total) {
                $journal = new \App\Journal();
                $journal->drledgers_id = 11;
                $journal->crledgers_id = 1;
                $journal->dramount = $purchase->paid;
                $journal->cramount = $purchase->paid;
                $journal->note = "Cash Purchase from ".$supplier->name;
                $journal->vouchertype = "Purchase";
                $journal->date = $purchase->date;
                $journal->purchases_id = $purchase->id;
                $journal->save();
            }else if($purchase->paid < $purchase->total){
                //To get Supplier's Ledger Account

                if ($purchase->paid == '0' || $purchase->paid == '0.00') {
                    $ledger=\App\Ledger::find($purchase->suppliers_id);
                    if($ledger != null) {
                        $journal = new \App\Journal();
                        $journal->drledgers_id = 11;
                        $journal->crledgers_id = $ledger->id;
                        $journal->dramount = $purchase->balance;
                        $journal->cramount = $purchase->balance;
                        $journal->note = "Credit Purchase from " . $supplier->name;
                        $journal->vouchertype = "Purchase";
                        $journal->date = $purchase->date;
                        $journal->purchases_id = $purchase->id;
                        $journal->save();
                    }

                } else if ($purchase->paid != 0 && $purchase->balance != 0) {

                    $ledger1=\App\Ledger::find($purchase->suppliers_id);
                    if($ledger1 != null) {
                        //To store credit amount
                        $journal = new \App\Journal();
                        $journal->drledgers_id = 11;
                        $journal->crledgers_id = $ledger1->id;
                        $journal->dramount = $purchase->balance;
                        $journal->cramount = $purchase->balance;
                        $journal->note = "Credit Purchase from " . $supplier->name;
                        $journal->vouchertype = "Purchase";
                        $journal->date = $purchase->date;
                        $journal->purchases_id = $purchase->id;
                        $journal->save();
                    }

                    //To store paid amount
                    $journal = new \App\Journal();
                    $journal->drledgers_id = 11;
                    $journal->crledgers_id = 1;
                    $journal->dramount = $purchase->paid;
                    $journal->cramount = $purchase->paid;
                    $journal->note = "Cash Purchase from " . $supplier->name;
                    $journal->vouchertype = "Purchase";
                    $journal->date = $purchase->date;
                    $journal->purchases_id = $purchase->id;
                    $journal->save();
                }
            }
        });

        static::deleted(function ($purchase) {
            \App\Journal::where('purchases_id','=',$purchase->id)->delete();
        });
    }
}
