<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase_Items extends Model
{
    protected $table='purchase_items';
    protected $fillable=['products_id','purchases_id','qty','rate','units_id','net'];

    public function purchase(){
        return $this->belongsTo('App\Purchase','purchases_id');
    }

    public function unit(){
        return $this->belongsTo('App\Unit','units_id');
    }

    public function product(){
        return $this->belongsTo('App\Product','products_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($purchase) {
            $stock=new \App\StockPosting();
            $pur=\App\Purchase::find($purchase->purchases_id);
            $stock->date=$pur->date;
            $stock->voucherType='Purchase';
            $stock->voucherId=$purchase->purchases_id;
            $stock->product_id=$purchase->products_id;
            $stock->inwardsQty=$purchase->qty;
            $stock->outwadsQty=0;
            $stock->rate=$purchase->rate;
            $stock->unit_id=$purchase->units_id;
            $stock->save();
        });

        static::updated(function ($purchase) {

        });

        static::deleted(function ($purchase) {
            \App\StockPosting::where('voucherId','=',$purchase->purchases_id)->where('voucherType','=','Purchase')->where('products_id','=',$purchase->products_id)->delete();
        });
    }
}
