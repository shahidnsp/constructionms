<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model {

    protected $fillable = ['name','description','amount','payDate'];

}
