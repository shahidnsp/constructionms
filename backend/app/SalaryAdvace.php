<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryAdvace extends Model
{
    protected $fillable = ['employees_id', 'date', 'amount', 'description'];
    protected $table = 'salaryAdvances';

    public function employee()
    {
        return $this->belongsTo('App\Employee','employees_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($advance) {

                $emp=Employee::find($advance->employees_id);

                    \App\Journal::create([
                        'drledgers_id' => '3',
                        'crledgers_id' => '1',
                        'dramount' => $advance->amount,
                        'cramount' => $advance->amount,
                        'note' => 'Salary Advance Paid to ' . $emp->name,
                        'vouchertype' => 'Salary Advance',
                        'date' => $advance->date,
                        'advance_id'=>$advance->id
                    ]);

        });

        static::updated(function ($advance) {
            \App\Journal::where('advance_id','=',$advance->id)->delete();

            $emp=Employee::find($advance->employees_id);

            \App\Journal::create([
                'drledgers_id' => '3',
                'crledgers_id' => '1',
                'dramount' => $advance->amount,
                'cramount' => $advance->amount,
                'note' => 'Salary Advance Paid to ' . $emp->name,
                'vouchertype' => 'Salary Advance',
                'date' => $advance->date,
                'advance_id'=>$advance->id
            ]);

        });

        static::deleted(function ($advance) {
            \App\Journal::where('advance_id','=',$advance->id)->delete();
        });
    }
}
