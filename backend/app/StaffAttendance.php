<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffAttendance extends Model
{
    protected $fillable = ['date', 'remark','note','employees_id','present'];

    public function staffattendancelist()
    {
        return $this->hasMany('App\StaffAttendanceDetail','staff_attendances_id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee', 'employees_id');
    }
}
