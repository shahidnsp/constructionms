<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffAttendanceDetail extends Model
{
    protected $fillable = ['staff_attendances_id', 'employees_id','status'];

    public function staffattendance()
    {
        return $this->belongsTo('App\StaffAttendance','staff_attendances_id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee','employees_id');
    }
}
