<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $fillable=['projects_id','from','date','description','amount'];

    public function purchase_item(){
        return $this->hasMany('App\Transfer_Item');
    }

    public function project(){
        return $this->belongsTo('App\Project','projects_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($transfer) {

            $ledger=\App\Ledger::where('projects_id',$transfer->projects_id)->get()->first();

            if($ledger!=null) {
                $journal = new \App\Journal();
                $journal->drledgers_id = $ledger->id;
                $journal->crledgers_id = 10;
                $journal->dramount = $transfer->amount;
                $journal->cramount = $transfer->amount;
                $journal->note = $transfer->description;
                $journal->vouchertype = "Material Transfer";
                $journal->date = $transfer->date;
                $journal->transfer_id = $transfer->id;
                $journal->save();
            }

        });

        static::updated(function ($transfer) {


            \App\Journal::where('transfer_id','=',$transfer->id)->delete();

            $ledger=\App\Ledger::where('projects_id',$transfer->projects_id)->get()->first();
            if($ledger!=null) {
                $journal = new \App\Journal();
                $journal->drledgers_id = $ledger->id;
                $journal->crledgers_id = 10;
                $journal->dramount = $transfer->amount;
                $journal->cramount = $transfer->amount;
                $journal->note = $transfer->description;
                $journal->vouchertype = "Material Transfer";
                $journal->date = $transfer->date;
                $journal->transfer_id = $transfer->id;
                $journal->save();
            }
        });

        static::deleted(function ($transfer) {
            \App\Journal::where('transfer_id','=',$transfer->id)->delete();
        });
    }
}
