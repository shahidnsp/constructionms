<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer_Item extends Model
{
    protected $table='transfer_items';
    protected $fillable=['products_id','transfers_id','qty','rate','units_id','net'];

    public function transfer(){
        return $this->belongsTo('App\Transfer','transfers_id');
    }

    public function unit(){
        return $this->belongsTo('App\Unit','units_id');
    }

    public function product(){
        return $this->belongsTo('App\Product','products_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($transfer) {
            $stock=new \App\StockPosting();
            $pur=\App\Transfer::find($transfer->transfers_id);
            $stock->date=$pur->date;
            $stock->voucherType='Transfer';
            $stock->voucherId=$transfer->transfers_id;
            $stock->product_id=$transfer->products_id;
            $stock->inwardsQty=0;
            $stock->outwadsQty=$transfer->qty;
            $stock->rate=$transfer->rate;
            $stock->unit_id=$transfer->units_id;
            $stock->save();
        });

        static::updated(function ($transfer) {

        });

        static::deleted(function ($transfer) {
            \App\StockPosting::where('voucherId','=',$transfer->transfers_id)->where('voucherType','=','Transfer')->where('products_id','=',$transfer->products_id)->delete();
        });
    }
}
