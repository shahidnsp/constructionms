<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Under extends Model
{
    protected $fillable = ['name','groupUnder','affectGrossProfit','narration','nature','isedit'];

    public function ledgers()
    {
        return $this->hasMany('App\Ledger','unders_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Under','groupUnder');
    }
}
