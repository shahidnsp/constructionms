<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Wage extends Model {

    protected $fillable = ['name','description','amount','advance','grandtotal','paid','balance','payDate','employees_id','projects_id'];

    public function employee()
    {
        return $this->belongsTo('App\Employee','employees_id');
    }

    public function projects()
    {
        return $this->belongsTo('App\Project','projects_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($wage) {

            $emp=Employee::find($wage->employees_id);
            if($emp!=null) {
                if($wage->paid!=0 || $wage->paid > 0) {
                    $journal = new \App\Journal();
                    $journal->drledgers_id = 4;
                    $journal->crledgers_id = 1;//$wage->ledgers_id;
                    $journal->dramount = $wage->paid;
                    $journal->cramount = $wage->paid;
                    $journal->note = "Wage Paid to " . $emp->name;
                    $journal->vouchertype = "Wage";
                    $journal->date = $wage->payDate;
                    $journal->wages_id = $wage->id;
                    $journal->save();
                }
            }

            $advance= \App\WageAdvance::where('employees_id',$wage->employees_id)->get()->last();
            if($advance!=null) {
                $advance->balance = $advance->balance - $wage->advance;
                $advance->save();
            }

        });

        static::updating(function ($wage) {
            $advance= \App\WageAdvance::where('employees_id',$wage->employees_id)->get()->last();
            $advance->balance=$advance->balance+$wage->advance;
            $advance->update();
        });

        static::updated(function ($wage) {

            \App\Journal::where('wages_id','=',$wage->id)->delete();

            $emp=Employee::find($wage->employees_id);
            if($emp!=null) {
                if($wage->paid!=0 || $wage->paid > 0) {
                    $journal = new \App\Journal();
                    $journal->drledgers_id = 4;
                    $journal->crledgers_id = 1;//$wage->ledgers_id;
                    $journal->dramount = $wage->paid;
                    $journal->cramount = $wage->paid;
                    $journal->note = "Wage Paid to " . $emp->name;
                    $journal->vouchertype = "Wage";
                    $journal->date = $wage->payDate;
                    $journal->wages_id = $wage->id;
                    $journal->save();
                }
            }

            $advance= \App\WageAdvance::where('employees_id',$wage->employees_id)->get()->last();
            if($advance!=null) {
                $advance->balance = $advance->balance - $wage->advance;
                $advance->update();
            }
        });

        static::deleted(function ($wage) {
            \App\Journal::where('wages_id','=',$wage->id)->delete();
        });
    }
}
