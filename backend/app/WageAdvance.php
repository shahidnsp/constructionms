<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WageAdvance extends Model
{
    protected $fillable = ['employees_id','projects_id', 'date', 'amount', 'description'];

    public function employee()
    {
        return $this->belongsTo('App\Employee','employees_id');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($advance) {

            $emp=Employee::find($advance->employees_id);

            \App\Journal::create([
                'drledgers_id' => '3',
                'crledgers_id' => '1',
                'dramount' => $advance->amount,
                'cramount' => $advance->amount,
                'note' => 'Wage Advance Paid to ' . $emp->name,
                'vouchertype' => 'Wage Advance',
                'date' => $advance->date,
                'wageadvance_id'=>$advance->id
            ]);

        });

        static::updated(function ($advance) {
            \App\Journal::where('wageadvance_id','=',$advance->id)->delete();

            $emp=Employee::find($advance->employees_id);

            \App\Journal::create([
                'drledgers_id' => '3',
                'crledgers_id' => '1',
                'dramount' => $advance->amount,
                'cramount' => $advance->amount,
                'note' => 'Wage Advance Paid to ' . $emp->name,
                'vouchertype' => 'Wage Advance',
                'date' => $advance->date,
                'wageadvance_id'=>$advance->id
            ]);

        });

        static::deleted(function ($advance) {
            \App\Journal::where('wageadvance_id','=',$advance->id)->delete();
        });
    }
}
