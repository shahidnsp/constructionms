<?php

return [

    /*
     |--------------------------------------------------------------------------
     | Application pages
     |--------------------------------------------------------------------------
     |
     | All application pages must listed here for user information and
     | permission management
     |
     */

    'pages' =>[
        [
            'title'=>'Dashboard',
            'name'=>'dashboard',
            'icon' => "dashboard",
            'group'=>'General'
        ],
        [
            'title'=>'Bank',
            'name'=>'banks',
            'icon' => "bank",
            'group'=>'Bank'
        ],
        [
            'title'=>'Loans',
            'name'=>'loan',
            'icon' => "tags",
            'group'=>'Bank'
        ],
        [
            'title'=>'Units',
            'name'=>'unit',
            'icon'=>'beer',
            'group'=>'Materials'
        ],
        [
            'title'=>'Supplier',
            'name'=>'supplier',
            'icon'=>'bullhorn',
            'group'=>'Materials'
        ],
        [
            'title'=>'Materials',
            'name'=>'product',
            'icon'=>'database',
            'group'=>'Materials'
        ],
        /*[
            'title'=>'Stock Update',
            'name'=>'stockupdate',
            'icon'=>'cube',
            'group'=>'Materials'
        ],*/
        [
            'title'=>'Material Purchase',
            'name'=>'purchase',
            'icon'=>'shopping-cart',
            'group'=>'Materials'
        ],
        [
            'title'=>'Material Transfer',
            'name'=>'transfer',
            'icon'=>'eye',
            'group'=>'Materials'
        ],
        [
            'title'=>'Material Return',
            'name'=>'materialreturn',
            'icon'=>'fire',
            'group'=>'Materials'
        ],
        [
            'title'=>'Phone Book',
            'name'=>'phonebook',
            'icon' => "users",
            'group'=>'General'
        ],
        [
            'title'=>'Clients',
            'name'=>'clients',
            'icon' => "user",
            'group'=>'General'
        ],
        [
            'title'=>'Employee',
            'name'=>'employee',
            'icon' => "users",
            'group'=>'Employee'
        ],
        [
            'title'=>'Salary',
            'name'=>'salary',
            'icon' => "money",
            'group'=>'Employee'
        ],
        [
            'title'=>'Salary Advance',
            'name'=>'salaryadvance',
            'icon' => "money",
            'group'=>'Employee'
        ],
        [
            'title'=>'Staff Attendance',
            'name'=>'staffattendance',
            'icon' => "book",
            'group'=>'Employee'
        ],
        [
            'title'=>'Project',
            'name'=>'project',
            'icon' => "briefcase",
            'group'=>'Project'
        ],
        [
            'title'=>'Project Documents',
            'name'=>'projectdocument',
            'icon' => "print",
            'group'=>'Project'
        ],
        [
            'title'=>'Partners',
            'name'=>'partner',
            'icon' => "users",
            'group'=>'Project'
        ],
        [
            'title'=>'Attendance',
            'name'=>'attendance',
            'icon'=>'book',
            'group'=>'Project'
        ],
        [
            'title'=>'Attendance List',
            'name'=>'attendancelist',
            'icon'=>'info',
            'group'=>'Project'
        ],
        [
            'title'=>'Worker\'s Wage',
            'name'=>'employeewage',
            'icon'=>'money',
            'group'=>'Project'
        ],
        [
            'title'=>'Worker\'s Wage Advance',
            'name'=>'wageadvance',
            'icon'=>'money',
            'group'=>'Project'
        ],
        [
            'title'=>'Income',
            'name'=>'income',
            'icon'=>'puzzle-piece',
            'group'=>'Cash'
        ],
        [
            'title'=>'Expense',
            'name'=>'expense',
            'icon'=>'credit-card',
            'group'=>'Cash'
        ],
        [
            'title'=>'Rent',
            'name'=>'rent',
            'icon'=>'building-o',
            'group'=>'Cash'
        ],
        [
            'title'=>'Profile',
            'name'=>'profile',
            'icon' => "user",
            'group'=>'Settings'
        ],
        [
            'title'=>'Settings',
            'name'=>'settings',
            'icon' => "cogs",
            'group'=>'Settings'
        ],
        [
            'title'=>'Account Group',
            'name'=>'under',
            'icon' => "dashboard",
            'group'=>'Account'
        ],
        [
            'title' => 'Ledger Account',
            'name' => 'ledger',
            'icon' => 'credit-card',
            'group'=>'Account'
        ],
        [
            'title' => 'Ledger Account Balance',
            'name' => 'accountbalance',
            'icon' => 'credit-card',
            'group'=>'Account'
        ],
        [
            'title'=>'Sundry Debtors',
            'name'=>'sundrydebtor',
            'icon' => "money",
            'group'=>'Account'
        ],
        [
            'title'=>'Sundry Creditors',
            'name'=>'sundrycreditor',
            'icon' => "money",
            'group'=>'Account'
        ],
        [
            'title' => 'Voucher',
            'name' => 'journal',
            'icon' => 'credit-card',
            'group'=>'Account'
        ],
        [
            'title' => 'Journal Report',
            'name' => 'journalreport',
            'icon' => 'credit-card',
            'group'=>'AccountReport'
        ],
        [
            'title' => 'Day Book',
            'name' => 'daybook',
            'icon' => 'credit-card',
            'group'=>'AccountReport'
        ],
        /*[
            'title' => 'Trial Balance',
            'name' => 'trialbalance',
            'icon' => 'credit-card',
            'group'=>'AccountReport'
        ],
        [
            'title' => 'Trading Account',
            'name' => 'tradingaccount',
            'icon' => 'credit-card',
            'group'=>'AccountReport'
        ],
        [
            'title' => 'P/L Account',
            'name' => 'profitandloss',
            'icon' => 'credit-card',
            'group'=>'AccountReport'
        ],
        [
            'title' => 'Balance Sheet',
            'name' => 'balancesheet',
            'icon' => 'credit-card',
            'group'=>'AccountReport'
        ],*/
        [
            'title' => 'Account Statement',
            'name' => 'accountstatement',
            'icon' => 'credit-card',
            'group'=>'AccountReport'
        ],
        [
            'title' => 'Machinery',
            'name' => 'machinery',
            'icon' => 'legal',
            'group'=>'Machinery'
        ],
        [
            'title' => 'Machinery Transfer',
            'name' => 'machinerytransfer',
            'icon' => 'motorcycle',
            'group'=>'Machinery'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG'),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => 'http://localhost',

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    // 'key' => env('APP_KEY', 'SomeRandomString'),
    'key' => env('APP_KEY', 'aL3s6hAk375ogGSJQVKVB1r3Jf6OHZ5j'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => 'single',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Foundation\Providers\ArtisanServiceProvider::class,
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Routing\ControllerServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        Illuminate\Html\HtmlServiceProvider::class,
        Intervention\Image\ImageServiceProvider::class,
        Maatwebsite\Excel\ExcelServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App'       => Illuminate\Support\Facades\App::class,
        'Artisan'   => Illuminate\Support\Facades\Artisan::class,
        'Auth'      => Illuminate\Support\Facades\Auth::class,
        'Blade'     => Illuminate\Support\Facades\Blade::class,
        'Bus'       => Illuminate\Support\Facades\Bus::class,
        'Cache'     => Illuminate\Support\Facades\Cache::class,
        'Config'    => Illuminate\Support\Facades\Config::class,
        'Cookie'    => Illuminate\Support\Facades\Cookie::class,
        'Crypt'     => Illuminate\Support\Facades\Crypt::class,
        'DB'        => Illuminate\Support\Facades\DB::class,
        'Eloquent'  => Illuminate\Database\Eloquent\Model::class,
        'Event'     => Illuminate\Support\Facades\Event::class,
        'File'      => Illuminate\Support\Facades\File::class,
        'Hash'      => Illuminate\Support\Facades\Hash::class,
        'Input'     => Illuminate\Support\Facades\Input::class,
        'Inspiring' => Illuminate\Foundation\Inspiring::class,
        'Lang'      => Illuminate\Support\Facades\Lang::class,
        'Log'       => Illuminate\Support\Facades\Log::class,
        'Mail'      => Illuminate\Support\Facades\Mail::class,
        'Password'  => Illuminate\Support\Facades\Password::class,
        'Queue'     => Illuminate\Support\Facades\Queue::class,
        'Redirect'  => Illuminate\Support\Facades\Redirect::class,
        'Redis'     => Illuminate\Support\Facades\Redis::class,
        'Request'   => Illuminate\Support\Facades\Request::class,
        'Response'  => Illuminate\Support\Facades\Response::class,
        'Route'     => Illuminate\Support\Facades\Route::class,
        'Schema'    => Illuminate\Support\Facades\Schema::class,
        'Session'   => Illuminate\Support\Facades\Session::class,
        'Storage'   => Illuminate\Support\Facades\Storage::class,
        'URL'       => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View'      => Illuminate\Support\Facades\View::class,
        'Form'      => Illuminate\Html\FormFacade::class,
        'HTML'      => Illuminate\Html\HtmlFacade::class,
        'Image'     => Intervention\Image\Facades\Image::class,
        'Excel'     => Maatwebsite\Excel\Facades\Excel::class,

        //User defined facades
        'UserHelper' =>App\Helpers\UserHelper::class,

    ],

];
