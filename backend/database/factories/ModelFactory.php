<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

//User Seed faker
$factory->define(App\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10),
        'photo' =>'profile.jpg',
    ];
});

//Contact Seed faker
$factory->define(App\Contact::class,function($faker){
    return [
        'title'=>$faker->title,
        'name'=>$faker->name,
        'lastname'=>$faker->lastName,
        'address'=>$faker->address,
        'email1'=>$faker->email,
        'email2'=>$faker->freeEmail,
        'website'=>$faker->url,
        'user_id'=>1,
        'file_id'=>1,
        'photo' =>'profile.jpg',
    ];
});

//Partner Seed faker
$factory->define(App\Partner::class,function($faker){
    return [
        'firstname'=>$faker->name,
        'lastname'=>$faker->lastName,
        'address'=>$faker->address,
        'phone'=>$faker->phoneNumber,
        'mobile1'=>$faker->phoneNumber,
        'mobile2'=>$faker->phoneNumber,
        'email'=>$faker->email,
        'amount'=>$faker->randomNumber,
        'share'=>'20',
        'user_id'=>1,
        'project_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
    ];
});

//Phone Seed faker
$factory->define(App\Phone::class,function($faker){
    return [
        'number' => $faker->phoneNumber
    ];
});



//Personaldetail Seed faker
$factory->define(App\Personaldetail::class,function($faker){
    return [
        'name' => $faker->firstName,
        'address' => $faker->address,
        'post' => $faker->city,
        'district' => $faker->city,
        'state' => $faker->state,
        'pin' => $faker->postcode,
        'phone' => $faker->phoneNumber,
        'mobile1' => $faker->phoneNumber,
        'mobile2' => $faker->phoneNumber,
        'file_id' => 1,        
        'email1' => $faker->email,
        'email2' => $faker->freeEmail,
    ];
});

//Notes seed faker
$factory->define(App\Note::class,function($faker){
    return[
        'name'          =>$faker->name,
        'description'   =>$faker->text
    ];
});

//Reminder seed faker
$factory->define(App\Reminder::class,function($faker){
    return [
        'name'         =>$faker->name,
        'description'  => $faker->text,
        'regDate'      =>\Carbon\Carbon::now()->subDays($faker->dayOfMonth),
        'remDate'      =>\Carbon\Carbon::now()->addDays($faker->dayOfMonth),
        'done'         =>$faker->randomElement(['true','false']),
    ];
});

//Project seed faker
$factory->define(App\Project::class,function($faker){
   return [
        'name'          =>$faker->company,
        'description'   =>$faker->sentence,
        'approxamount'  =>$faker->randomNumber,
        'startdate'     =>\Carbon\Carbon::now()->subMonth($faker->month),
        'enddate'       =>\Carbon\Carbon::now()->addDays($faker->dayOfMonth),
        'contactperson' =>$faker->name,
        'address'       =>$faker->address,
        'phone'         =>$faker->phoneNumber,
        'mobile'         =>$faker->phoneNumber,
        'active'         =>$faker->randomElement([1,0]),
        'user'          =>'admin'
   ];
});


//User seed faker

$factory->define(App\User::class,function($faker){
   return [
        'username' => strtolower($faker->userName),
        'name' => $faker->name,
        'lastname' => $faker->lastName,
        'email' => $faker->email,
        'password' => 'user',
        'type' => $faker->randomElement(['m','u']),
        'lastLogin' => $faker->ipv4,
        'photo' =>'profile.jpg',
        'permission'=>'111,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000',
   ];
});

//Bank seed faker
$factory->define(App\Bank::class,function($faker){
   return [
        'name'=>$faker->company,
        'branch'=>$faker->city,
        'address'=>$faker->address,
        'ifsc'=>$faker->word,
        'branchcode'=>$faker->word,
        'micrcode'=>$faker->postcode,
        'contact'=>$faker->phoneNumber,
        'swiftcode'=>$faker->swiftBicNumber,
        'user'=>'admin'
   ];
});


//Accounts seed faker
$factory->define(App\Account::class,function($faker){
    return [
        'bank_id' =>$faker->randomElement([1,2,3,4]),
        'number' =>$faker->creditCardNumber,
        'type' =>$faker->randomElement(['sb','cr','lo','o']),
        'name' =>$faker->name,
        'iban' =>$faker->postcode,
        'issueDate' =>$faker->date,
        'balance' =>$faker->randomNumber,
        'user'=>'admin'
    ];
});

//Card seed faker
$factory->define(App\Card::class,function($faker){
    return [
        'cardtype'  =>$faker->creditCardType,
        'cardno'    =>$faker->creditCardNumber,
        'validfrom' =>$faker->date,
        'expirydate'=>$faker->creditCardExpirationDate,
        'cvv'   =>$faker->buildingNumber,
        'cardholder'    =>$faker->name,
    ];
});

//Loan seed faker
$factory->define(App\Loan::class,function($faker){
    return [
        'bank_id'     =>$faker->randomElement([1,2,3,4]),
        'account_id'  =>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'name'        =>$faker->name,
        'description' =>$faker->sentence,
        'amount'      =>$faker->randomNumber,
        'paid'        =>$faker->randomNumber,
        'balance'     =>$faker->randomNumber,
        'date'        =>$faker->date,
        'closedate'   =>$faker->date,
        'interest'    =>$faker->numberBetween(1,100),
        'user'        =>'admin'
    ];
});

//Emi seed faker
$factory->define(App\Emi::class,function($faker){
        return[
            'loan_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
            'amount'=>$faker->randomNumber,
            'description'=>$faker->sentence,
            'date'=>$faker->date,
            'user'        =>'admin'
        ];
});
//Account_particular seed faker
$factory->define(App\Account_particular::class,function($faker){
    return[
        'account_id'   =>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'payDate'        =>$faker->date,
        'description'  =>$faker->text,
        'chequeNo' =>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'withdrawal'  =>$faker->randomNumber,
        'deposit'     =>$faker->randomNumber,
        'balance'     =>$faker->randomNumber,
        'user'       =>'admin'
    ];
});

//Cheque seed faker
$factory->define(App\Checkque::class,function($faker){
         return[
             'account_id'   =>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
             'issueDate'    =>$faker->date,
             'from'         =>$faker->randomNumber,
             'to'          =>$faker->randomNumber,
         ];
});

//Cheque_record seed faker
$factory->define(App\Checkque_record::class,function($faker){
        return[
            'cheque_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
            'number'=>$faker->randomDigitNotNull,
            'payDay'=>$faker->date,
            'favour'=>$faker->name,
            'amount'=>$faker->randomNumber,
            'deposit'=>$faker->randomNumber,
            'balance'=>$faker->randomNumber,
            ];
});

//Documents seed faker
$factory->define(App\Document::class,function($faker){
       return[
           'name'   =>$faker->name,
           'description' =>$faker->word,
           'date'  =>$faker->date,
           'projects_id'    =>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
       ];
});


//Employee seed faker
$factory->define(App\Employee::class,function($faker){
          return[
              'name' => $faker->name,
              'address' => $faker->address,
              'phone' => $faker->phoneNumber,
              'mobile' => $faker->phoneNumber,
              'place' => $faker->city,
              'basic' => $faker->numberBetween($min = 1000, $max = 10000),
              'ta' => $faker->numberBetween($min = 500, $max = 1000),
              'da' => $faker->numberBetween($min = 500, $max = 1000),
              'hra' => $faker->numberBetween($min = 500, $max = 1000),
              'medical' => $faker->numberBetween($min = 500, $max = 1000),
              'total' => $faker->numberBetween($min = 500, $max = 1000),
              'joinDate' => $faker->date,
              'releavingDate' => $faker->date,
              'employeeType' => $faker->randomElement(['Office Staff', 'Non Office Staff']),
              'active' => $faker->randomElement([1, 0]),
              'photo' =>'profile.jpg',
          ];
});
//Wage seed faker
$factory->define(App\Wage::class,function($faker){
        return[
            'name'            =>$faker->name,
            'description'     =>$faker->sentence,
            'amount'          =>$faker->randomNumber,
            'payDate'         =>$faker->date,
            'employees_id'    =>$faker->randomElement([1,2,3,4,5]),
            'projects_id'     =>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
            'balance' => $faker->numberBetween($min = 50, $max = 100),
            'advance' => $faker->numberBetween($min = 500, $max = 1000),
            'grandtotal' => $faker->numberBetween($min = 5000, $max = 10000),
            'paid' => $faker->numberBetween($min = 5000, $max = 10000),
            'ledgers_id' => 1
        ];
});


//CashDebit  seed faker
$factory->define(App\CashDebit::class,function($faker){
    return[
        'amount'        =>$faker->randomNumber,
        'description'   =>$faker->randomNumber,
        'date'          =>$faker->date,
        'user'          =>'admin'
    ];
});

//CashCebit  seed faker
$factory->define(App\CashCredit::class,function($faker){
    return[
        'amount'        =>$faker->randomNumber,
        'description'   =>$faker->randomNumber,
        'date'          =>$faker->date,
        'user'          =>'admin'
    ];
});

//WageProject seed faker
$factory->define(App\WageProject::class,function($faker){
        return[
            'wage_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
            'project_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        ];
});

//Rent seed faker
$factory->define(App\Rent::class,function($faker){
    return[
        'name'          =>$faker->name,
        'description'   =>$faker->word,
        'amount'        =>$faker->randomNumber,
        'payDate'       =>$faker->date,
        'user'          =>'admin'
    ];
});
//Expense seed faker
$factory->define(App\Expense::class,function($faker){
    return[
        'description'   =>$faker->word,
        'name'          =>$faker->name,
        'amount'        =>$faker->randomNumber,
        'payDate'       =>$faker->date,
        'user'          =>'admin',
        'projects_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
    ];
});

//Projectexpense seed faker
$factory->define(App\ProjectExpense::class,function($faker){
    return[
        'expense_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'project_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
    ];
});

//Income seed faker
$factory->define(App\Income::class,function($faker){
    return[
        'name'          =>$faker->name,
        'description'   =>$faker->word,
        'amount'        =>$faker->randomNumber,
        'date'       =>$faker->date,
        'projects_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'user'      =>'admin'
    ];
});

//Bill seed faker
$factory->define(App\Bill::class,function($faker){
    return[
        'number'        =>$faker->randomNumber,
        'name'          =>$faker->name,
        'description'   =>$faker->word,
        'payDate'       =>$faker->date,
        'amount'        =>$faker->randomNumber,
        'balance'       =>$faker->randomNumber,
        'paid'          =>$faker->randomNumber,
        'file_id'       =>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),

    ];
});

//Insurance seed faker
$factory->define(App\Insurance::class,function($faker){
    return[
        'name'              =>$faker->name,
        'insuranceNumber'   =>$faker->randomNumber,
        'term'              =>$faker->numberBetween(1,100),
        'description'       =>$faker->word,
        'amount'            =>$faker->randomNumber,
        'nominee'           =>$faker->name,
        'paidAmount'        =>0,
        'premiumAmount'     =>$faker->randomNumber,
        'takenDate'         =>$faker->date,
        'contact_id'        =>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'file_id'           =>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),

    ];
});

//Insurance_premium seed faker
$factory->define(App\Insurance_premium::class,function($faker){
    return[
        'insurance_id'  =>$faker->randomElement([1,2,3]),
        'amount'        =>$faker->randomNumber,
        'payDate'       =>$faker->date,

    ];
});

//Files seed faker
$factory->define(App\File::class,function($faker){
    return[
        'name'=>$faker->userName,
        'type'=>$faker->mimeType,
        'extension'=>$faker->fileExtension,
        'size'=>$faker->randomNumber,
        'user_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'documents_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
    ];
});

//Unit seed faker
$factory->define(App\Unit::class,function($faker){
    return[
        'name'=>$faker->randomElement(['Kg','PCS','Grams','Sqr.Ft']),
        'isunit'=>$faker->randomElement(['true','false']),
        'rate'=>'0',
    ];
});

//Product seed faker
$factory->define(App\Product::class,function($faker){
    return[
        'name'=>$faker->name,
        'description'=>$faker->sentence,
        'sales_price'=>$faker->randomNumber,
        'purchase_price'=>$faker->randomNumber,
        'qty'=>$faker->randomNumber,
        'unit_id'=>$faker->randomElement([1,2,3,4]),
        'supplier_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});

//Attendance seed faker
$factory->define(App\AttendanceGroup::class,function($faker){
    return[
        'date'=>$faker->date,
        'leaders_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'projects_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'month'=>$faker->randomElement(['May 2016','May 2010','May 2009','May 2004','May 2000','Jan 2006','Oct 2013','May 2012','Aug 2011','Jun 2016']),
    ];
});

//Attendance seed faker
$factory->define(App\Attendance::class,function($faker){
    return[
        'employees_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'attendance_groups_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'status'=>$faker->randomElement(['A','P']),
        'wage'=>'100',
        'hour'=>'8',
        'total'=>'800',
    ];
});



//Leader seed faker
$factory->define(App\Leader::class,function($faker){
    return[
        'project_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'employee_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
    ];
});


//ledger seed faker

//$factory->define(App\Ledger::class, function ($faker) {
//    return[
//        'name' => $faker->name,
//        'unders_id' => $faker->randomNumber([1,2,3,4,5,6,7,8,9,10]),
//        'type' => $faker->randomElement(['Expense', 'Income','Capital']),
//        'isedit'=>1,
//        'date'=>$faker->date,
//        'paymentmode'=>$faker->randomElement(['Debit', 'Credit']),
//    ];
//});

//Group seed faker
$factory->define(App\Group::class,function($faker){
    return[
        'leader_id'=>$faker->randomElement([1,2,3,4,5,6,7,8]),
        'employee_id'=>$faker->randomElement([1,2,3,4,5]),
    ];
});


//under seeder faker
$factory->define(\App\Under::class, function ($faker) {
    return [
        'name' => $faker->name,
    ];
});

//journal seeder faker

$factory->define(\App\Journal::class, function ($faker) {
    return [
        'drledgers_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'crledgers_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'dramount' => $faker->numberBetween($min = 1000, $max = 9000),
        'cramount' => $faker->numberBetween($min = 500, $max = 5000),
        'note' => $faker->sentence,
        'date' => $faker->date,
        'vouchertype'=>$faker->randomElement(['Journal','Payment','Receipt','Sales','Purchase','Credit Note','Debit Note','Income','Expense']),
    ];
});

//officeExpense seeder faker

$factory->define(\App\OfficeExpense::class, function ($faker) {
    return [
        'date' => $faker->date,
        'amount' => $faker->numberBetween($min = 100, $max = 5000),
        'description' => $faker->sentence,
    ];
});

//LoanExpense seeder faker

$factory->define(App\LoanExpense::class, function ($faker) {
    return [
        'loan_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'name' => $faker->name,
        'description' => $faker->sentence,
        'date' => $faker->date,
        'amount' => $faker->numberBetween($min = 500, $max = 5000)
    ];
});

//Purchase Seeder faker
$factory->define(App\Purchase::class, function ($faker) {
    return [
        'date' => $faker->date,
        'description' => $faker->sentence,
        'total'     =>$faker->numberBetween($min = 100, $max = 5000),
        'paid'     =>$faker->numberBetween($min = 100, $max = 5000),
        'balance'     =>$faker->numberBetween($min = 100, $max = 500),
        'suppliers_id'     =>$faker->randomElement([1, 2, 3, 4, 5]),
        'mode'        =>$faker->randomElement(['Cash','Credit', 'Cash Credit']),
    ];
});

//Purchase Item Seeder faker
$factory->define(App\Purchase_Items::class, function ($faker) {
    return [
        'products_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'purchases_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'qty'           =>$faker->randomDigit,
        'rate'        =>$faker->randomNumber,
        'units_id'    =>$faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'net'         =>$faker->randomNumber
    ];
});

//Transfer Seeder faker
$factory->define(App\Transfer::class, function ($faker) {
    return [
        'projects_id'=>$faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'date' => $faker->date,
        'description' => $faker->sentence,
        'from'=>'Main Godown',
        'amount'=>$faker->randomNumber,
    ];
});

//Transfer Item Seeder faker
$factory->define(App\Transfer_Item::class, function ($faker) {
    return [
        'products_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'transfers_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'qty'           =>$faker->randomDigit,
        'rate'        =>$faker->randomNumber,
        'units_id'    =>$faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'net'         =>$faker->randomNumber
    ];
});

//Material Return Seeder faker
$factory->define(App\MeterialReturn::class, function ($faker) {
    return [
        'projects_id'=>$faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'date' => $faker->date,
        'description' => $faker->sentence,
        'amount'=>$faker->randomNumber,
    ];
});

//Material Return Item Seeder faker
$factory->define(App\MeterialReturn_Item::class, function ($faker) {
    return [
        'products_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'materialreturns_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'qty'           =>$faker->randomDigit,
        'rate'        =>$faker->randomNumber,
        'units_id'    =>$faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'net'         =>$faker->randomNumber
    ];
});

//Machinari seed faker
$factory->define(App\Machinary::class,function($faker){
    return[
        'name'=>$faker->name,
        'description'=>$faker->sentence,
        'wageperday'=>$faker->randomNumber,
        'qty'=>$faker->randomNumber,
    ];
});

//Machinery Transfer Seeder faker
$factory->define(App\MachineryTransfer::class, function ($faker) {
    return [
        'projects_id'=>$faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'date' => $faker->date,
        'description' => $faker->sentence,
        'status'      =>1,
        'from'        =>'Godown',
        'amount'=>$faker->randomNumber,
    ];
});

//Machinery Transfer  Item Seeder faker
$factory->define(App\MachineryTransferItem::class, function ($faker) {
    return [
        'machinaries_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'machinerytransfers_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'qty'           =>$faker->randomDigit,
        'rate'        =>$faker->randomNumber,
        'net'         =>$faker->randomNumber,
        'days'        =>$faker->randomDigit,
    ];
});

//Employee salary faker
$factory->define(App\EmployeeSalary::class, function ($faker) {
    return [
        'date' => $faker->date,
        'employees_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'basic' => $faker->numberBetween($min = 1000, $max = 10000),
        'ta' => $faker->numberBetween($min = 500, $max = 1000),
        'da' => $faker->numberBetween($min = 500, $max = 1000),
        'hra' => $faker->numberBetween($min = 500, $max = 1000),
        'medical' => $faker->numberBetween($min = 500, $max = 1000),
        'total' => $faker->numberBetween($min = 500, $max = 1000),
        'paid' => $faker->numberBetween($min = 500, $max = 1000),
        'prebalance' => $faker->numberBetween($min = 50, $max = 100),
        'balance' => $faker->numberBetween($min = 500, $max = 1000),
        'description' => $faker->sentence,
        'ledgers_id' => 1
    ];
});

//salary advance faker
$factory->define(\App\SalaryAdvace::class, function ($faker) {
    return [
        'employees_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'date' => $faker->date,
        'amount' => $faker->numberBetween($min = 5000, $max = 10000),
        'balance' => $faker->numberBetween($min = 5000, $max = 10000),
        'description' => $faker->sentence,
    ];
});

//Wage advance faker
$factory->define(\App\WageAdvance::class, function ($faker) {
    return [
        'employees_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'date' => $faker->date,
        'amount' => $faker->numberBetween($min = 5000, $max = 10000),
        'balance' => $faker->numberBetween($min = 5000, $max = 10000),
        'description' => $faker->sentence,
    ];
});

//Staff Attendance  faker
$factory->define(\App\StaffAttendance::class, function ($faker) {
    return [
        'date' => $faker->date,
        'present' => $faker->randomElement([true, false]),
        'remark' => $faker->sentence,
        'employees_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    ];
});

//Staff Attendance Details faker
$factory->define(\App\StaffAttendanceDetail::class, function ($faker) {
    return [
        'staff_attendances_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'employees_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'status' => $faker->randomElement(['P', 'A']),
    ];
});

//clients faker
$factory->define(\App\Client::class, function ($faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'place' => $faker->city,
        'district' => $faker->city,
        'description' => $faker->sentence,
        'visited' => $faker->date,
        'firstvisiting' => $faker->date,
        'reminderdate' => $faker->date,
        'crOrDr'=>'Dr',
        'openBal'=>'0'
    ];
});

