<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaldetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('personaldetails', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
            $table->string('name',45);
            $table->string('address',250);
            $table->string('post',45);
            $table->string('district',45);
            $table->string('state',45);
            $table->string('pin',10);
            $table->string('phone',20);
            $table->string('mobile1',20);
            $table->string('mobile2',20);
            $table->integer('file_id');
            $table->char('email1',254);
            $table->char('email2',254);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('personaldetails');
	}

}
