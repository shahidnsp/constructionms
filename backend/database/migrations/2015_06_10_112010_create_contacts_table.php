<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('title',45);
            $table->string('name',45);
            $table->string('lastname',45);
            $table->string('address',250);
            $table->string('email1',254);
            $table->string('email2',254);
            $table->string('website',254);
            $table->string('photo',254);
            $table->integer('user_id');
            $table->integer('file_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacts');
	}

}
