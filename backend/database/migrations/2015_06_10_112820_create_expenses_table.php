<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
public function up()
	{
		Schema::create('expenses', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('description');
            $table->string('name');
            $table->decimal('amount',15,2);
            $table->date('payDate');
            $table->integer('projects_id');
            $table->string('user');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('expenses');
	}

}
