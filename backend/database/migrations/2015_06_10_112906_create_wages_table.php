<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wages', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->decimal('amount',15,2);
            $table->decimal('advance',15,2);
            $table->decimal('grandtotal',15,2);
            $table->decimal('paid',15,2);
            $table->decimal('balance',15,2);
            $table->date('payDate');
            $table->integer('projects_id');
            $table->integer('employees_id');
            $table->integer('ledgers_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wages');
	}

}
