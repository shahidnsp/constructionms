<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bills', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('number',50);
            $table->string('name');
            $table->string('description',500);
            $table->date('payDate');
            $table->decimal('amount',15,2);
            $table->decimal('balance',15,2);
            $table->decimal('paid',15,2);
            $table->integer('file_id'); // Bill scanned copy

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bills');
	}

}
