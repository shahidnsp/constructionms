<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsurancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('insurances', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->string('insuranceNumber',50);
            $table->decimal('term');
            $table->text('description');
            $table->decimal('amount',15,2);
            $table->string('nominee');
            $table->decimal('premiumAmount',15,2);
            $table->decimal('paidAmount',15,2);
            $table->date('takenDate');
            $table->integer('contact_id'); // Agent contact
            $table->integer('file_id'); // Insurance paper photo

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('insurances');
	}

}
