<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name',45);
            $table->string('description',500);
            $table->decimal('approxamount',15,2);
            $table->date('startdate');
            $table->date('enddate');
            $table->string('contactperson');
            $table->string('address');
            $table->string('phone');
            $table->string('mobile');
            $table->integer('active');
            $table->string('user');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
