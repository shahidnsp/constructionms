<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequeRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cheque_records', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('cheque_id');
            $table->string('number');
            $table->date('payDay');
            $table->string('favour');
            $table->decimal('amount',15,2);
            $table->decimal('deposit',15,2);
            $table->decimal('balance',15,2);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cheque_records');
	}

}
