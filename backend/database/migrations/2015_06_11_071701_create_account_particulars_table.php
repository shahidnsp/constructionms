<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountParticularsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('account_particulars', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('account_id');
            $table->date('payDate');
            $table->string('description');
            $table->string('chequeNo');
            $table->decimal('withdrawal',15,2)->nullable();
            $table->decimal('deposit',15,2)->nullable();
            $table->decimal('balance',15,2);
            $table->string('user');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('account_particulars');
	}

}
