<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banks', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name',60);
            $table->string('branch',60);
            $table->string('address',350);
            $table->string('ifsc',45);
            $table->string('branchcode',45);
            $table->string('micrcode',45);
            $table->string('contact',45);
            $table->string('swiftcode',45);
            $table->string('user',45);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banks');
	}

}
