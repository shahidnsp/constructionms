<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('loan', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('bank_id');
            $table->integer('account_id');
            $table->string('name',100);
            $table->string('description',250);
            $table->decimal('amount',15,2);
            $table->decimal('paid',15,2);
            $table->decimal('balance',15,2);
            $table->date('date');
            $table->date('closedate');
            $table->decimal('interest',4,2);
            $table->string('user');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('loan');
	}

}
