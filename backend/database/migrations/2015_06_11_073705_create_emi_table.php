<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emi', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('loan_id');
            $table->decimal('amount',15,2);
            $table->string('description',250);
            $table->date('date');
            $table->string('user');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emi');
	}

}
