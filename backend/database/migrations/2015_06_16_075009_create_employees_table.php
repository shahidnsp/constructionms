<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('mobile');
            $table->string('place');
            $table->decimal('basic',15,2);
            $table->decimal('ta', 15, 2);
            $table->decimal('da', 15, 2);
            $table->decimal('hra', 15, 2);
            $table->decimal('medical', 15, 2);
            $table->decimal('total', 15, 2);
            $table->date('joinDate');
            $table->date('releavingDate');
            $table->string('employeeType');
            $table->string('photo');
            $table->boolean('active');
            $table->string('user');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}
