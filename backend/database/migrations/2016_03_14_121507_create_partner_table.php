<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname',45);
            $table->string('lastname',45);
            $table->string('address',250);
            $table->string('phone',15);
            $table->string('mobile1',15);
            $table->string('mobile2',15);
            $table->string('email',254);
            $table->integer('project_id');
            $table->double('amount');
            $table->double('share');
            $table->date('regDate');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partners');
    }
}
