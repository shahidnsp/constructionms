<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledgers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unders_id');
            $table->string('name');
            $table->decimal('openingBalance',15,2);
            $table->string('crOrDr');
            $table->string('narration')->nullable();
            $table->string('mailingName')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->integer('creditPeriod')->nullable();
            $table->decimal('creditLimit')->nullable();
            $table->string('tin')->nullable();
            $table->string('cst')->nullable();
            $table->string('pan')->nullable();
            $table->string('bankAccountNumber')->nullable();
            $table->string('branchName')->nullable();
            $table->string('branchCode')->nullable();
            $table->integer('isedit');
            $table->integer('bankaccount_id')->nullable();
            $table->integer('projects_id')->nullable();
            $table->integer('loan_id')->nullable();
            $table->integer('clients_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ledgers');
    }
}
