<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUndersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('under');
            $table->integer('groupUnder');
            $table->integer('affectGrossProfit');
            $table->string('narration');
            $table->string('nature');
            $table->integer('isedit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('unders');
    }
}
