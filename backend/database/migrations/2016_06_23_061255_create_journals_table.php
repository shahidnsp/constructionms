<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('drledgers_id');
            $table->integer('crledgers_id');
            $table->decimal('dramount');
            $table->decimal('cramount');
            $table->string('note');
            $table->string('vouchertype');
            $table->date('date');
            $table->integer('purchases_id')->nullable();
            $table->integer('incomes_id')->nullable();
            $table->integer('expenses_id')->nullable();
            $table->integer('wages_id')->nullable();
            $table->integer('advance_id')->nullable();
            $table->integer('salary_id')->nullable();
            $table->integer('wageadvance_id')->nullable();
            $table->integer('bankpayment_id')->nullable();
            $table->integer('loanpayment_id')->nullable();
            $table->integer('loanexpense_id')->nullable();
            $table->integer('transfer_id')->nullable();
            $table->integer('materialreturn_id')->nullable();
            $table->integer('machinery_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('journals');
    }
}
