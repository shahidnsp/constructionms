<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialreturnsItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materialreturns_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('products_id');
            $table->integer('materialreturns_id');
            $table->decimal('qty');
            $table->decimal('rate');
            $table->integer('units_id');
            $table->decimal('net');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('materialreturns_items');
    }
}
