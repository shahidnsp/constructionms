<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachinerytransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machinerytransfers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('from');
            $table->integer('projects_id');
            $table->date('date');
            $table->string('description');
            $table->integer('status');
            $table->decimal('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('machinerytransfers');
    }
}
