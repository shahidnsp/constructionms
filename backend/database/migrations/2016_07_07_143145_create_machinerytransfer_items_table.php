<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachinerytransferItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machinerytransfer_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('machinaries_id');
            $table->integer('machinerytransfers_id');
            $table->decimal('qty');
            $table->decimal('rate');
            $table->decimal('net');
            $table->decimal('days');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('machinerytransfer_items');
    }
}
