<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employeeSalaries', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('employees_id');
            $table->integer('ledgers_id');
            $table->decimal('basic', 15, 2);
            $table->decimal('ta', 15, 2);
            $table->decimal('da', 15, 2);
            $table->decimal('hra', 15, 2);
            $table->decimal('medical', 15, 2);
            $table->decimal('total', 15, 2);
            $table->decimal('prebalance', 15, 2);
            $table->decimal('amount', 15, 2);
            $table->decimal('paid', 15, 2);
            $table->decimal('balance', 15, 2);
            $table->decimal('advance', 15, 2);
            $table->string('description');
            $table->string('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employeeSalaries');
    }
}
