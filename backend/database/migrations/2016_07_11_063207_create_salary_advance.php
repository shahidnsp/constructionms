<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryAdvance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaryAdvances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employees_id');
            $table->date('date');
            $table->decimal('amount', 15, 2);
            $table->decimal('balance', 15, 2);
            $table->string('description');
            $table->string('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salaryAdvances');
    }
}
