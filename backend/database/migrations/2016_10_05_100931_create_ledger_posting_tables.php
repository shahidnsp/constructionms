<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLedgerPostingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledger_postings', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('voucherType');
            $table->string('voucherNo');
            $table->integer('ledgers_id');
            $table->decimal('debit',15,2);
            $table->decimal('credit',15,2);
            $table->string('chequeNo');
            $table->date('chequeDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ledger_postings');
    }
}
