<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockpostingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_postings', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('voucherType');
            $table->integer('voucherId');
            $table->integer('product_id');
            $table->decimal('inwardsQty',15,2);
            $table->decimal('outwadsQty',15,2);
            $table->decimal('rate',15,2);
            $table->integer('unit_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stock_postings');
    }
}
