<?php

use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $account = factory(App\Account::class,10)
            ->create()
            ->each(function($a){
                 $a->card()->save(factory(App\Card::class)->make());
            });
    }
}
