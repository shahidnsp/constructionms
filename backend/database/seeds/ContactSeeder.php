<?php

use Illuminate\Database\Seeder;
use Faker\Factory;


class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $contacts = factory(App\Contact::class,10)
            ->create()
            ->each(function($c){
               $c->phone()->save(factory(App\Phone::class)->make());
            });
    }
}
