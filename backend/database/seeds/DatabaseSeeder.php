<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $info = $this->command;
		Model::unguard();


		\App\User::create([
		'email'=>'admin@admin.com',
		'username'=>'admin',
		'password'=>'admin',
		'name'=>'admin',
		'lastname'=>'administrator',
		'type'=>'a',
		'lastLogin'=>'test',
        'photo' =>'profile.jpg',
		'permission'=>'111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111,111']);
        \App\Personaldetail::create([
            'user_id'=>1,
            'name'=>'name'
        ]);

        \App\Under::create([
            'name'=>'Primary',
            'groupUnder'=>-1,
            'affectGrossProfit'=>0,
            'nature'=>'NA',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Capital Account',
            'groupUnder'=>1,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Loans(Liability)',
            'groupUnder'=>1,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Current Liabilities',
            'groupUnder'=>1,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Fixed Assets',
            'groupUnder'=>1,
            'affectGrossProfit'=>0,
            'nature'=>'Assets',
            'isedit'=>0
        ]);


        \App\Under::create([
            'name'=>'Investments',
            'groupUnder'=>1,
            'affectGrossProfit'=>0,
            'nature'=>'Assets',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Current Assets',
            'groupUnder'=>1,
            'affectGrossProfit'=>0,
            'nature'=>'Assets',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Branch/Divisions',
            'groupUnder'=>1,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Misc.Expenses(ASSET)',
            'groupUnder'=>1,
            'affectGrossProfit'=>0,
            'nature'=>'Assets',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Suspense A/C',
            'groupUnder'=>1,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Sales Account',
            'groupUnder'=>1,
            'affectGrossProfit'=>1,
            'nature'=>'Income',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Purchase Account',
            'groupUnder'=>1,
            'affectGrossProfit'=>1,
            'nature'=>'Expenses',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Direct Income',
            'groupUnder'=>1,
            'affectGrossProfit'=>1,
            'nature'=>'Income',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Direct Expenses',
            'groupUnder'=>1,
            'affectGrossProfit'=>1,
            'nature'=>'Expenses',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Indirect Income',
            'groupUnder'=>1,
            'affectGrossProfit'=>0,
            'nature'=>'Income',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Indirect Expenses',
            'groupUnder'=>1,
            'affectGrossProfit'=>0,
            'nature'=>'Expenses',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Reserves &Surplus',
            'groupUnder'=>2,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Bank OD A/C',
            'groupUnder'=>3,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Secured Loans',
            'groupUnder'=>3,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'UnSecured Loans',
            'groupUnder'=>3,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Duties & Taxes',
            'groupUnder'=>4,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Provisions',
            'groupUnder'=>4,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Sundry Creditors',
            'groupUnder'=>4,
            'affectGrossProfit'=>0,
            'nature'=>'Liabilities',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Stock-in-Hand',
            'groupUnder'=>7,
            'affectGrossProfit'=>0,
            'nature'=>'Assets',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Deposits(Assets)',
            'groupUnder'=>7,
            'affectGrossProfit'=>0,
            'nature'=>'Assets',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Loans & Advances(Asset)',
            'groupUnder'=>7,
            'affectGrossProfit'=>0,
            'nature'=>'Assets',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Sundry Debtors',
            'groupUnder'=>7,
            'affectGrossProfit'=>0,
            'nature'=>'Assets',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Cash-in Hand',
            'groupUnder'=>7,
            'affectGrossProfit'=>0,
            'nature'=>'Assets',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Bank Account',
            'groupUnder'=>7,
            'affectGrossProfit'=>0,
            'nature'=>'Assets',
            'isedit'=>0
        ]);

        \App\Under::create([
            'name'=>'Service Account',
            'groupUnder'=>7,
            'affectGrossProfit'=>1,
            'nature'=>'Income',
            'isedit'=>0
        ]);


        //Ledger Accounts

        \App\Ledger::create([
            'name'=>'Cash',
            'unders_id'=>'28',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Dr',
            'isedit'=>0,
        ]);


        \App\Ledger::create([
            'name'=>'Profit And Loss',
            'unders_id'=>'0',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Dr',
            'isedit'=>0,
        ]);


        \App\Ledger::create([
            'name'=>'Advance Payment',
            'unders_id'=>'26',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Cr',
            'isedit'=>0,
        ]);


        \App\Ledger::create([
            'name'=>'Salary',
            'unders_id'=>'16',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Dr',
            'isedit'=>0,
        ]);


        \App\Ledger::create([
            'name'=>'Service Account',
            'unders_id'=>'30',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Cr',
            'isedit'=>0,
        ]);


        \App\Ledger::create([
            'name'=>'PDC Payable',
            'unders_id'=>'4',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Cr',
            'isedit'=>0,
        ]);


        \App\Ledger::create([
            'name'=>'PDC Receivable',
            'unders_id'=>'7',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Dr',
            'isedit'=>0,
        ]);

        \App\Ledger::create([
            'name'=>'Discount Allowed',
            'unders_id'=>'16',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Dr',
            'isedit'=>0,
        ]);

        \App\Ledger::create([
            'name'=>'Discount Received',
            'unders_id'=>'15',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Dr',
            'isedit'=>0,
        ]);

        \App\Ledger::create([
            'name'=>'Sales Account',
            'unders_id'=>'11',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Cr',
            'isedit'=>0,
        ]);

        \App\Ledger::create([
            'name'=>'Purchase Account',
            'unders_id'=>'12',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Dr',
            'isedit'=>0,
        ]);

        \App\Ledger::create([
            'name'=>'Forex Gain/Loss',
            'unders_id'=>'15',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Cr',
            'isedit'=>0,
        ]);

        \App\Ledger::create([
            'name'=>'Rent Account',
            'unders_id'=>'16',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Dr',
            'isedit'=>0,
        ]);

        \App\Ledger::create([
            'name'=>'Loan Expense Account',
            'unders_id'=>'16',
            'openingBalance'=>'0.00',
            'crOrDr'=>'Dr',
            'isedit'=>0,
        ]);


        $info->comment('Admin user created');
        $info->error('Username : admin Password:admin');

        $info->info('User table seeding started...');
        $this->call('UserSeeder');

        $info->info('Reminder table seeding started...');
        $this->call('ReminderSeeder');

        $info->info('Projects table seeding started...');
        $this->call('ProjectsSeeder');

        $info->info('Notes table seeding started...');
        $this->call('NotesSeeder');

        $info->info('Contact table seeding started...');
        $this->call('ContactSeeder');

        $info->info('Bank table seeding started...');
        $this->call('BankSeeder');

        $info->info('Account table seeding started...');
        $this->call('AccountSeeder');

        $info->info('Files table seeding started...');
        $this->call('FileSeeder');

        $info->info('Partners table seeding started...');
        $this->call('PartnerSeeder');

        $info->info('Loan table seeding started...');
        $this->call('LoanSeeder');

        $info->info('Emi table seeding started...');
        $this->call('EmiSeeder');


        $info->info('Cheque table seeding started...');
        $this->call('ChequeSeeder');

        $info->info('Cheque_record table seeding started...');
        $this->call('ChequerecordSeeder');

        $info->info('Documents table seeding started...');
        $this->call('DocumentsSeeder');

        $info->info('Employee table seeding started...');
        $this->call('EmployeeSeeder');


        $info->info('CashDebit table seeding started...');
        $this->call('CashDebitSeeder');

        $info->info('CashCredit table seeding started...');
        $this->call('CashCreditSeeder');

        $info->info('WageProject table seeding started...');
        $this->call('WageProjectSeeder');



        $info->info('Projectexpense table seeding started...');
        $this->call('ProjectexpenseSeeder');


        $info->info('Bill table seeding started...');
        $this->call('BillSeeder');

        $info->info('Insurance table seeding started...');
        $this->call('InsuranceSeeder');

        $info->info('Insurance_premium table seeding started...');
        $this->call('InsurancepremiumSeeder');

        $info->info('Unit table seeding started...');
        $this->call('UnitSeeder');

        $info->info('Product table seeding started...');
        $this->call('ProductSeeder');


        $info->info('Attendance table seeding started...');
        $this->call('AttendanceSeeder');


        $info->info('Leader table seeding started...');
        $this->call('LeaderSeeder');

        $info->info('Group table seeding started...');
        $this->call('GroupSeeder');

        $info->info('journals Table seeding started...');
        $this->call('JournalSeeder');


        $info->info('Loan expense table seeding started..');
        $this->call('LoanexpenseSeeder');


        $info->info('Transfer table seeding started..');
        $this->call('TransferSeeder');

        $info->info('Transfer Items table seeding started..');
        $this->call('TransferItemSeeder');

        $info->info('Material Return table seeding started..');
        $this->call('MaterialReturnSeeder');

        $info->info('Material Return Item table seeding started..');
        $this->call('MaterialReturnItemSeeder');

        $info->info('Machinary table seeding started..');
        $this->call('MachinarySeeder');

        $info->info('Machinery Transfer table seeding started..');
        $this->call('MachineryTransferSeeder');

        $info->info('Machinery Transfer Item table seeding started..');
        $this->call('MachineryTransferItemSeeder');

        $info->info('Purchase table seeding started..');
        $this->call('PurchaseSeeder');

        $info->info('Purchase Item table seeding started..');
        $this->call('PurchaseItemSeeder');

        $info->info('Expense table seeding started...');
        $this->call('ExpenseSeeder');

        $info->info('Income table seeding started...');
        $this->call('IncomeSeeder');

        $info->info('Wage table seeding started...');
        $this->call('WageSeeder');

        $info->info('Employee Salary table seeding started...');
        $this->call('EmployeeSalariesSeeder');

        $info->info('Salary Advance table seeding started...');
        $this->call('SalaryAdvanceSeeder');

        $info->info('Wage Advance table seeding started...');
        $this->call('WageAdvanceSeeder');

        $info->info('Attendance Group table seeding started...');
        $this->call('AttendanceGroupSeeder');

        $info->info('Staff Attendance table seeding started...');
        $this->call('StaffAttendanceSeeder');

        $info->info('Staff Attendance Details table seeding started...');
        $this->call('StaffAttendanceDetailSeeder');

        $info->info('Clients table seeding started...');
        $this->call('ClientSeeder');


        $info->comment('Seeding finished');

		Model::reguard();
	}
}
