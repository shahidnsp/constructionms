<?php

use Illuminate\Database\Seeder;

class EmployeeSalariesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee_salary = factory(App\EmployeeSalary::class, 10)->create();
    }
}
