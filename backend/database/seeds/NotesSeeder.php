<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class NotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $notes = factory(App\Note::class,10)->create();
    }
}
