<?php

use Illuminate\Database\Seeder;

class OfficeExpensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $officeExpense = factory(\App\OfficeExpense::class, 10)->create();
    }
}
