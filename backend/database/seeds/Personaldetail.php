<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class Personaldetail extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $personalDetail = factory(App\Personaldetail::class)->create();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
