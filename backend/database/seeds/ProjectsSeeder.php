<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class ProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //TODO add relating table
        $projects = factory(App\Project::class,10)->create();
    }
}
