<?php

use Illuminate\Database\Seeder;

class TransferItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item=factory(App\Transfer_Item::class,50)->create();
    }
}
