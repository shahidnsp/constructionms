<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $user = factory(App\User::class,10)->create()
                ->each(function($user){
                    $user->personaldetail()->save(factory(App\Personaldetail::class)->create());
                });
    }
}
