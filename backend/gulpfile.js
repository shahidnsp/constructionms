var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |n(
 */

elixir(function(mix) {

    //mix.rubySass('font-awesome.scss', null, {container: 'test-css', verbose: true});


    mix.rubySass('app.scss', null, {container: 'application-css', verbose: true});


    //Copy entire fonts
    mix.copy('resources/assets/fonts','public/fonts');

    mix.copy('resources/assets/img','public/img');

    mix.scripts(['angular.min.js','angular-route.min.js','angular-resource.min.js','d3.min.js','angular-charts.min.js','angular-ui-notification.js'],'public/js/angularjs.min.js')
       .scripts(['jquery.min.js','bootstrap.min.js','custom.js','graph/jquery.sparkline.min.js','ui-bootstrap-tpls.min.js'],'public/js/bootstrap.min.js');
       
    mix.scripts(['angularscript.js','services/*.js','controller/*.js'],'public/js/app.js');

    mix.version(['css/app.css','js/angularjs.min.js','js/bootstrap.min.js','js/app.js','fonts/']);
});
