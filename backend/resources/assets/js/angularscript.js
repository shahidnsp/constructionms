var app = angular.
    module('myApp', [
        'ngRoute',
        'ngResource',
        'ui.bootstrap',
        'angularCharts',
        'ContactService',
        'NoteService',
        'BankService',
        'LoanService',
        'ProjectService',
        'EmployeeService',
        'ExpenseService',
        'IncomeService',
		'CashService',
		'RentService',
        'UserService',
        'UserInfoService',
        'ReminderService',
        'UnitService',
        'SupplierService',
        'ProductService',
        'PartnerService',
        'AttendanceService',
        'LeaderService',
        'GroupService',
        'UnderService',
        'LedgerService',
        'JournalService',
        'LoanExpenseService',
        'PurchaseService',
        'PurchaseitemService',
        'TransferService',
        'TransferitemService',
        'MaterialReturnService',
        'MaterialReturnItemService',
        'MachineryService',
        'MachineryTransferService',
        'MachineryTransferItemService',
        'AttendanceGroupService',
        'EmployeeSalaryService',
        'SalaryAdvanceService',
        'AttendanceListService',
        'DocumentService',
        'WageAdvanceService',
        'StaffAttendanceService',
        'ui-notification',
        'ClientService',
    ]);
	app.config(function($routeProvider, $locationProvider) {
		// $locationProvider.html5Mode(true);
		$routeProvider
		.when('/', {
			templateUrl: 'template/dashboard'
		})
		.when('/dashboard', {
			templateUrl: 'template/dashboard',
			controller: 'dashboardController'
		})
		.when('/accountstatement', {
			templateUrl: 'template/accountstatement',
			controller: 'accountstatementController'
		})
        .when('/attendance', {
            templateUrl: 'template/attendance',
            controller: 'AttendanceController'
        })
        .when('/attendancelist', {
            templateUrl: 'template/attendancelist',
            controller: 'attendancelistController'
        })
		.when('/invoice', {
			templateUrl: 'template/invoice',
			controller: 'invoiceController'
		})
		.when('/recur', {
			templateUrl: 'template/recur',
			controller: 'recurController'
		})
        .when('/unit', {
            templateUrl: 'template/unit',
            controller: 'unitController'
        })
        .when('/supplier', {
            templateUrl: 'template/supplier',
            controller: 'supplierController'
        })
        .when('/product', {
            templateUrl: 'template/product',
            controller: 'productController'
        })
        .when('/partner', {
            templateUrl: 'template/partner',
            controller: 'partnerController'
            })
		.when('/profile', {
			templateUrl: 'template/profile',
			controller: 'profileController'
		})
		.when('/settings', {
			templateUrl: 'template/settings',
			controller: 'settingsController'
		})
		.when('/login', {
			templateUrl: 'template/login',
			controller: 'loginController'
		})
		.when('/banks', {
			templateUrl: 'template/banks',
			controller: 'bankController'
		})
		.when('/loan', {
			templateUrl: 'template/loan',
			controller: 'loanController'
		})
		.when('/phonebook', {
			templateUrl: 'template/phonebook',
			controller: 'phonebookController'
		})
		.when('/employee', {
			templateUrl: 'template/employee',
			controller: 'employeeController'
		})
		.when('/note', {
			templateUrl: 'template/note',
			controller: 'noteController'
		})
		.when('/project', {
			templateUrl: 'template/project',
			controller: 'projectController'
		})
		.when('/insure', {
			templateUrl: 'template/insure',
			controller: 'insureController'
		})
		.when('/expense', {
			templateUrl: 'template/expense',
			controller: 'expenseController'
		})
		.when('/income', {
			templateUrl: 'template/income',
			controller: 'incomeController'
		})
		.when('/cash', {
			templateUrl: 'template/cash',
			controller: 'cashController'
		})
		.when('/under', {
			templateUrl: 'template/under',
			controller: 'underController'
		})
        .when('/rent', {
            templateUrl: 'template/rent',
            controller: 'rentController'
        })
        .when('/ledger',{
            templateUrl: 'template/ledger',
            controller:'ledgerController'
        })
        .when('/journal',{
                templateUrl: 'template/journal',
                controller:'journalController'
         })
         .when('/purchase',{
             templateUrl: 'template/purchase',
             controller:'purchaseController'
         })
         .when('/transfer',{
             templateUrl: 'template/transfer',
             controller:'transferController'
         })
         .when('/stockupdate',{
             templateUrl: 'template/stockupdate',
             controller:'stockController'
         })
         .when('/materialreturn',{
             templateUrl: 'template/materialreturn',
             controller:'materialreturnController'
         })
         .when('/machinery',{
             templateUrl: 'template/machinery',
             controller:'machineryController'
         })
        .when('/machinerytransfer',{
            templateUrl: 'template/machinerytransfer',
            controller:'machinerytransferController'
        })
        .when('/journalreport',{
            templateUrl: 'template/journalreport',
            controller:'journalreportController'
        })
        .when('/accountbalance',{
            templateUrl: 'template/accountbalance',
            controller:'accountbalanceController'
        })
        .when('/salary',{
            templateUrl:'template/salary',
            controller:'employeesalaryController'
        })
        .when('/salaryadvance',{
                templateUrl:'template/salaryadvance',
                controller:'salaryadvanceController'
            })
        .when('/daybook',{
            templateUrl:'template/daybook',
            controller:'daybookController'
        })
        .when('/trialbalance',{
            templateUrl:'template/trialbalance',
            controller:'trialbalanceController'
        })
        .when('/tradingaccount',{
            templateUrl:'template/tradingaccount',
            controller:'tradingaccountController'
        })
        .when('/profitandloss',{
            templateUrl:'template/profitandloss',
            controller:'profitandlossController'
        })
        .when('/employeewage',{
            templateUrl:'template/employeewage',
            controller:'employeewageController'
        })
        .when('/projectdocument',{
            templateUrl:'template/projectdocument',
            controller:'projectdocumentController'
        })
        .when('/wageadvance',{
            templateUrl:'template/wageadvance',
            controller:'wageadvanceController'
        })
        .when('/sundrydebtor',{
            templateUrl:'template/sundrydebtor',
            controller:'sundrydebtorController'
        })
        .when('/sundrycreditor',{
            templateUrl:'template/sundrycreditor',
            controller:'sundrycreditorController'
        })
        .when('/staffattendance',{
            templateUrl:'template/staffattendance',
            controller:'staffattendanceController'
        })
        .when('/clients',{
            templateUrl:'template/clients',
            controller:'ClientsController'
        })
		.otherwise({
			redirectTo: 'template/dashboard'
		});
	});
	app.filter('pagination', function() {
	  return function(input, currentPage, pageSize) {
		if(angular.isArray(input)) {
		  var start = (currentPage-1)*pageSize;
		  var end = currentPage*pageSize;
		  return input.slice(start, end);
		}
	  };
	});
	app.filter('percentage', ['$filter', function ($filter) {
		return function (input, decimals) {
			return $filter('number')(input * 100, decimals) + '%';
		};
	}]);
	app.filter('sum', function(){
		return function(items, prop){
			return items.reduce(function(a, b){
				return a + b[prop];
			}, 0);
		};
	});


    /*app.config(function(NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 10000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'left',
            positionY: 'bottom'
        });
    });*/