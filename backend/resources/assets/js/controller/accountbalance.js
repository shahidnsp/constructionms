app.controller('accountbalanceController', function($scope,$http,$filter,Under,Ledger){
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);
    $scope.ledgers=[];

    $scope.accountledgers=[];
    $scope.accountgroups=[];
    $scope.accountgroup_id='All';
    $scope.accountledger_id='All';
    Ledger.query(function(ledger){
        $scope.accountledgers=ledger;
    });
    Under.query(function(group){
        $scope.accountgroups=group;
    });

    loadBalance();
    function loadBalance() {
        $http.get('/api/getledgerbalance').
            success(function (data, status, headers, config) {
                $scope.ledgers = data;
                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    }

    $scope.searchDate=function(fromDate,toDate,accountgroup_id,accountledger_id){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.ledgers=[];
        $http.get('/api/getledgerbalancedate',{params:{fromDate:from,toDate:to,accountgroup_id:accountgroup_id,accountledger_id:accountledger_id}}).
            success(function (data, status, headers, config) {
                $scope.ledgers = data;
                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

    $scope.changeGroup=function(group_id){
        $scope.accountledgers=[];
        if(group_id=='All'){
            Ledger.query(function(ledger){
                $scope.accountledgers=ledger;
            });
        }else{
            Ledger.query({under_id:group_id},function(ledger){
                $scope.accountledgers=ledger;
            });
        }
    }

});