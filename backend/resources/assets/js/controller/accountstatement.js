app.controller('accountstatementController', function($scope,$http,$filter,Ledger){

    $scope.fromDate=new Date();
    $scope.toDate=new Date();
    $scope.ledgers={};
    $scope.ledgerName='';
    $scope.journals=[];

    $scope.ledgers_id=0;
    $scope.drTotal=0;
    $scope.crTotal=0;
    $scope.balance=0;

    Ledger.query(function(ledger){
        $scope.ledgers = ledger;
    });

    $scope.changeLedger=function(ledger_id){
        $http.get('/api/ledger/'+ledger_id).
            success(function (data, status, headers, config) {
                $scope.ledgerName=data.name;
            }).error(function (data, status, headers, config) {
                console.log(data);
                $scope.ledgerName='';
            });
    };
    $scope.searchDate=function(fromDate,toDate){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.journals=[];
        $http.get('/api/accountstatement',{params:{fromDate:from,toDate:to,ledgers_id:$scope.ledgers_id}}).
            success(function (data, status, headers, config) {
                $scope.journals = data;
                getBalance($scope.journals);
                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

    function getBalance(journals){
        var dramt= 0,cramt= 0,balance=0;
        var length=journals.length;
        for(var i=0;i<length;i++){
            if(journals[i].drledgers_id==$scope.ledgers_id)
                dramt+=parseFloat(journals[i].dramount);
            if(journals[i].crledgers_id==$scope.ledgers_id)
                cramt+=parseFloat(journals[i].cramount);
        }
        balance=dramt-cramt;
        $scope.drTotal=dramt;
        $scope.crTotal=cramt;
        if(balance<0) {
            balance=-1*balance;
            $scope.balance = balance+' Cr';
        }else{
            $scope.balance = balance+' Dr';
        }
    }

});