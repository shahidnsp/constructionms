app.controller('AttendanceController', function($scope,$http,$filter,$modal,Project,Employee,Attendance,Leader,Group,AttendanceGroup){

    $scope.curProject = {};
    $scope.curGroup={};
    $scope.editAttendance=false;
    $scope.newGroup={};
    $scope.projects = [];
    $scope.leaders=[];
    $scope.date=new Date();
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    $scope.attendances=[];
    $scope.attenLists=[];

    loadAttendanceGroup();
    function loadAttendanceGroup() {

        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');

        AttendanceGroup.query({fromDate:from,toDate:to},function (attendance) {
            $scope.attenLists = attendance;
        });
    }

    $scope.searchAttendance=function(fromDate,toDate){
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        AttendanceGroup.query({fromDate:from,toDate:to},function(attendance){
            $scope.attenLists=attendance;
        });
    };

    $scope.newAttendance=function(){
        $scope.editAttendance=true;
        $scope.newGroup=new AttendanceGroup();
        $scope.newGroup.date=new Date();
    };

    $scope.editAttendanceGroup = function (thisGroup) {
        $scope.editAttendance = true;
        $scope.curGroup =  thisGroup;
        $scope.newGroup = angular.copy(thisGroup);

        $http.get('/api/attendance/'+thisGroup.id).
            success(function(data,status,headers,config){

                $scope.attendances=[];
                for(var i=0;i<data.length;i++)
                {
                    $scope.updateitem={
                        id:data[i].id,
                        employees_id:data[i].employees_id,
                        wage:data[i].wage,
                        hour:data[i].hour,
                        total:data[i].total,
                        status:data[i].status,
                        employee:data[i].employees.name
                    };

                    $scope.attendances.push($scope.updateitem);
                }

            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.addAttendance = function () {
        if( $scope.newGroup.date.toISOString)
            $scope.newGroup.date = $scope.newGroup.date.toISOString();

        if ($scope.curGroup.id) {

            deleteattendance($scope.curGroup.id);

            $scope.newGroup.$update(function (group) {
                angular.extend($scope.curGroup, $scope.curGroup, group);
                 saveattendance(group.id);
                 loadAttendanceGroup();
            });
        } else {
            $scope.newGroup.$save(function (group) {
                $scope.attenLists.push(group);
                 saveattendance(group.id);
                 loadAttendanceGroup();
            });
        }

        $scope.editAttendance = false;
        $scope.newGroup=new AttendanceGroup();
    };

    $scope.deleteAttendanceGroup = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            deleteattendance(item.id);
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.attenLists.indexOf(item);
                $scope.attenLists.splice(curIndex, 1);

            });
        }
    };

    $scope.changeWage=function(employee)
    {
        var wage=parseFloat(employee.wage);
        var hour=parseFloat(employee.hour);
        var total=wage*hour;
        employee.total=total;

        var newData={
            "wage": wage,
            "hour":hour,
            "total":total
         }

        angular.extend(employee,employee,newData);
    };

    //var project=
    Project.query(function(project){
        $scope.projects= project;
    });



    $scope.cancel=function(){
        $scope.editAttendance=false;
        $scope.attendances=[];
    };

    $scope.changeProject=function(thisProject){
        if (!($scope.curGroup.id)) {
            Leader.query({project_id: thisProject}, function (leader) {
                $scope.leaders = leader;
            });
        }
    };

    $scope.changeLeader=function(thisLeader){
        if (!($scope.curGroup.id)) {
            Group.query({leader_id: thisLeader}, function (leaders) {
                var length = leaders.length;
                $scope.attendances = [];
                for (var i = 0; i < length; i++) {
                    var wage = parseFloat(leaders[i].employees.total);
                    var newData = {
                        "employees_id": leaders[i].employee_id,
                        "employee": leaders[i].employees.name,
                        "wage": wage / 8,
                        "status": "P",
                        "hour": 8,
                        "total": leaders[i].employees.total
                    }
                    $scope.attendances.push(newData);
                }
            });
        }
    };

    function saveattendance(id){


        var length=$scope.attendances.length;

        for(var i=0;i<length;i++)
        {
            $http.post('/api/attendance',{attendance_groups_id:id,employees_id:$scope.attendances[i].employees_id,wage:$scope.attendances[i].wage,hour:$scope.attendances[i].hour,total:$scope.attendances[i].total,status:$scope.attendances[i].status}).
                success(function(data,status,headers,config){
                    //console.log(data);
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        }
    }

    function deleteattendance(id)
    {
        $http.delete('/api/attendance/' + id)
            .success(function (data, status, headers) {
                //$scope.ServerResponse = data;
            })
            .error(function(data,status,headers,config){
                console.log(data);
            });
    }


    $scope.viewAttendance = function (thisItem) {

        $http.get('/api/attendance/'+thisItem.id).
            success(function(data,status,headers,config){
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewAttendance',
                    controller:'viewAttendanceController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

});

app.controller('viewAttendanceController', function ($scope, $modalInstance, item) {

    $scope.attendances = angular.copy(item);
    $scope.total=0;
    var length=$scope.attendances.length;
    for(var i=0;i<length;i++){
        if($scope.attendances[i].status=='P') {
            var amt = parseFloat($scope.attendances[i].total);
            $scope.total += amt;
        }
    }
    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };

    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();
    };

    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
    $scope.maxSize = 5;
});