app.controller('attendancelistController', function($scope,$http,$filter,AttendanceList){
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);
    $scope.ledgers=[];

    AttendanceList.query().success(function(data){
        $scope.cashoverviews = data;
        $scope.curCashoverview = $scope.cashoverviews[0];
        console.log(data);
    });

    loadAttendance();

    function loadAttendance() {
        //To making Headings of days (1-31)
        $scope.days = [];
        for (var i = 1; i <= 31; i++) {
            $scope.days.push(i);
        }

        $http.get('api/attendancereport',{params:{month:'May 2016'}}).
            success(function(data,status) {
                $scope.attendancelist = data;
                console.log($scope.attendancelist);
                console.log(status);
            }).error(function(data,status) {
                console.log('failed');
                console.log(status);
                console.log(data);
            })
    }

});