app.controller('bankController', function($scope,$http,Bank,Account,AccountParticular,Card){

	// Accordion
	$scope.status = {
		isFirstOpen: true,
		oneAtATime: true,
		bankedit: false, 
		accountedit: false,
		particularedit: false
	};
	$scope.curBank = {};
	$scope.curAccount = {};
	$scope.curParticular= {};
	$scope.openDiv = 'banks';
	$scope.breadCrumbs = [];

    $scope.fromDate=new Date();
    $scope.toDate=new Date();

    $scope.openBank = function (){
		$scope.openDiv ='banks';
		$scope.breadCrumbs = [];'success'
		$scope.myBank = {};
		$scope.myAccount = {};
		$scope.myStatement = {};

	};

	$scope.switchDiv = function (argument) {
		var targetDiv = argument.target.attributes["data-target"].value;
		if (targetDiv == "home") {
            //Load all banks
			$scope.openBank();
		} else if(targetDiv == "account"){
            //Load all accounts belongs to current bank
			$scope.openAccount();
		}
	};

	$scope.newbank = {};
  //Load banks from server
  var banks = Bank.query(function () {
      $scope.banks = banks;
  });

	$scope.newBank = function () {
		$scope.status.bankedit = true;
		$scope.newbank = new Bank();
	};
	$scope.addBank = function(){
	    if($scope.curBank.id){
	      //TODO error display
	      $scope.newbank.$update(function () {
	        angular.extend($scope.curBank,$scope.curBank, $scope.newbank);
	      });
	    }else{
	      $scope.newbank.$save(function (bank) {
	        //TODO error display
	        $scope.banks.push(bank);
	      });
	    }
	    $scope.curBank = {};
	    $scope.newbank = {};
	    $scope.status.bankedit = false;
	};
	$scope.editBank = function(bank) {
	    $scope.status.bankedit = true;
	    $scope.curBank= bank;
	    $scope.newbank = angular.copy(bank);
	};
	$scope.deleteBank = function(bank,indx){

	    var confirmDelete = confirm("Do you really need to delete " + bank.name + "?");
	    if (confirmDelete) {
	        //TODO error handling
	        bank.$delete(function () {
	            $scope.banks.splice(indx, 1);
	        });
	    }
	};

    $scope.cancelBank = function () {
          $scope.status.bankedit = false;
          $scope.curBank = {};
    };

    $scope.openAccount = function (bank) {
        $scope.myAccount = {};

        $scope.openDiv = 'accounts';
        if (bank) {

            var account =Account.query({bank_id:bank.id},function(){
                $scope.accounts= account;
            });
            $scope.curBank = bank;
            $scope.breadCrumbs = [
                {
                    "target": "home",
                    "title": "Home"
                },{
                    "target": "account",
                    "title": $scope.curBank.name
                }
            ];
        } else {
            $scope.breadCrumbs.splice(2, 1);
        }
    };


	$scope.newaccount = {};

    $scope.newAccount = function(){
        $scope.status.accountedit = true;
        $scope.newaccount = new Account();
		$scope.newaccount.issueDate = new Date();
    };

    $scope.addAccount = function(){
        if( $scope.newaccount.issueDate.toISOString)
		    $scope.newaccount.issueDate = $scope.newaccount.issueDate.toISOString();
        if ($scope.curAccount.id) {
            $scope.newaccount.$update(function(account){
                console.log(account);
                //TODO error display
                angular.extend($scope.curAccount,$scope.curAccount,account);
                $scope.curAccount = {};
            });
        }else{
            $scope.newaccount.bank_id = $scope.curBank.id;
            $scope.newaccount.$save(function(account){
                console.log(account);
                //TODO error display
                //$scope.accounts.push($scope.newaccount);
                $scope.accounts.push(account);
            });
        }
        $scope.newaccount = new Account();
        $scope.status.accountedit = false;
    };

    $scope.editAccount = function(thisAccount) {
        $scope.status.accountedit = true;
        $scope.curAccount = thisAccount;
        $scope.newaccount = angular.copy(thisAccount);
    };

	$scope.deleteAccount = function(item){
        var confirmDelete = confirm("Do you really need to delete " + item.name + "?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var account = $scope.accounts.indexOf(item);
                $scope.accounts.splice(account, 1);
            });
        }
    };

    $scope.cancelAccount = function () {
		$scope.status.accountedit = false;
		$scope.curAccount = {};
    };

  // Cards
	// TODO edit, add & update
	$scope.cards = [];
	/*	{
			"id": 1,
			"account_id": 1,
			"cardtype": "Visa",
			"cardno": "1234 5678 9012 3456",
			"validfrom": "01/06",
			"expirydate": "05/18",
			"cvv": 345,
			"cardholder": "Kallayi Basheer Shah"
		},{
			"id": 2,
            "account_id": 1,
            "cardtype": "Visa",
            "cardno": "1234 5678 9012 3456",
            "validfrom": "01/06",
            "expirydate": "05/18",
            "cvv": 345,
            "cardholder": "Kallayi Basheer Shah"
		},{
			"id": 3,
            "account_id": 1,
            "cardtype": "Visa",
            "cardno": "1234 5678 9012 3456",
            "validfrom": "01/06",
            "expirydate": "05/18",
            "cvv": 345,
            "cardholder": "Kallayi Basheer Shah"
		}
	];*/
	// Cheque
	// TODO edit, add & update
	$scope.cheques =  [
		{
		    "id": 1,
		    "accountno": 87867867,
		    "issuedate": "20-May-2015",
		    "from": 123456,
		    "to": 1234654,
		    "chequerecords": [
			    {
					    "id": 1,
					    "cheque_id": 2,
					    "number": 2222222,
					    "date": "21-Feb-2015",
					    "favor": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non at, ad deserunt eveniet veritatis, modi voluptatum molestiae excepturi molestias amet asperiores doloribus dolorum aliquid, quaerat praesentium. Id ea, officia mollitia.",
					    "amount": 12000,
					    "deposit": 98521,
					    "balance": 56980
					}, {
					    "id": 2,
					    "cheque_id": 2,
					    "number": 44444444,
					    "date": "22-March-2015",
					    "favor": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus aliquid ex numquam aperiam excepturi officia, nihil eum vero consectetur quaerat praesentium iure, iste fugit vel quidem consequatur illo veniam recusandae?",
					    "amount": 6000,
					    "deposit": 1520,
					    "balance": 48000
					}, {
					    "id": 3,
					    "cheque_id": 2,
					    "number": 66666666,
					    "date": "22-March-2015",
					    "favor": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, sapiente accusamus libero consequatur beatae enim animi ipsum incidunt voluptatum. Dolores deleniti dolorum omnis repellat quisquam, maxime ex pariatur eveniet necessitatibus.",
					    "amount": 6000,
					    "deposit": 1520,
					    "balance": 48000
					}, {
					    "id": 4,
					    "cheque_id": 2,
					    "number": 88888888,
					    "date": "22-March-2015",
					    "favor": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus ducimus mollitia nobis aspernatur possimus aut explicabo asperiores? Eveniet error aliquid qui in nihil assumenda officiis, distinctio commodi, enim at, facilis.",
					    "amount": 6000,
					    "deposit": 1520,
					    "balance": 48000
					}
				]
		}, {
		    "id": 2,
		    "accountno": 87867867,
		    "issuedate": "12-June-2015",
		    "from": 123657,
		    "to": 1234752,
		    "chequerecords": [
					{
					    "id": 5,
					    "cheque_id": 1,
					    "number": 66666666,
					    "date": "22-March-2015",
					    "favor": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore enim, ipsum. Perspiciatis necessitatibus, officia maxime, itaque ad, impedit, qui sint voluptatibus aperiam cumque eos at quisquam quaerat quidem accusantium consectetur.",
					    "amount": 6000,
					    "deposit": 1520,
					    "balance": 48000
					}, {
					    "id": 6,
					    "cheque_id": 1,
					    "number": 88888888,
					    "date": "22-March-2015",
					    "favor": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore exercitationem ipsa accusantium deserunt laborum voluptates quis sint quia architecto ad voluptate illum cupiditate, harum ullam earum laboriosam ipsum, at assumenda.",
					    "amount": 6000,
					    "deposit": 1520,
					    "balance": 48000
					}
				]
		}
	];


	// Particulars

    $scope.openStatement = function (account) {
        $scope.openDiv = 'selectedAcc';
        $scope.myAccount = account;

        var particular = AccountParticular.query({account_id:account.id}, function () {
            $scope.particulars = particular;
            getTotalStatement(particular);
            openCard(account);
        });
        $scope.breadCrumbs.push({"title": $scope.myAccount.name});
    };

    $scope.totalwithdrawal=0;
    $scope.totaldeposit=0;

    var getTotalStatement=function (curparticular) {
        var count=curparticular.length;
        var withdrawal=0;
        var deposit=0;
        for(var i=0;i<count;i++) {
            var withamt=parseFloat(curparticular[i].withdrawal);
            var depamt=parseFloat(curparticular[i].deposit);
            if(withamt>=0)
                withdrawal += withamt;
            if(depamt>=0)
                deposit+=depamt;
        }
        $scope.totalwithdrawal=withdrawal;
        $scope.totaldeposit=deposit;
    }

	$scope.addParticular = function(){
        if( $scope.newparticular.payDate.toISOString)
		    $scope.newparticular.payDate = $scope.newparticular.payDate.toISOString();
		if ($scope.curParticular.id) {
            var temp = $scope.curParticular; //TODO optimize
            $scope.newparticular.$update(function (particular) {
                angular.extend(temp,temp,particular);
                $scope.curParticular = {};
                getTotalStatement($scope.particulars);
            });
		}else{

            $scope.newparticular.account_id = $scope.myAccount.id;
            $scope.newparticular.$save(function (particular) {
                $scope.particulars.push(particular);
                getTotalStatement($scope.particulars);
            });
		}
		$scope.curParticular = {};
		$scope.status.particularedit = false;

	};


    $scope.newParticular = function (argument) {
        $scope.status.particularedit = true;
        $scope.newparticular = new AccountParticular();
        $scope.newparticular.payDate = new Date();
    };


	$scope.editParticular = function(thisParticular){
		$scope.status.particularedit = true;
		$scope.curParticular = thisParticular;
		$scope.newparticular = angular.copy(thisParticular);
	};

	$scope.deleteParticular = function(particular){
		var confirmDelete = confirm("Do you really need to delete ?");
		if (confirmDelete) {
            particular.$delete(function () {
                var particular = $scope.particulars.indexOf(particular);
                $scope.particulars.splice(particular, 1);
            });
		}
	};

	$scope.cancelParticular = function () {
		$scope.status.particularedit = false;
		$scope.curParticular = {};
	};


    var openCard = function (account) {
        $scope.cards = Card.query({account_id:account.id}, function () {
        });
    };


	$scope.addCard = function(){
		alert("Hello mate");
	};

});