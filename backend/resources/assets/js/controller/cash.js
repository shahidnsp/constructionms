app.controller('cashController', function($scope,$http,Cash,CashDebit,CashCredit){
	// Debit overview
	$scope.cashoverviews = [];

	function updateCash(){
		Cash.query().success(function(data){
			$scope.cashoverviews = data;
			$scope.curCashoverview = $scope.cashoverviews[0];
		});
	}updateCash();

	// Debit part
	$scope.debitedit = false;
	$scope.newdebit = {};
	$scope.curDebit = {};
	$scope.cashdebits = [];
    $scope.fromDate=new Date();
    $scope.toDate=new Date();

	//TODO add month query
	CashDebit.query(function(debit){
		$scope.cashdebits = debit;
        getTotalDebit($scope.cashdebits);
	});


	$scope.newDebit = function () {

        $scope.curDebit = {};
		$scope.debitedit = true;
		$scope.newdebit = new CashDebit();
		$scope.newdebit.date = new Date();


	};
	$scope.editDebit = function (thisDebit) {
		$scope.debitedit = true;
		$scope.curDebit =  thisDebit;
		$scope.newdebit = angular.copy(thisDebit);
	};
	$scope.addDebit = function () {

        if($scope.newdebit.date.toISOString)
            $scope.newdebit.date = $scope.newdebit.date.toISOString();
        
		if ($scope.curDebit.id) {
			$scope.newdebit.$update(function(debit){
				angular.extend($scope.curDebit, $scope.curDebit, debit);
                getTotalDebit($scope.cashdebits);
			});
		} else{
			$scope.newdebit.$save(function(debit){
				$scope.cashdebits.push(debit);
                getTotalDebit($scope.cashdebits);
			});
		}

		$scope.debitedit = false;
        $scope.newdebit = new CashDebit();
        $scope.newdebit.date = new Date();

		updateCash();
	};
	$scope.deleteDebit = function (item) {
		var confirmDelete = confirm("Do you really need to delete the item ?");
		if (confirmDelete) {
			item.$delete(function(){
				var curIndex = $scope.cashdebits.indexOf(item);
				$scope.cashdebits.splice(curIndex, 1);
                getTotalDebit($scope.cashdebits);
				updateCash();
			});
		}
	};
	$scope.cancelDebit = function () {
		$scope.debitedit = false;
        $scope.curDebit = {};
        $scope.newdebit = new CashDebit();
        $scope.newdebit.date = new Date();
	};
	// Credit part
	$scope.creditedit = false;
	$scope.newcredit = {};
	$scope.curCredit = {};
	$scope.cashcredits = [];

    $scope.totaldebit=0;
    var getTotalDebit=function (cashdebits) {
        var count=cashdebits.length;
        var sum=0;
        for(var i=0;i<count;i++)
            sum+=parseFloat(cashdebits[i].amount);
        $scope.totaldebit=sum;
    }

    $scope.totalcredit=0;
    var getTotalCredit=function (cashcredits) {
        var count=cashcredits.length;
        var sum=0;
        for(var i=0;i<count;i++)
            sum+=parseFloat(cashcredits[i].amount);
        $scope.totalcredit=sum;
    }

	//TODO month query();
	CashCredit.query(function(credit){
		$scope.cashcredits = credit;
        getTotalCredit($scope.cashcredits);
	});

	$scope.newCredit = function () {
		$scope.creditedit = true;
        $scope.curCredit =  {};
		$scope.newcredit = new CashCredit();
		$scope.newcredit.date = new Date();

	};

	$scope.editCredit = function (thisCredit) {
		$scope.creditedit = true;
		$scope.curCredit =  thisCredit;
		$scope.newcredit = angular.copy(thisCredit);
	};

	$scope.addCredit = function () {

        if($scope.newcredit.date.toISOString)
            $scope.newcredit.date =$scope.newcredit.date.toISOString();

		if ($scope.curCredit.id) {
			$scope.newcredit.$update(function(credit){
				angular.extend($scope.curCredit, $scope.curCredit, credit);
                getTotalCredit($scope.cashcredits);
			});

		} else{

			$scope.newcredit.$save(function(credit){
				$scope.cashcredits.push(credit);
                getTotalCredit($scope.cashcredits);
			});
		}
		$scope.creditedit = false;
		$scope.newcredit = new CashCredit();
        $scope.newcredit.date = new Date();

        updateCash();
	};

	$scope.deleteCredit = function (item) {
		var confirmDelete = confirm("Do you really need to delete the item ?");
		if (confirmDelete) {
			item.$delete(function(){
				var curIndex = $scope.cashcredits.indexOf(item);
				$scope.cashcredits.splice(curIndex, 1);
                getTotalCredit($scope.cashcredits);
			});
		}
	};
	$scope.cancelCredit = function () {
		$scope.creditedit = false;
		$scope.newcredit = new CashCredit();
        $scope.newcredit.date = new Date();
	};
});