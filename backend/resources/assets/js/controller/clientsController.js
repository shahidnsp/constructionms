/**
 * Created by noushi on 11/10/16.
 */
app.controller('ClientsController', function($scope,$http,$filter,Client){
    $scope.clientedit = false;
    $scope.newclient = {};
    $scope.curclient = {};
    $scope.clients = [];
    $scope.curdate= $filter('date')(new Date(), 'yyyy-MM-dd');
    console.log($scope.curdate);

    Client.query(function(client){
        $scope.clients = client;
    });

    $scope.newClient = function () {
        $scope.clientedit = true;
        $scope.newclient = new Client();
        $scope.newclient.visited=new Date();
        $scope.newclient.firstvisiting=new Date();
        $scope.newclient.reminderdate=new Date();
        $scope.newclient.crOrDr='Dr';
        $scope.newclient.openBal='0';
        $scope.curclient = {};
    };
    $scope.editClient = function (thisClient) {
        $scope.clientedit = true;
        $scope.curclient =  thisClient;
        $scope.newclient = angular.copy(thisClient);
    };
    $scope.addClient = function () {
        if($scope.newclient.visited.toISOString)
            $scope.newclient.visited = $scope.newclient.visited.toISOString();
        if($scope.newclient.firstvisiting.toISOString)
            $scope.newclient.firstvisiting = $scope.newclient.firstvisiting.toISOString();
        if($scope.newclient.reminderdate.toISOString)
            $scope.newclient.reminderdate = $scope.newclient.reminderdate.toISOString();

        if ($scope.curclient.id) {
            $scope.newclient.$update(function(client){
                angular.extend($scope.curclient, $scope.curclient, client);
            });
        } else{
            $scope.newclient.$save(function(client){
                $scope.clients.push(client);
            });
        }
        $scope.clientedit = false;
        $scope.newclient = new Client();
    };
    $scope.deleteClient = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.clients.indexOf(item);
                $scope.clients.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelClient = function () {
        $scope.clientedit = false;
        $scope.newclient = new Client();
    };
});