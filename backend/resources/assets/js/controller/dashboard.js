app.controller('dashboardController', function($scope,$http,Reminder,Notification){
	/* Task widget */
	$scope.newTask = {};
    $scope.curTask = {};
    $scope.tasks=[];

    $scope.status={};

    $http.get('/api/getDashboardStatus')
        .then(function(response){
            $scope.status=response.data;
        });


	/*$scope.tasks = [
		{'name': 'I have to do this', 'detail': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere asperiores at magni! Officia vero sunt, rerum tempore, deserunt illo expedita iure blanditiis deleniti doloremque molestias fuga quod nemo perferendis ipsam. ipsum dolor sit amet kili paari poyi', 'date': '2015-05-18T18:30:00.000Z', 'done': true},
		{'name': 'I have to do that', 'detail': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum corporis commodi voluptate suscipit ratione, ipsa sapiente vitae dolores, veritatis magnam modi reiciendis ullam, officiis inventore facere mollitia ipsam error quam? ipsum dolor sit amet kili paari poyi', 'date': '2014-06-24T18:30:00.000Z', 'done': false},
		{'name': 'He has to do those', 'detail': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati dolorum fugit, dolore inventore minus, labore, voluptas blanditiis corrupti perspiciatis illum velit excepturi voluptates delectus odio assumenda earum maxime molestias minima. ipsum dolor sit amet kili paari poyi', 'date': '2015-07-12T18:30:00.000Z', 'done': true}
	];*/
    $scope.newTask = {};
    function loadDate(){
        $scope.newTask = new Reminder();
        $scope.newTask.regDate = new Date();
        $scope.newTask.remDate = new Date();
    }loadDate();




    //Load task from server
    function loadTask() {
        Reminder.query(function (task) {
            $scope.tasks = task;

            var length = $scope.tasks.length;
            var nowDate = new Date().getDate();
            var nowMonth = new Date().getMonth() + 1;
            var nowYear = new Date().getFullYear();

            var now = nowYear + '-' + nowMonth + '-' + nowDate;
            //alert(now);
            for (var i = 0; i < length; i++) {
                var date = $scope.tasks[i].remDate;
                //alert($scope.tasks[i].remDate);
                if (date == now) {
                    if ($scope.tasks[i].done == 'false') {
                        Notification.error({message: $scope.tasks[i].description, title: $scope.tasks[i].name});
                    }
                }
            }
        });
    }loadTask();



	// New task
    $scope.addTask = function () {
        if($scope.newTask.regDate.toISOString)
            $scope.newTask.regDate = $scope.newTask.regDate.toISOString();
        if($scope.newTask.remDate.toISOString)
            $scope.newTask.remDate = $scope.newTask.remDate.toISOString();

        if ($scope.curTask.id) {
            $scope.newTask.$update(function(task){
                angular.extend($scope.curTask, $scope.curTask, task);
            });
        } else{
            $scope.newTask.$save(function(task){
                $scope.tasks.push(task);
            });
        }
        $scope.newTask = new Reminder();
        $scope.newTask.regDate = new Date();
        $scope.newTask.remDate = new Date();
    };
    //Edit Task
    $scope.editTask = function (thisTask) {
        $scope.curTask =  thisTask;
        $scope.newTask = angular.copy(thisTask);
    };
	// Delete task
	$scope.deleteTask = function (item) {
        var confirmDelete = confirm("Do you really need to delete the Task ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.tasks.indexOf(item);
                $scope.tasks.splice(curIndex, 1);
            });
        }
	};
    //cancel Task
    $scope.cancelTask = function () {
        $scope.newTask = new Reminder();
        $scope.newTask.regDate = new Date();
        $scope.newTask.remDate = new Date();
        /*Notification.error('Error notification');
        Notification('Primary notification');
        Notification.success('Success notification');
        Notification({message: 'Primary notification', title: 'Primary notification'});*/
        //Notification.error({message: '<b>Error</b> <s>notification</s>', title: '<i>Html</i> <u>message</u>'});
    };
	/* End task */
	// Project tab
	$scope.projectMonth = new Date();
	$scope.project_status = [
		{
			"name": "Lorem PC",
			"percentage": 184
		},{
			"name": "Ligula Nullam Feugiat Company",
			"percentage": 65
		},{
			"name": "Mauris Aliquam Eu Inc.",
			"percentage": 47
		},{
			"name": "Class Aptent Inc.",
			"percentage": 52
		},{
			"name": "Vitae Diam PC",
			"percentage": 18
		},{
			"name": "Semper Company",
			"percentage": 89
		},{
			"name": "Dui Cras Foundation",
			"percentage": 54
		}
	];
});