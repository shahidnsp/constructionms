app.controller('daybookController', function($scope,$http,$filter,$modal){
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);
    $scope.cashbooks=[];
    $scope.cashDebitTotal=0;
    $scope.cashCreditTotal=0;
    $scope.cashBalance=0;
    $scope.cashBalanceSide='';

    $scope.AccountBalance=[];

    $http.get('/api/cashbook/',{params:{fromDate:$scope.fromDate,toDate:$scope.toDate}}).
        success(function(data,status,headers,config){
            var length=data.length;
            $scope.AccountBalance=data;
            var cashdebit= 0,cashcredit= 0,cashopening=0,cashclosing=0;
            var bankdebit= 0,bankcredit= 0,bankopening=0,bankclosing=0;
            for(var i=0;i<length;i++){
                if(data[i].unders_id=='28'){
                    cashdebit+=parseFloat(data[i].debit);
                    cashcredit+=parseFloat(data[i].credit);
                    if(data[i].crOrDr=='Dr')
                        cashclosing+=parseFloat(data[i].balance);
                    else
                        cashclosing-=parseFloat(data[i].balance);

                    if(data[i].openingDrorCr=='Dr')
                        cashopening+=parseFloat(data[i].openingBalance);
                    else
                        cashopening-=parseFloat(data[i].openingBalance);
                }

                if(data[i].unders_id=='29'){
                    bankdebit+=parseFloat(data[i].debit);
                    bankcredit+=parseFloat(data[i].credit);
                    if(data[i].crOrDr=='Dr')
                        bankclosing+=parseFloat(data[i].balance);
                    else
                        bankclosing-=parseFloat(data[i].balance);
                    if(data[i].openingDrorCr=='Dr')
                        bankopening+=parseFloat(data[i].openingBalance);
                    else
                        bankopening-=parseFloat(data[i].openingBalance);
                }
            }
            if(cashcredit!=0 || cashdebit !=0 || cashopening != 0){
                var closeCash='',cashOpen='';
                if(cashclosing<0) {
                    closeCash=(cashclosing*-1)+'Cr';
                }else{
                    closeCash=cashclosing+'Dr';
                }

                if(cashopening<0) {
                    cashOpen=(cashopening*-1)+'Cr';
                }else{
                    cashOpen=cashopening+'Dr';
                }

                $scope.cashbooks.push({
                    particular: 'Cash-in Hand',
                    debit: cashdebit,
                    credit: cashcredit,
                    openingBalance: cashOpen,
                    closing: closeCash,
                    unders_id: 28
                });
            }

            if(bankcredit!=0 || bankdebit !=0  || bankopening != 0){

                var closeBank='',bankOpen='';
                if(bankclosing<0) {
                    closeBank=(bankclosing*-1)+'Cr';
                }else{
                    closeBank=bankclosing+'Dr';
                }

                if(bankopening<0) {
                    bankOpen=(bankopening*-1)+'Cr';
                }else{
                    bankOpen=bankopening+'Dr';
                }

                $scope.cashbooks.push({particular:'Bank Account',debit:bankdebit,credit:bankcredit,openingBalance:bankOpen,closing:closeBank,unders_id:29});
            }
            console.log($scope.cashbooks);
            //getCashBalance($scope.cashbooks);
        }).error(function(data,status,headers,config){
            console.log(data);
        });

    $scope.getCashBookByDate=function(fromDate,toDate){

        $scope.cashbooks=[];
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

         $http.get('/api/cashbook',{params:{'fromDate':from,'toDate':to}})
            .then(function(response){
                 var data=response.data;
                 $scope.AccountBalance=data;
                 var length=data.length;
                 var cashdebit= 0,cashcredit= 0,cashopening=0,cashclosing=0;
                 var bankdebit= 0,bankcredit= 0,bankopening=0,bankclosing=0;
                 for(var i=0;i<length;i++){
                     if(data[i].unders_id=='28'){
                         cashdebit+=parseFloat(data[i].debit);
                         cashcredit+=parseFloat(data[i].credit);
                         if(data[i].crOrDr=='Dr')
                             cashclosing+=parseFloat(data[i].balance);
                         else
                             cashclosing-=parseFloat(data[i].balance);

                         if(data[i].openingDrorCr=='Dr')
                             cashopening+=parseFloat(data[i].openingBalance);
                         else
                             cashopening-=parseFloat(data[i].openingBalance);
                     }

                     if(data[i].unders_id=='29'){
                         bankdebit+=parseFloat(data[i].debit);
                         bankcredit+=parseFloat(data[i].credit);
                         if(data[i].crOrDr=='Dr')
                             bankclosing+=parseFloat(data[i].balance);
                         else
                             bankclosing-=parseFloat(data[i].balance);
                         if(data[i].openingDrorCr=='Dr')
                             bankopening+=parseFloat(data[i].openingBalance);
                         else
                             bankopening-=parseFloat(data[i].openingBalance);
                     }
                 }
                 if(cashcredit!=0 || cashdebit !=0 || cashopening != 0){
                     var closeCash='',cashOpen='';
                     if(cashclosing<0) {
                         closeCash=(cashclosing*-1)+' Cr';
                     }else{
                         closeCash=cashclosing+' Dr';
                     }

                     if(cashopening<0) {
                         cashOpen=(cashopening*-1)+' Cr';
                     }else{
                         cashOpen=cashopening+' Dr';
                     }

                     $scope.cashbooks.push({
                         particular: 'Cash-in Hand',
                         debit: cashdebit,
                         credit: cashcredit,
                         openingBalance: cashOpen,
                         closing: closeCash,
                         unders_id: 28
                     });
                 }

                 if(bankcredit!=0 || bankdebit !=0  || bankopening != 0){

                     var closeBank='',bankOpen='';
                     if(bankclosing<0) {
                         closeBank=(bankclosing*-1)+' Cr';
                     }else{
                         closeBank=bankclosing+' Dr';
                     }

                     if(bankopening<0) {
                         bankOpen=(bankopening*-1)+' Cr';
                     }else{
                         bankOpen=bankopening+' Dr';
                     }

                     $scope.cashbooks.push({particular:'Bank Account',debit:bankdebit,credit:bankcredit,openingBalance:bankOpen,closing:closeBank,unders_id:29});
                 }
                 console.log($scope.cashbooks);
            });
    };

    $scope.OpenAccountBalance=function(unders_id){
        var accountBalance=[];
        var length=$scope.AccountBalance.length;
        for(var i=0;i<length;i++){
            if($scope.AccountBalance[i].unders_id==unders_id)
                accountBalance.push($scope.AccountBalance[i]);
        }
        var modalInstance = $modal.open({
            templateUrl: 'template/viewAccountBalance',
            controller:'viewAccountBalanceController',
            size: 'lg',
            resolve: {
                account: function () {
                    return accountBalance;
                },
                fromDate:function() {
                    return $scope.fromDate;
                },
                toDate:function() {
                    return $scope.toDate;
                }
            }
        });
    };




    $scope.daybooks=[];
    $scope.debitTotal=0;
    $scope.creditTotal=0;

    $http.get('/api/daybook/',{params:{fromDate:$scope.fromDate,toDate:$scope.toDate}}).
        success(function(data,status,headers,config){
            $scope.daybooks=data;
            getDaybookBalance($scope.daybooks);
        }).error(function(data,status,headers,config){
            console.log(data);
        });

    $scope.getPurchaseBookByDate=function(fromDate,toDate){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $http.get('/api/daybook',{params:{'fromDate':from,'toDate':to}})
            .then(function(response){
                $scope.daybooks=response.data;
                getDaybookBalance($scope.daybooks);
            });
    };

    function getDaybookBalance(daybooks){
        $scope.debitTotal=0;
        $scope.creditTotal=0;

        var length=daybooks.length;
        var dramt=0;
        var cramt=0;

        for(var i=0;i<length;i++){
            dramt+= parseFloat(daybooks[i].debit);
            cramt+= parseFloat(daybooks[i].credit);
        }
        $scope.debitTotal=dramt;
        $scope.creditTotal=cramt;
    }
});

app.controller('viewAccountBalanceController', function ($scope,$http, $modalInstance,$modal, account,fromDate,toDate) {
    $scope.accounts = angular.copy(account);
    $scope.fromDate=fromDate;
    $scope.toDate=toDate;

    $scope.close = function () {
        $modalInstance.dismiss('Close');
    };

    $scope.OpenLedgerDetails=function(ledger_id){
        $http.get('/api/getLedgerDetailsForCashBook/',{params:{ledgers_id:ledger_id,fromDate:fromDate,toDate:toDate}}).
            success(function(data,status,headers,config){
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewLedgerDetails',
                    controller:'viewLedgerDetailsController',
                    size: 'lg',
                    resolve: {
                        account: function () {
                            return data;
                        },
                        fromDate:function() {
                            return $scope.fromDate;
                        },
                        toDate:function() {
                            return $scope.toDate;
                        }
                    }
                });
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };
});

app.controller('viewLedgerDetailsController', function ($scope,$modalInstance,$modal, account,fromDate,toDate) {
    $scope.accounts = angular.copy(account);
    $scope.fromDate=fromDate;
    $scope.toDate=toDate;
    $scope.close = function () {
        $modalInstance.dismiss('Close');
    };
});