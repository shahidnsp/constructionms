/**
 * Created by noushid on 12/7/16.
 */

app.controller('employeeController', function ($scope, $http,$modal, Employee) {
    $scope.employeeedit = false;
    $scope.newemployee = {};
    $scope.curemployee = {};
    $scope.employees = [];

    loadEmployee();

    function loadEmployee() {
        Employee.query(function (employee) {
            $scope.employees = employee;
        });
    }

    $scope.newEmployee = function () {
        $scope.employeeedit = true;
        $scope.newemployee = new Employee();
        $scope.newemployee.joinDate=new Date();
        $scope.newemployee.employeeType='Office Staff';
        $scope.curemployee = {};
    };

    $scope.addEmployee = function() {

        if ($scope.curemployee.id) {
           /* $scope.newemployee.$update(function(employee) {
                angular.extend($scope.curemployee, $scope.curemployee, employee);
            })*/
            var fd = new FormData();
            $scope.newemployee['id']=$scope.curemployee.id;
            for(var key in $scope.newemployee)
                fd.append(key,$scope.newemployee[key]);

           // var uri='api/row_material/'+$scope.curRow.id;

            $http.post('/api/employee', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(data,status,headers,config){
                    console.log(data);
                    loadEmployee();
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        }else {
           /* $scope.newemployee.$save(function (employee) {
                $scope.employees.push(employee);
            });*/

            var fd = new FormData();
            //fd.append('photo', $scope.myFile);

            for(var key in $scope.newemployee)
                fd.append(key,$scope.newemployee[key]);

            $http.post('/api/employee', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(data,status,headers,config){
                    console.log(data);
                    loadEmployee();
                }).error(function(data,status,headers,config){
                    console.log(data);
                });

        }
        $scope.employeeedit = false;
        $scope.newemployee = new Employee();


    };

    $scope.editEmployee = function(thisemployee) {
        $scope.employeeedit = true;
        $scope.curemployee = thisemployee;
        $scope.newemployee = angular.copy(thisemployee);
    };

    $scope.deleteEmployee = function (item) {
        var confirmDelete = confirm("Do you really need to delete this item ?");
        if (confirmDelete) {
            item.$delete(function () {
                var curIndex = $scope.employees.indexOf(item);
                $scope.employees.splice(curIndex, 1);
            });
        }
        loadEmployee();
    };

    $scope.cancelEmployee = function () {
        $scope.employeeedit = false;
        $scope.newemployee = new Employee();
    };


    $scope.totalSalary = function () {

        var basic = ($scope.newemployee.basic) ? parseFloat($scope.newemployee.basic) : 0;
        var ta = ($scope.newemployee.ta) ? parseFloat($scope.newemployee.ta) : 0;
        var da = ($scope.newemployee.da) ? parseFloat($scope.newemployee.da) : 0;
        var hra = ($scope.newemployee.hra) ? parseFloat($scope.newemployee.hra) : 0;
        var medical = ($scope.newemployee.medical) ? parseFloat($scope.newemployee.medical) : 0;
        var total = basic + ta + da + hra + medical;
        $scope.newemployee.total = total;
    };

    $scope.viewPhoto = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewphoto',
            controller:'viewphotoController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };
    $scope.open = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewemployee',
            controller:'viewemployeeController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };

});

app.controller('viewemployeeController', function ($scope, $modalInstance,$modal, item) {

    $scope.employee = angular.copy(item);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };

    $scope.viewPhoto = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewphoto',
            controller:'viewphotoController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };
});