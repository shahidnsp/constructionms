
app.controller('employeesalaryController', function ($scope, $http,$filter,$modal, EmployeeSalary,Employee,Ledger) {
    $scope.empsalaryedit = false;
    $scope.newempsalary = {};
    $scope.curempsalary = {};
    $scope.empsalaries = [];
    $scope.employees = [];
    $scope.totalPaid = 0;
    $scope.totalBalance = 0;
    $scope.totalTotal = 0;
    $scope.employees_id=0;
    $scope.totalPreBalance=0;
    $scope.lists=[];


    $scope.fromDate='';
    $scope.toDate='';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();

    if(month<5) {
        $scope.fromDate = new Date(year-1, 5, 1);
        $scope.toDate = new Date(year, 2, 31);
    }else{
        $scope.fromDate = new Date(year, 5, 1);
        $scope.toDate = new Date(year+1, 2, 31);
    }
    $scope.cashLedgers=[];
    Ledger.query(function(ledger){
        var length=ledger.length;
        $scope.cashLedgers=[];
        for(var i=0;i<length;i++){
            if(ledger[i].unders_id == '18' || ledger[i].unders_id == '28' || ledger[i].unders_id == '29' )
                $scope.cashLedgers.push(ledger[i]);
        }
    });

    function loadempSalary() {

        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');

        EmployeeSalary.query({fromDate:from,toDate:to},function (empsalary) {
            $scope.empsalaries = empsalary;
            getTotal($scope.empsalaries);
        });
    }

    $scope.searchSalary=function(fromDate,toDate){
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        EmployeeSalary.query({fromDate:from,toDate:to,employees_id:$scope.employees_id},function (empsalary) {
            $scope.empsalaries = empsalary;
            getTotal($scope.empsalaries);
        });
    };

    function getTotal(salaries){
        var length=salaries.length,total= 0,paid= 0,balance=0;
        for(var i=0;i<length;i++) {
            total += parseFloat(salaries[i].total);
            paid += parseFloat(salaries[i].paid);
            balance += parseFloat(salaries[i].balance);
        }
        $scope.totalPaid = paid;
        $scope.totalBalance = balance;
        $scope.totalTotal = total;
    }

    Employee.query({type:1},function (employee) {
        $scope.employees = employee;
    });

    loadempSalary();

    $scope.totalSalary = function () {
        var basic = ($scope.newempsalary.basic) ? parseFloat($scope.newempsalary.basic) : 0;
        var ta = ($scope.newempsalary.ta) ? parseFloat($scope.newempsalary.ta) : 0;
        var da = ($scope.newempsalary.da) ? parseFloat($scope.newempsalary.da) : 0;
        var hra = ($scope.newempsalary.hra) ? parseFloat($scope.newempsalary.hra) : 0;
        var medical = ($scope.newempsalary.medical) ? parseFloat($scope.newempsalary.medical) : 0;
        var advance = ($scope.newempsalary.advance) ? parseFloat($scope.newempsalary.advance) : 0;
        var prebalance = ($scope.newempsalary.prebalance) ? parseFloat($scope.newempsalary.prebalance) : 0;
        var total = basic + ta + da + hra + medical;
        $scope.newempsalary.amount=total;
        $scope.newempsalary.total = total-advance;
        var gross=($scope.newempsalary.total) ? parseFloat($scope.newempsalary.total) : 0;
        $scope.newempsalary.paid=gross+prebalance;
    };

    $scope.changePaid = function () {
        var total = ($scope.newempsalary.total) ? parseFloat($scope.newempsalary.total) : 0;
        var paid = ($scope.newempsalary.paid) ? parseFloat($scope.newempsalary.paid) : 0;
        var prebalance=($scope.newempsalary.prebalance) ? parseFloat($scope.newempsalary.prebalance) : 0;
        $scope.newempsalary.balance=(total+prebalance)-paid;
    };
    $scope.changeBalance = function () {
        var total = ($scope.newempsalary.total) ? parseFloat($scope.newempsalary.total) : 0;
        var balance = ($scope.newempsalary.balance) ? parseFloat($scope.newempsalary.balance) : 0;
        var prebalance=($scope.newempsalary.prebalance) ? parseFloat($scope.newempsalary.prebalance) : 0;
        $scope.newempsalary.paid=(total+prebalance)-balance;
    };



    $scope.newEmpSalary = function () {
        $scope.empsalaryedit = true;
        $scope.newempsalary = new EmployeeSalary();
        $scope.newempsalary.ledgers_id=1;
        $scope.newempsalary.date=new Date();
        $scope.curempsalary = {};
    };

    $scope.addEmpSalary = function() {
        if ($scope.curempsalary.id) {
            $scope.newempsalary.$update(function (employeesal) {
                angular.extend($scope.curempsalary, $scope.curempsalary, employeesal);
                loadempSalary();
            });
        }else {
            $scope.newempsalary.$save(function (employeesal) {
                $scope.empsalaries.push(employeesal);
                loadempSalary();
            });
        }
        $scope.empsalaryedit = false;
        $scope.newempsalary = new EmployeeSalary();


    };

    $scope.editEmpSalary = function(thisempsalary) {
        $scope.empsalaryedit = true;
        $scope.curempsalary = thisempsalary;
        $scope.newempsalary = angular.copy(thisempsalary);
    };

    $scope.deleteEmployee = function (item) {
        var confirmDelete = confirm("Do you really need to delete this item ?");
        if (confirmDelete) {
            item.$delete(function () {
                var curIndex = $scope.empsalaries.indexOf(item);
                $scope.empsalaries.splice(curIndex, 1);
                getTotal($scope.empsalaries);
            });
        }
    };

    $scope.cancelEmployee = function () {
        $scope.empsalaryedit = false;
        $scope.newempsalary = new EmployeeSalary();
    };

    $scope.getEmployeeJoinDate=function(id) {
        if (id != 0) {
            $http.get('/api/employee/' + id).
                success(function (data, status, headers, config) {
                    $scope.fromDate = data.joinDate;
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
        }
    };

    function getSalaryBalanceList(){
        $http.get('/api/getSalaryBalanceList').
            success(function (data, status, headers, config) {
                $scope.lists=data;
                getSalaryBalanceSum($scope.lists);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    }getSalaryBalanceList();
    function getSalaryBalanceSum(advance){
        var sum=0;
        var length=advance.length;
        for(var i=0;i<length;i++){
            sum+=parseFloat(advance[i].balance);
        }
        $scope.totalPreBalance=sum;
    }

    $scope.getSalary=function(id){
        if(id != null) {
            $http.get('/api/employee/' + id).
                success(function (data, status, headers, config) {

                    $scope.newempsalary.basic = data.basic;
                    $scope.newempsalary.ta = data.ta;
                    $scope.newempsalary.da = data.da;
                    $scope.newempsalary.hra = data.hra;
                    $scope.newempsalary.medical = data.medical;
                    $scope.newempsalary.total = data.total;
                    $scope.newempsalary.amount=data.total;

                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
            $http.get('/api/getAdvance',{params:{employees_id:id}}).
                success(function (data, status, headers, config) {
                    var total=parseFloat($scope.newempsalary.total);
                    var advance=parseFloat(data);
                    $scope.newempsalary.advance=advance;
                    $scope.newempsalary.total=total-advance;

                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
            $http.get('/api/getSalaryBalance',{params:{employees_id:id}}).
                success(function (data, status, headers, config) {
                    var total=parseFloat($scope.newempsalary.total);
                    var balance=parseFloat(data);
                    $scope.newempsalary.prebalance=balance;
                    $scope.newempsalary.paid=total+balance;
                    $scope.newempsalary.balance=0;

                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
        }

    };

    $scope.open = function (p,size) {
        $http.get('/api/employee/'+ p.employees_id).
            success(function (data, status, headers, config) {
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewemployee',
                    controller:'viewemployeeController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
            }).error(function (data, status, headers, config) {
                console.log(data);
            });


    };


});
