app.controller('employeewageController', function($scope,$http,$filter,$modal,Project,Leader,Ledger,Wage){

    $scope.projects = [];
    $scope.leaders=[];
    $scope.wages=[];
    $scope.totalWage=0;
    $scope.totalPresent=0;
    $scope.totalAbsent=0;
    $scope.leaders_id=0;
    $scope.paid=0;
    $scope.balance=0;
    $scope.grandtotal=0;
    $scope.totalBalance=0;
    $scope.lists=[];
    $scope.wages=[];
    $scope.date=new Date();

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    Project.query(function(project){
        $scope.projects= project;
    });

    Wage.query(function(wage){
        $scope.wages= wage;
    });

    $scope.cashLedgers=[];
    Ledger.query(function(ledger){
        var length=ledger.length;
        $scope.cashLedgers=[];
        for(var i=0;i<length;i++){
            if(ledger[i].unders_id == '18' || ledger[i].unders_id == '28' || ledger[i].unders_id == '29' )
                $scope.cashLedgers.push(ledger[i]);
        }
    });
    $scope.ledgers_id=1;

    $scope.changeProject=function(thisProject){
            Leader.query({project_id: thisProject}, function (leader) {
                $scope.leaders = leader;
            });
    };

    $scope.searchWage=function(fromDate,toDate){
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');


        $http.get('/api/leader/'+ $scope.leaders_id).
            success(function (data, status, headers, config) {
                $http.get('/api/getWageAdvance',{params:{employees_id:data.employee_id}}).
                    success(function (data, status, headers, config) {
                        $scope.advance=data;
                        $http.get('/api/getwage/',{params:{fromDate:from,toDate:to,leaders_id:$scope.leaders_id}}).
                            success(function(data,status,headers,config){
                                $scope.wages=data;
                                getSum($scope.wages);
                            }).error(function(data,status,headers,config){
                                console.log(data);
                            });

                    }).error(function (data, status, headers, config) {
                        console.log(data);
                    });
            }).error(function (data, status, headers, config) {
                console.log(data);
            });


    };


    function getSum(wages){
        var wage= 0,present= 0,absent= 0,advance=0;
        var length=wages.length;

        for(var i=0;i<length;i++){
            wage+=parseFloat(wages[i].total);
            present+=parseFloat(wages[i].present);
            absent+=parseFloat(wages[i].absent);
        }

        $scope.totalWage=wage;
        $scope.totalPresent=present;
        $scope.totalAbsent=absent;
        $scope.paid=wage;
        advance=parseFloat($scope.advance);
        $scope.grandtotal=wage-advance;
        $scope.paid=wage-advance;
    }

    $scope.changeAdvance=function(){
        var advance=parseFloat($scope.advance);
        var wage=parseFloat($scope.totalWage);
        $scope.grandtotal=wage-advance;
        $scope.paid=wage-advance;
    };

   $scope.changeBalance=function(){
        var bal=parseFloat($scope.balance);
        var wage=parseFloat($scope.totalWage);
        var advance=parseFloat($scope.advance);
        $scope.paid=(wage-advance)-bal;
    };

    $scope.changePaid=function(){
        var paid=parseFloat($scope.paid);
        var wage=parseFloat($scope.totalWage);
        var advance=parseFloat($scope.advance);
        $scope.balance=(wage-advance)-paid;
    };

    $scope.saveWage=function(){

        if($scope.date.toISOString)
            $scope.date = $scope.date.toISOString();

        var emp={};
        var confirmDelete = confirm("Do you really need to Pay this Wage ?");
        if (confirmDelete) {
            $http.get('/api/leader/' + $scope.leaders_id).
                success(function (data, status, headers, config) {
                    emp = data;
                    $http.post('/api/wage', {
                        name: 'Wage Paid to ' + emp.employee.name,
                        description: "Wage paid",
                        amount: $scope.totalWage,
                        advance: $scope.advance,
                        payDate: $scope.date,
                        projects_id: $scope.projects_id,
                        employees_id: emp.employee.id,
                        ledgers_id:$scope.ledgers_id,
                        grandtotal: $scope.grandtotal,
                        paid: $scope.paid,
                        balance: $scope.balance
                    }).
                        success(function (data, status, headers, config) {
                            //console.log(data);
                            $scope.wages=[];
                            $scope.totalWage=0;
                            $scope.totalPresent=0;
                            $scope.totalAbsent=0;
                            $scope.leaders_id=0;
                            $scope.paid=0;
                            $scope.balance=0;
                            $scope.grandtotal=0;
                            alert('Wage Paid to ' + emp.employee.name + ' Successfully');
                        }).error(function (data, status, headers, config) {
                            console.log(data);
                        });
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
        }
    };

    function getWageBalanceList(){
        $http.get('/api/getWageBalanceList').
            success(function (data, status, headers, config) {
                $scope.lists=data;
                getWageBalanceSum($scope.lists);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    }getWageBalanceList();
    function getWageBalanceSum(advance){
        var sum=0;
        var length=advance.length;
        for(var i=0;i<length;i++){
            sum+=parseFloat(advance[i].balance);
        }
        $scope.totalBalance=sum;
    }

    $scope.open = function (p,size) {
        $http.get('/api/employee/'+ p.employees_id).
            success(function (data, status, headers, config) {
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewemployee',
                    controller:'viewemployeeController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
            }).error(function (data, status, headers, config) {
                console.log(data);
            });


    };

    $scope.deleteWage = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.wages.indexOf(item);
                $scope.wages.splice(curIndex, 1);
            });
        }
    };

});