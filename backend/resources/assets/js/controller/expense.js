app.controller('expenseController', function($scope,$http,$filter,$modal,Journal,Ledger){
	$scope.expenseedit = false;
	$scope.newexpense = {};
	$scope.curExpense = {};
	$scope.expenses = [];
    $scope.ledgers = [];
    $scope.allledgers = [];

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    $scope.debit='Debit';

    loadExpense();
    function loadExpense() {
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');

        Journal.query({fromDate:from,toDate:to},function(expense){
            $scope.expenses = [];
            var length = expense.length;
            for (var i = 0; i < length; i++) {
                if (expense[i].vouchertype === 'Expense')
                    $scope.expenses.push(expense[i]);
            }
            getTotalExpense($scope.expenses);
        });
    }

    Ledger.query(function(ledger){
        $scope.ledgers=[];
        $scope.allledgers=ledger;
        var length=ledger.length;
        for(var i=0;i<length;i++){
            if(ledger[i].unders_id == '18' || ledger[i].unders_id == '28' || ledger[i].unders_id == '29' )
                $scope.ledgers.push(ledger[i]);
        }

    });

    $scope.searchExpenseDate=function(fromDate,toDate) {

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        Journal.query({fromDate:from,toDate:to},function(expense){
            $scope.expenses = [];
            var length = expense.length;
            for (var i = 0; i < length; i++) {
                if (expense[i].vouchertype === 'Expense')
                    $scope.expenses.push(expense[i]);
            }
            getTotalExpense($scope.expenses);
        });
    };
	$scope.newExpense = function (argument) {
		$scope.expenseedit = true;
		$scope.newexpense = new Journal();
        $scope.newexpense.date = new Date();
        $scope.newexpense.crledgers_id=1;
		$scope.curExpense = {};
	};
	$scope.editExpense = function (thisExpense) {
		$scope.expenseedit = true;
		$scope.curExpense =  thisExpense;
		$scope.newexpense = angular.copy(thisExpense);
	};
	$scope.addExpense = function () {
        if( $scope.newexpense.date.toISOString)
            $scope.newexpense.date = $scope.newexpense.date.toISOString();

        $scope.newexpense.cramount=$scope.newexpense.dramount;
        $scope.newexpense.vouchertype='Expense';

        if($scope.newexpense.date.toISOString)
			$scope.newexpense.date = $scope.newexpense.date.toISOString();
		if ($scope.curExpense.id) {
			$scope.newexpense.$update(function(expense){
				angular.extend($scope.curExpense, $scope.curExpense, expense);
                loadExpense();
			});
		} else{
			$scope.newexpense.$save(function(expense){
				$scope.expenses.push(expense);
                loadExpense();
			});
		}
		$scope.expenseedit = false;
        $scope.newexpense = new Journal();
        $scope.newexpense.date = new Date();
	};
	$scope.deleteExpense = function (item) {
		var confirmDelete = confirm("Do you really need to delete the item ?");
		if (confirmDelete) {
			//TODO error handling
			item.$delete(function(){
				var curIndex = $scope.expenses.indexOf(item);
				$scope.expenses.splice(curIndex, 1);
                getTotalExpense($scope.expenses);
			});
		}
	};
	$scope.cancelExpense = function () {
		$scope.expenseedit = false;
        $scope.newexpense = new Journal();
        $scope.newexpense.date = new Date();
	};
    $scope.totalexpense=0;
    var getTotalExpense=function (expense) {
        var count=expense.length;
        var sum=0;
        for(var i=0;i<count;i++)
            sum+=parseFloat(expense[i].dramount);
        $scope.totalexpense=sum;
    }

    $scope.createLedger = function (combo) {
        var modalInstance = $modal.open({
            templateUrl: 'template/createledger',
            controller:'createledgerController'
            /*size: size,*/
            /*resolve: {
                item: function () {
                    return thisItem;
                }
            }*/

        });
        modalInstance.result.then(function(selectedItem){
            if(combo=='Dr')
                $scope.newexpense.drledgers_id=selectedItem.id;
            else if(combo=='Cr')
                $scope.newexpense.crledgers_id=selectedItem.id;

        }, function(){
            Ledger.query(function(ledger){
                $scope.ledgers=[];
                $scope.allledgers=ledger;
                var length=ledger.length;
                for(var i=0;i<length;i++){
                    if(ledger[i].type==='Expense')
                        $scope.ledgers.push(ledger[i]);
                }

            });
        });

    };

});