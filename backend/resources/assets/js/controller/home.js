app.controller('homecontroller', function($scope,$http,UserInfo){
    /* paggination */
    //TODO factory
    $scope.extra=false;
    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();
    };



    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
	$scope.maxSize = 5;
	/* Nav menu */
    $scope.user = {};

    UserInfo.query().success(function(data){
        $scope.user = data;
        console.log(data);
    });





    $scope.exportToExcel=function(tableId){ // ex: '#my-table'
        var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j=0;
        tab = document.getElementById(tableId); // id of table

        for(j = 0 ; j < tab.rows.length ; j++)
        {
            tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text=tab_text+"</table>";
        tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
        tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html","replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
        }
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);
    }

	//// Toggle side menu
	$scope.menuStatus = true;
	$scope.toggleMenu = function(){
		$scope.menuStatus = $scope.menuStatus === false ? true : false;
	};

    $scope.showExtra=function(){
        if($scope.extra==false)
            $scope.extra=true;
        else
            $scope.extra=false;
    }

    $scope.showMenu=true;
});


app.directive('clickAnywhereButHere', function($document){
    return {
        restrict: 'A',
        link: function(scope, elem, attr, ctrl) {
            elem.bind('click', function(e) {
                // this part keeps it from firing the click on the document.
                e.stopPropagation();
            });
            $document.bind('click', function() {
                // magic here.
                scope.$apply(attr.clickAnywhereButHere);
            })
        }
    }
})