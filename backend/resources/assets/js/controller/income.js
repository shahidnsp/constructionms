app.controller('incomeController', function($scope,$http,$filter,$modal,Journal,Ledger){
	$scope.incomeedit = false;
	$scope.newincome = {};
	$scope.curIncome = {};
	$scope.incomes = [];
    $scope.totalincome=0;
    $scope.ledgers = [];
    $scope.allledgers=[];

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    Ledger.query(function(ledger){
       // $scope.ledgers=ledger;
        $scope.allledgers=ledger;
        var length=ledger.length;
        for(var i=0;i<length;i++){
            if(ledger[i].unders_id == '18' || ledger[i].unders_id == '28' || ledger[i].unders_id == '29' )
                $scope.ledgers.push(ledger[i]);
        }

    });

    loadIncome();
    function loadIncome() {
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');

        Journal.query({fromDate:from,toDate:to},function(income){
            $scope.incomes = [];
            var length = income.length;
            for (var i = 0; i < length; i++) {
                if (income[i].vouchertype === 'Income')
                    $scope.incomes.push(income[i]);
            }
            getTotalIncome($scope.incomes);
        });
    }

    $scope.searchIncomeDate=function(fromDate,toDate) {

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        Journal.query({fromDate:from,toDate:to},function(income){
            $scope.incomes = [];
            var length = income.length;
            for (var i = 0; i < length; i++) {
                if (income[i].vouchertype === 'Income')
                    $scope.incomes.push(income[i]);
            }
            getTotalIncome($scope.incomes);
        });
    };

	$scope.newIncome = function (argument) {
		$scope.incomeedit = true;
		$scope.newincome = new Journal();
        $scope.newincome.drledgers_id=1;
		$scope.newincome.date = new Date();
	};
	$scope.editIncome = function (thisIncome) {
		$scope.incomeedit = true;
		$scope.curIncome =  thisIncome;
		$scope.newincome = angular.copy(thisIncome);
	};
	$scope.addIncome = function () {
        if( $scope.newincome.date.toISOString)
            $scope.newincome.date = $scope.newincome.date.toISOString();

        $scope.newincome.cramount=$scope.newincome.dramount;
        $scope.newincome.vouchertype='Income';

		if ($scope.curIncome.id) {
			$scope.newincome.$update(function(income){
				angular.extend($scope.curIncome, $scope.curIncome, income);
                loadIncome();
			});
		} else{
			$scope.newincome.$save(function(income){
				$scope.incomes.push(income);
                loadIncome();
			});
		}
		$scope.incomeedit = false;
		$scope.newincome = new Journal();
	};
	$scope.deleteIncome = function (item) {
		var confirmDelete = confirm("Do you really need to delete the item ?");
		if (confirmDelete) {
			//TODO error handling
			item.$delete(function(){
				var curIndex = $scope.incomes.indexOf(item);
				$scope.incomes.splice(curIndex, 1);
                getTotalIncome($scope.incomes);
			});
		}
	};
	$scope.cancelIncome = function () {
		$scope.incomeedit = false;
		$scope.newincome = new Journal();
	};

    var getTotalIncome=function (incomes) {
        var count=incomes.length;
        var sum=0;
        for(var i=0;i<count;i++)
            sum+=parseFloat(incomes[i].dramount);
        $scope.totalincome=sum;
    }

    $scope.createLedger = function (combo) {
        var modalInstance = $modal.open({
            templateUrl: 'template/createledger',
            controller:'createledgerController'
            /*size: size,*/
            /*resolve: {
             item: function () {
             return thisItem;
             }
             }*/

        });
        modalInstance.result.then(function(selectedItem){
            if(combo=='Dr')
                $scope.newincome.drledgers_id=selectedItem.id;
            else if(combo=='Cr')
                $scope.newincome.crledgers_id=selectedItem.id;

        }, function(){
            Ledger.query(function(ledger){
                $scope.ledgers=ledger;
                $scope.allledgers=ledger;
                /*var length=ledger.length;
                for(var i=0;i<length;i++){
                    if(ledger[i].type !='Income')
                        $scope.ledgers.push(ledger[i]);
                }*/

            });
        });
    };

});