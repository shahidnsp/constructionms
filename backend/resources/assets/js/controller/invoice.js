app.controller('invoiceController', function($scope){
	$scope.invoicebill = {
		id: 12, invoicefrom: '10-November-2014', invoicedate: '12-December-2014', status:'pending', rent: '1200',
		invoiceprice:[
			{item: 'Rent', price: 1200},
		    {item: 'Mess', price: 50.56},
		    {item: 'Broadband', price: 25.00},
		    {item: 'Electricity', price: 65.87},
		    {item: 'Gas', price: 55.80}
		],
		contribution: 148.62
	};
});