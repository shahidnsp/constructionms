app.controller('journalController', function($scope,$http,$filter,Journal,$modal,Ledger){

    //Journal Voucher Section
    $scope.journaledit = false;
    $scope.newjournal = {};
    $scope.curJournal = {};
    $scope.journals = [];
    $scope.ledgers = [];

    $scope.fromDate='';
    $scope.toDate='';

    $scope.totalJournalDr=0;
    $scope.totalJournalCr=0;
    $scope.totalPaymentDr=0;
    $scope.totalPaymentCr=0;
    $scope.totalReceiptDr=0;
    $scope.totalReceiptCr=0;
    $scope.totalCreditDr=0;
    $scope.totalCreditCr=0;
    $scope.totalDebitDr=0;
    $scope.totalDebitCr=0;
    $scope.cashLedgers=[];
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();

    if(month<5) {
        $scope.fromDate = new Date(year-1, 5, 1);
        $scope.toDate = new Date(year, 2, 31);
    }else{
        $scope.fromDate = new Date(year, 5, 1);
        $scope.toDate = new Date(year+1, 2, 31);
    }



    loadJournal();
    function loadJournal(){
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');

        $scope.journals = [];
        Journal.query({fromDate:from,toDate:to},function(journal){
            $scope.journals = [];
            var length = journal.length;
            for (var i = 0; i < length; i++) {
                if (journal[i].vouchertype==='Journal')
                    $scope.journals.push(journal[i]);
            }
            getTotalJournal($scope.journals);
        });
    }

    function getTotalJournal(items){
        var length=items.length;
        var dramt=0,cramt=0;
        for(var i=0;i<length;i++){
            dramt+=parseFloat(items[i].dramount);
            cramt+=parseFloat(items[i].cramount);
        }
        $scope.totalJournalDr=dramt;
        $scope.totalJournalCr=cramt;
    }

    $scope.searchJournalDate=function(fromDate,toDate) {

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        Journal.query({fromDate:from,toDate:to},function(journal){
            $scope.journals = [];
            var length = journal.length;
            for (var i = 0; i < length; i++) {
                if (journal[i].vouchertype==='Journal')
                    $scope.journals.push(journal[i]);
            }
            getTotalJournal($scope.journals);
        });
    };

    Ledger.query(function(ledger){
        $scope.ledgers = ledger;
        var length=ledger.length;
        $scope.cashLedgers=[];
        for(var i=0;i<length;i++){
            if(ledger[i].unders_id == '18' || ledger[i].unders_id == '28' || ledger[i].unders_id == '29' )
                $scope.cashLedgers.push(ledger[i]);
        }
    });



    $scope.newJournal = function () {
        $scope.journaledit = true;
        $scope.newjournal = new Journal();
        $scope.newjournal.date=new Date();
        $scope.newjournal.vouchertype='Journal';
        $scope.curJournal = {};
    };
    $scope.editJournal = function (thisJournal) {
        $scope.journaledit = true;
        $scope.curJournal =  thisJournal;
        $scope.newjournal = angular.copy(thisJournal);
    };
    $scope.addJournal = function () {

        if( $scope.newjournal.date.toISOString)
            $scope.newjournal.date = $scope.newjournal.date.toISOString();

        if ($scope.curJournal.id) {
            $scope.newjournal.$update(function(journal){
                angular.extend($scope.curJournal, $scope.curJournal, journal);
                loadJournal();
            });
        } else{
            $scope.newjournal.$save(function(journal){
                $scope.journals.push(journal);
                loadJournal();
            });
        }
        $scope.journaledit = false;
        $scope.newjournal = new Journal();

    };
    $scope.deleteJournal = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.journals.indexOf(item);
                $scope.journals.splice(curIndex, 1);
                loadJournal();
            });
        }
    };
    $scope.cancelJournal = function () {
        $scope.journaledit = false;
        $scope.newjournal = new Journal();
    };

    $scope.changeJournalAmountDr=function(){
        $scope.newjournal.cramount=$scope.newjournal.dramount;
    };

    $scope.changeJournalAmountCr=function(){
        $scope.newjournal.dramount=$scope.newjournal.cramount;
    };

    //Payment Voucher Section
    $scope.paymentedit = false;
    $scope.newpayment = {};
    $scope.curPayment = {};
    $scope.payments = [];
    //$scope.ledgers = [];

    loadPayment();
    function loadPayment(){
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');
        $scope.payments = [];
        Journal.query({fromDate:from,toDate:to},function(payment){
            var length=payment.length;
            for(var i=0;i<length;i++){
                if(payment[i].vouchertype==='Payment')
                    $scope.payments.push(payment[i]);
            }
            getTotalPayment($scope.payments);
        });
    }

    function getTotalPayment(items){
        var length=items.length;
        var dramt=0,cramt=0;
        for(var i=0;i<length;i++){
            dramt+=parseFloat(items[i].dramount);
            cramt+=parseFloat(items[i].cramount);
        }
        $scope.totalPaymentDr=dramt;
        $scope.totalPaymentCr=cramt;
    }

    $scope.searchPaymentDate=function(fromDate,toDate) {
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');
        $scope.payments = [];
        Journal.query({fromDate:from,toDate:to},function(payment){
            var length=payment.length;
            for(var i=0;i<length;i++){
                if(payment[i].vouchertype==='Payment')
                    $scope.payments.push(payment[i]);
            }
            getTotalPayment($scope.payments);
        });
    };

   /* Ledger.query(function(ledger){
        $scope.ledgers = ledger;
    });*/
    $scope.newPayment = function () {
        $scope.paymentedit = true;
        $scope.newpayment = new Journal();
        $scope.newpayment.date=new Date();
        $scope.newpayment.vouchertype='Payment';
        $scope.newpayment.crledgers_id='3';
        $scope.curPayment = {};
    };
    $scope.editPayment = function (thisPayment) {
        $scope.paymentedit = true;
        $scope.curPayment =  thisPayment;
        $scope.newpayment = angular.copy(thisPayment);
    };
    $scope.addPayment = function () {
        if( $scope.newpayment.date.toISOString)
            $scope.newpayment.date = $scope.newpayment.date.toISOString();

        if ($scope.curPayment.id) {
            $scope.newpayment.$update(function(payment){
                angular.extend($scope.curPayment, $scope.curPayment, payment);
                loadPayment();
            });
        } else{
            $scope.newpayment.$save(function(payment){
                $scope.payments.push(payment);
                loadPayment();
            });
        }
        $scope.paymentedit = false;
        $scope.newpayment = new Journal();

    };
    $scope.deletePayment = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.payments.indexOf(item);
                $scope.payments.splice(curIndex, 1);
                loadPayment();
            });
        }
    };
    $scope.cancelPayment = function () {
        $scope.paymentedit = false;
        $scope.newpayment = new Journal();
    };

    $scope.changePaymentAmountDr=function(){
        $scope.newpayment.cramount=$scope.newpayment.dramount;
    };

    $scope.changePaymentAmountCr=function(){
        $scope.newpayment.dramount=$scope.newpayment.cramount;
    };


    //Receipt Voucher Section
    $scope.receiptedit = false;
    $scope.newreceipt = {};
    $scope.curReceipt = {};
    $scope.receipts = [];
    //$scope.ledgers = [];

    loadReceipt();
    function loadReceipt(){
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');
        $scope.receipts = [];
        Journal.query({fromDate:from,toDate:to},function(receipt){
            var length=receipt.length;
            for(var i=0;i<length;i++){
                if(receipt[i].vouchertype==='Receipt')
                    $scope.receipts.push(receipt[i]);
            }

            getTotalReceipt($scope.receipts);
        });
    }

    function getTotalReceipt(items){
        var length=items.length;
        var dramt=0,cramt=0;
        for(var i=0;i<length;i++){
            dramt+=parseFloat(items[i].dramount);
            cramt+=parseFloat(items[i].cramount);
        }
        $scope.totalReceiptDr=dramt;
        $scope.totalReceiptCr=cramt;
    }

    $scope.searchReceiptDate=function(fromDate,toDate) {
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');
        $scope.receipts = [];
        Journal.query({fromDate:from,toDate:to},function(receipt){
            var length=receipt.length;
            for(var i=0;i<length;i++){
                if(receipt[i].vouchertype==='Receipt')
                    $scope.receipts.push(receipt[i]);
            }
            getTotalReceipt($scope.receipts);
        });
    };


    /* Ledger.query(function(ledger){
     $scope.ledgers = ledger;
     });*/
    $scope.newReceipt = function () {
        $scope.receiptedit = true;
        $scope.newreceipt = new Journal();
        $scope.newreceipt.date=new Date();
        $scope.newreceipt.vouchertype='Receipt';
        $scope.newreceipt.drledgers_id='3';
        $scope.curReceipt = {};
    };
    $scope.editReceipt = function (thisReceipt) {
        $scope.receiptedit = true;
        $scope.curReceipt =  thisReceipt;
        $scope.newreceipt = angular.copy(thisReceipt);
    };
    $scope.addReceipt = function () {
        if( $scope.newreceipt.date.toISOString)
            $scope.newreceipt.date = $scope.newreceipt.date.toISOString();

        if ($scope.curReceipt.id) {
            $scope.newreceipt.$update(function(receipt){
                angular.extend($scope.curReceipt, $scope.curReceipt, receipt);
                loadReceipt();
            });
        } else{
            $scope.newreceipt.$save(function(receipt){
                $scope.receipts.push(receipt);
                loadReceipt();
            });
        }
        $scope.receiptedit = false;
        $scope.newreceipt = new Journal();

    };
    $scope.deleteReceipt = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.receipts.indexOf(item);
                $scope.receipts.splice(curIndex, 1);
                loadReceipt();
            });
        }
    };
    $scope.cancelReceipt = function () {
        $scope.receiptedit = false;
        $scope.newreceipt = new Journal();
    };

    $scope.changeReceiptAmountDr=function(){
        $scope.newreceipt.cramount=$scope.newreceipt.dramount;
    };

    $scope.changeReceiptAmountCr=function(){
        $scope.newreceipt.dramount=$scope.newreceipt.cramount;
    };


    //Credit Note Voucher Section
    $scope.creditedit = false;
    $scope.newcredit = {};
    $scope.curCredit = {};
    $scope.credits = [];
    //$scope.ledgers = [];

    loadCredit();
    function loadCredit(){
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');
        $scope.credits = [];
        Journal.query({fromDate:from,toDate:to},function(credit){
            var length=credit.length;
            for(var i=0;i<length;i++){
                if(credit[i].vouchertype==='Credit Note')
                    $scope.credits.push(credit[i]);
            }
            getTotalCredit($scope.credits);
        });
    }
    function getTotalCredit(items){
        var length=items.length;
        var dramt=0,cramt=0;
        for(var i=0;i<length;i++){
            dramt+=parseFloat(items[i].dramount);
            cramt+=parseFloat(items[i].cramount);
        }
        $scope.totalCreditDr=dramt;
        $scope.totalCreditCr=cramt;
    }
    $scope.searchCreditDate=function(fromDate,toDate) {
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');
        $scope.credits = [];
        Journal.query({fromDate:from,toDate:to},function(credit){
            var length=credit.length;
            for(var i=0;i<length;i++){
                if(credit[i].vouchertype==='Credit Note')
                    $scope.credits.push(credit[i]);
            }
            getTotalCredit($scope.credits);
        });
    };

    /* Ledger.query(function(ledger){
     $scope.ledgers = ledger;
     });*/
    $scope.newCredit = function () {
        $scope.creditedit = true;
        $scope.newcredit = new Journal();
        $scope.newcredit.date=new Date();
        $scope.newcredit.vouchertype='Credit Note';
        $scope.curCredit = {};
    };
    $scope.editCredit = function (thisCredit) {
        $scope.creditedit = true;
        $scope.curCredit =  thisCredit;
        $scope.newcredit = angular.copy(thisCredit);
    };
    $scope.addCredit = function () {

        if( $scope.newcredit.date.toISOString)
            $scope.newcredit.date = $scope.newcredit.date.toISOString();

        if ($scope.curCredit.id) {
            $scope.newcredit.$update(function(credit){
                angular.extend($scope.curCredit, $scope.curCredit, credit);
                loadCredit();
            });
        } else{
            $scope.newcredit.$save(function(credit){
                $scope.credits.push(credit);
                loadCredit();
            });
        }
        $scope.creditedit = false;
        $scope.newcredit = new Journal();

    };
    $scope.deleteCredit = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.credits.indexOf(item);
                $scope.credits.splice(curIndex, 1);
                loadCredit();
            });
        }
    };
    $scope.cancelCredit = function () {
        $scope.creditedit = false;
        $scope.newcredit = new Journal();
    };

    $scope.changeCreditAmountDr=function(){
        $scope.newcredit.cramount=$scope.newcredit.dramount;
    };

    $scope.changeCreditAmountCr=function(){
        $scope.newcredit.dramount=$scope.newcredit.cramount;
    };


    //Debit Note Voucher Section
    $scope.debitedit = false;
    $scope.newdebit = {};
    $scope.curDebit = {};
    $scope.debits = [];
    //$scope.ledgers = [];

    loadDebit();
    function loadDebit(){
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');
        $scope.debits = [];
        Journal.query({fromDate:from,toDate:to},function(debit){
            var length=debit.length;
            for(var i=0;i<length;i++){
                if(debit[i].vouchertype==='Debit Note')
                    $scope.debits.push(debit[i]);
            }
            getTotalDebit($scope.debits);
        });
    }

    function getTotalDebit(items){
        var length=items.length;
        var dramt=0,cramt=0;
        for(var i=0;i<length;i++){
            dramt+=parseFloat(items[i].dramount);
            cramt+=parseFloat(items[i].cramount);
        }
        $scope.totalDebitDr=dramt;
        $scope.totalDebitCr=cramt;
    }

    $scope.searchDebitDate=function(fromDate,toDate) {
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');
        $scope.debits = [];
        Journal.query({fromDate:from,toDate:to},function(debit){
            var length=debit.length;
            for(var i=0;i<length;i++){
                if(debit[i].vouchertype==='Debit Note')
                    $scope.debits.push(debit[i]);
            }
            getTotalDebit($scope.debits);
        });
    };

    /* Ledger.query(function(ledger){
     $scope.ledgers = ledger;
     });*/
    $scope.newDebit = function () {
        $scope.debitedit = true;
        $scope.newdebit = new Journal();
        $scope.newdebit.date=new Date();
        $scope.newdebit.vouchertype='Debit Note';
        $scope.curDebit = {};
    };
    $scope.editDebit = function (thisDebit) {
        $scope.debitedit = true;
        $scope.curDebit =  thisDebit;
        $scope.newdebit = angular.copy(thisDebit);
    };
    $scope.addDebit = function () {

        if( $scope.newdebit.date.toISOString)
            $scope.newdebit.date = $scope.newdebit.date.toISOString();

        if ($scope.curDebit.id) {
            $scope.newdebit.$update(function(debit){
                angular.extend($scope.curDebit, $scope.curDebit, debit);
                loadDebit();
            });
        } else{
            $scope.newdebit.$save(function(debit){
                $scope.debits.push(debit);
                loadDebit();
            });
        }
        $scope.debitedit = false;
        $scope.newdebit = new Journal();

    };
    $scope.deleteDebit = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.debits.indexOf(item);
                $scope.debits.splice(curIndex, 1);
                loadDebit();
            });
        }
    };
    $scope.cancelDebit = function () {
        $scope.debitedit = false;
        $scope.newdebit = new Journal();
    };

    $scope.changeDebitAmountDr=function(){
        $scope.newdebit.cramount=$scope.newdebit.dramount;
    };

    $scope.changeDebitAmountCr=function(){
        $scope.newdebit.dramount=$scope.newdebit.cramount;
    };

    $scope.createLedger = function (combo) {
        var modalInstance = $modal.open({
            templateUrl: 'template/createledger',
            controller:'createledgerController'
            /*size: size,*/
            /*resolve: {
             item: function () {
             return thisItem;
             }
             }*/

        });
        modalInstance.result.then(function(selectedItem){

           /* Ledger.query(function(ledger){
                $scope.ledgers = ledger;
                var length=ledger.length;
                $scope.cashLedgers=[];
                for(var i=0;i<length;i++){
                    if(ledger[i].unders_id == '18' || ledger[i].unders_id == '28' || ledger[i].unders_id == '29' )
                        $scope.cashLedgers.push(ledger[i]);
                }
            });*/

            if(combo=='JournalDr')
                $scope.newjournal.drledgers_id=selectedItem.id;
            else if(combo=='JournalCr')
                $scope.newjournal.crledgers_id=selectedItem.id;
            else if(combo=='PaymentDr')
                $scope.newpayment.drledgers_id=selectedItem.id;
            else if(combo=='PaymentCr')
                $scope.newpayment.crledgers_id=selectedItem.id;
            else if(combo=='ReceiptDr')
                $scope.newreceipt.drledgers_id=selectedItem.id;
            else if(combo=='ReceiptCr')
                $scope.newreceipt.crledgers_id=selectedItem.id;
            else if(combo=='CreditDr')
                $scope.newcredit.drledgers_id=selectedItem.id;
            else if(combo=='CreditCr')
                $scope.newcredit.crledgers_id=selectedItem.id;
            else if(combo=='DebitDr')
                $scope.newdebit.drledgers_id=selectedItem.id;
            else if(combo=='DebitCr')
                $scope.newdebit.crledgers_id=selectedItem.id;

        }, function(){
            Ledger.query(function(ledger){
                $scope.ledgers = ledger;
                var length=ledger.length;
                $scope.cashLedgers=[];
                for(var i=0;i<length;i++){
                    if(ledger[i].unders_id == '18' || ledger[i].unders_id == '28' || ledger[i].unders_id == '29' )
                        $scope.cashLedgers.push(ledger[i]);
                }
            });
        });

    };
});

app.controller('createledgerController', function($scope, $http,$modalInstance,$modal, Ledger, Under,UserInfo) {
    $scope.ledgerEdit = false;
    $scope.newledger = {};
    $scope.curLedger = {};
    $scope.ledgers = [];
    $scope.unders=[];

    UserInfo.query().success(function(data){
        $scope.user = data;
        //console.log(data);
    });

    $scope.close = function () {
        $modalInstance.dismiss('Close');
    };

    $scope.select = function (item) {
        $modalInstance.close(item);
    };

    loadLedger();

    function loadLedger(){
        Ledger.query(function (ledger) {
            $scope.ledgers = ledger;
        });
    }

    Under.query(function(under) {
        $scope.unders = under;
    })

    $scope.newLedger = function() {
        $scope.ledgerEdit = true;
        $scope.newledger = new Ledger();
        $scope.newledger.crOrDr='Dr';
        $scope.curLedger = {};
    };

    $scope.editLedger  = function(thisLedger) {
        $scope.ledgerEdit = true;
        $scope.curLedger = thisLedger;
        $scope.newledger = angular.copy(thisLedger);

    };

    $scope.addLedger = function() {

        if ($scope.curLedger.id) {
            $scope.newledger.$update(function (ledger) {
                angular.extend($scope.curLedger, $scope.curLedger, ledger);
                loadLedger();
            });
        }else{
            $scope.newledger.$save(function (ledger) {
                $scope.ledgers.push(ledger);
                loadLedger();
            });
        }
        $scope.ledgerEdit = false;
        $scope.newledger = new Ledger();

    };

    $scope.deleteLedger = function(item) {
        var confirmdelete = confirm('Do you really need to delete the items ?');
        if (confirmdelete) {
            item.$delete(function () {
                var curIndex = $scope.ledgers.indexOf(item);
                $scope.ledgers.splice(curIndex, 1);
            });
        }
    };

    $scope.cancel =function() {
        $scope.ledgerEdit = false;
        $scope.newledger = new Ledger();
    };

    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();
    };

    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
    $scope.maxSize = 5;
});