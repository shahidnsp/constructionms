/**
 * Created by shahi on 01/06/16.
 */
app.controller('journalreportController', function($scope,$http,$filter,Journal){
    $scope.journals = [];

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    loadJournal();
    function loadJournal(){
        Journal.query(function(journal){
            $scope.journals = journal;
        });
    }

    $scope.searchJournalDate=function(fromDate,toDate) {
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

       /* $http.get('/api/getjournalwithdate',{params:{'fromDate':from,'toDate':to}})
            .then(function(response){
                $scope.journals=response.data;
            });*/
        Journal.query({'fromDate':from,'toDate':to},function(journal){
            $scope.journals = journal;
        });
    };

    $scope.deleteJournal = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.journals.indexOf(item);
                $scope.journals.splice(curIndex, 1);
            });
        }
    };
});



