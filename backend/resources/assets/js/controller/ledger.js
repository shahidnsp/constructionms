/**
 * Created by noushid on 28/6/16.
 */

app.controller('ledgerController', function($scope, $http, Ledger, Under) {
    $scope.ledgerEdit = false;
    $scope.newledger = {};
    $scope.curLedger = {};
    $scope.ledgers = [];
    $scope.unders=[];
    $scope.showBalance=true;
    $scope.bank=false;
    $scope.secondary=false;

    loadLedger();

    function loadLedger(){
        Ledger.query(function (ledger) {
            $scope.ledgers = ledger;
        });
    }

    $scope.accountLedgerForBank=function(under){
        $http.get('/api/accountLedgerForBank',{params:{id:under}}).
            success(function (data, status, headers, config) {
                if(data.bank=='true')
                    $scope.bank=true;
                else
                    $scope.bank=false;

                if(data.sec=='true')
                    $scope.secondary=true;
                else
                    $scope.secondary=false;
            }).error(function (data, status, headers, config) {
                $scope.bank=false;
                $scope.secondary=false;
            });
    };

    Under.query(function(under) {
        $scope.unders = under;
    });


    $scope.newLedger = function() {
        $scope.ledgerEdit = true;
        $scope.newledger = new Ledger();
        $scope.newledger.paymentmode='Debit';
        $scope.newledger.date = new Date();
        $scope.curLedger = {};
    };

    $scope.editLedger  = function(thisLedger) {
        $scope.ledgerEdit = true;
        $scope.curLedger = thisLedger;
        $scope.newledger = angular.copy(thisLedger);
        $scope.accountLedgerForBank(thisLedger.unders_id);
    };

    $scope.addLedger = function() {

        if( $scope.newledger.date.toISOString)
            $scope.newledger.date = $scope.newledger.date.toISOString();

        if ($scope.curLedger.id) {
            $scope.newledger.$update(function (ledger) {
                angular.extend($scope.curLedger, $scope.curLedger, ledger);
            });
        }else{
            $scope.newledger.$save(function (ledger) {
                $scope.ledgers.push(ledger);
            });
        }
        $scope.ledgerEdit = false;
        $scope.newledger = new Ledger();
        loadLedger();
    };

    $scope.deleteLedger = function(item) {
        var confirmdelete = confirm('Do you really need to delete the items ?');
        if (confirmdelete) {
            item.$delete(function () {
                var curIndex = $scope.ledgers.indexOf(item);
                $scope.ledgers.splice(curIndex, 1);
            });
        }
        loadLedger();
    };

    $scope.cancel =function() {
        $scope.ledgerEdit = false;
        $scope.newledger = new Ledger();
    };
})
