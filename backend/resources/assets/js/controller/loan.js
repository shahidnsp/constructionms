app.controller('loanController', function($scope,$http,Loan,Bank,Emi,LoanExpense){
	$scope.loanedit = false;
	$scope.curloan = {};
	$scope.emiedit = false;
	$scope.curemi = {};
	$scope.breadCrumbs = [];
	$scope.openDiv = null;
    $scope.myLoan = {};
    $scope.fromDate=new Date();
    $scope.toDate=new Date();
    $scope.totalExpense=0;

    //Loan Expense
    $scope.loanExpEdit = false;
    $scope.newloanExp = {};
    $scope.curloanExp = {};
    $scope.loanExpenses = [];

    $scope.switchDiv = function () {
		$scope.breadCrumbs = [];
		$scope.openDiv = null;
	};


    //Banks
    Bank.query(function (banks) {
        $scope.banks = banks;
    });

	// Loans
    Loan.query(function (loans) {
        $scope.loans = loans;
        getTotalLoan($scope.loans);
    });


    $scope.newloan = new Loan();
	$scope.newLoan = function(){
		$scope.loanedit = true;
		$scope.newloan = new Loan();
        $scope.newloan.date = new Date();
        $scope.newloan.closedate = new Date();
	};

	$scope.editLoan = function (thisloan) {
  	$scope.loanedit = true;
  	$scope.curloan = thisloan;
    $scope.newloan = angular.copy(thisloan);
	};

	$scope.addLoan = function(){
        if($scope.newloan.date.toISOString)
			$scope.newloan.date = $scope.newloan.date.toISOString();
        if($scope.newloan.closedate.toISOString)
            $scope.newloan.date = $scope.newloan.closedate.toISOString();
        //TODO error handling
		if ($scope.curloan.id) {
            //Update
            $scope.newloan.$update(function (loan) {
                angular.extend($scope.curloan,$scope.curloan,loan);
                $scope.curloan = {};
                getTotalLoan($scope.loans);
            });
		}else{
            //New

            $scope.newloan.$save(function (loan) {
                $scope.loans.push(loan);
                getTotalLoan($scope.loans);
            });
        }
        $scope.newloan = new Loan();
        $scope.newloan.date = new Date();
        $scope.newloan.closedate = new Date();
		$scope.loanedit = false;
	};
    
	$scope.deleteLoan = function (item) {
		var confirmDelete = confirm("Do you really need to delete " + item.name + "?");
		if (confirmDelete) {
            var bankIdx = $scope.loans.indexOf(item);
            //TODO error handling
            item.$delete(function (item) {
                $scope.loans.splice(bankIdx, 1);
                getTotalLoan($scope.loans);
            });
		}
	};
	$scope.cancelLoan = function () {
		$scope.loanedit = false;
		$scope.curloan = {};
	};

    $scope.totalloan=0;
    $scope.totalpaid=0;
    $scope.totaldue=0;

    var getTotalLoan=function (loans) {
        var count=loans.length;
        var loan=0;
        var paid=0;
        var due=0;
        for(var i=0;i<count;i++) {
            loan +=parseFloat(loans[i].amount);
            paid +=parseFloat(loans[i].paid);
            due +=parseFloat(loans[i].balance);
        }
        $scope.totalloan=loan;
        $scope.totalpaid=paid;
        $scope.totaldue=due;
    }

	$scope.newemi = new Emi();

    $scope.openEmi = function (loan) {
        $scope.myEmi = {};
        $scope.openDiv = 'emi';
        $scope.myLoan = loan;
        Emi.query({loan_id:loan.id},function (emi) {
            $scope.emis = emi;
            getTotalEmi($scope.emis);
        });
        //$scope.emis= Emi.query({loan_id:loan.id});
        $scope.breadCrumbs.push("home", $scope.myLoan.name);
    };

    $scope.totalemi=0;
    var getTotalEmi=function (emi) {
        var count=emi.length;
        var sum=0;
        for(var i=0;i<count;i++) {
            sum +=parseFloat(emi[i].amount);
        }
        $scope.totalemi=sum;
    }

    var getTotalExpense=function (expense) {
        var count=expense.length;
        var sum=0;
        for(var i=0;i<count;i++) {
            sum +=parseFloat(expense[i].amount);
        }
        $scope.totalExpense=sum;
    }

	$scope.newEmi = function(){
		$scope.emiedit = true;
        $scope.curemi = {};
		$scope.newemi = new Emi();
        $scope.newemi.date = new Date();
    };
	$scope.editEmi = function (thisemi) {
  	$scope.emiedit = true;
  	$scope.curemi = thisemi;
    $scope.newemi = angular.copy(thisemi);
	};
	$scope.addEmi = function(){

        if($scope.newemi.date.toISOString)
			$scope.newemi.date = $scope.newemi.date.toISOString();
		if ($scope.curemi.id) {

            //TODO error handling
            $scope.newemi.$update(function (emi) {
                angular.extend($scope.curemi,$scope.curemi,emi);
                getTotalEmi($scope.emis);
            });

		}else{
            $scope.newemi.loan_id = $scope.myLoan.id;

            $scope.newemi.$save(function (emi) {
                $scope.emis.push(emi);
                getTotalEmi($scope.emis);
            })
		}
        $scope.newemi = new Emi();
        $scope.newemi.date = new Date();
		$scope.emiedit = false;
	};

	$scope.deleteEmi = function (item) {
		var confirmDelete = confirm("Do you really need to delete ?");
		if (confirmDelete) {
            item.$delete(function(){
                var bank = $scope.emis.indexOf(item);
                $scope.emis.splice(bank, 1);
                getTotalEmi($scope.emis);
            });
        }
	};
	$scope.cancelEmi = function () {
		$scope.emiedit = false;
		$scope.curemi = {};
	};




    /*Loan Expense*/


    $scope.openExpense = function () {
        $scope.myExp = {};
        LoanExpense.query({loan_id:$scope.myLoan.id},function (expense) {
            $scope.loanExpenses = expense;
            getTotalExpense($scope.loanExpenses);
        });
        //$scope.loanExpenses= LoanExpense.query({loan_id:$scope.myLoan.id});
    };



    $scope.newLoanExp = function() {
        $scope.loanExpEdit = true;
        $scope.newloanExp = new LoanExpense();
        $scope.newloanExp.date = new Date();
        $scope.curloanExp = {};
    };

    $scope.editLoanExp = function (thisLoanexp) {
        $scope.loanExpEdit = true;
        $scope.curloanExp = thisLoanexp;
        $scope.newloanExp = angular.copy(thisLoanexp);
    };

    $scope.addLoanExp = function(){
        if ($scope.curloanExp.id) {
            $scope.newloanExp.$update(function (loanexpense) {
                angular.extend($scope.curloanExp, $scope.curloanExp, loanexpense);
                getTotalExpense($scope.loanExpenses);
            });
        }
        else{
            $scope.newloanExp.loan_id = $scope.myLoan.id;

            $scope.newloanExp.$save(function (loanexpense) {
                $scope.loanExpenses.push(loanexpense);
                getTotalExpense($scope.loanExpenses);
            });
        }
        $scope.loanExpEdit = false;
        //$scope.newloanExp = new LoanExpense();
        $scope.openExpense();
    }

    $scope.deleteLoanExp = function(item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if(confirmDelete) {
            item.$delete(function () {
                var curIndex = $scope.loanExpenses.indexOf(item);
                $scope.loanExpenses.splice(curIndex, 1);
                getTotalExpense($scope.loanExpenses);
            });
        }
        $scope.openExpense();
    };

    $scope.cancelLoanExp = function() {
        $scope.loanExpEdit = false;
        $scope.newloanExp = new LoanExpense();
    };

    $scope.loanBal = function () {
        var amount = ($scope.newloan.amount) ? parseFloat($scope.newloan.amount) : 0;
        var paid = ($scope.newloan.paid) ? parseFloat($scope.newloan.paid) : 0;
        var balance = amount - paid;
        $scope.newloan.balance = balance;
    };


});