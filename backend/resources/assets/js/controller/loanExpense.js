/**
 * Created by noushid
 * on 25/6/16.
 */
app.controller('loanExpenseController', function($scope, $http, LoanExpense){
    $scope.loanExpEdit = false;
    $scope.newloanExp = {};
    $scope.curloanExp = {};
    $scope.loanExpenses = [];
    console.log('success');

    LoanExpense.query(function(loanexpense) {
        $scope.loanExpenses = loanexpense;
    });

    $scope.newLoanExp = function() {
        $scope.loanExpEdit = true;
        $scope.newloanExp = new OfficeExpense();
        $scope.newloanExp.date = new Date();
        $scope.curloanExp = {};
    };

    $scope.editLoanExp = function (thisOffice) {
        $scope.loanExpEdit = true;
        $scope.curloanExp = thisOffice;
        $scope.newloanExp = angular.copy(thisOffice);
    };

    $scope.addLoanExp = function(){
        if ($scope.curloanExp.id) {
            $scope.newloanExp.$update(function (loanexpense) {
                angular.extend($scope.curloanExp, $scope.curloanExp, loanexpense);
            });
        }
        else{
            $scope.newloanExp.$save(function (loanexpense) {
                $scope.loanExpenses.push(loanexpense);
            });
        }
        $scope.loanExpEdit = false;
        $scope.newloanExp = new LoanExpense();
    }

    $scope.deleteLoanExp = function(item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if(confirmDelete) {
            item.$delete(function () {
                var curIndex = $scope.loanExpenses.indexOf(item);
                $scope.loanExpenses.splice(curIndex, 1);
            });
        }
    };

    $scope.cancel = function() {
        $scope.loanExpEdit = false;
        $scope.newloanExp = new LoanExpense();
    };

})
