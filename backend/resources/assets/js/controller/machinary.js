/**
 * Created by shahi on 01/06/16.
 */
app.controller('machineryController', function($scope,$http,$modal,Machinery,Unit){
    $scope.machinaryedit = false;
    $scope.newmachinary = {};
    $scope.curMachinary = {};
    $scope.machinaries = [];
    $scope.units=[];
    $scope.selectedFile=[];

    loadMachinary();

    function loadMachinary(){
        Machinery.query(function(machinary){
            $scope.machinaries = machinary;
        });
    }


    Unit.query(function(unit){
        $scope.units = unit;
    });


    $scope.newMachinary = function () {
        $scope.machinaryedit = true;
        $scope.newmachinary = new Machinery();
        $scope.newmachinary.date = new Date();
        $scope.newmachinary.units_id=1;
        $scope.curMachinary = {};
    };
    $scope.editMachinary = function (thisMachinary) {
        $scope.machinaryedit = true;
        $scope.curMachinary =  thisMachinary;
        $scope.newmachinary = angular.copy(thisMachinary);
    };
    $scope.addMachinary = function () {
        if ($scope.curMachinary.id) {

            var fd = new FormData();
            $scope.newmachinary['id']=$scope.curMachinary.id;
            for(var key in $scope.newmachinary)
                fd.append(key,$scope.newmachinary[key]);

            var uri='api/machinery/'+$scope.curMachinary.id;

            $http.post('/api/machinery', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(data,status,headers,config){
                    console.log(data);
                    loadMachinary();
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        } else{
            var fd = new FormData();
            //fd.append('photo', $scope.myFile);

            for(var key in $scope.newmachinary)
                fd.append(key,$scope.newmachinary[key]);

            $http.post('/api/machinery', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(data,status,headers,config){
                    console.log(data);
                    loadMachinary();
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        }
        $scope.machinaryedit = false;
        $scope.newmachinary = new Machinery();

    };


    $scope.getTheFiles = function ($file) {
        $scope.selectedFile=$file;
    };


    $scope.deleteMachinary = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.machinaries.indexOf(item);
                $scope.machinaries.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelMachinary = function () {
        $scope.machinaryedit = false;
        $scope.newmachinary = new Machinery();
    };

    $scope.open = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewmachinery',
            controller:'viewproductController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });
    };
    $scope.viewPhoto = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewphoto',
            controller:'viewphotoController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };
});



