app.controller('partnerController', function($scope,$http,Partner,Project){
    $scope.partneredit = false;
    $scope.newpartner = {};
    $scope.curPartner = {};
    $scope.partners = [];
    $scope.projects = [];

    Partner.query(function(partner){
        $scope.partners = partner;
    });

    Project.query(function(project){
        $scope.projects = project;
    });

    $scope.newPartner = function () {
        $scope.partneredit = true;
        $scope.newpartner = new Partner();
        $scope.newpartner.regDate = new Date();
        $scope.curPartner = {};
    };
    $scope.editPartner = function (thisPartner) {
        $scope.partneredit = true;
        $scope.curPartner =  thisPartner;
        $scope.newpartner = angular.copy(thisPartner);
    };
    $scope.addPartner = function () {
        if ($scope.curPartner.id) {
            $scope.newpartner.$update(function(partner){
                angular.extend($scope.curPartner, $scope.curPartner, partner);
            });
        } else{
            $scope.newpartner.$save(function(partner){
                $scope.partners.push(partner);
            });
        }
        $scope.partneredit = false;
        $scope.newpartner = new Partner();
    };
    $scope.deletePartner = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.partners.indexOf(item);
                $scope.partners.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelPartner = function () {
        $scope.partneredit = false;
        $scope.newpartner = new Partner();
    };
});