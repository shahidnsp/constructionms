app.controller('phonebookController', function($scope, $http,Contact){
	$scope.contactMode = {
		activeList : 0,
		editMode : false,
		newMode: false,
		titleList : ['Mr','Mrs','Dr']
	};

	$scope.phoneContacts = {};

    //Load contacts from server
    var contacts = Contact.query(function(){
        $scope.phoneContacts = contacts;
        $scope.initialContact(0);
    });





    $scope.selectedContact = $scope.phoneContacts[0];

	$scope.dynamicField = function(buttonStatus, inputIndex){
		if (buttonStatus) {
			$scope.currentContact.phone.push({"number": ""});
		}else{
			$scope.currentContact.phone.splice(inputIndex,1);
		}
	};
	$scope.removeField = function (argument) {
		$scope.inputs.splice(inputIndex,1);
	};


	$scope.initialContact = function (contactIndex) {
		if (!contactIndex) {
			contactIndex = 0;
		}
		$scope.selectedContact = {};
		$scope.selectedContact = $scope.phoneContacts[contactIndex];
		$scope.contactMode.activeList = contactIndex;
		$scope.contactMode.editMode = false;
		$scope.contactMode.newMode = false;
	};

	// Take clicked item to the table
	// TODO show item from the json list intead of making new json
	$scope.getDeatils = function (detailsIndex){
		$scope.contactMode.editMode = false;
		$scope.selectedContact = $scope.phoneContacts[detailsIndex];
		$scope.contactMode.activeList = detailsIndex;
	};

	$scope.newContact = function (argument) {
		$scope.contactMode.editMode = true;
		$scope.contactMode.newMode = true;

		// Clear selected JSON list
        $scope.currentContact = new Contact();
        $scope.currentContact.title='Mr';
        $scope.currentContact.phone =  [{"number": ""}];
        console.log($scope.currentContact);
	};
	$scope.editContact = function () {
		var currentPersonId = $scope.contactMode.activeList;

		$scope.contactMode.editMode = true;
		$scope.contactMode.newMode = false;

		$scope.currentContact = angular.copy($scope.phoneContacts[currentPersonId]);

        //var contact = new Contact();
        //
        ////Add resource method to select record
        //angular.extend($scope.currentContact,contact);
	};
	$scope.deleteContact = function (contact) {
		var currentPersonId = $scope.contactMode.activeList;
		var confirmDelete = confirm("Do you really need to delete " + contact.name + " " + contact.lastname);
		if (confirmDelete) {
            contact.$delete(function(){
                $scope.phoneContacts.splice(currentPersonId, 1);
                $scope.initialContact(0);
            });
		}
	};

	$scope.addContact = function () {
		if ($scope.contactMode.newMode){

            console.log($scope.currentContact);
           /* $scope.currentContact.$save(function(){
                //TODO  error display
                $scope.phoneContacts.push($scope.currentContact);
                $scope.initialContact($scope.phoneContacts.length - 1);

            });*/
            var fd = new FormData();
            //fd.append('photo', $scope.myFile);

            for(var key in $scope.currentContact)
                fd.append(key,$scope.currentContact[key]);


            $http.post('/api/contact', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(data,status,headers,config){
                    console.log(data);
                    $scope.phoneContacts.push(data);
                    $scope.initialContact($scope.phoneContacts.length - 1);
                }).error(function(data,status,headers,config){
                    console.log(data);
                });

        }else{

            //console.log($scope.currentContact);

            $scope.currentContact.$update(function(){
                console.log('Update request');
            });
            /*var fd = new FormData();
            //fd.append('photo', $scope.myFile);

            for(var key in $scope.currentContact)
                fd.append(key,$scope.currentContact[key]);

            $http.post('/api/contact', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(data,status,headers,config){
                    console.log(data);
                    $scope.phoneContacts[$scope.contactMode.activeList] = angular.copy(data);
                    $scope.initialContact($scope.contactMode.activeList);
                }).error(function(data,status,headers,config){
                    console.log(data);
                });*/

            $scope.phoneContacts[$scope.contactMode.activeList] = angular.copy($scope.currentContact);
            $scope.initialContact($scope.contactMode.activeList);
		}
	};

    $scope.cancelBank = function () {
        $scope.status.accountedit = false;
        $scope.curBank={};
    }
});