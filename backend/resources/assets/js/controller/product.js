app.controller('productController', function($scope,$http,Product,Unit,$modal,Ledger){
    $scope.productedit = false;
    $scope.newproduct = {};
    $scope.curProduct = {};
    $scope.products = [];
    $scope.units=[];
    $scope.suppliers=[];

    Product.query(function(product){
        $scope.products = product;
    });

    Unit.query(function(unit){
        $scope.units = unit;
    });

    Ledger.query({under_id:23},function (supplier) {
        $scope.suppliers = supplier;
    });

    $scope.newProduct = function () {
        $scope.productedit = true;
        $scope.newproduct = new Product();
        $scope.newproduct.date = new Date();
        $scope.curProduct = {};
    };
    $scope.editProduct = function (thisProduct) {
        $scope.productedit = true;
        $scope.curProduct =  thisProduct;
        $scope.newproduct = angular.copy(thisProduct);
    };
    $scope.addProduct = function () {
        if ($scope.curProduct.id) {
            $scope.newproduct.$update(function(product){
                angular.extend($scope.curProduct, $scope.curProduct, product);
            });
        } else{
            $scope.newproduct.$save(function(product){
                $scope.products.push(product);
            });
        }
        $scope.productedit = false;
        $scope.newproduct = new Product();
    };
    $scope.deleteProduct = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.products.indexOf(item);
                $scope.products.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelProduct = function () {
        $scope.productedit = false;
        $scope.newproduct = new Product();
    };

    $scope.open = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewproduct',
            controller:'viewproductController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };

    $scope.salesPrice = function () {
        var purchase_price = $scope.newproduct.purchase_price;

        $scope.newproduct.sales_price = purchase_price;
    };
});


app.controller('viewproductController', function ($scope, $modalInstance,$modal, item) {

    $scope.product = angular.copy(item);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };

    $scope.viewPhoto = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewphoto',
            controller:'viewphotoController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };
});

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);