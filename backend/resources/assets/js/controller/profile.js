app.controller('profileController', function($scope,$http){
    $scope.user={};
    function getUser() {
        $http.get('/api/getUserDetails').
            success(function (data, status, headers, config) {
                $scope.user=data;
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    }getUser();
});