app.controller('profitandlossController', function($scope,$http,$filter){

    $scope.drTotal=0;
    $scope.crTotal=0;
    $scope.balanceSide='';

    $scope.saleTotal=0;
    $scope.purchaseTotal=0;
    $scope.debitTotal=0;
    $scope.creditTotal=0;
    $scope.balance=0;

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();

    if(month<5) {
        $scope.fromDate = new Date(year-1, 5, 1);
        $scope.toDate = new Date(year, 2, 31);
    }else{
        $scope.fromDate = new Date(year, 5, 1);
        $scope.toDate = new Date(year+1, 2, 31);
    }
    $scope.journals=[];

    loadBalance();
    function loadBalance() {

        $scope.journals=[];
        $http.get('/api/profitandloss',{params:{fromDate:$scope.fromDate,toDate:$scope.toDate}}).
            success(function (data, status, headers, config) {
                $scope.journals = data;
                getTotal($scope.journals);
               // console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    }

    $scope.searchDate=function(fromDate,toDate){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.journals=[];
        $http.get('/api/profitandloss',{params:{fromDate:from,toDate:to}}).
            success(function (data, status, headers, config) {
                $scope.journals = data;
                getTotal($scope.journals);
                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

    function getTotal(journals){
        var length=journals.length;
        var debit= 0,credit=0;

       for(var i=0;i<length;i++){

            if(journals[i].side=='Dr') {
                    debit += parseFloat(journals[i].balance);
            }else {
                    credit += parseFloat(journals[i].balance);
            }
        }

        var totalDebit=debit;
        var totalCredit=credit;

        $scope.debitTotal=totalDebit;
        $scope.creditTotal=totalCredit;

        var balance=0;

        if(totalCredit>totalDebit){
            balance=totalCredit-totalDebit;
            $scope.debitTotal=totalDebit+balance;
            $scope.creditTotal=totalCredit;
            $scope.balance=balance;
            $scope.balanceSide='Dr';
        }else if(totalDebit>totalCredit){
            balance=totalDebit-totalCredit;
            $scope.creditTotal=totalCredit+balance;
            $scope.debitTotal=totalDebit;
            $scope.balance=balance;
            $scope.balanceSide='Cr';
        }


    }

});