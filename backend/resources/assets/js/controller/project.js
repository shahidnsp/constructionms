app.controller('projectController', function($scope,$http,$filter,$modal,Project,Wage,ProjectExpense,ProjectIncome,Employee,Leader,Group){
	$scope.projectedit = false;
    $scope.empedit = false;
    $scope.empaddedit=false;
	$scope.curProject = {};
    $scope.curLeader = {};
	$scope.newproject = {};
	$scope.wageedit = false;
	$scope.curwage = {};
	$scope.newwage = {};
    $scope.projectactive = false;
    $scope.leaderactive=false;

	$scope.projects = [];
	$scope.employees = [];
    $scope.leaders = [];
	$scope.expenses = [];
    $scope.incomes = [];
    $scope.groups = [];
    $scope.totalwage=0;
    $scope.totalexpense=0;
    $scope.totalincome=0;

    $scope.projectProfit=0;
    $scope.projectExpense=0;
    $scope.projectIncome=0;
    $scope.config={};
    $scope.data={};
    $scope.projectstatus= 'all';
    $scope.projectaction = false;
    $scope.prjctemployees = [];

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

	//var project=
    Project.query({active:'all'},function(project){
        $scope.projects= project;
    });


	Employee.query(function(employees){
	   $scope.employees = employees;
	});

    $scope.changeStatus=function(status){
        $scope.projectaction = false;
        $scope.curProject = {};
        Project.query({active:status},function(project){
            $scope.projects= project;
        });
    };



    /*$scope.getFromToWage=function(curproject){
        var df = $scope.fromDate.toISOString();
        var dt = $scope.toDate.toISOString();
        var result = [];
        for (var i=0; i<curproject.length; i++){
            var tf = new Date(curproject[i].payDate),
                tt = new Date(curproject[i].payDate);
            if (tf >= df && tt <= dt)  {
                result.push(curproject[i]);
            }
        }
        $scope.wages= result;
        console.log(result);
    }*/


    var getTotalWage=function (wages) {
        var count=wages.length;
        var curwage=wages;
        var sum=0;
        for(var i=0;i<count;i++)
            sum+=parseFloat(curwage[i].amount);
        $scope.totalwage=sum;
    }

    var getTotalExpense=function (expenses) {
        var count=expenses.length;
        var curexpense=expenses;
        var sum=0;
        for(var i=0;i<count;i++)
            sum+=parseFloat(curexpense[i].amount);
        $scope.totalexpense=sum;
    }

    var getTotalIncome=function (incomes) {
        var count=incomes.length;
        var sum=0;
        for(var i=0;i<count;i++)
            sum+=parseFloat(incomes[i].amount);
        $scope.totalincome=sum;
    }

	$scope.initialProject = function (projectIndex) {
		$scope.projectedit = false;
		$scope.curProject = $scope.projects[projectIndex];
	};

	$scope.openProject = function(thisProject){

        $scope.groups=[];
		$scope.projectedit = false;
		$scope.curProject = thisProject;

        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');

		// initialProject($scope.projects.indexOf(thisProject));
		ProjectExpense.query({projects_id:thisProject.id,fromDate:from,toDate:to},function(expenses){
			$scope.expenses= expenses;
            getTotalExpense($scope.expenses);
            $scope.projectaction = true;
		});

        ProjectIncome.query({projects_id:thisProject.id,fromDate:from,toDate:to},function(income){
            $scope.incomes= income;
            getTotalIncome($scope.incomes);
        });

        Leader.query({project_id:thisProject.id},function(leader){
            $scope.leaders= leader;
        });

        Wage.query({projects_id:thisProject.id,fromDate:from,toDate:to},function(wage){
            $scope.wages= wage;
            getTotalWage($scope.wages);
        });

        $scope.getProjectStatus(thisProject.id);

        $scope.projectactive = true;

	};


    $scope.getProjectStatus = function (id){
        $http.get('/api/projectstatus/',{params:{projects_id:id}}).
            success(function (data, status, headers, config) {

                $scope.projectProfit=data.profit;
                $scope.projectExpense=data.expense;
                $scope.projectIncome=data.income;

                //graph( $scope.projectIncome,$scope.projectExpense);
                graph( '150','100');

            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    }

    function graph(value1,value2) {
        $scope.config = {
            title: 'Project Status',
            tooltips: true,
            labels: false,
            mouseover: function () {
            },
            mouseout: function () {
            },
            click: function () {
            },
            legend: {
                display: true,
                //could be 'left, right'
                position: 'right'
            },
            innerRadius:0,
            lineLegend:'lineEnd'
        };

        $scope.data = {
            series: ['Income', 'Expense'],
            data: [{
                x: "Income",
                y: [value1],
                tooltip: "Income " + value1
            }, {
                x: "Expense",
                y: [value2],
                tooltip: "Expense " + value2
            }]
        };
    }

    $scope.searchWage=function(fromDate,toDate){
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        Wage.query({projects_id:$scope.curProject.id,fromDate:from,toDate:to},function(wage){
            $scope.wages= wage;
            getTotalWage($scope.wages);
        });
    };

    $scope.searchExpense=function(fromDate,toDate){
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        ProjectExpense.query({projects_id:$scope.curProject.id,fromDate:from,toDate:to},function(expenses){
            $scope.expenses= expenses;
            getTotalExpense($scope.expenses);
        });
    };

    $scope.searchIncome=function(fromDate,toDate){
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        ProjectIncome.query({projects_id:$scope.curProject.id,fromDate:from,toDate:to},function(income){
            $scope.incomes= income;
            getTotalIncome($scope.incomes);
        });
    };


    function loadWage(projects_id){
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');
        Wage.query({projects_id:projects_id,fromDate:from,toDate:to},function(wage){
            $scope.wages= wage;
            getTotalWage($scope.wages);
        });
    }
	$scope.newProject = function (argument) {
		$scope.projectedit = true;
		$scope.curProject = {};
		$scope.newproject = new Project();
        $scope.newproject.startdate = new Date();
        $scope.newproject.enddate = new Date();
    };


	$scope.editProject = function (thisproject) {
		$scope.projectedit = true;
		$scope.curProject = thisproject;
		$scope.newproject = angular.copy(thisproject);
	};

	$scope.addProject = function () {

        if($scope.newproject.startdate.toISOString)
			$scope.newproject.startdate = $scope.newproject.startdate.toISOString();
        if($scope.newproject.enddate.toISOString)
			$scope.newproject.enddate = $scope.newproject.enddate.toISOString();

        if ($scope.curProject.id) {

            $scope.newproject.$update(function (project) {
                angular.extend($scope.curProject,$scope.curProject,project);
                $scope.initialProject($scope.projects.indexOf($scope.curProject));
            });
        }
        else{
            $scope.newproject.$save(function(project){
                $scope.projects.push(project);

                $scope.initialProject($scope.projects.length);
            });
        }

        $scope.newproject = new Project();
        $scope.newproject.startdate = new Date();
        $scope.newproject.enddate = new Date();
		$scope.projectedit = false;
	};

	$scope.deleteProject = function (item) {
		var confirmDelete = confirm("Do you really need to delete the " + item.name + " ?");
		if (confirmDelete) {
			//TODO error handling
			item.$delete(function(){
				var project = $scope.projects.indexOf(item);
				$scope.projects.splice(project, 1);
				$scope.initialProject(0);
			});
		}
	};

    $scope.deactivateProject = function (item) {
        var confirmDelete = confirm("Do you really need to Deactivate the " + item.name + " ?");
        if (confirmDelete) {
            $http.post('/api/deactivateproject',{projects_id:item.id}).
                success(function (data, status, headers, config) {
                    Project.query(function(project){
                        $scope.projects= project;
                    });
                    console.log(data);
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
        }
    };

	$scope.cancelProject = function () {
		$scope.projectedit = false;
        $scope.newproject = new Project();
        $scope.newproject.startdate = new Date();
        $scope.newproject.enddate = new Date();

		$scope.initialProject(0);
	};

    $scope.newLeader = function (argument) {
        $scope.empedit = true;
        $scope.newleader=new Leader();
        $scope.leaderactive=false;
    };

    $scope.addLeader = function () {
        var curIndex = $scope.projects.indexOf($scope.curProject);
        //Adding current Project id for Leader
        $scope.newleader.project_id = $scope.projects[curIndex].id;
        $scope.newleader.$save(function(leader){
            $scope.leaders.push(leader);
        });

        Leader.query({project_id:$scope.curProject.id},function(leader){
            $scope.leaders= leader;
        });
        $scope.newleader = new Leader();
        $scope.empedit = false;

    };

    $scope.deleteLeader = function (item) {
        var confirmDelete = confirm("Do you really need to delete Group Leader ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var leader = $scope.leaders.indexOf(item);
                $scope.leaders.splice(leader, 1);
                $scope.initialProject(0);
            });
        }
    };

    $scope.cancelLeader = function () {
        $scope.empedit = false;
    };

    $scope.openLeader = function(thisLeader){
        $scope.curLeader = thisLeader;
        $scope.groups=[];
        Group.query({leader_id:thisLeader.id},function(group){
            $scope.groups= group;
        });
        $scope.leaderactive=true;
    };

    $scope.newGroup = function () {
        $scope.newgroup=new Group();
        $scope.empaddedit = true;
    };

    $scope.addGroup = function () {

        $scope.newgroup.leader_id = $scope.curLeader.id;

        $scope.newgroup.$save(function(group){
            $scope.groups.push(group);
        });

        Group.query({leader_id:$scope.curLeader.id},function(group){
            $scope.groups= group;
        });
        $scope.newgroup=new Group();
        $scope.empaddedit = false;

    };

    $scope.deleteGroup = function (item) {
        var confirmDelete = confirm("Do you really need to remove "+item.employees.name+" from Group ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var group = $scope.groups.indexOf(item);
                $scope.groups.splice(group, 1);
            });
        }
    };

    $scope.cancelGroup = function () {
        $scope.empaddedit = false;
    };

	// Wage part
	$scope.newWage = function (argument) {
        Leader.query({project_id: $scope.curProject.id}, function (employees) {
            $scope.prjctemployees = employees;
        });

		$scope.wageedit = true;

		$scope.newwage = new Wage();

        $scope.newwage.payDate = new Date();
    };

	$scope.editWage = function (thiswage) {
		$scope.projectedit = false;
		$scope.wageedit = true;
		$scope.curwage = thiswage;
		$scope.newwage = angular.copy(thiswage);
	};

	$scope.addWage = function () {

        if($scope.newwage.payDate.toISOString)
		    $scope.newwage.payDate = $scope.newwage.payDate.toISOString();
		var curIndex = $scope.projects.indexOf($scope.curProject);
        $scope.newwage.projects_id = $scope.projects[curIndex].id;
        $scope.newwage.grandtotal=$scope.newwage.amount;
        $scope.newwage.paid=$scope.newwage.amount;
		if ($scope.curwage.id) {
            $scope.newwage.$update(function(wage){
                angular.extend($scope.curwage, $scope.curwage, wage);
                loadWage(wage.projects_id);
                getTotalWage($scope.wages);
            });
			/*var wageobj = new Wage();
			angular.extend(wageobj,wageobj,$scope.newwage);
			wageobj.$update(function(wage){
				angular.extend($scope.curwage,$scope.curwage,wage);
				$scope.curwage = {};

			});*/

		}else{

			//Adding current Project id for wage
			//$scope.newwage.projects_id = $scope.projects[curIndex].id;
			$scope.newwage.$save(function(wage){
				$scope.wages.push(wage);
                loadWage(wage.projects_id);
                getTotalWage($scope.wages);
			});
		}
        $scope.newwage = new Wage();
        $scope.newwage.date = new Date();
		$scope.wageedit = false;
	};

	$scope.deleteWage = function (item) {
		//var curIndex = $scope.projects.indexOf($scope.curProject);
		var confirmDelete = confirm("Do you really need to delete the item ?");
		if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.wages.indexOf(item);
                $scope.wages.splice(curIndex, 1);
                getTotalWage($scope.wages);
            });
			/*var wage = new Wage();
			angular.extend(wage,wage,item);
			wage.$delete(function(){
				var wage = $scope.projects[curIndex].wages.indexOf(item);
				$scope.projects[curIndex].wages.splice(wage, 1);
                getTotalWage($scope.projects[curIndex].wages);
			});*/

		}
	};

	$scope.cancelWage = function () {
		$scope.wageedit = false;
		$scope.curwage = {};
	};

	$scope.expenseedit = false;
	$scope.newexpense  = new ProjectExpense();
	$scope.curExpense  = {};

	$scope.newExpense = function (argument) {
		$scope.expenseedit = true;

		$scope.newexpense = new ProjectExpense();
        var curIndex = $scope.projects.indexOf($scope.curProject);
        //Adding current Project id for Leader
        $scope.newexpense.projects_id = $scope.projects[curIndex].id;
        $scope.newexpense.payDate = new Date();
    };
	$scope.editExpense = function (thisExpense) {
			$scope.expenseedit = true;
			$scope.curExpense =  thisExpense;
			$scope.newexpense = angular.copy(thisExpense);
	};
	$scope.addExpense = function () {
        if($scope.newexpense.payDate.toISOString)
			$scope.newexpense.payDate = $scope.newexpense.payDate.toISOString();
		if ($scope.curExpense.id) {
			$scope.newexpense.$update(function(expense){
				angular.extend($scope.curExpense, $scope.curExpense, expense);
                getTotalExpense($scope.expenses);
			});
		} else{
			$scope.newexpense.$save(function(expense){
				$scope.expenses.push(expense);
                getTotalExpense($scope.expenses);
			});
		}
		$scope.expenseedit = false;
		$scope.newexpense = new ProjectExpense();
        $scope.newexpense.payDate = new Date();
	};
	$scope.deleteExpense = function (item) {
		var confirmDelete = confirm("Do you really need to delete the item ?");
		if (confirmDelete) {
			item.$delete(function(){
				var curIndex = $scope.expenses.indexOf(item);
				$scope.expenses.splice(curIndex, 1);
                getTotalExpense($scope.expenses);
			});
		}
	};
	$scope.cancelExpense = function () {
		$scope.expenseedit = false;
		$scope.newexpense = new ProjectExpense();
        $scope.newexpense.date = new Date();
	};


    $scope.incomeedit = false;
    $scope.newincome  = new ProjectIncome();
    $scope.curIncome  = {};

    $scope.newIncome = function (argument) {
        $scope.incomeedit = true;

        $scope.newincome = new ProjectIncome();
        var curIndex = $scope.projects.indexOf($scope.curProject);
        //Adding current Project id for Leader
        $scope.newincome.projects_id = $scope.projects[curIndex].id;
        $scope.newincome.date = new Date();
    };
    $scope.editIncome = function (thisIncome) {
        $scope.incomeedit = true;
        $scope.curIncome =  thisIncome;
        $scope.newincome = angular.copy(thisIncome);
    };
    $scope.addIncome = function () {
        if($scope.newincome.date.toISOString)
            $scope.newincome.date = $scope.newincome.date.toISOString();
        if ($scope.curIncome.id) {
            $scope.newincome.$update(function(income){
                angular.extend($scope.curIncome, $scope.curIncome, income);
                getTotalIncome($scope.incomes);
            });
        } else{
            $scope.newincome.$save(function(income){
                $scope.incomes.push(income);
                getTotalIncome($scope.incomes);
            });
        }
        $scope.incomeedit = false;
        $scope.newincome = new ProjectIncome();
        $scope.newincome.date = new Date();
    };
    $scope.deleteIncome = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.incomes.indexOf(item);
                $scope.incomes.splice(curIndex, 1);
                getTotalIncome($scope.incomes);
            });
        }
    };
    $scope.cancelIncome = function () {
        $scope.incomeedit = false;
        $scope.newincome = new ProjectIncome();
        $scope.newincome.date = new Date();
    };

    $scope.open = function (p,size) {
        $http.get('/api/employee/'+ p.employees_id).
            success(function (data, status, headers, config) {
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewemployee',
                    controller:'viewemployeeController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
            }).error(function (data, status, headers, config) {
                console.log(data);
            });


    };
    $scope.openGroupEmployee = function (p,size) {
        $http.get('/api/employee/'+ p.employee_id).
            success(function (data, status, headers, config) {
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewemployee',
                    controller:'viewemployeeController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
            }).error(function (data, status, headers, config) {
                console.log(data);
            });


    };
});