app.controller('projectdocumentController', function($scope,$http,$filter,Document,Project){
    $scope.documentedit = false;
    $scope.newdocument = {};
    $scope.curDocument = {};
    $scope.documents = [];
    $scope.projects={};
    $scope.fromDate='';
    $scope.toDate='';

    $scope.files=[];
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();

    if(month<5) {
        $scope.fromDate = new Date(year-1, 5, 1);
        $scope.toDate = new Date(year, 2, 31);
    }else{
        $scope.fromDate = new Date(year, 5, 1);
        $scope.toDate = new Date(year+1, 2, 31);
    }


    Project.query(function(project){
        $scope.projects = project;
    });

    loadDocument();
    function loadDocument(){
        Document.query(function(document){
            $scope.documents = document;
        });
    }

    /*$scope.searchDocumentDate=function(fromDate,toDate) {

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        Document.query({fromDate:from,toDate:to},function(document){
            $scope.documents = [];
            var length = document.length;
            for (var i = 0; i < length; i++) {
                if (document[i].drledgers_id === '8')
                    $scope.documents.push(document[i]);
            }
        });
    };*/

    $scope.newDocument = function () {
        $scope.documentedit = true;
        $scope.newdocument = new Document();
        $scope.newdocument.date = new Date();
        $scope.curDocument = {};
    };
    $scope.editDocument = function (thisDocument) {
        $scope.documentedit = true;
        $scope.curDocument =  thisDocument;
        $scope.newdocument = angular.copy(thisDocument);
    };
    $scope.addDocument = function () {

        if ($scope.curDocument.id) {
            /* $scope.newemployee.$update(function(employee) {
             angular.extend($scope.curemployee, $scope.curemployee, employee);
             })*/
            var fd = new FormData();
            for(var i in $scope.files)
                fd.append('photo', $scope.files[i]);

            $scope.newdocument['id']=$scope.curDocument.id;
            for(var key in $scope.newdocument)
                fd.append(key,$scope.newdocument[key]);

            // var uri='api/row_material/'+$scope.curRow.id;

            $http.post('/api/document', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(data,status,headers,config){
                    console.log(data);
                    loadDocument();
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        }else {


            var fd = new FormData();


            for(var key in $scope.newdocument)
                fd.append(key,$scope.newdocument[key]);

            for(var i in $scope.files)
                fd.append('photo', $scope.files[i]);

            $http.post('/api/document', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(data,status,headers,config){
                    console.log(data);
                    loadDocument();
                }).error(function(data,status,headers,config){
                    console.log(data);
                });

        }

        /*if ($scope.curDocument.id) {
            $scope.newdocument.$update(function(document){
                angular.extend($scope.curDocument, $scope.curDocument, document);
                loadDocument();
            });
        } else{
            $scope.newdocument.$save(function(document){
                $scope.documents.push(document);
                loadDocument();
            });
        }*/
        $scope.documentedit = false;
        $scope.newdocument = new Document();
    };
    $scope.deleteDocument = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.documents.indexOf(item);
                $scope.documents.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelDocument = function () {
        $scope.documentedit = false;
        $scope.newdocument = new Document();
    };


    $scope.getFileDetails=function(e){
        $scope.files=[];
            for(var i=0;i< e.files.length;i++){
                $scope.files.push(e.files[i]);
            }
    }


});