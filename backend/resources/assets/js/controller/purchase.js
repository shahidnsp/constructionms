/**
 * Created by shahi on 04/06/16.
 */
app.controller('purchaseController', function($scope,$http,$modal,$filter,Purchase,Product,Unit,PurchaseItem,Ledger){
    $scope.purchaseedit = false;
    $scope.newpurchase = {};
    $scope.curPurchase = {};
    $scope.purchases = [];
    $scope.newitems = [];
    $scope.updateitem = {};
    $scope.products = [];
    $scope.currow = {};
    $scope.units = [];
    $scope.suppliers=[];
    $scope.curunit = {};
    $scope.editingData=[];

    $scope.totalGross=0;
    $scope.totalPaid=0;
    $scope.totalBalance=0;

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    loadPurchase();
    function loadPurchase() {
        Purchase.query(function (purchase) {
            var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
            var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');

            Purchase.query({fromDate: from, toDate: to}, function (purchase) {
                $scope.purchases = purchase;
                getTotalSum($scope.purchases);
            });
        });
    }
    Product.query(function(product){
        $scope.products = product;
    });
    Unit.query(function(unit){
        $scope.units = unit;
    });

    Ledger.query({under_id:23},function (supplier) {
        $scope.suppliers = supplier;
    });

    $scope.searchPurchaseDate=function(fromDate,toDate) {

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        Purchase.query({fromDate:from,toDate:to},function(purchase){
            $scope.purchases=purchase;
        });
    };
    //$scope.newitems=new PurchaseItem();

    $scope.newPurchase = function () {
        $scope.purchaseedit = true;
        $scope.newpurchase = new Purchase();
        $scope.newpurchase.date = new Date();
        $scope.newpurchase.total=0;
        $scope.newpurchase.paid=0;
        $scope.newpurchase.balance=0;
        $scope.curPurchase = {};
        $scope.newitems = [];
    };
    $scope.editPurchase = function (thisPurchase) {
        $scope.purchaseedit = true;
        $scope.curPurchase =  thisPurchase;
        $scope.newpurchase = angular.copy(thisPurchase);

        $http.get('/api/purchaseitem/'+thisPurchase.id).
            success(function(data,status,headers,config){
                //$scope.newitems=data;
                $scope.newitems=[];
                for(var i=0;i<data.length;i++)
                {
                    $scope.updateitem={
                        id:data[i].id,
                        qty:data[i].qty,
                        units_id:data[i].units_id,
                        unit:data[i].unit.name,
                        products_id:data[i].products_id,
                        name:data[i].product.name,
                        rate:data[i].rate,
                        net:data[i].net
                    };

                    $scope.newitems.push($scope.updateitem);

                    $scope.editingData[data[i].id] = false;
                }

            }).error(function(data,status,headers,config){
                console.log(data);
            });

    };
    $scope.addPurchase = function () {
        if($scope.newpurchase.date.toISOString)
            $scope.newpurchase.date = $scope.newpurchase.date.toISOString();
        if($scope.newitems.length!=0) {
            if ($scope.curPurchase.id) {

                deletepurchaseitems($scope.curPurchase.id);

                $scope.newpurchase.$update(function (purchase) {
                    angular.extend($scope.curPurchase, $scope.curPurchase, purchase);
                    savepurchaseitems(purchase.id);
                    loadPurchase();
                });
            } else {
                $scope.newpurchase.$save(function (purchase) {
                    $scope.purchases.push(purchase);
                    savepurchaseitems(purchase.id);
                    loadPurchase();
                });
            }
            $scope.purchaseedit = false;
            $scope.newpurchase = new Purchase();
        }else{
            alert('No Items in List');
        }
    };

    function savepurchaseitems(id){
        for(var i=0;i<$scope.newitems.length;i++)
        {
            $http.post('/api/purchaseitem',{purchases_id:id,products_id:$scope.newitems[i].products_id,qty:$scope.newitems[i].qty,units_id:$scope.newitems[i].units_id,rate:$scope.newitems[i].rate,net:$scope.newitems[i].net}).
                success(function(data,status,headers,config){
                    //console.log(data);
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        }
    }

    function deletepurchaseitems(id)
    {
        $http.delete('/api/purchaseitem/' + id)
            .success(function (data, status, headers) {
                //$scope.ServerResponse = data;
            })
            .error(function(data,status,headers,config){
                console.log(data);
            });
    }


    $scope.deletePurchase = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            deletepurchaseitems(item.id);
            item.$delete(function(){
                var curIndex = $scope.purchases.indexOf(item);
                $scope.purchases.splice(curIndex, 1);
                getTotalSum($scope.purchases);
            });
        }
    };
    $scope.cancelPurchase = function () {
        $scope.purchaseedit = false;
        $scope.newpurchase = new Purchase();
    };

    $scope.addItem = function () {
        var product=$scope.newitems.length;
        //for(var i=0;i<product;i++) {
        //  if($scope.editingData[item.id] !=true) {
        $scope.inserted = {
            id: product,
            qty: '1'
        };
        $scope.newitems.push($scope.inserted);
        $scope.editingData[product] = true;
        // }
        // }
    };

    $scope.updateItem=function(item){
        if(item.products_id!=null) {
            if(item.units_id==null)
            {
                alert('Unit not selected');
                return;
            }
            includeunit(item);
        }else{
            alert('Item not selected');
        }
    }

    function includeunit(item){
        $http.get('/api/unit/'+item.units_id).
            success(function(data,status,headers,config){
                $scope.curunit=data;

                $http.get('/api/product/'+item.products_id).
                    success(function(data,status,headers,config){
                        $scope.currow=data;

                        updateRow(item);

                        //console.log(data);
                    }).error(function(data,status,headers,config){
                        console.log(data);
                    });
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }

    function updateRow(item){
        $scope.updateitem={
            id:item.id,
            qty:item.qty,
            units_id:item.units_id,
            unit:$scope.curunit.name,
            products_id:item.products_id,
            name:$scope.currow.name,
            rate:item.rate,
            net:item.net
        };

        $scope.newitems[item.id]=$scope.updateitem;

        $scope.editingData[item.id] = false;
        //$scope.newitems[item.id].reload();
    }

    $scope.editItem=function(item){
        $scope.newitems[item.id]=item;
        $scope.editingData[item.id] = true;
    }

    $scope.removeItem = function (item) {
        var confirmDelete = confirm("Do you really need to remove the item ?");
        if (confirmDelete) {
            var curIndex = $scope.newitems.indexOf(item);
            var netFisrt=parseFloat(item.net);
            var total1=parseFloat($scope.newpurchase.total);
            $scope.newpurchase.total=total1-netFisrt;
            $scope.newitems.splice(curIndex, 1);
        }
    };

    $scope.selectItem=function(item){
/*
        if(item.net!="") {
            var net = parseFloat(item.net);
            var total1 = parseFloat($scope.newpurchase.total);
            $scope.newpurchase.total = total1 - net;
        }*/
        $http.get('/api/product/'+item.products_id).
            success(function(data,status,headers,config){
                item.rate=data.purchase_price;
                item.units_id=data.unit_id;
                item.net=data.purchase_price;
                item.qty=1;
                var net=parseFloat(data.purchase_price);
                var total1=parseFloat($scope.newpurchase.total);
                $scope.newpurchase.total=total1+net;
                $scope.newpurchase.paid=$scope.newpurchase.total;
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.changePrice=function(item){

        $http.get('/api/unit/'+item.units_id).
            success(function(data,status,headers,config){
                var rate= 0,qty= 0;

                if(item.rate!="")
                    rate=parseFloat(item.rate);
                if(item.qty!="")
                    qty=parseFloat(item.qty);

                var netFisrt=parseFloat(item.net);
                var total1=parseFloat($scope.newpurchase.total);
                $scope.newpurchase.total=total1-netFisrt;

                if(data.isunit==='true'){
                    var kg=parseFloat(data.rate);
                    item.net=rate*(qty*kg);
                }else{
                    item.net=rate*qty;
                }
                var net=parseFloat(item.net);
                var total=parseFloat($scope.newpurchase.total);
                $scope.newpurchase.total=total+net;
                $scope.newpurchase.paid=$scope.newpurchase.total;
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };
    $scope.changePaid=function(){

        var paid= 0,bal= 0,total=0;
        if($scope.newpurchase.paid!='')
            paid=parseFloat($scope.newpurchase.paid);
        if($scope.newpurchase.total!='')
            total=parseFloat($scope.newpurchase.total);

        $scope.newpurchase.balance=total-paid;
    };

    $scope.viewItem = function (item,size) {

        var thisItem={};
        $http.get('/api/product/'+item.products_id).
            success(function(data,status,headers,config){
                thisItem=data;
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewproduct',
                    controller:'viewproductController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return thisItem;
                        }
                    }
                });
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.viewPurchaseItem=function(purchase){

        $http.get('/api/purchaseitem/'+purchase.id).
            success(function(data,status,headers,config){
                console.log(data);
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewpurchaseitems',
                    controller:'viewpurchaseitemController',
                    /*size: size,*/
                    resolve: {
                        purchase: function () {
                            return data;
                        }
                    }
                });
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.createSupplier = function () {
        var modalInstance = $modal.open({
            templateUrl: 'template/createledger',
            controller:'createledgerController'
            /*size: size,*/
            /*resolve: {
             item: function () {
             return thisItem;
             }
             }*/

        });
        modalInstance.result.then(function(selectedItem){
            Ledger.query({under_id:23},function (supplier) {
                $scope.suppliers = supplier;
            });
            $scope.newpurchase.suppliers_id=selectedItem.id;

        }, function(){
            Ledger.query({under_id:23},function (supplier) {
                $scope.suppliers = supplier;
            });
        });

    };

    function getTotalSum(purchases){
        var length=purchases.length;
        var gross=0,paid=0,balance=0;
        for(var i=0;i<length;i++){
            gross+=parseFloat(purchases[i].total);
            paid+=parseFloat(purchases[i].paid);
            balance+=parseFloat(purchases[i].balance);
        }
        $scope.totalGross=gross;
        $scope.totalPaid=paid;
        $scope.totalBalance=balance;
    }
});

app.controller('viewpurchaseitemController', function ($scope, $modalInstance, purchase) {

    $scope.items = angular.copy(purchase);
    $scope.total=0;
    var length=$scope.items.length;
    for(var i=0;i<length;i++){
        var amt=parseFloat($scope.items[i].net);
        $scope.total+=amt;
    }
    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };
});

/*
app.controller('createsupplierController', function($scope, $http,$modalInstance,$modal,Supplier,UserInfo) {

    UserInfo.query().success(function(data){
        $scope.user = data;
        //console.log(data);
    });

    $scope.close = function () {
        $modalInstance.dismiss('Close');
    };

    $scope.supplieredit = false;
    $scope.newsupplier = {};
    $scope.curSupplier = {};
    $scope.suppliers = [];

    loadSupplier();
    function loadSupplier() {
        Supplier.query(function (supplier) {
            $scope.suppliers = supplier;
        });
    }

    $scope.newSupplier = function () {
        $scope.supplieredit = true;
        $scope.newsupplier = new Supplier();
        $scope.newsupplier.date = new Date();
        $scope.newsupplier.paymentmode='Debit';
        $scope.curSupplier = {};
    };
    $scope.editSupplier = function (thisSupplier) {
        $scope.supplieredit = true;
        $scope.curSupplier =  thisSupplier;
        $scope.newsupplier = angular.copy(thisSupplier);
    };
    $scope.addSupplier = function () {

        if( $scope.newsupplier.date.toISOString)
            $scope.newsupplier.date = $scope.newsupplier.date.toISOString();

        if ($scope.curSupplier.id) {
            $scope.newsupplier.$update(function(supplier){
                angular.extend($scope.curSupplier, $scope.curSupplier, supplier);
                loadSupplier();
            });
        } else{
            $scope.newsupplier.$save(function(supplier){
                $scope.suppliers.push(supplier);
                loadSupplier();
            });
        }
        $scope.supplieredit = false;
        $scope.newsupplier = new Supplier();
    };
    $scope.deleteSupplier = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.suppliers.indexOf(item);
                $scope.suppliers.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelSupplier = function () {
        $scope.supplieredit = false;
        $scope.newsupplier = new Supplier();
    };


    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();
    };

    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
    $scope.maxSize = 5;
});

*/






