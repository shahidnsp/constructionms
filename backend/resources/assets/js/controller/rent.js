app.controller('rentController', function($scope,$http,$filter,Journal){
	$scope.rentedit = false;
	$scope.newrent = {};
	$scope.curRent = {};
	$scope.rents = [];
    $scope.totalrent=0;
    $scope.fromDate='';
    $scope.toDate='';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();

    if(month<5) {
        $scope.fromDate = new Date(year-1, 5, 1);
        $scope.toDate = new Date(year, 2, 31);
    }else{
        $scope.fromDate = new Date(year, 5, 1);
        $scope.toDate = new Date(year+1, 2, 31);
    }

    loadRent();
    function loadRent(){
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');

        Journal.query({fromDate:from,toDate:to},function(rent){
            $scope.rents = [];
            var length = rent.length;
            for (var i = 0; i < length; i++) {
                if (rent[i].drledgers_id === '13')
                    $scope.rents.push(rent[i]);
            }
            getTotalRent($scope.rents);
        });
    }

    $scope.searchRentDate=function(fromDate,toDate) {

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        Journal.query({fromDate:from,toDate:to},function(rent){
            $scope.rents = [];
            var length = rent.length;
            for (var i = 0; i < length; i++) {
                if (rent[i].drledgers_id === '13')
                    $scope.rents.push(rent[i]);
            }
            getTotalRent($scope.rents);
        });
    };

	$scope.newRent = function () {
		$scope.rentedit = true;
		$scope.newrent = new Journal();
		$scope.newrent.date = new Date();
		$scope.curRent = {};
	};
	$scope.editRent = function (thisRent) {
		$scope.rentedit = true;
		$scope.curRent =  thisRent;
		$scope.newrent = angular.copy(thisRent);
	};
	$scope.addRent = function () {
        $scope.newrent.cramount=$scope.newrent.dramount;
        $scope.newrent.vouchertype='Expense';
        $scope.newrent.crledgers_id=1;
        $scope.newrent.drledgers_id=13;

		if ($scope.curRent.id) {
			$scope.newrent.$update(function(rent){
				angular.extend($scope.curRent, $scope.curRent, rent);
                loadRent();
			});
		} else{
			$scope.newrent.$save(function(rent){
				$scope.rents.push(rent);
                loadRent();
			});
		}
		$scope.rentedit = false;
		$scope.newrent = new Journal();
	};
	$scope.deleteRent = function (item) {
		var confirmDelete = confirm("Do you really need to delete the item ?");
		if (confirmDelete) {
			item.$delete(function(){
				var curIndex = $scope.rents.indexOf(item);
				$scope.rents.splice(curIndex, 1);
                getTotalRent($scope.rents);
			});
		}
	};
	$scope.cancelRent = function () {
		$scope.rentedit = false;
		$scope.newrent = new Journal();
	};

    var getTotalRent=function (rents) {
        var count=rents.length;
        var sum=0;
        for(var i=0;i<count;i++)
            sum+=parseFloat(rents[i].dramount);
        $scope.totalrent=sum;
    }
});