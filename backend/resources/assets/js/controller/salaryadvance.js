/**
 * Created by noushid on 12/7/16.
 */

app.controller('salaryadvanceController', function ($scope, $http,$filter,$modal, SalaryAdvance, Employee) {
    $scope.salaryadvedit = false;
    $scope.newsalaryadv = {};
    $scope.cursalaryadv = {};
    $scope.salaryadvances = [];
    $scope.employees = [];
    $scope.totalAdvance=0;
    $scope.totalBalance=0;
    $scope.lists=[];
    $scope.previous=0;

    $scope.fromDate='';
    $scope.toDate='';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();

    if(month<5) {
        $scope.fromDate = new Date(year-1, 5, 1);
        $scope.toDate = new Date(year, 2, 31);
    }else{
        $scope.fromDate = new Date(year, 5, 1);
        $scope.toDate = new Date(year+1, 2, 31);
    }

    $scope.getAdvance=function(id){
        $http.get('/api/getAdvance',{params:{employees_id:id}}).
            success(function (data, status, headers, config) {
                $scope.previous=data;
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

    function getAdvanceList(){
        $http.get('/api/getSalaryAdvanceList').
            success(function (data, status, headers, config) {
                $scope.lists=data;
                getAdvanceBalanceSum($scope.lists);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    }getAdvanceList();
    function getAdvanceBalanceSum(advance){
        var sum=0;
        var length=advance.length;
        for(var i=0;i<length;i++){
            sum+=parseFloat(advance[i].balance);
        }
        $scope.totalBalance=sum;
    }

    Employee.query(function (employee) {
        $scope.employees = employee;
    });

    function loadAdvance() {
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');

        SalaryAdvance.query({fromDate:from,toDate:to},function (salaryadv) {
            $scope.salaryadvances = salaryadv;
            getTotal($scope.salaryadvances);
        });
    }

    loadAdvance();

    $scope.searchAdvance=function(fromDate,toDate){
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        SalaryAdvance.query({fromDate:from,toDate:to},function (salaryadv) {
            $scope.salaryadvances = salaryadv;
            getTotal($scope.salaryadvances);
        });
    };

    function getTotal(advances){
        var length=advances.length,total=0;
        for(var i=0;i<length;i++)
            total+=parseFloat(advances[i].amount);
        $scope.totalAdvance=total;
    }

    $scope.newSalaryAdv = function () {
        $scope.salaryadvedit = true;
        $scope.newsalaryadv = new SalaryAdvance();
        $scope.newsalaryadv.date=new Date();
        $scope.cursalaryadv = {};
    };

    $scope.addSalaryAdv = function() {

        if ($scope.cursalaryadv.id) {
            $scope.newsalaryadv.$update(function(salaryadv) {
                angular.extend($scope.cursalaryadv, $scope.cursalaryadv, salaryadv);
                loadAdvance();
            })
        }else {
            $scope.newsalaryadv.$save(function (salaryadv) {
                $scope.salaryadvances.push(salaryadv);
                loadAdvance();
            });
        }
        $scope.salaryadvedit = false;
        $scope.newsalaryadv = new SalaryAdvance();
    };

    $scope.editSalaryAdv = function(thisemployee) {
        $scope.salaryadvedit = true;
        $scope.cursalaryadv = thisemployee;
        $scope.newsalaryadv = angular.copy(thisemployee);
    };

    $scope.deleteSalaryAdv = function (item) {
        var confirmDelete = confirm("Do you really need to delete this item ?");
        if (confirmDelete) {
            item.$delete(function () {
                var curIndex = $scope.salaryadvances.indexOf(item);
                $scope.salaryadvances.splice(curIndex, 1);
                getTotal($scope.salaryadvances);
            });
        }
    };

    $scope.cancelSalaryAdv= function () {
        $scope.salaryadvedit = false;
        $scope.newsalaryadv = new SalaryAdvance();
    };

    $scope.open = function (p,size) {
        $http.get('/api/employee/'+ p.employees_id).
            success(function (data, status, headers, config) {
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewemployee',
                    controller:'viewemployeeController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
            }).error(function (data, status, headers, config) {
                console.log(data);
            });


    };

});