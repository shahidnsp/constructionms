app.controller('staffattendanceController', function($scope,$http,$filter,Employee,StaffAttendance){
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);
    //$scope.curdate=new Date();
    $scope.curdate= $filter('date')(new Date(), 'yyyy-MM-dd');
    //$scope.curdate= '2016-10-20';


    $scope.employees = [];
    $scope.staffattendance = [];
    $scope.newtteandance = {};
    $scope.curattendance = {};
    $scope.showaddform = true;
    $scope.showeditform = false;
    $scope.showreport = false;
    //$scope.isDisabled = true;
    $scope.emprecords = [];

    loademployee();

    function loademployee () {
        Employee.query(function (employee) {
            $scope.employees = employee;

            console.log(employee);

            for(var i = 0; i < employee.length; i ++) {
                if (employee[i].staff_attendance.length > 1) {
                    for(var j=0 ; j < employee[i].staff_attendance.length ; j++) {
                        if ($scope.curdate == employee[i].staff_attendance[j].date) {
                            $scope.isDisabled = true;
                        }
                    }
                }else{
                    if ($scope.curdate == employee[i].staff_attendance.date) {
                        $scope.isDisabled = true;
                    }
                }
            }

        });
    };

    $scope.loadAttendance = function (date) {
        $scope.days = [];
        $scope.showreport = true;
        $scope.temp = [];

        $http.get('api/getattendance', {params: {date: date}}).
            success(function (data, status) {
                //to create start date
                $scope.startfrom = data[0].attendance[0].day;
                console.log($scope.startfrom);
                //for make heading of days
                for (var i = $scope.startfrom; i <= 31; i++) {
                    $scope.days.push(i);
                    $scope.temp.push(i);
                }
                console.log($scope.days);

                $scope.emprecords = data;
            }).
            error(function(data,status) {
                console.log('failed');
                console.log(status);
                console.log(data);
            });
    };

    $scope.cancelReport = function() {
        console.log('cancel');
        $scope.emprecords = '';
        $scope.startfrom = '';
        $scope.showreport = false;
    };

    $scope.number = 31;
    $scope.getNumber = function (num) {
        return new Array(num);
    };


    $scope.newAttendance = function () {
        console.log('add attendance');
        console.log($scope.employees);
        console.log($scope.employees.length);
        var confir = confirm('Do yo Want to Submit?');
        if (confir) {
            for (var i = 0 ; i < $scope.employees.length ; i++) {
                console.log($scope.employees[i].present);
                $http.post('api/staffattendance', {employees_id: $scope.employees[i].id, present: $scope.employees[i].present, note:$scope.employees[i].note, date:$scope.curdate}).
                    success(function (data, headers, status) {
                        console.log('success');
                        $scope.message = 'Success';
                    }).error(function (data, status) {
                        console.log('failed');
                        $scope.message = false;
                    });
            }
            if ($scope.message != false) {
                alert('success');
                $scope.isDisabled = true;
            }
        }else {
            console.log('Canceled by user');
        }

    };

    $scope.editAttendance = function(item) {
        console.log('edit');
        curdate = $filter('date')(new Date(item), 'yyyy-MM-dd');
        console.log(curdate);

        $scope.showaddform = false;
        $scope.showeditform = true;

        $http.get('api/staffattendance', {params:{date: curdate}}).
            success(function (data, status) {
                console.log(data);
                if (data == '') {
                    console.log('null');
                    console.log($scope.employees);
                    console.log($scope.employees.length);
                    for (var i = 0 ; i < $scope.employees.length ; i++) {
                        console.log($scope.employees[i].present);
                        $http.post('api/staffattendance', {employees_id: $scope.employees[i].id, present: '2', note:'', date:curdate}).
                            success(function (data, headers, status) {
                            }).error(function (data, status) {
                                console.log('failed');
                            });
                    }
                    $http.get('api/staffattendance', {params:{date: curdate}}).
                        success(function(data,status) {
                            $scope.curattendance = angular.copy(data);
                            $scope.attendance = angular.copy(data);
                        })
                    console.log($scope.curattendance);
                    console.log($scope.attendance);
                }
                else {
                    $scope.curattendance = angular.copy(data);
                    $scope.attendance = angular.copy(data);
                    console.log('value exist');
                }
            }).error(function (data, status) {
                console.log(data);
                console.log('error');
            });
    };


    $scope.updateAttendance=function(curattendance) {
        var confir = confirm('Do you Want to Update Register?');
        if (confir) {
            $scope.curattendance = angular.copy(curattendance);

            console.log($scope.attendance[2].id);
            console.log($scope.attendance[2].present);
            console.log($scope.curattendance[2].present);

            for (var i = 0 ; i < $scope.curattendance.length ; i++) {
                if ($scope.attendance[i].note !== $scope.curattendance[i].note || $scope.attendance[i].present !== $scope.curattendance[i].present) {
                    console.log($scope.curattendance[i].present);
                    $http.put('api/staffattendance/'+$scope.curattendance[i].id, {
                        present: $scope.curattendance[i].present,
                        note: $scope.curattendance[i].note,
                        date:$scope.curattendance[i].date,
                        remark: 'edited'
                    }).
                        success(function (data, status) {
                            $scope.message = 'Update success';
                        }).error(function (data, status) {
                            console.log('error');
                            $scope.message = false;
                        });
                }
            }
            if ($scope.message != false) {
                alert('Update success');

            }
        }else {
            console.log('Update canceled by user');
        }
    };

    $scope.cancelEdit= function () {
        $scope.showaddform = true;
        $scope.showeditform = false;
    };


});

app.directive('datetimez', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            element.datetimepicker({
                format: "MM-yyyy",
                viewMode: "months",
                minViewMode: "months",
                pickTime: false,
            }).on('changeDate', function(e) {
                ngModelCtrl.$setViewValue(e.date);
                scope.$apply();
            });
        }
    };
});



app.directive('datetimez', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            element.datetimepicker({
                format: "MM-yyyy",
                viewMode: "months",
                minViewMode: "months",
                pickTime: false,
            }).on('changeDate', function(e) {
                ngModelCtrl.$setViewValue(e.date);
                scope.$apply();
            });
        }
    };
});
