/**
 * Created by shahi on 01/06/16.
 */
app.controller('stockController', function($scope,$http,$modal,Product,Unit,Supplier){
    $scope.products = [];
    $scope.units=[];
    $scope.suppliers=[];

    loadProduct();

    function loadProduct(){
        Product.query(function(product){
            $scope.products = product;
        });
    }


    Unit.query(function(unit){
        $scope.units = unit;
    });


    Supplier.query(function(supplier){
        $scope.suppliers = supplier;
    });


    $scope.updateItem = function (item) {
        var uri ='api/product/'+item.id;

        $http.put(uri, item,{headers: '{"Content-Type": "application/json;charset=UTF-8"}'} )
            .success(function (data, status, headers, config) {
                alert('Updated Successfully.');
            })
            .error(function (data, status, header, config) {
                alert('Server error');
                console.log(data);
            });
    };

    $scope.open = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewproduct',
            controller:'viewproductController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };

    $scope.viewPhoto = function (p,size) {
        var modalInstance = $modal.open({
            templateUrl: 'template/viewphoto',
            controller:'viewphotoController',
            /*size: size,*/
            resolve: {
                item: function () {
                    return p;
                }
            }
        });

    };
});

app.controller('viewphotoController', function ($scope, $modalInstance, item) {

    $scope.product = angular.copy(item);

    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };
});





