app.controller('sundrycreditorController', function($scope,$http,$filter){
    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);
    $scope.journals=[];
    $scope.totalAdvance=0;
    $scope.totalDue=0;

    loadBalance();
    function loadBalance() {
        $http.get('/api/getsundrycreditor').
            success(function (data, status, headers, config) {
                $scope.journals = data;
                getSum($scope.journals);
                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    }

    function getSum(journals){
        var advance=0;
        var due=0;
        var length=journals.length;
        for(var i=0;i<length;i++){
            if(journals[i].advance != '')
                advance+=parseFloat(journals[i].advance);
            if(journals[i].due != '')
                due+=parseFloat(journals[i].due);
        }
        $scope.totalAdvance=advance;
        $scope.totalDue=due;
    }

});