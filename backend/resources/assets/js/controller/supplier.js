app.controller('supplierController', function($scope,$http,Ledger){
    $scope.supplieredit = false;
    $scope.newsupplier = {};
    $scope.curSupplier = {};
    $scope.suppliers = [];

    loadSupplier();
    function loadSupplier() {
        Ledger.query({under_id:23},function (supplier) {
            $scope.suppliers = supplier;
        });
    }

    $scope.newSupplier = function () {
        $scope.supplieredit = true;
        $scope.newsupplier = new Ledger();
        $scope.newsupplier.crOrDr='Dr';
        $scope.newsupplier.unders_id=23;
        $scope.curSupplier = {};
    };
    $scope.editSupplier = function (thisSupplier) {
        $scope.supplieredit = true;
        $scope.curSupplier =  thisSupplier;
        $scope.newsupplier = angular.copy(thisSupplier);
    };
    $scope.addSupplier = function () {
        if ($scope.curSupplier.id) {
            $scope.newsupplier.$update(function(supplier){
                angular.extend($scope.curSupplier, $scope.curSupplier, supplier);
                loadSupplier();
            });
        } else{
            $scope.newsupplier.$save(function(supplier){
                $scope.suppliers.push(supplier);
                loadSupplier();
            });
        }
        $scope.supplieredit = false;
        $scope.newsupplier = new Ledger();
    };
    $scope.deleteSupplier = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.suppliers.indexOf(item);
                $scope.suppliers.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelSupplier = function () {
        $scope.supplieredit = false;
        $scope.newsupplier = new Ledger();
    };
});