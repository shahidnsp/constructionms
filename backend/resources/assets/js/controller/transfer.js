/**
 * Created by shahi on 04/06/16.
 */
app.controller('transferController', function($scope,$http,$modal,$filter,Transfer,Product,Unit,TransferItem,Project){
    $scope.transferedit = false;
    $scope.newtransfer = {};
    $scope.curTransfer = {};
    $scope.transfers = [];
    $scope.newitems = [];
    $scope.updateitem = {};
    $scope.products = [];
    $scope.currow = {};
    $scope.units = [];
    $scope.projects = [];
    $scope.curunit = {};
    $scope.editingData=[];
    $scope.totalNet=0;

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();
    $scope.fromDate=new Date(year,month,1);
    $scope.toDate=new Date(year,month+1,0);

    loadTransfer();
    function loadTransfer() {
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');

        Transfer.query({fromDate:from,toDate:to},function(transfer){
            $scope.transfers=transfer;
        });
    }
    Project.query(function(project){
        $scope.projects = project;
    });
    Product.query(function(product){
        $scope.products = product;
    });
    Unit.query(function(unit){
        $scope.units = unit;
    });

    $scope.searchTransferDate=function(fromDate,toDate) {
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        Transfer.query({fromDate:from,toDate:to},function(transfer){
            $scope.transfers=transfer;
        });
        /*$http.get('/api/gettransferwithdate',{params:{'fromDate':from,'toDate':to}})
            .then(function(response){
                $scope.transfers=response.data;
            });*/
    };
    //$scope.newitems=new TransferItem();

    $scope.newTransfer = function () {
        $scope.transferedit = true;
        $scope.newtransfer = new Transfer();
        $scope.newtransfer.date = new Date();
        $scope.newtransfer.from ='Main Godown';
        $scope.newtransfer.amount=0;
        $scope.curTransfer = {};
        $scope.newitems = [];
    };
    $scope.editTransfer = function (thisTransfer) {
        $scope.transferedit = true;
        $scope.curTransfer =  thisTransfer;
        $scope.newtransfer = angular.copy(thisTransfer);

        $http.get('/api/transferitem/'+thisTransfer.id).
            success(function(data,status,headers,config){
                //$scope.newitems=data;
                $scope.newitems=[];
                for(var i=0;i<data.length;i++)
                {
                    $scope.updateitem={
                        id:data[i].id,
                        qty:data[i].qty,
                        units_id:data[i].units_id,
                        unit:data[i].unit.name,
                        products_id:data[i].products_id,
                        name:data[i].product.name,
                        rate:data[i].rate,
                        net:data[i].net
                    };

                    $scope.newitems.push($scope.updateitem);

                    $scope.editingData[data[i].id] = false;
                }

            }).error(function(data,status,headers,config){
                console.log(data);
            });

    };
    $scope.addTransfer = function () {
        if($scope.newtransfer.date.toISOString)
            $scope.newtransfer.date = $scope.newtransfer.date.toISOString();
        if($scope.newitems.length!=0) {
            if ($scope.curTransfer.id) {

                deletetransferitems($scope.curTransfer.id);

                $scope.newtransfer.$update(function (transfer) {
                    angular.extend($scope.curTransfer, $scope.curTransfer, transfer);
                    savetransferitems(transfer.id);
                    loadTransfer();
                });
            } else {
                $scope.newtransfer.$save(function (transfer) {
                    $scope.transfers.push(transfer);
                    savetransferitems(transfer.id);
                    loadTransfer();
                });
            }
            $scope.transferedit = false;
            $scope.newtransfer = new Transfer();
        }else{
            alert('No Items in List');
        }
    };

    function savetransferitems(id){
        for(var i=0;i<$scope.newitems.length;i++)
        {
            $http.post('/api/transferitem',{transfers_id:id,products_id:$scope.newitems[i].products_id,qty:$scope.newitems[i].qty,units_id:$scope.newitems[i].units_id,rate:$scope.newitems[i].rate,net:$scope.newitems[i].net}).
                success(function(data,status,headers,config){
                    //console.log(data);
                }).error(function(data,status,headers,config){
                    console.log(data);
                });
        }
    }

    function deletetransferitems(id)
    {
        $http.delete('/api/transferitem/' + id)
            .success(function (data, status, headers) {
                //$scope.ServerResponse = data;
            })
            .error(function(data,status,headers,config){
                console.log(data);
            });
    }


    $scope.deleteTransfer = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            deletetransferitems(item.id);
            item.$delete(function(){
                var curIndex = $scope.transfers.indexOf(item);
                $scope.transfers.splice(curIndex, 1);
                //loadTransfer();
            });
        }
    };
    $scope.cancelTransfer = function () {
        $scope.transferedit = false;
        $scope.newtransfer = new Transfer();
    };

    $scope.changePrice=function(item){

        $http.get('/api/unit/'+item.units_id).
            success(function(data,status,headers,config){
                var rate= 0,qty= 0;

                if(item.rate!="")
                    rate=parseFloat(item.rate);
                if(item.qty!="")
                    qty=parseFloat(item.qty);

                var netFisrt=parseFloat(item.net);
                var total1=parseFloat($scope.newtransfer.amount);
                $scope.newtransfer.amount=total1-netFisrt;

                if(data.isunit==='true'){
                    var kg=parseFloat(data.rate);
                    item.net=rate*(qty*kg);
                }else{
                    item.net=rate*qty;
                }
                var net=parseFloat(item.net);
                var total=parseFloat($scope.newtransfer.amount);
                $scope.newtransfer.amount=total+net;
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.addItem = function () {
        var product=$scope.newitems.length;
        //for(var i=0;i<product;i++) {
        //  if($scope.editingData[item.id] !=true) {
        $scope.inserted = {
            id: product,
            qty: '1'
        };
        $scope.newitems.push($scope.inserted);
        $scope.editingData[product] = true;
        // }
        // }
    };

    $scope.updateItem=function(item){
        if(item.products_id!=null) {
            if(item.units_id==null)
            {
                alert('Unit not selected');
                return;
            }
            includeunit(item);
        }else{
            alert('Item not selected');
        }
    }

    function includeunit(item){
        $http.get('/api/unit/'+item.units_id).
            success(function(data,status,headers,config){
                $scope.curunit=data;

                $http.get('/api/product/'+item.products_id).
                    success(function(data,status,headers,config){
                        $scope.currow=data;

                        updateRow(item);

                        //console.log(data);
                    }).error(function(data,status,headers,config){
                        console.log(data);
                    });
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }

    function updateRow(item){
        $scope.updateitem={
            id:item.id,
            qty:item.qty,
            units_id:item.units_id,
            unit:$scope.curunit.name,
            products_id:item.products_id,
            name:$scope.currow.name,
            rate:item.rate,
            net:item.net
        };

        $scope.newitems[item.id]=$scope.updateitem;

        $scope.editingData[item.id] = false;
        //$scope.newitems[item.id].reload();
    }

    $scope.editItem=function(item){
        $scope.newitems[item.id]=item;
        $scope.editingData[item.id] = true;
    }

    $scope.removeItem = function (item) {
        var confirmDelete = confirm("Do you really need to remove the item ?");
        if (confirmDelete) {
            var curIndex = $scope.newitems.indexOf(item);
            $scope.newitems.splice(curIndex, 1);
        }
    };

    $scope.selectItem=function(item){
        $http.get('/api/product/'+item.products_id).
            success(function(data,status,headers,config){
                item.rate=data.sales_price;
                item.units_id=data.unit_id;
                item.net=data.sales_price;
                item.qty=1;
                var net=parseFloat(data.sales_price);
                var total1=parseFloat($scope.newtransfer.amount);
                $scope.newtransfer.amount=total1+net;
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    /*$scope.changePrice=function(item){

        $http.get('/api/unit/'+item.units_id).
            success(function(data,status,headers,config){
                var rate=parseFloat(item.rate);
                var qty=parseFloat(item.qty);
                if(data.isunit==='true'){
                    var kg=parseFloat(data.rate);
                    item.net=rate*(qty*kg);
                }else{
                    item.net=rate*qty;
                }
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });


    }*/

    $scope.viewItem = function (item,size) {

        var thisItem={};
        $http.get('/api/product/'+item.products_id).
            success(function(data,status,headers,config){
                thisItem=data;
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewproduct',
                    controller:'viewproductController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return thisItem;
                        }
                    }
                });
                //console.log(data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.viewTransferItem=function(transfer){

        $http.get('/api/transferitem/'+transfer.id).
            success(function(data,status,headers,config){
                console.log(data);
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewtransferitems',
                    controller:'viewtransferitemController',
                    /*size: size,*/
                    resolve: {
                        transfer: function () {
                            return data;
                        }
                    }
                });
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    }
});

app.controller('viewtransferitemController', function ($scope, $modalInstance, transfer) {

    $scope.items = angular.copy(transfer);
    $scope.total=0;
    var length=$scope.items.length;
    for(var i=0;i<length;i++){
        var amt=parseFloat($scope.items[i].net);
        $scope.total+=amt;
    }
    $scope.cancel = function () {
        $modalInstance.dismiss('Close');
    };
});







