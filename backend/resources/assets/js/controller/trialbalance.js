app.controller('trialbalanceController', function($scope,$http,$filter){

    $scope.drTotal=0;
    $scope.crTotal=0;

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();

    if(month<5) {
        $scope.fromDate = new Date(year-1, 5, 1);
        $scope.toDate = new Date(year, 2, 31);
    }else{
        $scope.fromDate = new Date(year, 5, 1);
        $scope.toDate = new Date(year+1, 2, 31);
    }
    $scope.journals=[];

    loadBalance();
    function loadBalance() {

        $scope.journals=[];
        $http.get('/api/trialbalance',{params:{fromDate:$scope.fromDate,toDate:$scope.toDate}}).
            success(function (data, status, headers, config) {
                $scope.journals = data;
                getTrial($scope.journals);
                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    }

    $scope.searchDate=function(fromDate,toDate){

        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        $scope.journals=[];
        $http.get('/api/trialbalance',{params:{fromDate:from,toDate:to}}).
            success(function (data, status, headers, config) {
                $scope.journals = data;
                getTrial($scope.journals);
                //console.log(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

    function getTrial(journals){
        var length=journals.length;
        var drtotal= 0,crtotal=0;

        for(var i=0;i<length;i++){
            if(journals[i].credit != '')
                crtotal+=parseFloat(journals[i].credit);
            if(journals[i].debit != '')
                drtotal+=parseFloat(journals[i].debit);
        }
        $scope.drTotal=drtotal;
        $scope.crTotal=crtotal;
    }

});