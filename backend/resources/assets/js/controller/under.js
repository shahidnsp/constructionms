app.controller('underController', function($scope,$http,Under){
    $scope.underedit = false;
    $scope.newunder = {};
    $scope.curUnder = {};
    $scope.unders = [];

    function loadUnders(){
        Under.query(function(under){
            $scope.unders = under;
        });
    }loadUnders();


    $scope.newUnder = function () {
        $scope.underedit = true;
        $scope.newunder = new Under();
        $scope.newunder.affectGrossProfit=0;
        $scope.curUnder = {};
    };
    $scope.editUnder = function (thisUnder) {
        $scope.underedit = true;
        $scope.curUnder =  thisUnder;
        $scope.newunder = angular.copy(thisUnder);
    };
    $scope.addUnder = function () {
        if ($scope.curUnder.id) {
            $scope.newunder.$update(function(under){
                angular.extend($scope.curUnder, $scope.curUnder, under);
                loadUnders();
            });
        } else{
            $scope.newunder.$save(function(under){
                $scope.unders.push(under);
                loadUnders();
            });
        }
        $scope.underedit = false;
        $scope.newunder = new Under();
    };
    $scope.deleteUnder = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            item.$delete(function(){
                var curIndex = $scope.unders.indexOf(item);
                $scope.unders.splice(curIndex, 1);
            });
        }
    };
    $scope.cancelUnder = function () {
        $scope.underedit = false;
        $scope.newunder = new Under();
    };
});