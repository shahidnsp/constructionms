/**
 * Created by Shahid on 23/7/16.
 */

app.controller('wageadvanceController', function ($scope, $http,$filter,$modal, WageAdvance, Project,Leader) {
    $scope.advanceedit = false;
    $scope.newadvance = {};
    $scope.curadvance = {};
    $scope.advances= [];
    $scope.leaders = [];
    $scope.leaders_id=null;
    $scope.totalAdvance=0;
    $scope.totalBalance=0;
    $scope.lists=[];
    $scope.fromDate='';
    $scope.toDate='';

    var date=new Date(),year=date.getFullYear(),month=date.getMonth();

    if(month<5) {
        $scope.fromDate = new Date(year-1, 5, 1);
        $scope.toDate = new Date(year, 2, 31);
    }else{
        $scope.fromDate = new Date(year, 5, 1);
        $scope.toDate = new Date(year+1, 2, 31);
    }

    Project.query(function(project){
        $scope.projects= project;
    });
    $scope.changeProject=function(thisProject){
            Leader.query({project_id: thisProject}, function (leader) {
                $scope.leaders = leader;
            });
    };


    function loadAdvance() {
        var from = $filter("date")(Date.parse($scope.fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse($scope.toDate), 'yyyy-MM-dd');

        WageAdvance.query({fromDate:from,toDate:to},function (advance) {
            $scope.advances = advance;
            getTotal($scope.advances);
        });
    }

    loadAdvance();

    $scope.searchAdvance=function(fromDate,toDate){
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd');

        WageAdvance.query({fromDate:from,toDate:to},function (advance) {
            $scope.advances = advance;
            getTotal($scope.advances);
        });
    };

    function getTotal(advances){
        var length=advances.length,total=0;
        for(var i=0;i<length;i++)
            total+=parseFloat(advances[i].amount);
        $scope.totalAdvance=total;
    }

    $scope.newAdvance = function () {
        $scope.advanceedit = true;
        $scope.newadvance = new WageAdvance();
        $scope.newadvance.date=new Date();
        $scope.curadvance = {};
    };

    $scope.addAdvance = function() {
        var confirmDelete = confirm("Do you really need to Pay this Wage ?");
        if (confirmDelete) {
            $http.get('/api/leader/' + $scope.leaders_id).
                success(function (data, status, headers, config) {
                    $scope.newadvance.employees_id = data.employee_id;

                    if ($scope.curadvance.id) {
                        $scope.newadvance.$update(function (advance) {
                            angular.extend($scope.curadvance, $scope.curadvance, advance);
                            loadAdvance();
                        })
                    } else {
                        $scope.newadvance.$save(function (advance) {
                            $scope.advances.push(advance);
                            loadAdvance();
                        });
                    }
                    $scope.advanceedit = false;
                    $scope.newadvance = new WageAdvance();
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
        }

    };

    function getAdvanceList(){
        $http.get('/api/getWageAdvanceList').
            success(function (data, status, headers, config) {
                $scope.lists=data;
                getAdvanceBalanceSum($scope.lists);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    }getAdvanceList();
    function getAdvanceBalanceSum(advance){
        var sum=0;
        var length=advance.length;
        for(var i=0;i<length;i++){
            sum+=parseFloat(advance[i].balance);
        }
        $scope.totalBalance=sum;
    }

    $scope.editAdvance = function(thisemployee) {
        $scope.advanceedit = true;
        $scope.curadvance = thisemployee;

        $http.get('/api/getleaderEmployeeID', {params:{employee_id:thisemployee.employees_id,project_id:thisemployee.projects_id}}).
            success(function (data, status, headers, config) {
                Leader.query(function (leader) {
                    $scope.leaders = leader;
                    $scope.leaders_id=data.id;
                    $scope.newadvance = angular.copy(thisemployee);
                });
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

    $scope.deleteAdvance = function (item) {
        var confirmDelete = confirm("Do you really need to delete this item ?");
        if (confirmDelete) {
            item.$delete(function () {
                var curIndex = $scope.advances.indexOf(item);
                $scope.advances.splice(curIndex, 1);
                getTotal($scope.advances);
            });
        }
    };

    $scope.cancelAdvance= function () {
        $scope.advanceedit = false;
        $scope.newadvance = new WageAdvance();
    };

    $scope.open = function (p,size) {
        $http.get('/api/employee/'+ p.employees_id).
            success(function (data, status, headers, config) {
                var modalInstance = $modal.open({
                    templateUrl: 'template/viewemployee',
                    controller:'viewemployeeController',
                    /*size: size,*/
                    resolve: {
                        item: function () {
                            return data;
                        }
                    }
                });
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };

});