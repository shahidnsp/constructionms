/**
 * Created by Shahid on 17/3/16.
 */

angular.module('AttendanceGroupService',[]).factory('AttendanceGroup',['$resource',
    function($resource){
        return $resource('/api/attendancegroup/:attendanceId',{
            attendanceId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);

angular.module('AttendanceListService',[]).factory('AttendanceList',['$http',function($http){

    var AttendanceList = {};
    AttendanceList.query = function(){
        return $http.get('/api/attendancelist');
    };
    return AttendanceList;
}]);