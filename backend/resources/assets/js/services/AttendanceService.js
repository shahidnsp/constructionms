/**
 * Created by Shahid on 17/3/16.
 */

angular.module('AttendanceService',[]).factory('Attendance',['$resource',
    function($resource){
        return $resource('/api/attendance/:attendanceId',{
            attendanceId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);

