/**
 * Created by psybo on 6/7/15.
 */

angular.module('BankService',[]).factory('Bank',['$resource',
    function($resource){
        return $resource('/api/bank/:bankId',{
            bankId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
angular.module('BankService').factory('Account',['$resource',
        function($resource){
            return $resource('/api/account/:accountId',{
                accountId:'@id'
            },{
                update: {
                    method: 'PUT'
                },
                get:{
                    params:{
                        bank_id:'@bank_id'
                    }
                }
            });
        }
    ]);

//TODO test AccountParticular

angular.module('BankService').factory('AccountParticular',['$resource',
    function($resource){
        return $resource('/api/accountParticular/:accountId',{
            accountId:'@id'
        },{
            update: {
                method: 'PUT'
            },
            get:{
                params:{
                    account_id:'@account_id'
                }
            }
        });
    }
]);

angular.module('BankService').factory('Card',['$resource',
    function($resource){
        return $resource('/api/card/:cardId',{
            cardId:'@id'
        },{
            update: {
                method: 'PUT'
            },
            get:{
                params:{
                    account_id:'@account_id'
                }
            }
        });
    }
]);