/**
 * Created by shanoop on 31/7/15.
 */

//TODO url
angular.module('CashService',[]).factory('Cash',['$http',function($http){

    var Cash = {};
    Cash.query = function(){
        return $http.get('/api/cash');
    };
    return Cash;
}]);

angular.module('CashService').factory('CashDebit',['$resource',
    function($resource){
        return $resource('/api/cashDebit/:cashDebitId',{
            cashDebitId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);

angular.module('CashService').factory('CashCredit',['$resource',
    function($resource){
        return $resource('/api/cashCredit/:cashCreditId',{
            cashCreditId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);