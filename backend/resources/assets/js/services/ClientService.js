/**
 * Created by noushi on 11/10/16.
 */
/**
 * Created by shahid on 15/3/16.
 */

angular.module('ClientService',[]).factory('Client',['$resource',
    function($resource){
        return $resource('/api/clients/:clientId',{
            unitId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
