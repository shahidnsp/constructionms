angular.module('ContactService',[]).factory('Contact',['$resource',
    function($resource){
        return $resource('/api/contact/:contactId',{
            contactId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);