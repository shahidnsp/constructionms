/**
 * Created by shahid .
 */

angular.module('DocumentService',[]).factory('Document',['$resource',
    function($resource){
        return $resource('/api/document/:documentId',{
            documentId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
