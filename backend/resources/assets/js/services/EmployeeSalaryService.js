

angular.module('EmployeeSalaryService',[]).factory('EmployeeSalary',['$resource',
    function($resource){
        return $resource('/api/employeesalary/:employeesalId',{
            employeesalId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
