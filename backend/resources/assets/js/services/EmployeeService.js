/**
 * Created by shahid .
 */

angular.module('EmployeeService',[]).factory('Employee',['$resource',
    function($resource){
        return $resource('/api/employee/:employeeId',{
            employeeId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
