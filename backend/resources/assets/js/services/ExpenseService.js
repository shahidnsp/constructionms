/**
 * Created by shanoop on 31/7/15.
 */

angular.module('ExpenseService',[]).factory('Expense',['$resource',
    function($resource){
        return $resource('/api/expense/:expenseId',{
            expenseId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);