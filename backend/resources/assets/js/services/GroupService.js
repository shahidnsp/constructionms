/**
 * Created by shahid on 18/3/16.
 */

angular.module('GroupService',[]).factory('Group',['$resource',
    function($resource){
        return $resource('/api/group/:groupId',{
            groupId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);