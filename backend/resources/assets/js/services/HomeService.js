/**
 * Created by shanoop on 21/7/15.
 */

//TODO error in url
angular.module('HomeService',[]).factory('Home',['$resource',
    function($resource){
        return $resource('/api/contact/:contactId',{
            contactId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);