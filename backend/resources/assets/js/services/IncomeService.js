/**
 * Created by shanoop on 31/7/15.
 */


angular.module('IncomeService',[]).factory('Income',['$resource',
    function($resource){
        return $resource('/api/income/:incomeId',{
            incomeId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
