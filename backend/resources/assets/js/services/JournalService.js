/**
 * Created by Shahid on 17/3/16.
 */

angular.module('JournalService',[]).factory('Journal',['$resource',
    function($resource){
        return $resource('/api/journal/:journalId',{
            journalId:'@id'
        },{
            update:{
                method:'PUT'
            },
            get:{
                params:{
                    fromDate:'@fromDate',
                    toDate:'@toDate'
                }
            }
        });
    }
]);