/**
 * Created by Shahid on 18/3/16.
 */

angular.module('LeaderService',[]).factory('Leader',['$resource',
    function($resource){
        return $resource('/api/leader/:leaderId',{
            leaderId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);