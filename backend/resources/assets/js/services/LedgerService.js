/**
 * Created by noushi on 28/6/16.
 */

angular.module('LedgerService',[]).factory('Ledger', ['$resource',
    function($resource){
        return $resource('/api/ledger/:ledgerId', {
            ledgerId: '@id'
        },{
            update: {
                method:'PUT'
            }
        });
    }
]);
