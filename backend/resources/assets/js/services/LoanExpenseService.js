/**
 * Created by noushid
 * on 25/6/16.
 */

angular.module('LoanExpenseService', []).factory('LoanExpense', ['$resource',
    function ($resource) {
        return $resource('/api/loanexp/:loanExpId', {
            loanExpId: '@id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
