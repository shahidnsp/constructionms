/**
 * Created by shanoop on 14/7/15.
 */

angular.module('LoanService',[]).factory('Loan',['$resource',
    function($resource){
        return $resource('/api/loan/:loanId',{
            loanId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);

angular.module('LoanService').factory('Emi',['$resource',
    function($resource){
        return $resource('/api/emi/:emiId',{
            emiId:'@id'
        },{
            update:{
                method:'PUT'
            },
            get:{
                params:{
                    loan_id:'@loan_id'
                }
            }
        });
    }
]);

angular.module('LoanService').factory('LoanExpense',['$resource',
    function($resource){
        return $resource('/api/loanexp/:loneExpID',{
            loneExpID:'@id'
        },{
            update:{
                method:'PUT'
            },
            get:{
                params:{
                    loan_id:'@loan_id'
                }
            }
        });
    }
]);