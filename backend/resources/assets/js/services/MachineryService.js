/**
 * Created by shahid on 16/3/16.
 */

angular.module('MachineryService',[]).factory('Machinery',['$resource',
    function($resource){
        return $resource('/api/machinery/:machineryId',{
            machineryId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);