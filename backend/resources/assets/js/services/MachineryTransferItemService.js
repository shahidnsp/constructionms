/**
 * Created by shahid on 16/3/16.
 */
angular.module('MachineryTransferItemService',[]).factory('MachineryTransferItem',['$resource',
    function($resource){
        return $resource('/api/machinerytransferitem/:machineryId',{
            machineryId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);