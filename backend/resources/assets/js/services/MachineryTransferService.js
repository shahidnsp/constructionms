/**
 * Created by shahid on 16/3/16.
 */
angular.module('MachineryTransferService',[]).factory('MachineryTransfer',['$resource',
    function($resource){
        return $resource('/api/machinerytransfer/:machineryId',{
            machineryId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);