/**
 * Created by shahi on 4/6/16.
 */
angular.module('MaterialReturnItemService',[]).factory('MaterialReturnItem',['$resource',
    function($resource){
        return $resource('/api/materialreturn_item/:itemId',{
            itemId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);