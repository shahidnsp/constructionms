/**
 * Created by shahi on 4/6/16.
 */
angular.module('MaterialReturnService',[]).factory('MaterialReturn',['$resource',
    function($resource){
        return $resource('/api/materialreturn/:materialreturnId',{
            materialreturnId:'@id'
        },{
            update:{
                method:'PUT'
            },
            get:{
                params:{
                    fromDate:'@fromDate',
                    toDate:'@toDate'
                }
            }
        });
    }
]);