angular.module('NoteService',[])
    .factory('Note',['$resource',
        function ($resource) {
            return $resource('api/note/:noteId',{
                noteId:'@id'
            },{
                update:{
                    method:'PUT'
                }
            })
        }
    ]);
