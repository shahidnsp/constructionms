/**
 * Created by shanoop on 2/8/15.
 */

angular.module('PartnerService',[]).factory('Partner',['$resource',
    function($resource){
        return $resource('/api/partner/:partnerId',{
            partnerId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);