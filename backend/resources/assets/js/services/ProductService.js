/**
 * Created by shahid on 16/3/16.
 */

angular.module('ProductService',[]).factory('Product',['$resource',
    function($resource){
        return $resource('/api/product/:productId',{
            productId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);