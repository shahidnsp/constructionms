/**
 * Created by Shahid on 12/7/16.
 *
 * Project resource
 */


angular.module('ProjectService',[]).factory('Project',['$resource',
    function($resource){
        return $resource('/api/project/:projectId',{
            projectId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);

/**
 * Wage resource
 */

angular.module('ProjectService').factory('Wage',['$resource',
    function($resource){
        return $resource('/api/wage/:wageId',{
            wageId:'@id'
        },{
            update:{
                method:'PUT'
            },
            get:{
                params:{
                    projects_id:'@projects_id',
                    fromDate:'@fromDate',
                    toDate:'@toDate'
                }
            }
        });
    }
]);


/**
 * Project Expense resource
 */


angular.module('ProjectService').factory('ProjectExpense',['$resource',
    function($resource){
        return $resource('/api/expense/:expenseId',{
            expenseId:'@id'
        },{
            update:{
                method:'PUT'
            },
            get:{
                params:{
                    projects_id:'@projects_id',
                    fromDate:'@fromDate',
                    toDate:'@toDate'
                }
            }
        });
    }
]);

angular.module('ProjectService').factory('ProjectIncome',['$resource',
    function($resource){
        return $resource('/api/income/:incomeId',{
            incomeId:'@id'
        },{
            update:{
                method:'PUT'
            },
            get:{
                params:{
                    projects_id:'@projects_id',
                    fromDate:'@fromDate',
                    toDate:'@toDate'
                }
            }
        });
    }
]);
