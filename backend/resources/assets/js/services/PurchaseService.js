/**
 * Created by shahi on 4/6/16.
 */
angular.module('PurchaseService',[]).factory('Purchase',['$resource',
    function($resource){
        return $resource('/api/purchase/:purchaseId',{
            purchaseId:'@id'
        },{
            update:{
                method:'PUT'
            },
            get:{
                params:{
                    fromDate:'@fromDate',
                    toDate:'@toDate'
                }
            }
        });
    }

]);

