/**
 * Created by Shahid on 2/3/16.
 */

angular.module('ReminderService',[]).factory('Reminder',['$resource',
    function($resource){
        return $resource('/api/reminder/:reminderId',{
            reminderId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);