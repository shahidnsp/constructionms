/**
 * Created by shanoop on 2/8/15.
 */

angular.module('RentService',[]).factory('Rent',['$resource',
    function($resource){
        return $resource('/api/rent/:rentId',{
            rentId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);