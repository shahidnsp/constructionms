/**
 * Created by noushi on 15/7/16.
 */
/**
 * Created by noushid .
 */

angular.module('SalaryAdvanceService',[]).factory('SalaryAdvance',['$resource',
    function($resource){
        return $resource('/api/salaryadvance/:salaryadvId',{
            salaryadvId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
