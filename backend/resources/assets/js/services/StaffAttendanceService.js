/**
 * Created by shahid on 25/7/16.
 */

angular.module('StaffAttendanceService',[]).factory('StaffAttendance',['$resource',
    function($resource){
        return $resource('/api/staffattendance/:staffattendanceId',{
            staffattendanceId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);