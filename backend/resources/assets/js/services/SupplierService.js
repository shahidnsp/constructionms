/**
 * Created by shahid on 15/3/16.
 */

angular.module('SupplierService',[]).factory('Supplier',['$resource',
    function($resource){
        return $resource('/api/supplier/:supplierId',{
            supplierId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);