/**
 * Created by shahi on 4/6/16.
 */
angular.module('TransferitemService',[]).factory('TransferItem',['$resource',
    function($resource){
        return $resource('/api/transferitem/:itemId',{
            itemId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);