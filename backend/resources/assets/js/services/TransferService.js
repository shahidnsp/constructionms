/**
 * Created by shahi on 4/6/16.
 */
angular.module('TransferService',[]).factory('Transfer',['$resource',
    function($resource){
        return $resource('/api/transfer/:transferId',{
            transferId:'@id'
        },{
            update:{
                method:'PUT'
            },
            get:{
                params:{
                    fromDate:'@fromDate',
                    toDate:'@toDate'
                }
            }
        });
    }
]);