/**
 * Created by shahid on 15/3/16.
 */

angular.module('UnderService',[]).factory('Under',['$resource',
    function($resource){
        return $resource('/api/under/:underId',{
            underId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);