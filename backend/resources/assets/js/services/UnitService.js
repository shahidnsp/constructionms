/**
 * Created by shahid on 15/3/16.
 */

angular.module('UnitService',[]).factory('Unit',['$resource',
    function($resource){
        return $resource('/api/unit/:unitId',{
            unitId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);