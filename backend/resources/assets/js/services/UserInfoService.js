/**
 * Created by psybo on 5/8/15.
 */
angular.module('UserInfoService',[]).factory('UserInfo',['$http',function($http){
       var UserInfo = {};

       UserInfo.resetPassword=function(userId){
           $http.post('/api/resetPassword',{id:userId}).
               success(function(data,status,headers,config){
                   alert('User password reset to admin');
               }).error(function(data,status,headers,config){
                   console.log(data);
                   alert('Sorry,user password reset failed');
               });
       };

       UserInfo.query=function(){
           return $http.get('/api/userinfo')
       };

       UserInfo.get = function(id){
           return $http.get('/api/user/'+id);
       };

       UserInfo.save = function(id,data){
           return $http.put('/api/userinfo/'+id,data);
       };

       UserInfo.update=function(data){
           return $http.post('/api/userinfo',data);
       };

       UserInfo.changePassword=function(oldPassword,password){
                $http.post('/api/changePassword',
                    {oldPassword:oldPassword,password:password})
                    .success(function(data,status){
                        //TODO redirect to logout
                        alert('Password has been changed.');
                    })
                    .error(function(data){
                        alert('Sorry, Password change failed.');
                    });
       };

       UserInfo.getAllPage=function(id){
           return $http.get('/api/getAllPages/'+id);
       };

       UserInfo.getUserPermission=function(userId){
           return $http.get('/api/getPermission/?id='+userId);
       };

       UserInfo.setUserPermission = function(data){
           // data = {id:userid,permission:jsonPermission}
           return $http.post('/api/changePermission',data);
       };

       return UserInfo;
   }
]);