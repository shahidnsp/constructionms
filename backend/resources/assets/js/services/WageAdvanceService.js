/**
 * Created by Shahid on 23/7/16.
 */


angular.module('WageAdvanceService',[]).factory('WageAdvance',['$resource',
    function($resource){
        return $resource('/api/wageadvance/:wageadvanceId',{
            wageadvanceId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
