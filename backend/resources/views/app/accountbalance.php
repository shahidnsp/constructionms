<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Ledger Account Balance </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <label for="">Show
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-sm-3 text-center">
        <div>
            <button class="btn-info" ng-click="exportToExcel('incomeTable')">Export To Excel</button>
        </div>
    </div>
    <div class="col-md-5 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 text-right">
        <div class="form-inline form-group">
            <label for="" class="control-label">From</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
        </div>
    </div>
    <div class="col-sm-6 text-left">
        <div class="form-inline form-group">
            <label for="" class="control-label">To</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 text-right">
        <div class="form-inline form-group">
            <label for="" class="control-label">Account Group</label>
            <select class="form-control" ng-model="accountgroup_id" ng-change="changeGroup(accountgroup_id);">
                <option value="All">All</option>
                <option ng-repeat="group in accountgroups" value="{{group.id}}" ng-show="group.id != '1'">{{group.name}}</option>
            </select>
        </div>
    </div>
    <div class="col-sm-6 text-left">
        <div class="form-inline form-group">
            <label for="" class="control-label">Account Ledger</label>
            <select class="form-control" ng-model="accountledger_id">
                <option value="All">All</option>
                <option ng-repeat="ledger in accountledgers" value="{{ledger.id}}">{{ledger.name}}</option>
            </select>
            <button class="btn btn-warning" ng-click="searchDate(fromDate,toDate,accountgroup_id,accountledger_id)">Search</button>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <p class="text-primary">Ledger Account Balance and Details</p>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="tableTransfer">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Account Name</th>
                        <th>Opening Balance</th>
                        <th>Debit Amount</th>
                        <th>Credit Amount</th>
                        <th>Balance</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="ledger in listCount  = (ledgers | filter:filterlist) | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{ledger.name}}</td>
                        <td style="text-align: center"><i class="fa fa-inr"></i> {{ledger.openingBalance}}</td>
                        <td style="text-align: center"><i class="fa fa-inr"></i> {{ledger.debit}}</td>
                        <td style="text-align: center"><i class="fa fa-inr"></i> {{ledger.credit}}</td>
                        <td ng-class="ledger.balance<0 ? 'text-danger':'text-primary'" style="text-align: center"><i class="fa fa-inr"></i> {{ledger.balance}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="clearfix" ng-show="ledgers.length > numPerPage">
    <pagination
        ng-model="currentPage"
        total-items="listCount.length"
        max-size="maxSize"
        items-per-page="numPerPage"
        boundary-links="true"
        class="pagination-sm pull-right"
        previous-text="&lsaquo;"
        next-text="&rsaquo;"
        first-text="&laquo;"
        last-text="&raquo;"
        ></pagination>
</div>


