<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Account Statement </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <label for="">Show
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-sm-3 text-center">
        <div>
            <button class="btn btn-sm btn-info" ng-click="exportToExcel('accountStatementTable')">Export To Excel</button>
        </div>
    </div>
    <div class="col-md-5 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 text-center">
        <div class="form-inline form-group">
            <label for="" class="control-label">Account</label>
            <div class="input-group">
                <select class="form-control" ng-model="ledgers_id" ng-change="changeLedger(ledgers_id);" required>
                    <option value="">Select</option>
                    <option ng-repeat="ledger in ledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                </select>
            </div>
            <label for="" class="control-label">From</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
            <label for="" class="control-label">To</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
            <button class="btn btn-warning" ng-click="searchDate(fromDate,toDate)">Search</button>
        </div>
    </div>

</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">Journals from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="accountStatementTable" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>SlNo</th>
                        <th>Date</th>
                        <th>Dr Account</th>
                        <th>Cr Account</th>
                        <th>Dr Amount</th>
                        <th>Cr Amount</th>
                        <th>Voucher Type</th>
                        <th>Note</th>
                        <th ng-show="extra">Created</th>
                        <th ng-show="extra">Updated</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="journal in listCount  = (journals | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{journal.date | date:'dd-MMMM-yyyy'}}</td>
                        <td>{{journal.drledger.name}}</td>
                        <td>{{journal.crledger.name}}</td>
                        <td><i class="fa fa-inr"></i>{{journal.dramount}}</td>
                        <td><i class="fa fa-inr"></i>{{journal.cramount}}</td>
                        <td>{{journal.vouchertype}}</td>
                        <td><p class="description" popover="{{journal.note}}" popover-trigger="mouseenter">{{journal.note}}</p></td>
                        <td ng-show="extra">{{journal.created_at}}</td>
                        <td ng-show="extra">{{journal.updated_at}}</td>

                    </tr>
                    <!--<tr class="text-danger">
                        <td  colspan="4">Total</td><td style="text-align: center"><i class="fa fa-inr"></i> {{drTotal}}</td><td style="text-align: center"><i class="fa fa-inr"></i> {{crTotal}}</td><td>Balance</td><td><i class="fa fa-inr"></i> {{balance}}</td>
                    </tr>-->
                    <tr>
                        <td colspan="8" class="text-danger" ng-show="ledgerName != ''">
                            Ledger Account Report of {{ledgerName}}
                        </td>
                    </tr>
                    <tr class="text-danger text-center">
                        <td colspan="2">Total Debit Amount </td><td><i class="fa fa-inr"></i> {{drTotal}}</td><td colspan="2">Total Credit Amount</td><td><i class="fa fa-inr"></i> {{crTotal}}</td><td>Balance</td><td><i class="fa fa-inr"></i> {{balance}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix" ng-show="journals.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>
</div>