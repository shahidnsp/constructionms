<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-briefcase"> </i>  Attendance Register</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <p>You Can add project like Build an office or making a House.It also used to enter project related expenses and wage paid.</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
   <!-- <div class="box">-->
        <button ng-if="user.permissions.attendance.write =='true'" ng-click="newAttendance();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Attendance</button>
    <!--</div>-->
    </div>
</div>
<div class="row" ng-show="editAttendance">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Project</label>
                    <div class="col-sm-10">
                        <select class="form-control" ng-change="changeProject(newGroup.projects_id);" ng-model="newGroup.projects_id">
                            <option value="">Select</option>
                            <option ng-repeat="project in projects" value="{{project.id}}">{{project.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Employee Leader</label>
                    <div class="col-sm-10">
                        <select class="form-control" ng-model="newGroup.leaders_id" ng-change="changeLeader(newGroup.leaders_id);">
                            <option value="">Select</option>
                            <option ng-repeat="leader in leaders" value="{{leader.id}}">{{leader.employee.name}}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <label for="">Show
                    <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                    entries
                </label>
            </div>
            <div class="col-md-6 text-right">
                <div class="form-inline form-group">
                    <label for="filter-list">Search </label>
                    <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                </div>
            </div>
        </div>

        <div class="box">

                <div class="form-group form-horizontal">
                    <label class="control-label col-sm-5">Attendance Date</label>
                    <div class="input-group col-sm-3">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newGroup.date"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
			        							<span class="input-group-btn">
			        								<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
			        							</span>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="text-success">Attendance Register </span>

                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="rentTable" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SlNo</th>
                                    <th>Employee</th>
                                    <th>Status</th>
                                    <th>Wage</th>
                                    <th>Hour</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="attendance in listCount  = (attendances | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage" ng-class="attendance.status=='A' ? 'text-danger' : 'text-success'">
                                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                        <td>{{attendance.employee}}</td>
                                        <td>
                                            <select class="form-control" ng-model="attendance.status">
                                                <option value="P">Present</option>
                                                <option value="A">Absent</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" ng-model="attendance.wage" ng-change="changeWage(attendance);">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" ng-model="attendance.hour" ng-change="changeWage(attendance);">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" ng-model="attendance.total">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="clearfix" ng-show="attendances.length > numPerPage">
                    <pagination
                        ng-model="currentPage"
                        total-items="listCount.length"
                        max-size="maxSize"
                        items-per-page="numPerPage"
                        boundary-links="true"
                        class="pagination-sm pull-right"
                        previous-text="&lsaquo;"
                        next-text="&rsaquo;"
                        first-text="&laquo;"
                        last-text="&raquo;"
                        ></pagination>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <button ng-click="addAttendance();" class="btn btn-info col-md-12">Save Attendance</button>
                    </div>
                    <div class="col-sm-4">
                        <button ng-click="cancel();" class="btn btn-default col-md-12">Cancel</button>
                    </div>
                </div>

        </div>
    </div>
</div>
</div>

<div class="row" ng-hide="editAttendance">
    <div class="col-md-12">
       <div class="box">
           <h3>Attendance and details</h3>
           <div class="row">
               <div class="col-md-6">
                   <label for="">Show
                       <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                       entries
                   </label>
               </div>
               <div class="col-md-6 text-right">
                   <div class="form-inline form-group">
                       <label for="filter-list">Search </label>
                       <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlistGroup">
                   </div>
               </div>
           </div>
           <div class="row">
               <div class="col-sm-5 text-right">
                   <div class="form-inline form-group">
                       <label for="" class="control-label">From</label>
                       <div class="input-group">
                           <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                       </div>
                   </div>
               </div>
               <div class="col-sm-6 text-left">
                   <div class="form-inline form-group">
                       <label for="" class="control-label">To</label>
                       <div class="input-group">
                           <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                       </div>
                       <button class="btn btn-warning" ng-click="searchAttendance(fromDate,toDate)">Search</button>
                   </div>
               </div>

           </div>
           <div class="panel panel-default">
               <div class="panel-heading">
                   <span class="text-success">Attendance and details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
               </div>
               <div class="panel-body">
                   <div class="table-responsive">
                       <table id="rentTable" class="table table-striped table-bordered table-hover">
                           <thead>
                           <tr>
                               <th>#</th>
                               <th>SlNo</th>
                               <th>Date</th>
                               <th>Project</th>
                               <th>Leader</th>
                               <th>Edit</th>
                               <th>View</th>
                               <th ng-show="extra">Created</th>
                               <th ng-show="extra">Updated</th>
                           </tr>
                           </thead>
                           <tbody>
                           <tr ng-repeat="attenList in listCount  = (attenLists | filter:filterlistGroup) | orderBy:'-date' | pagination: currentPage : numPerPage">
                               <td>{{attenList.id}}</td>
                               <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                               <td>{{attenList.date | date:'dd-MMMM-yyyy'}}</td>
                               <td>{{attenList.projects.name}}</td>
                               <td>{{attenList.leaders.employee.name}}</td>
                               <td ng-show="extra">{{attenList.created_at}}</td>
                               <td ng-show="extra">{{attenList.updated_at}}</td>
                               <td>
                                   <div ng-if="user.permissions.attendance.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                       <button type="button" class="btn btn-info" ng-click="editAttendanceGroup(attenList);">
                                           <i class="fa fa-pencil"></i>
                                       </button>
                                       <button type="button" class="btn btn-danger" ng-click="deleteAttendanceGroup(attenList); editmode = !editmode">
                                           <i class="fa fa-trash-o"></i>
                                       </button>
                                   </div>
                               </td>
                               <td>
                                   <button  ng-click="viewAttendance(attenList);" class="btn-sm btn-success">View</button>
                               </td>
                           </tr>
                           </tbody>
                       </table>
                   </div>
               </div>
           </div>
           <div class="clearfix" ng-show="attenLists.length > numPerPage">
               <pagination
                   ng-model="currentPage"
                   total-items="listCount.length"
                   max-size="maxSize"
                   items-per-page="numPerPage"
                   boundary-links="true"
                   class="pagination-sm pull-right"
                   previous-text="&lsaquo;"
                   next-text="&rsaquo;"
                   first-text="&laquo;"
                   last-text="&raquo;"
                   ></pagination>
       </div>
    </div>
</div>