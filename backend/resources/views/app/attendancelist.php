<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Attendance List </h2>
    </div>
</div>
<div class="col-md-12">
        <div class="box">
            <p>Please select the year-month to get Attendance List</p>
            <div class="form-inline form-group">
                <label for="exampleInputName2">Show Month: </label>
                <select class="form-control" ng-model="curCashoverview" ng-options="cashoverview.month for cashoverview in cashoverviews"></select>

            </div>
        </div>
        <div class="box">
            <!--<div class="col-lg-12">-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <p class="text-primary">Ledger Account Balance and Details</p>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tableTransfer">
                                <thead>
                                  <!--  <th>Employee</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th><th>17</th><th>18</th><th>19</th><th>20</th><th>21</th><th>22</th><th>23</th><th>24</th><th>25</th><th>26</th><th>27</th><th>28</th><th>29</th><th>30</th><th>31</th>-->
                                    <th>Employee</th>
                                  <th ng-repeat="day in days">{{day}}</th>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="employee in attendancelist">
                                        <td>{{employee.name}}</td>
                                        <td ng-repeat="day in days track by $index"><div ng-if=""></div></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
           <!-- </div>-->

            <div class="clearfix" ng-show="ledgers.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
</div>