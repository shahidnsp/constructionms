
<div class="row">
	<div class="col-md-12">
		<h2><i class="fa fa-bank"></i> Banks</h2>
        <ol class="breadcrumb-cus" ng-show="breadCrumbs.length">
            <li ng-repeat="breadCrumb in breadCrumbs" ng-class="{active: $last}">
                <a ng-if="!$last" href ng-click="switchDiv($event);" data-target="{{breadCrumb.target}}"><i class="fa fa-home" ng-show="$first"></i> {{breadCrumb.title}}</a>
                <span ng-if="$last">{{breadCrumb.title}}</span>
            </li>
        </ol>
	</div>
</div>
<div class="row" ng-switch on="openDiv">
	<div class="col-md-12" ng-switch-default>
		<div class="box">
			<button ng-if="user.permissions.banks.write =='true'" type="button" class="btn btn-primary pull-right" ng-hide="status.bankedit" ng-click="newBank();"><i class="fa fa-plus"></i> Add bank</button>
			<form class="form-horizontal" ng-submit="addBank()" ng-show="status.bankedit">
				<h3>New Bank</h3><br>
			  <div class="form-group">
					<label class="col-sm-2 control-label">Bank Name</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" ng-model="newbank.name" placeholder="Bank Name" required>
					</div>
			  </div>
				<div class="form-group">
				  <label class="col-sm-2 control-label">Branch Code</label>
				  <div class="col-sm-2">
				    <input type="text" class="form-control" ng-model="newbank.branchcode" placeholder="Branch Code">
				  </div>
				  <label class="col-sm-1 control-label">IFSC Code</label>
				  <div class="col-sm-3">
				    <input type="text" class="form-control" ng-model="newbank.ifsc" placeholder="IFSC Code">
				  </div>
				  <label class="col-sm-1 control-label">MICR</label>
				  <div class="col-sm-3">
				    <input type="text" class="form-control" ng-model="newbank.micrcode" placeholder="MICR">
				  </div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Swift</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" ng-model="newbank.swiftcode" placeholder="Swift">
					</div>
				  <label class="col-sm-1 control-label">Contact</label>
				  <div class="col-sm-3">
				    <input type="text" class="form-control" ng-model="newbank.contact" placeholder="Contact Number">
				  </div>
				  <label class="col-sm-1 control-label">Branch</label>
				  <div class="col-sm-3">
				    <input type="text" class="form-control" ng-model="newbank.branch" placeholder="Branch" required>
				  </div>
				</div>
			  <div class="form-group">
					<label class="col-sm-2 control-label">Address</label>
					<div class="col-sm-10">
						<textarea class="form-control" ng-model="newbank.address" placeholder="Address"></textarea>
					</div>
			  </div>
                <div class="form-group">
                  <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default" ng-click="cancelBank()">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </div>
				<hr>
			</form>
			<h3>Banks and details</h3>
			<div class="row">
				<div class="col-md-4">
					<label for="">Show 
						<select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
						entries
					</label>
				</div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn-info" ng-click="exportToExcel('bankTable')">Export To Excel</button>
                    </div>
                </div>
				<div class="col-md-5 text-right">
					<div class="form-inline form-group">
						<label for="filter-list">Search </label>
						<input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
					</div>
				</div>
			</div>
            <div class="col-lg-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                Banks and details
            </div>
            <div class="panel-body">
                <div class="table-responsive">
			<table id="bankTable" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
                        <th>#</th>
                        <th>SlNo</th>
						<th>Bank name</th>
						<th>Branch code</th>
						<th>IFSC</th>
						<th>MICR</th>
						<th>Swift</th>
						<th>Contact</th>
						<th>Branch</th>
                        <th ng-show="extra">User</th>
                        <th ng-show="extra">Created</th>
                        <th ng-show="extra">Updated</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="bank in listCount  = ( banks | filter:filterlist) | orderBy:'name' | pagination: currentPage : numPerPage ">
                        <td>{{bank.id}}</td>
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
						<td>
							<a href title="{{bank.name}}" ng-click="openAccount(bank);">{{bank.name}}</a>
						</td>
						<td>{{bank.branchcode}}</td>
						<td>{{bank.ifsc}}</td>
						<td>{{bank.micrcode}}</td>
						<td>{{bank.swiftcode}}</td>
						<td><i class="fa fa-phone"></i> <a href="tel:{{bank.contact}}" title="Contact bank">{{bank.contact}}</a></td>
						<td>{{bank.branch}}</td>
                        <td ng-show="extra">{{bank.user}}</td>
                        <td ng-show="extra">{{bank.created_at}}</td>
                        <td ng-show="extra">{{bank.updated_at}}</td>
						<td>
							<div ng-if="user.permissions.banks.edit =='true'"  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
								<button type="button" class="btn btn-default" ng-hide="editmode" ng-click="editBank(bank);">
									<i class="fa fa-pencil"></i>
								</button>
								<button type="button" class="btn btn-default" ng-hide="editmode" ng-click="deleteBank(bank,$index);">
									<i class="fa fa-trash-o"></i>
								</button>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
                </div>

            </div>
            </div>
            </div>
			<div class="clearfix">
				<pagination 
					ng-model="currentPage" 
					total-items="listCount.length" 
					max-size="maxSize" 
					items-per-page="numPerPage"
					boundary-links="true" 
					class="pagination-sm pull-right" 
					previous-text="&lsaquo;" 
					next-text="&rsaquo;" 
					first-text="&laquo;" 
					last-text="&raquo;"
				></pagination>
			</div>
		</div>
	</div>
    <div class="col-md-12" ng-switch-when="accounts">
        <div class="box">
            <button ng-if="user.permissions.banks.write =='true'" type="button" class="btn btn-primary pull-right" ng-hide="status.accountedit" ng-click="newAccount();"><i class="fa fa-plus"></i> Add Account</button>
            <form class="form-horizontal" ng-submit="addAccount()" ng-show="status.accountedit">
                <h3>New Bank Account</h3><br>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newaccount.name" placeholder="Name" required>
                    </div>
                    <label class="col-sm-1 control-label">Issue date</label>
                    <div class="col-sm-2">
						<div class="input-group">
							<input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newaccount.issueDate" is-open="issuepicker" show-button-bar="false" show-weeks="false" readonly>
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" ng-click="issuepicker=true"><i class="fa fa-calendar"></i></button>
							</span>
						</div>
                    </div>
                    <label class="col-sm-1 control-label">Balance</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" ng-model="newaccount.balance" placeholder="Account Balance" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Account Type</label>
                    <div class="col-sm-2">
                        <select class="form-control" ng-model="newaccount.type" required>
                            <option value="sb" label="Saving">Saving Account</option>
                            <option value="cr" label="Current">Current Account</option>
                            <option value="lo" label="Loan">Loan Account</option>
                            <option value="o" label="Other">Other</option>
                        </select>
                       <!-- <input type="text" class="form-control" ng-model="newaccount.type" placeholder="Account Type">-->
                    </div>
                    <label class="col-sm-2 control-label">Account Number</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" ng-model="newaccount.number" placeholder="Account Number" required>
                    </div>
                    <label class="col-sm-1 control-label">IBAN</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newaccount.iban" placeholder="IBAN">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelAccount();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3>Accounts and details</h3>
			<div class="row">
				<div class="col-md-4">
					<label for="">Show 
						<select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
						entries
					</label>
				</div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn-info" ng-click="exportToExcel('accountTable')">Export To Excel</button>
                    </div>
                </div>
				<div class="col-md-5 text-right">
					<div class="form-inline form-group">
						<label for="filter-list">Search </label>
						<input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
					</div>
				</div>
			</div>
            <div class="col-lg-12">
            <div class="panel panel-default">
            <div class="panel-heading">
                Account and details
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="accountTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>SlNo</th>
                    <th>Name</th>
                    <th>Number</th>
                    <th>Type</th>
                    <th>IBAN</th>
                    <th>Issue date</th>
                    <th>Balance</th>
                    <th ng-show="extra">User</th>
                    <th ng-show="extra">Created</th>
                    <th ng-show="extra">Updated</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
					<tr  ng-repeat="account in listCount = (accounts | filter:filterlist) | orderBy:'-issuedate' | pagination: currentPage : numPerPage">
                                <td>{{account.id}}</td>
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td><a href ng-click="openStatement(account);">{{account.name}}</a></td>
                                <td>{{account.number}}</td>
                                <td>{{account.type}}</td>
                                <td>{{account.iban}}</td>
                                <td>{{account.issueDate | date:'dd-MMMM-yyyy'}}</td>
                                <td><i class="fa fa-inr"></i> {{account.balance}}</td>
                                <td ng-show="extra">{{account.user}}</td>
                                <td ng-show="extra">{{account.created_at}}</td>
                                <td ng-show="extra">{{account.updated_at}}</td>
                                <td>
                                    <div ng-if="user.permissions.banks.edit =='true'" class="btn-group btn-group-xs" role="group">
                                        <button type="button" class="btn btn-default" ng-click="editAccount(account);">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="button" class="btn btn-default" ng-click="deleteAccount(account);">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                        </tr>
                </tbody>
            </table>
                    </div>
                </div>
                </div>
                </div>
			<div class="clearfix">
				<pagination 
					ng-model="currentPage" 
					total-items="listCount.length" 
					max-size="maxSize" 
					items-per-page="numPerPage"
					boundary-links="true" 
					class="pagination-sm pull-right" 
					previous-text="&lsaquo;" 
					next-text="&rsaquo;" 
					first-text="&laquo;" 
					last-text="&raquo;"
				></pagination>
			</div>
		</div>
	</div>
	<div ng-switch-when="selectedAcc">
		<tabset class="col-md-12">
			<tab>
				<tab-heading><i class="fa fa-newspaper-o"></i> Statement</a></tab-heading>
				<div class="box">
					<button type="button" class="btn btn-primary pull-right" ng-hide="status.particularedit" ng-click="newParticular();"><i class="fa fa-plus"></i> Add payment</button>
					<form class="form-horizontal" ng-submit="addParticular()" ng-show="status.particularedit">
						<h3>New Account Particular</h3><br>
					  <div class="form-group">
						  <label class="col-sm-2 control-label">Account</label>
							<div class="col-sm-2">
								<p class="form-control-static">{{myAccount.number}}</p>
							</div>
							<label class="col-sm-2 control-label">Date</label>
							<div class="col-sm-2">
								<div class="input-group">
									<input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newparticular.payDate" is-open="particularpicker" show-button-bar="false" show-weeks="false" readonly>
									<span class="input-group-btn">
										<button type="button" class="btn btn-default" ng-click="particularpicker=true"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
							</div>
							<label class="col-sm-2 control-label">Cheque No</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" ng-model="newparticular.chequeNo" placeholder="Cheque Number">
							</div>

					  </div>
						<div class="form-group">
						  <label class="col-sm-2 control-label">Withdraw</label>
						  <div class="col-sm-3">
						    <input type="text" class="form-control" ng-model="newparticular.withdrawal" placeholder="Withdraw Amount">
						  </div>
						  <label class="col-sm-2 control-label">Deposit</label>
						  <div class="col-sm-3">
						    <input type="text" class="form-control" ng-model="newparticular.deposit" placeholder="Deposit Amount">
						  </div>
<!--						  <label class="col-sm-2 control-label">Balance</label>-->
<!--						  <div class="col-sm-2">-->
<!--						    <input type="text" class="form-control" ng-model="newparticular.balance" placeholder="Balance Amount">-->
<!--						  </div>-->
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Description</label>
							<div class="col-sm-10">
							  <textarea class="form-control" ng-model="newparticular.description" placeholder="Description" required></textarea>
							</div>
						</div>
						<div class="form-group">
						  <div class="col-md-12 text-right">
						  	<button type="button" class="btn btn-default" ng-click="cancelParticular();">Cancel</button>
							  <button type="submit" class="btn btn-primary">Save</button>
						  </div>
						</div>
						<hr>
					</form>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto magnam eius quia, sint mollitia, ipsum error consequatur corrupti. Voluptas tenetur totam aut quo ea consequatur et illo optio, deleniti officia!</p>
					<div class="row">
						<div class="col-md-4">
							<label for="">Show 
								<select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
								entries
							</label>
						</div>
                        <div class="col-sm-3 text-center">
                            <div>
                                <button class="btn-info" ng-click="exportToExcel('statementTable')">Export To Excel</button>
                            </div>
                        </div>
						<div class="col-md-5 text-right">
							<div class="form-inline form-group">
								<label for="filter-list">Search </label>
								<input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
							</div>
						</div>
					</div>
                    <div class="row">
                        <div class="col-sm-5 text-right">
                            <div class="form-inline form-group">
                                <label for="" class="control-label">From</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 text-left">
                            <div class="form-inline form-group">
                                <label for="" class="control-label">To</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                                </div>
                                <button class="btn-warning" >Search</button>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Account Statements and details
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="statementTable" class="table table-striped table-bordered table-hover">
						<thead>
							<tr class="bg-primary">
                                <th>#</th>
                                <th>SlNo</th>
								<th>Date</th>
								<th>Description</th>
								<th>Cheque No</th>
								<th>Withdrawals</th>
								<th>Deposits</th>
<!--							<th>Balance</th>-->
                                <th ng-show="extra">User</th>
                                <th ng-show="extra">Created</th>
                                <th ng-show="extra">Updated</th>
								<th>Edit</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="particular in listCount = (particulars | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                <td>{{particular.id}}</td>
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
								<td>{{particular.payDate | date:'dd-MMMM-yyyy'}}</td>
								<td><p class="description" popover="{{particular.description}}" popover-trigger="mouseenter">{{particular.description}}</p></td>
								<td>{{particular.chequeNo}}</td>
								<td><i class="fa fa-inr"></i> {{particular.withdrawal}}</td>
								<td><i class="fa fa-inr"></i> {{particular.deposit}}</td>
								<!--<td><i class="fa fa-inr"></i> {{particular.balance}}</td>-->
                                <td ng-show="extra"> {{particular.user}}</td>
                                <td ng-show="extra"> {{particular.created_at}}</td>
                                <td ng-show="extra"> {{particular.updated_at}}</td>
								<td>
									<div class="btn-group btn-group-xs" role="group">
										<button type="button" class="btn btn-default" ng-click="editParticular(particular);">
											<i class="fa fa-pencil"></i>
										</button>
										<button type="button" class="btn btn-default" ng-click="deleteParticular(particular);">
											<i class="fa fa-trash-o"></i>
										</button>
									</div>
								</td>
							</tr>
                        <tr>
                            <td colspan="5" class="alert-danger">Total</td><td class="alert-danger"><i class="fa fa-inr"></i> {{totalwithdrawal}}</td><td class="alert-danger"><i class="fa fa-inr"></i> {{totaldeposit}}</td>
                        </tr>
						</tbody>
					</table>
                                    </div>
                                </div>
                            </div>
                        </div>
					<div class="clearfix">
						<pagination 
							ng-model="currentPage" 
							total-items="listCount.length" 
							max-size="maxSize" 
							items-per-page="numPerPage"
							boundary-links="true" 
							class="pagination-sm pull-right" 
							previous-text="&lsaquo;" 
							next-text="&rsaquo;" 
							first-text="&laquo;" 
							last-text="&raquo;"
						></pagination>
					</div>
				</div>
			</tab>
			<!--<tab>
				<tab-heading><i class="fa fa-bookmark-o"></i> Cheque</a></tab-heading>
				<div class="row box">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius necessitatibus nam reprehenderit animi optio voluptas aspernatur alias dolore libero, atque cumque, inventore molestias nisi nihil modi quae, hic velit eos. ipsum dolor sit amet, consectetur adipisicing elit. Quam magni aperiam nisi eum explicabo consequatur veritatis, totam iure ipsum distinctio! Fugiat et optio molestias ut officiis impedit nam aliquam dignissimos. ipsum dolor sit amet, consectetur adipisicing elit. Dolore ducimus aut esse quidem. Ratione cumque, officia vero voluptate facere laborum accusantium velit consectetur nobis delectus, dolorem, illum commodi laudantium quaerat! ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem esse, ratione nam ea. Sint voluptates dolore id sapiente ea, architecto adipisci dolorem natus culpa itaque, ad repellat doloribus eius quas.</p>
					<accordion>
					  <accordion-group ng-repeat="cheque in cheques" is-open="$first">
					  	<accordion-heading><i class="fa fa-files-o"></i> From: {{cheque.from}} To:{{cheque.to}}</accordion-heading>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore ducimus aut esse quidem. Ratione cumque, officia vero voluptate facere laborum accusantium velit consectetur nobis delectus, dolorem, illum commodi laudantium quaerat! ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem esse, ratione nam ea. Sint voluptates dolore id sapiente ea, architecto adipisci dolorem natus culpa itaque, ad repellat doloribus eius quas.</p>
								<form class="form-horizontal">
									<div class="form-group">
										<label for="nameon-card" class="col-md-1 control-label">Accoount No</label>
										<div class="col-md-2">
											<p class="form-control-static">{{cheque.accountno}}</p>
										</div>
										<label for="nameon-card" class="col-md-1 control-label">Issue Date</label>
										<div class="col-md-2">
											<p class="form-control-static">{{cheque.issuedate}}</p>
										</div>
										<label for="nameon-card" class="col-md-1 control-label">From</label>
										<div class="col-md-2">
											<p class="form-control-static">{{cheque.from}}</p>
										</div>
										<label for="nameon-card" class="col-md-1 control-label">To</label>
										<div class="col-md-2">
											<p class="form-control-static">{{cheque.from}}</p>
										</div>
									</div>
								</form>
								<table class="table table-hover table-responsive">
									<thead>
										<tr class="bg-primary">
											<th>Cheque Number</th>
											<th>Date</th>
											<th>Favour</th>
											<th>Amount</th>
											<th>Deposit</th>
											<th>Balance</th>
											<th>Edit</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="chequerecord in cheque.chequerecords">
											<td>{{chequerecord.number}}</td>
											<td>{{chequerecord.date}}</td>
											<td><p class="description" popover="{{chequerecord.favor}}" popover-trigger="mouseenter">{{chequerecord.favor}}</p></td>
											<td><i class="fa fa-inr"></i> {{chequerecord.amount}}</td>
											<td><i class="fa fa-inr"></i> {{chequerecord.deposit}}</td>
											<td><i class="fa fa-inr"></i> {{chequerecord.balance}}</td>
											<td>
												<div class="btn-group btn-group-xs" role="group">
													<button type="button" class="btn btn-default" ng-click="editAccount(account);">
														<i class="fa fa-pencil"></i>
													</button>
													<button type="button" class="btn btn-default" ng-click="deleteAccount(account);">
														<i class="fa fa-trash-o"></i>
													</button>
												</div>
											</td>
										</tr>
									</tbody>
								</table>

					  </accordion-group>
					</accordion>
				</div>
			</tab>
			<tab>
				<tab-heading><i class="fa fa-credit-card"></i> Cards</a></tab-heading>
				<div class="row box">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla totam natus voluptate nam dolore eveniet modi, tempora quis accusantium repellendus, similique, tenetur aliquam repellat eligendi incidunt maiores. Repudiandae, explicabo, consequatur.</p>
					<accordion close-others="status.oneAtATime">
						<accordion-group ng-repeat="card in cards" is-open="$first">
							<accordion-heading><i class="fa fa-credit-card"></i> {{card.cardno}}</accordion-heading>
							<div class="row">
								<div class="col-sm-12">
									<form class="form-horizontal" ng-submit="addCard();">
									  <div class="form-group">
											<label for="nameon-card" class="col-md-2 control-label">Cardholder Name</label>
											<div class="col-md-10">
												<p class="form-control-static">{{card.cardholder}}</p>
											  <!-- <input type="text" class="form-control" id="nameon-card" placeholder="Name on card" required> -->
											<!--</div>
									  </div>

									  <div class="form-group">
											<div class="row">
											  <div class="col-md-8">
													<label for="card-number" class="col-md-3 control-label">Card number</label>
													<div class="col-md-9">
														<p class="form-control-static">{{card.cardno}}</p>
													  <!-- <input type="text" class="form-control" id="card-number" placeholder="Card number" maxlength="16" required> -->
													<!--</div>
											  </div>
											  <div class="col-md-4">
													<label for="card-type" class="col-md-2 control-label">Type</label>
													<div class="col-md-10">
														<p class="form-control-static">{{card.cardtype}}</p>
														<!-- <select name="card-type" id="card-type" class="form-control" required>
															<option value="visa">VISA</option>
															<option value="master">Master</option>
														</select> -->
											<!--		</div>
												</div>
											</div>
									  </div>
									  <div class="form-group">
											<div class="row">
												<div class="col-md-4">
													<label class="col-md-6 control-label">Card from</label>
													<div class="col-md-3">
														<p class="form-control-static">{{card.validfrom}}</p>
															<!-- <select name="day" class="form-control">
																<option value="01">01</option>
																<option value="02">02</option>
																<option value="03">03</option>
																<option value="04">04</option>
																<option value="05">05</option>
																<option value="06">06</option>
																<option value="07">07</option>
																<option value="08">08</option>
																<option value="09">09</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
															</select>
													</div>
													<div class="col-md-3">
													  <input type="text" class="form-control" placeholder="year" maxlength="2"> -->
													<!--</div>
											  </div>
												<div class="col-md-4">
													<label class="col-md-4 control-label">Expiry</label>
													<div class="col-md-4">
														<p class="form-control-static">{{card.expirydate}}</p>
													  <!-- <select name="day" class="form-control" required>
															<option value="01">01</option>
															<option value="02">02</option>
															<option value="03">03</option>
															<option value="04">04</option>
															<option value="05">05</option>
															<option value="06">06</option>
															<option value="07">07</option>
															<option value="08">08</option>
															<option value="09">09</option>
															<option value="10">10</option>
															<option value="11">11</option>
															<option value="12">12</option>
													  </select>
													</div>
													<div class="col-md-4">
													  <input type="text" class="form-control" placeholder="year" maxlength="2" required> -->
												<!--	</div>
											  </div>
												<div class="col-md-3">
													<label class="col-md-6 control-label">Security Code</label>
													<div class="col-md-6">
														<p class="form-control-static">{{card.cvv}}</p>
													  <!-- <input type="text" class="form-control" placeholder="CCV" maxlength="3" required> -->
												<!--	</div>
											  </div>
											</div>
									  <div class="form-group">
									  	<div class="col-md-12">
											  <button type="submit" class="btn btn-primary pull-right">Edit card</button>
										  </div>
									  </div>
									</form>
								</div>
							</div>
						</accordion-group>
						<accordion-group class="panel panel-primary">
							<accordion-heading><i class="fa fa-credit-card"></i> Add new card</accordion-heading>
							<form class="form-horizontal" ng-submit="addCard();">
							  <div class="form-group">
									<label for="nameon-card" class="col-md-2 control-label">Cardholder Name</label>
									<div class="col-md-10">
									  <input type="text" class="form-control" id="nameon-card" ng-model="newcard.cardholder" placeholder="Name on card" required>
									</div>
							  </div>
							  <div class="form-group">
									<div class="row">
									  <div class="col-md-8">
											<label for="card-number" class="col-md-3 control-label">Card number</label>
											<div class="col-md-9">
											  <input type="text" class="form-control" id="card-number" ng-model="newcard.cardno" placeholder="Card number" maxlength="16" required>
											</div>
									  </div>
									  <div class="col-md-4">
											<label for="card-type" class="col-md-2 control-label">Type</label>
											<div class="col-md-10">
												<select name="card-type" id="card-type" ng-model="newcard.cardtype" class="form-control" required>
													<option value="Visa">VISA</option>
													<option value="Master">Master</option>
                                                    <option value="Mastro">Mastro</option>
												</select>
											</div>
										</div>
									</div>
							  </div>
							  <div class="form-group">
									<div class="row">
										<div class="col-md-4">
											<label class="col-md-6 control-label">Card from</label>
											<!--<div class="col-md-3">
													<select name="day" class="form-control">
														<option value="01">01</option>
														<option value="02">02</option>
														<option value="03">03</option>
														<option value="04">04</option>
														<option value="05">05</option>
														<option value="06">06</option>
														<option value="07">07</option>
														<option value="08">08</option>
														<option value="09">09</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
													</select>
											</div>-->
										<!--	<div class="col-md-6">
											  <input type="text" class="form-control" ng-model="newcard.validfrom" placeholder="Month/Year">
											</div>
									  </div>
										<div class="col-md-4">
											<label class="col-md-4 control-label">Expiry</label>
											<!--<div class="col-md-4">
											  <select name="day" class="form-control" required>
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
												<option value="06">06</option>
												<option value="07">07</option>
												<option value="08">08</option>
												<option value="09">09</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
											  </select>
											</div>-->
											<!--<div class="col-md-6">
											  <input type="text" class="form-control" ng-model="newcard.expirydate" placeholder="Month/Year"  required>
											</div>
									  </div>
										<div class="col-md-3">
											<label class="col-md-6 control-label">Security Code</label>
											<div class="col-md-6">
											  <input type="text" class="form-control" ng-model="newcard.cvv" placeholder="CCV" maxlength="5" required>
											</div>
									  </div>
									</div>
								</div>
							  <div class="form-group">
							  	<div class="col-md-12">
									  <button type="submit" class="btn btn-primary pull-right">Submit new card</button>
								  </div>
							  </div>
							</form>
						</accordion-group>
					</accordion>
				</div>
			</tab>-->
		</tabset>
	</div>
</div>
