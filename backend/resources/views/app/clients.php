<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Clients</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.clients.write =='true'" ng-click="newClient();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Client</button>
            <form class="form-horizontal" ng-show="clientedit" ng-submit="addClient();">
                <h3>New Client</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newclient.name" placeholder="Name" required="">
                    </div>
                    <label for="" class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-3">
                        <textarea class="form-control" ng-model="newclient.address"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Place</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newclient.place" placeholder="Place"/>
                    </div>

                    <label for="" class="col-sm-2 control-label">District</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" placeholder="District" ng-model="newclient.district"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Mobile</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newclient.mobile" placeholder="Mobile"/>
                    </div>

                    <label for="" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-3">
                        <input type="email" class="form-control" placeholder="Email" ng-model="newclient.email"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newclient.phone" placeholder="Phone"/>
                    </div>
                    <label for="" class="col-sm-2 control-label">Cli.Visited Date</label>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newclient.visited"  is-open="visitedpicker" show-button-bar="false" show-weeks="false" readonly>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="visitedpicker=true"><i class="fa fa-calendar"></i></button>
                                </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Site Visiting</label>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newclient.firstvisiting"  is-open="fvisitepicker" show-button-bar="false" show-weeks="false" readonly required="">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="fvisitepicker=true"><i class="fa fa-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                    <label for="" class="col-sm-2 control-label">Reminder Date</label>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newclient.reminderdate"  is-open="reminderpicker" show-button-bar="false" show-weeks="false" readonly>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="reminderpicker=true"><i class="fa fa-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" ng-model="description"></textarea>
                        </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Opening Balance</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" ng-model="newclient.openBal" placeholder="Opening Balance"/>
                    </div>

                    <label for="" class="col-sm-1 control-label">Cr/Dr</label>
                    <div class="col-sm-2">
                        <select ng-model="newclient.crOrDr" class="form-control">
                            <option value="Dr">Debit</option>
                            <option value="Cr">Credit</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelClient();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="rentedit">Clients and details</h3>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-md-6 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Clients and details
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="unitTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                               <!-- <th>#</th>-->
                                <th>SlNo</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Place</th>
                                <th>Visiting date</th>
                                <th ng-show="extra">Cli.Visited Date</th>
                                <th ng-show="extra">Created</th>
                                <th ng-show="extra">Updated</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="client in listCount  = (clients | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                <!--<td>{{client.id}}</td>-->
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td>{{client.name}}</td>
                                <td>{{client.phone}}</td>
                                <td>{{client.address}}</td>
                                <td>{{client.place}}</td>
                                <td style="font-weight: bold">{{client.firstvisiting | date : "dd-mm-y"}}</td>
                                <td ng-show="extra">{{client.visited}}</td>
                                <td ng-show="extra">{{client.created_at}}</td>
                                <td ng-show="extra">{{client.updated_at}}</td>
                                <td>
                                    <div ng-if="user.permissions.clients.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                        <button type="button" class="btn btn-default" ng-click="editClient(client);">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="button" class="btn btn-default" ng-click="deleteClient(client); editmode = !editmode">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="clearfix" ng-show="clients.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>