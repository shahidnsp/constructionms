

<button type="button" class="close" ng-click="close();">
    <i class="fa fa-times-circle-o" style="margin:10px;color:blue;"></i>
</button>
<div class="modal-header">
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-credit-card"> </i> Create New Ledger Account </h2>
        </div>
    </div>
</div>
<div class="modal-body">

    <div class="container">
        <div class="row">

            <div class="toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Accounts Ledger</h3>
                    </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box">
                                    <button ng-if="user.permissions.ledger.write =='true'" class="btn btn-primary pull-right" type="button" ng-click="newLedger()"><i class="fa fa-plus"></i>Add new ledger</button>
                                    <form class="form-horizontal" ng-show="ledgerEdit" ng-submit="addLedger()" >
                                        <h3>New Ledger</h3>

                                        <tabset class="col-md-12">
                                            <tab>
                                                <tab-heading>Main Details</tab-heading>

                                                <div class="form-group">
                                                    <label class="control-label col-sm-1">Name</label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control" placeholder="Account Name" ng-model="newledger.name" required/>
                                                    </div>
                                                    <label class="col-sm-1 control-label">Group</label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control" ng-model="newledger.unders_id" ng-change="accountLedgerForBank(newledger.unders_id);" required>
                                                            <option value="">Select</option>
                                                            <option ng-repeat="under in unders" value="{{under.id}}">{{under.name}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div ng-show="bank" class="form-group">
                                                    <label class="control-label col-sm-2">Account No</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" placeholder="Account Number" ng-model="newledger.bankAccountNumber"/>
                                                    </div>
                                                </div>
                                                <div ng-show="bank" class="form-group">
                                                    <label class="control-label col-sm-2">Branch Name</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" placeholder="Branch Name" ng-model="newledger.branchName"/>
                                                    </div>
                                                    <label class="control-label col-sm-2">Branch Code</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" placeholder="Branch Code" ng-model="newledger.branchCode"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-2 control-label">Opening Balance</label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control" ng-model="newledger.openingBalance" placeholder="Opening Balance">
                                                    </div>
                                                    <label for="" class="col-sm-1 control-label">Mode</label>
                                                    <div class="col-sm-4">
                                                        <select class="form-control" ng-model="newledger.crOrDr">
                                                            <option value="Dr">Dr</option>
                                                            <option value="Cr">Cr</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-1 control-label">Narration</label>
                                                    <div class="col-sm-11">
                                                        <textarea class="form-control" ng-model="newledger.narration" placeholder="Narration"></textarea>
                                                    </div>
                                                </div>
                                            </tab>
                                            <tab>
                                                <tab-heading>Secondary Details</tab-heading>
                                                <div ng-show="secondary">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2">Mailing Name</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control" placeholder="Mailing Name" ng-model="newledger.mailingName"/>
                                                        </div>
                                                        <label class="control-label col-sm-2">Phone</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control" placeholder="Phone" ng-model="newledger.phone"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2">Address</label>
                                                        <div class="col-sm-4">
                                                            <textarea class="form-control" placeholder="Mailing Name" ng-model="newledger.address"></textarea>
                                                        </div>
                                                        <label class="control-label col-sm-2">Mobile</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control" placeholder="Mobile" ng-model="newledger.mobile"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2">Email</label>
                                                        <div class="col-sm-4">
                                                            <input type="email" class="form-control" placeholder="Email" ng-model="newledger.email"/>
                                                        </div>
                                                        <label class="control-label col-sm-2">Credit Limit</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control" placeholder="Credit Limit" ng-model="newledger.creditLimit"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2">Credit Period</label>
                                                        <div class="col-sm-4">
                                                            <input type="email" class="form-control" placeholder="Credit Period" ng-model="newledger.creditPeriod"/>
                                                        </div>
                                                        <label class="control-label col-sm-2">CST</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control" placeholder="CST" ng-model="newledger.cst"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2">TIN</label>
                                                        <div class="col-sm-4">
                                                            <input type="email" class="form-control" placeholder="TIN" ng-model="newledger.tin"/>
                                                        </div>
                                                        <label class="control-label col-sm-2">PAN</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control" placeholder="PAN" ng-model="newledger.pan"/>
                                                        </div>
                                                    </div>
                                                </div>

                                            </tab>
                                        </tabset>
                                        <div class="form-group">
                                            <div class="col-sm-12 text-right">
                                                <br/>
                                                <button class="btn btn-danger" ng-click="cancel()">Cancel</button>
                                                <button class="btn btn-default">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div ng-hide="ledgerEdit">
                                    <h3 ng-hide="ledgeredit">Ledger Account and details</h3>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="">Show
                                                <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                                entries
                                            </label>
                                        </div>

                                        <div class="col-md-8 text-right">
                                            <div class="form-inline form-group">
                                                <label for="filter-list">Search </label>
                                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Ledger Accounts
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-responsive table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Under</th>
                                                        <th>Type</th>
                                                        <th ng-show="extra">Created_at</th>
                                                        <th ng-show="extra">Updated_at</th>
                                                        <td>Edit</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr ng-repeat="ledger in listCount = (ledgers | filter:filterlist) | orderBy:'-created_at' | pagination:currentPage : numPerPage">
                                                        <td>{{ledger.id}}</td>
                                                        <td><a ng-click="select(ledger);">{{ledger.name}}</a></td>
                                                        <td>{{ledger.unders.name}}</td>
                                                        <td>{{ledger.type}}</td>
                                                        <td ng-show="extra">{{ledger.created_at}}</td>
                                                        <td ng-show="extra">{{ledger.updated_at}}</td>
                                                        <td>
                                                            <div ng-if="user.permissions.ledger.edit =='true'" ng-hide="ledger.isedit==0" class="btn-group btn-group-xs" role="group">
                                                                <button type="button" class="btn btn-default" ng-click="editLedger(ledger)"><i class="fa fa-pencil"></i></button>
                                                                <button type="button" class="btn btn-default" ng-click="deleteLedger(ledger)"><i class="fa fa-trash-o"></i></button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix" ng-show="ledgers.length > numPerPage">
                                        <pagination
                                            ng-model="currentPage"
                                            total-items="listCount.length"
                                            max-size="maxSize"
                                            items-per-page="numPerPage"
                                            boundary-links="true"
                                            class="pagination-sm pull-right"
                                            previous-text="&lsaquo;"
                                            next-text="&rsaquo;"
                                            first-text="&laquo;"
                                            last-text="&raquo;"
                                            ></pagination>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <div class="panel-footer">
                        <span class="pull-right">
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" ng-click="close();"><i class="fa fa-close"></i></a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



