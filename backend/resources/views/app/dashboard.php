<div ng-controller="dashboardController">

    <!-- top tiles -->
    <div class="row tile_count">
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Cash Balance</span>
            <div class="count"><i class="fa fa-inr"></i> {{status.cashBalance}}</div>
<!--            <span class="count_bottom"><i class="greenDark">4% </i> From last Week</span>-->
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-clock-o"></i> Sundry Debtors</span>
            <div class="count"><i class="fa fa-inr"></i> {{status.sundryDebtor}}</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Sundry Creditors</span>
            <div class="count"><i class="fa fa-inr"></i> {{status.sundryCreditor}}</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Indirect Expense</span>
            <div class="count"><i class="fa fa-inr"></i> {{status.indirectExpense}}</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Indirect Income</span>
            <div class="count"><i class="fa fa-inr"></i> {{status.indirectIncome}}</div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
            <div class="count">{{status.usercount}}</div>
        </div>
    </div>
    <!-- /top tiles -->
    <br />
  <div class="row">
    <!--<tabset justified="true" class="col-md-4">
      <tab>
        <tab-heading>
          <strong><i class="fa fa-clock-o fa-lg"></i> Project status</strong>
        </tab-heading>
        <ul id="last-pay" class="list-group">
          <li class="list-group-item" ng-repeat="project in project_status">
            <span class="badge progress-bar-warning">{{project.percentage}}%</span>
            {{project.name}}
            <div class="progress">
              <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{{project.percentag}}" aria-valuemin="0" aria-valuemax="100" style="width: {{project.percentage}}%">
                <span class="sr-only">{{project.percentag}}% Complete</span>
              </div>
            </div>
          </li>
        </ul>
      </tab>
      <tab>
        <tab-heading>
          <strong><i class="fa fa-hand-o-left fa-lg"></i> Project details</strong>
        </tab-heading>
        <div class="box">
          <form class="form-horizontal" ng-submit="projectReport()">
            <div class="form-group">
              <label for="project-name" class="col-sm-2 control-label">Project</label>
              <div class="col-sm-10" id="project-name">
                <select class="form-control" ng-model="projectName" ng-options="project.name as project.name for project in project_status"></select>
              </div>
            </div>
            <div class="form-group">
              <label for="filter-date" class="col-sm-2 control-label">Date</label>
              <div class="col-sm-7">
                <div class="input-group">
                  <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="projectMonth" is-open="projectpicker" show-button-bar="false" show-weeks="false" readonly>
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-default" ng-click="projectpicker=true"><i class="fa fa-calendar"></i></button>
                  </span>
                </div>
              </div>
              <div class="col-sm-3">
                <button type="submit" class="btn btn-primary btn-block">Load</button>
              </div>
            </div>
          </form>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, natus ratione facere, pariatur excepturi mollitia velit doloribus et fuga quo quos? Neque voluptate eum temporibus esse porro, voluptates illum quas.</p>
        </div>
      </tab>
    </tabset>-->
      <div class="col-md-4" ng-controller="projectController">
          <div class="list-group">
              <div class="list-group-item active">
                  <i class="fa fa-lg fa-briefcase"> </i> Projects
              </div>
              <div class="list-group-item" ng-repeat="project in projects | orderBy: '-date'">

              </div>
          </div>
      </div>
    <div class="col-md-4">
      <div class="list-group">
        <div class="list-group-item active"> 
          <i class="fa fa-lg fa-tasks"> </i> Reminders
        </div>
        <div class="list-group-item" ng-repeat="task in tasks | orderBy: '-date'">
        <!-- <div class="list-group-item" ng-repeat="task in tasks"> -->
            <a href class="edit-task" ng-click="editTask(task)">
                <span class="fa  fa-edit"></span>
            </a>
          <a href class="delete-task" ng-click="deleteTask(task)">
            <span class="fa  fa-trash-o"></span>
          </a>
          <label>
            <input type="checkbox" class="hide task-checkbox" ng-model="task.done">
            <i class="fa fa-lg" ng-class="task.done ? 'fa-check-square-o  text-muted' : 'fa-square-o'"></i> 
          </label>
          <a href class="complete-{{task.done}}" ng-init="readItem = false" ng-click="readItem = !readItem;">{{task.name}}</a>
          <div class="task-date text-muted"><i class="fa fa-calendar"></i> <span class="complete-{{task.done}}">{{task.regDate | date : 'fullDate'}}</span></div>
            <div class="task-date text-muted"><i class="fa fa-calendar"></i> <span class="complete-{{task.done}}">{{task.remDate | date : 'fullDate'}}</span></div>
          <p class="task-detail complete-{{task.done}}" ng-show="readItem">{{task.description}}</p>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading"> 
          <h3 class="panel-title"><i class="fa fa-lg fa-tasks"></i> New Reminder</h3>
        </div>
        <div class="panel-body">
          <form ng-submit="addTask()">
            <div class="form-group">
              <label>Task</label>
              <input type="text" class="form-control" placeholder="What you need to do ?" ng-model="newTask.name" required>
            </div>
            <div class="form-group">
              <label for="">Description</label>
              <textarea class="form-control" placeholder="Details" ng-model="newTask.description"></textarea>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
                  <label>Reg Date</label>
                  <div class="input-group">
                    <input type="text" class="form-control" ng-model="newTask.regDate" datepicker-popup="dd-MMMM-yyyy" is-open="taskregpicker" show-button-bar="false" show-weeks="false" readonly>
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default" ng-click="taskregpicker=true"><i class="fa fa-calendar"></i></button>
                    </span>
                  </div>
                </div>
                  <div class="col-md-6">
                      <label>Reminder Date</label>
                      <div class="input-group">
                          <input type="text" class="form-control" ng-model="newTask.remDate" datepicker-popup="dd-MMMM-yyyy" is-open="taskrempicker" show-button-bar="false" show-weeks="false" readonly>
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default" ng-click="taskrempicker=true"><i class="fa fa-calendar"></i></button>
                    </span>
                      </div>
                  </div>
              </div>
            </div>
              <div class="form-group">
                  <div class="col-md-8">
                      <label for="">&nbsp;</label>
                      <input type="submit" class="btn btn-primary btn-block" value="New Reminder">
                  </div>
                  <div class="col-md-4">
                      <label for="">&nbsp;</label>
                      <input type="button" class="btn btn-primary btn-block" ng-click="cancelTask()" value="Cancel">
                  </div>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>