<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Day Book </h2>
    </div>
</div>
<div class="box">
    <p class="text-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta corrupti ipsum quo, autem placeat assumenda repellat? Rem alias minima quas quis, aut quidem, labore amet accusantium ad earum officiis doloremque.</p>
</div>
<div class="row">
<tabset class="col-md-12">
<tab>
    <tab-heading>Cash/Bank Book</tab-heading>
    <div class="row">
        <p class="text-success">Journal entry is an entry to the journal. Journal is a record that keeps accounting transactions in chronological order, i.e. as they occur. Ledger is a record that keeps accounting transactions by accounts. Account is a unit to record and summarize accounting transactions.</p>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="row">
                    <div class="col-md-4">
                        <label for="">Show
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                            entries
                        </label>
                    </div>
                    <div class="col-sm-3 text-center">
                        <div>
                            <button class="btn btn-sm btn-info" ng-click="exportToExcel('cashbookTable')">Export To Excel</button>
                        </div>
                    </div>
                    <div class="col-md-5 text-right">
                        <div class="form-inline form-group">
                            <label for="filter-list">Search </label>
                            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5 text-right">
                        <div class="form-inline form-group">
                            <label for="" class="control-label">From</label>
                            <div class="input-group">
                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 text-left">
                        <div class="form-inline form-group">
                            <label for="" class="control-label">To</label>
                            <div class="input-group">
                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                            </div>
                            <button class="btn btn-warning" ng-click="getCashBookByDate(fromDate,toDate)">Search</button>
                        </div>
                    </div>

                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="text-success">Cash/Bank Book from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <table id="cashbookTable" class="table table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Particular</th>
                                        <th>Opening</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                        <th>Closing</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="text-center" ng-repeat="cashbook in listCount  = (cashbooks | filter:filterlist) | pagination: currentPage : numPerPage">
                                            <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                            <td><a ng-click="OpenAccountBalance(cashbook.unders_id);">{{cashbook.particular}}</a></td>
                                            <td><i class="fa fa-inr"></i> {{cashbook.openingBalance}}</td>
                                            <td><i class="fa fa-inr"></i> {{cashbook.debit}}</td>
                                            <td><i class="fa fa-inr"></i> {{cashbook.credit}}</td>
                                            <td class="text-right text-danger"><i class="fa fa-inr"></i> {{cashbook.closing}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
<!--                            <div class="col-lg-6">-->
<!--                                <table class="table table-responsive table-hover">-->
<!--                                    <thead>-->
<!--                                    <tr>-->
<!--                                        <th><input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlistCr"> </th><th colspan="2"> <span class="col-lg-12 text-right">Cr</span></th>-->
<!--                                    </tr>-->
<!--                                    <tr>-->
<!--                                        <th>Date</th>-->
<!--                                        <th>Particular</th>-->
<!--                                        <th>Amount</th>-->
<!--                                    </tr>-->
<!--                                    </thead>-->
<!--                                    <tbody>-->
<!--                                    <tr ng-repeat="cashbook in listCount  = (cashbooks | filter:filterlistCr) | orderBy:'-date' | pagination: currentPage : numPerPage" ng-if="cashbook.side=='Cr'">-->
<!--                                        <td>{{cashbook.date | date:'dd-MMMM-yyyy'}}</td>-->
<!--                                        <td>{{cashbook.particular}}</td>-->
<!--                                        <td><i class="fa fa-inr"></i> {{cashbook.amount}}</td>-->
<!--                                    </tr>-->
<!--                                    <tr class="text-danger" ng-show="cashBalanceSide=='Cr'">-->
<!--                                        <td></td>-->
<!--                                        <td>Balance c/d</td>-->
<!--                                        <td> <i class="fa fa-inr"></i> {{cashBalance}} </td>-->
<!--                                    </tr>-->
<!--                                    <tr class="text-success">-->
<!--                                        <td></td><td></td><td><i class="fa fa-inr"></i> {{cashCreditTotal}}</td>-->
<!--                                    </tr>-->
<!--                                    </tbody>-->
<!--                                </table>-->
<!--                            </div>-->
                        </div>
                       <!-- <div class="row">
                            <div ng-show="cashBalanceSide=='Dr'" class="col-lg-6 text-danger">
                                <div class="col-lg-4">

                                </div>
                                <div class="col-lg-4">
                                    Balance b/d
                                </div>
                                <div class="col-lg-4">
                                    <i class="fa fa-inr"></i> {{cashBalance}}
                                </div>
                            </div>
                            <div ng-show="cashBalanceSide=='Cr'" class="col-lg-6 text-danger">
                                <div class="col-lg-4">

                                </div>
                                <div class="col-lg-4">
                                    Balance c/d
                                </div>
                                <div class="col-lg-4">
                                    <i class="fa fa-inr"></i> {{cashBalance}}
                                </div>
                            </div>
                        </div>-->

                    </div>
                </div>
                <div class="clearfix" ng-show="cashbooks.length > numPerPage">
                    <pagination
                        ng-model="currentPage"
                        total-items="listCount.length"
                        max-size="maxSize"
                        items-per-page="numPerPage"
                        boundary-links="true"
                        class="pagination-sm pull-right"
                        previous-text="&lsaquo;"
                        next-text="&rsaquo;"
                        first-text="&laquo;"
                        last-text="&raquo;"
                        ></pagination>
                </div>
            </div>
        </div>
    </div>
</tab>
<tab>
    <tab-heading> Day Book </tab-heading>
    <div class="row">
        <p class="text-success">Journal entry is an entry to the journal. Journal is a record that keeps accounting transactions in chronological order, i.e. as they occur. Ledger is a record that keeps accounting transactions by accounts. Account is a unit to record and summarize accounting transactions.</p>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Show
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                            entries
                        </label>
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="form-inline form-group">
                            <label for="filter-list">Search </label>
                            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5 text-right">
                        <div class="form-inline form-group">
                            <label for="" class="control-label">From</label>
                            <div class="input-group">
                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 text-left">
                        <div class="form-inline form-group">
                            <label for="" class="control-label">To</label>
                            <div class="input-group">
                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                            </div>
                            <button class="btn btn-warning" ng-click="getPurchaseBookByDate(fromDate,toDate)">Search</button>
                        </div>
                    </div>

                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="text-success">Day Book from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-responsive table-hover">
                                    <thead>
                                    <tr>
                                        <th>SlNo</th>
                                        <th>Date</th>
                                        <th>Voucher Type</th>
                                        <th>Ledger</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="daybook in listCount  = (daybooks | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                        <td>{{daybook.date | date:'dd-MMMM-yyyy'}}</td>
                                        <td>{{daybook.voucherType}}</td>
                                        <td>
                                            {{daybook.debitLedger}}<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{daybook.creditLedger}}
                                        </td>
                                        <td><i class="fa fa-inr"></i> {{daybook.debit}}</td>
                                        <td><br/><i class="fa fa-inr"></i> {{daybook.credit}}</td>
                                    </tr>

                                    <tr class="text-danger">
                                        <td colspan="4">Total</td><td><i class="fa fa-inr"></i> {{debitTotal}}</td><td><i class="fa fa-inr"></i> {{creditTotal}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="clearfix" ng-show="daybooks.length > numPerPage">
                    <pagination
                        ng-model="currentPage"
                        total-items="listCount.length"
                        max-size="maxSize"
                        items-per-page="numPerPage"
                        boundary-links="true"
                        class="pagination-sm pull-right"
                        previous-text="&lsaquo;"
                        next-text="&rsaquo;"
                        first-text="&laquo;"
                        last-text="&raquo;"
                        ></pagination>
                </div>
            </div>
        </div>
    </div>
</tab>

</tabset>
</div>