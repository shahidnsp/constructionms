<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-credit-card"> </i>  Employees </h2>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <button ng-if="user.permissions.employee.write =='true'" class="btn btn-primary pull-right" type="button" ng-click="newEmployee()"><i class="fa fa-plus"></i>Add Employee</button>
                <form class="form-horizontal" ng-show="employeeedit" ng-submit="addEmployee()">
                    <h3>New Employee</h3>
                    <div class="col-sm-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="clearfix">
                                    <h2>Employee Details</h2>
                                </div>
                            </div>
                            <div class="x_content">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-2">Join Date</label>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newemployee.joinDate" is-open="wagepicker" show-button-bar="false" show-weeks="false" readonly>
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default" ng-click="wagepicker=true"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <label for="" class="col-sm-1 control-label">Type</label>
                                        <div class="col-sm-5">
                                            <select class="form-control" ng-model="newemployee.employeeType" required="">
                                                <option value="Office Staff">Office Staff</option>
                                                <option value="Non Office Staff">Non Office Staff</option>
                                            </select>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder="Name" required="" ng-model="newemployee.name"/>
                                    </div>
                                    <label for="" class="control-label col-sm-1">Place</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="Place" ng-model="newemployee.place"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label col-sm-2">Phone</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder="Phone" ng-model="newemployee.phone"/>
                                    </div>
                                    <label for="" class="control-label col-sm-1">Mobile</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="Mobile" ng-model="newemployee.mobile" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label col-sm-2">Address</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" ng-model="newemployee.address" placeholder="Address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label col-sm-2">Photo</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="file" file-model="newemployee.photo" placeholder="Photo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <div class="clearfix">
                                    <h2>Salary Details</h2>
                                </div>
                            </div>
                            <div class="x_content">
                                <div class="form-group">
                                    <label for="" class="control-label col-sm-1">Basic</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="Basic Salary" ng-change="totalSalary()" ng-model="newemployee.basic"/>
                                    </div>
                                    <label for="" class="control-label col-sm-1">TA</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="Travelling allowance" ng-change="totalSalary()" ng-model="newemployee.ta"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label col-sm-1">DA</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="Dearness allowance" ng-change="totalSalary()" ng-model="newemployee.da"/>
                                    </div>
                                    <label for="" class="control-label col-sm-1">HRA</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="House Rent Allowance" ng-change="totalSalary()" ng-model="newemployee.hra"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label col-sm-1">Medical</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="Medical Allowance" ng-change="totalSalary()" ng-model="newemployee.medical"/>
                                    </div>
                                    <label for="" class="control-label col-sm-1">Total</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="Total" ng-model="newemployee.total" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button type="button" class="btn btn-default" ng-click="cancelEmployee()">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
                <h3>Employees and Details</h3>
                <div class="row">
                    <div class="col-md-4">
                        <label for="">Show
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="item for item in numsForPage"></select>
                            Entries
                        </label>
                    </div>
                    <div class="col-sm-3 text-center">
                        <div>
                            <button class="btn btn-sm btn-info" ng-click="exportToExcel('employeeTable')">Export To Excel</button>
                        </div>
                    </div>
                    <div class="col-md-5 text-right">
                        <div class="form-inline form-group">
                            <label for="filter-list">Search</label>
                            <input type="text" class="form-control" placeholder="Search" ng-model="filterlist"/>
                        </div>
                    </div>
                    </div>
                <div class="box">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="text-success">Employees and Details</span>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="employeeTable" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>SlNo</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Mobile</th>
                                    <th>Type</th>
                                    <th>Salary</th>
                                    <td>Photo</td>
                                    <th ng-show="extra">User</th>
                                    <th ng-show="extra">Created at</th>
                                    <th ng-show="extra">Updated at</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr ng-repeat="employee in listCount = (employees | filter:filterlist) |orderBy:'-created_at' | pagination:currentPage : numPerPage">
                                   <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                   <td><a ng-click="open(employee);">{{employee.name}}</a></td>
                                   <td>{{employee.address}}</td>
                                   <td>{{employee.phone}}</td>
                                   <td>{{employee.mobile}}</td>
                                   <td>{{employee.employeeType}}</td>
                                   <td><i class="fa fa-inr"></i> {{employee.total}}</td>
                                   <td>
                                       <img ng-click="viewPhoto(employee);" src="images/{{employee.photo}}" width="40" height="40">
                                   </td>
                                   <td ng-show="extra">{{employee.user}}</td>
                                   <td ng-show="extra">{{employee.created_at}}</td>
                                   <td ng-show="extra">{{employee.updated_at}}</td>
                                   <td>
                                       <div ng-if="user.permissions.employee.edit =='true'" class="btn-group btn-group-xs" role="group">
                                           <button type="button" class="btn btn-default" ng-click="editEmployee(employee)"><i class="fa fa-pencil"></i></button>
                                           <button type="button" class="btn btn-default" ng-click="deleteEmployee(employee)"><i class="fa fa-trash-o"></i></button>
                                       </div>
                                   </td>
                               </tr>
                            </tbody>
                        </table>
                                </div>
                            </div>
                        </div>
                    <div class="clearfix" ng-show="employees.length > numPerPage">
                        <pagination
                            ng-model="currentPage"
                            total-items="listCount.length"
                            max-size="maxSize"
                            items-per-page="numPerPage"
                            boundary-links="true"
                            class="pagination-sm pull-right"
                            previous-text="&lsaquo;"
                            next-text="&rsaquo;"
                            first-text="&laquo;"
                            last-text="&raquo;"
                            ></pagination>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>