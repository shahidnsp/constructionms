<div class="row">
<tabset class="col-md-12">
    <tab>
        <tab-heading>Worker's Wage Calculation</tab-heading>
<div class="box">
<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Employee's Wage Calculation </h2>
    </div>
</div>
<br>


    <div class="row form-horizontal">
        <div class="col-lg-12">
        <div class="row">
            <div class="form-group">
                <label for="" class="col-sm-1 control-label">Cash/Bank</label>
                <div class="col-sm-11">
                    <select class="form-control" ng-model="ledgers_id" required="">
                        <option ng-repeat="item in cashLedgers" value="{{item.id}}">{{item.name}}</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-1">
                    <label class="control-label">Project</label>
                </div>
                <div class="col-lg-5">
                    <select class="form-control" ng-change="changeProject(projects_id);" ng-model="projects_id" required>
                        <option  value="">Select</option>
                        <option ng-repeat="project in projects" value="{{project.id}}">{{project.name}}</option>
                    </select>
                </div>
                <div class="col-lg-1">
                    <label class="control-label">Leader</label>
                </div>
                <div class="col-lg-5">
                    <select class="form-control" ng-model="leaders_id" required>
                        <option  value="">Select</option>
                        <option ng-repeat="leader in leaders" value="{{leader.id}}">{{leader.employee.name}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row text-center">
            <div class="form-group">
                <div class="col-sm-5 text-right">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">From</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
    	    									<span class="input-group-btn">
    	    										<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
    	    									</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">To</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
    	    									<span class="input-group-btn">
    	    										<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
    	    									</span>
                        </div>
                        <button type="submit" class="btn btn-warning" ng-click="searchWage(fromDate,toDate)">Search</button>
                    </div>
                </div>
            </div>
        </div>
        </div>

    </div>

    <h3>Employees and Details</h3>
    <div class="row">
        <div class="col-md-6">
            <label for="">Show
                <select class="form-control pagiantion" ng-model="numPerPage" ng-options="item for item in numsForPage"></select>
                Entries
            </label>
        </div>
        <div class="col-md-6 text-right">
            <div class="form-inline form-group">
                <label for="filter-list">Search</label>
                <input type="text" class="form-control" placeholder="Search" ng-model="filterlist"/>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">Wage List </span>

        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="rentTable" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>SlNo</th>
                        <th>Employee</th>
                        <th>Total Present</th>
                        <th>Total Absent</th>
                        <th>Total Wage</th>
                    </tr>
                    </thead>
                    <tbody>
                     <tr ng-repeat="wage in listCount  = (wages | filter:filterlist)  | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{wage.name}}</td>
                         <td class="text-center text-primary">{{wage.present}}</td>
                         <td class="text-center text-warning">{{wage.absent}}</td>
                         <td class="text-center text-danger"><i class="fa fa-inr"></i> {{wage.total}}</td>
                     </tr>
                    <tr class="text-center text-danger">
                        <td colspan="2">Total</td><td>{{totalPresent}}</td><td>{{totalAbsent}}</td><td><i class="fa fa-inr"></i> {{totalWage}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix" ng-show="wages.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>
    <form class="form-horizontal" ng-submit="saveWage();">
    <div class="row form-horizontal">
        <div class="form-group center-block">
            <label class="col-lg-1 control-label">Advance</label>
            <div class="col-lg-2">
                <input type="text" class="form-control" ng-model="advance" ng-change="changeAdvance();" required>
            </div>
            <label class="col-lg-1 control-label">Total</label>
            <div class="col-lg-2">
                <input type="text" class="form-control" ng-model="grandtotal" required>
            </div>
            <label class="col-lg-1 control-label">Date</label>
            <div class="col-lg-3">
                <div class="input-group">
                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="date"  is-open="datepicker" show-button-bar="false" show-weeks="false" readonly>
    	    									<span class="input-group-btn">
    	    										<button type="button" class="btn btn-default" ng-click="datepicker=true"><i class="fa fa-calendar"></i></button>
    	    									</span>
                </div>
            </div>

        </div>
        <div class="form-group">
            <label class="col-lg-1 control-label">Paid</label>
            <div class="col-lg-2">
                <input type="text" class="form-control" ng-model="paid" ng-change="changePaid();" required>
            </div>
            <label class="col-lg-1 control-label">Balance</label>
            <div class="col-lg-2">
                <input type="text" class="form-control" ng-model="balance" ng-change="changeBalance();" required>
            </div>
            <div class="col-lg-1">
                <button ng-if="user.permissions.employeewage.write =='true'" type="submit" class="btn btn-primary">Save Wage</button>
            </div>
        </div>
    </div>
    </form>

</div>
        </tab>


    <tab>
        <tab-heading>Worker's Wage List</tab-heading>
        <h3>Worker's Wage List</h3>
        <div class="row">
            <div class="col-md-4">
                <label for="">Show
                    <select class="form-control pagiantion" ng-model="numPerPage" ng-options="item for item in numsForPage"></select>
                    Entries
                </label>
            </div>
            <div class="col-sm-3 text-center">
                <div>
                    <button class="btn btn-sm btn-info" ng-click="exportToExcel('salaryAdvanceListTable')">Export To Excel</button>
                </div>
            </div>
            <div class="col-md-5 text-right">
                <div class="form-inline form-group">
                    <label for="filter-list">Search</label>
                    <input type="text" class="form-control" placeholder="Search" ng-model="filterlists"/>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="text-success">Worker's Wage List and details</span>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <div class="table-responsive">
                            <table id="salaryAdvanceListTable" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SlNo</th>
                                    <th>Date</th>
                                    <th>Employee</th>
                                    <th>Amount</th>
                                    <th>Advance</th>
                                    <th>Grand Total</th>
                                    <th>Paid</th>
                                    <th>Balance</th>
                                    <th>Project</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr   ng-repeat="wage in listCount = (wages | filter:filterlists)  | pagination:currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{wage.payDate | date:'dd-MMMM-yyyy'}}</td>
                                    <td><a ng-click="open(wage);">{{wage.employee.name}}</a></td>
                                    <td style="text-align: center"><i class="fa fa-inr"></i> {{wage.amount}}</td>
                                    <td style="text-align: center"><i class="fa fa-inr"></i> {{wage.advance}}</td>
                                    <td style="text-align: center"><i class="fa fa-inr"></i> {{wage.grandtotal}}</td>
                                    <td style="text-align: center"><i class="fa fa-inr"></i> {{wage.paid}}</td>
                                    <td style="text-align: center"><i class="fa fa-inr"></i> {{wage.balance}}</td>
                                    <td>{{wage.projects.name}}</td>
                                    <td>
                                        <div ng-if="user.permissions.employeewage.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-default" ng-click="deleteWage(wage); editmode = !editmode">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
<!--                                <tr class="text-danger">-->
<!--                                    <td  colspan="2">Total</td><td style="text-align: center"><i class="fa fa-inr"></i> {{totalBalance}}</td>-->
<!--                                </tr>-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix" ng-show="wages.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </tab>




    <tab>
        <tab-heading>Worker's Wage Balance</tab-heading>
        <h3>Wage Balance List</h3>
        <div class="row">
            <div class="col-md-4">
                <label for="">Show
                    <select class="form-control pagiantion" ng-model="numPerPage" ng-options="item for item in numsForPage"></select>
                    Entries
                </label>
            </div>
            <div class="col-sm-3 text-center">
                <div>
                    <button class="btn btn-sm btn-info" ng-click="exportToExcel('salaryAdvanceListTable')">Export To Excel</button>
                </div>
            </div>
            <div class="col-md-5 text-right">
                <div class="form-inline form-group">
                    <label for="filter-list">Search</label>
                    <input type="text" class="form-control" placeholder="Search" ng-model="filterlists"/>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="text-success">Wage Balance and details</span>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <div class="table-responsive">
                            <table id="salaryAdvanceListTable" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Slno</th>
                                    <th>Name</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr   ng-repeat="list in listCount = (lists | filter:filterlists)  | pagination:currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td><a ng-click="open(list);">{{list.name}}</a></td>
                                    <td style="text-align: center"><i class="fa fa-inr"></i> {{list.balance}}</td>
                                </tr>
                                <tr class="text-danger">
                                    <td  colspan="2">Total</td><td style="text-align: center"><i class="fa fa-inr"></i> {{totalBalance}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix" ng-show="lists.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </tab>
    </tabset>
</div>