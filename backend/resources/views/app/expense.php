<div class="row">
  <div class="col-md-12">
    <h2><i class="fa fa-credit-card"> </i>  Expenses</h2>
  </div>
</div>
<div class="box">
    <p class="text-primary">All the expense comes from firm are enter in this section <span class="text-success">expect expense related to Project/Construction.Because of it as treated as Project Expenses</span></p>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <button ng-if="user.permissions.expense.write =='true'" ng-click="newExpense();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Expense</button>
      <form class="form-horizontal" ng-show="expenseedit" ng-submit="addExpense();">
        <h3>New Expense</h3><br>
        <div class="form-group">
          <label for="" class="col-sm-2 control-label">Account</label>
          <div class="col-sm-4">
              <div class="input-group">
                  <select class="form-control" ng-model="newexpense.drledgers_id" required>
                      <option value="">Select</option>
                      <option ng-repeat="ledger in allledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                  </select>
              <span class="input-group-btn">
                   <a data-toggle="tooltip" data-placement="top" title="Create a new Ledger Account">
                        <button type="button" class="btn btn-default" ng-click="createLedger('Dr');"><i class="fa fa-search"></i></button>
                   </a>
              </span>
              </div>

          </div>

          <label for="" class="col-sm-1 control-label">Date</label>
          <div class="col-sm-5">
            <div class="input-group">
              <input type="text" class="form-control" datepicker-popup ng-model="newexpense.date" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
              </span>
            </div>
          </div>
        </div>
          <div class="form-group">
              <!-- <label class="col-sm-3 control-label">Payment Mode:</label>
             <div class="col-sm-3">
                  <select class="form-control" ng-model="debit">
                      <option value="Debit">Debit</option>
                      <option value="Credit">Credit</option>
                  </select>
              </div>-->
              <label for="" class="col-sm-2 control-label">Amount</label>
              <div class="col-sm-4">
                  <input type="text" class="form-control" ng-model="newexpense.dramount" placeholder="Amount">
              </div>
              <label class="col-sm-2 control-label">Cr Account:</label>
              <div  class="col-sm-4">
                  <div class="input-group">
                      <select class="form-control" ng-model="newexpense.crledgers_id" required>
                          <option value="">Select</option>
                          <option ng-repeat="ledger in ledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                      </select>
                        <span class="input-group-btn">
                             <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                 <button type="button" class="btn btn-default" ng-click="createLedger('Cr');"><i class="fa fa-search"></i></button>
                             </a>
                        </span>
                  </div>

              </div>
          </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Description</label>
          <div class="col-sm-10">
            <textarea class="form-control" ng-model="newexpense.note" placeholder="Description"></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12 text-right">
            <button type="button" class="btn btn-default" ng-click="cancelExpense();">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
        <hr>
      </form>
      <h3 ng-hide="expenseedit">Expense and details</h3>
      <div class="row">
        <div class="col-md-4">
          <label for="">Search:  
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
          </label>
        </div>
          <div class="col-sm-3 text-center">
              <div>
                  <button class="btn btn-sm btn-info" ng-click="exportToExcel('expenseTable')">Export To Excel</button>
              </div>
          </div>
        <div class="col-md-5 text-right">
          <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
          </div>
        </div>
      </div>
        <div class="row">
            <div class="col-sm-5 text-right">
                <div class="form-inline form-group">
                    <label for="" class="control-label">From</label>
                    <div class="input-group">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-inline form-group">
                    <label for="" class="control-label">To</label>
                    <div class="input-group">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                    </div>
                    <button class="btn btn-warning" ng-click="searchExpenseDate(fromDate,toDate);">Search</button>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="text-success">Expense and details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="expenseTable" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Dr Account</th>
                            <th>Cr Account</th>
                            <th>Dr Amount</th>
                            <th>Cr Amount</th>
                            <th>Note</th>
                            <th ng-show="extra">User</th>
                            <th ng-show="extra">Created</th>
                            <th ng-show="extra">Updated</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="expense in listCount  = (expenses | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                            <td>{{expense.id}}</td>
                            <td>{{expense.date | date:'dd-MMMM-yyyy'}}</td>
                            <td>{{expense.drledger.name}}</td>
                            <td>{{expense.crledger.name}}</td>
                            <td><i class="fa fa-inr"></i> {{expense.dramount}}</td>
                            <td><i class="fa fa-inr"></i> {{expense.cramount}}</td>
                            <td><p class="description" popover="{{expense.note}}" popover-trigger="mouseenter">{{expense.note}}</p></td>
                            <td ng-show="extra">{{expense.user}}</td>
                            <td ng-show="extra">{{expense.created_at}}</td>
                            <td ng-show="extra">{{expense.updated_at}}</td>
                            <td>
                                <div ng-if="user.permissions.expense.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                    <button type="button" class="btn btn-default" ng-click="editExpense(expense);">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    <button type="button" class="btn btn-default" ng-click="deleteExpense(expense); editmode = !editmode">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        <tr class="text-danger"><td colspan="4">Total</td><td style="text-align: center"><i class="fa fa-inr"></i> {{totalexpense}}</td></tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
      <div class="clearfix" ng-show="expenses.length > numPerPage">
        <pagination 
          ng-model="currentPage" 
          total-items="listCount.length" 
          max-size="maxSize" 
          items-per-page="numPerPage"
          boundary-links="true" 
          class="pagination-sm pull-right" 
          previous-text="&lsaquo;" 
          next-text="&rsaquo;" 
          first-text="&laquo;" 
          last-text="&raquo;"
        ></pagination>
      </div>
    </div>
  </div>
</div>