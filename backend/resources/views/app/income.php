<div class="row">
  <div class="col-md-12">
    <h2><i class="fa fa-credit-card"> </i>  Income</h2>
  </div>
</div>
<div class="box">
    <p class="text-primary">All the income comes to firm are enter in this section <span class="text-success">expect income related to Project/Construction..Because of it as treated as Project Incomes</span></p>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <button ng-if="user.permissions.income.write =='true'" ng-click="newIncome();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Income</button>
      <form class="form-horizontal" ng-show="incomeedit" ng-submit="addIncome();">
        <h3>New Income</h3><br>
          <div class="form-group">
              <label for="" class="col-sm-2 control-label">Account</label>
              <div class="col-sm-4">
                  <div class="input-group">
                      <select class="form-control" ng-model="newincome.drledgers_id" required>
                          <option value="">Select</option>
                          <option ng-repeat="ledger in ledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                      </select>
                        <span class="input-group-btn">
                             <a data-toggle="tooltip" data-placement="top" title="Create a new Ledger Account">
                                 <button type="button" class="btn btn-default" ng-click="createLedger('Dr');"><i class="fa fa-search"></i></button>
                             </a>
                        </span>
                  </div>
              </div>

              <label for="" class="col-sm-1 control-label">Date</label>
              <div class="col-sm-5">
                  <div class="input-group">
                      <input type="text" class="form-control" datepicker-popup ng-model="newincome.date" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
              </span>
                  </div>
              </div>
          </div>
          <div class="form-group">
              <label for="" class="col-sm-2 control-label">Amount</label>
              <div class="col-sm-4">
                  <input type="text" class="form-control" ng-model="newincome.dramount" placeholder="Amount">
              </div>
              <label class="col-sm-2 control-label">Cr Account:</label>
              <div  class="col-sm-4">
                  <div class="input-group">
                      <select class="form-control" ng-model="newincome.crledgers_id" required>
                          <option value="">Select</option>
                          <option ng-repeat="ledger in allledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                      </select>
                        <span class="input-group-btn">
                             <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                 <button type="button" class="btn btn-default" ng-click="createLedger('Cr');"><i class="fa fa-search"></i></button>
                             </a>
                        </span>
                  </div>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-2 control-label">Description</label>
              <div class="col-sm-10">
                  <textarea class="form-control" ng-model="newincome.note" placeholder="Description"></textarea>
              </div>
          </div>
        <div class="form-group">
          <div class="col-sm-12 text-right">
            <button type="button" class="btn btn-default" ng-click="cancelIncome();">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
        <hr>
      </form>
      <h3 ng-hide="incomeedit">Income and details</h3>
      <div class="row">
        <div class="col-md-4">
          <label for="">Show 
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
          </label>
        </div>
          <div class="col-sm-3 text-center">
              <div>
                  <button class="btn btn-sm btn-info" ng-click="exportToExcel('incomeTable')">Export To Excel</button>
              </div>
          </div>
        <div class="col-md-5 text-right">
          <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
          </div>
        </div>
      </div>
        <div class="row">
            <div class="col-sm-5 text-right">
                <div class="form-inline form-group">
                    <label for="" class="control-label">From</label>
                    <div class="input-group">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-inline form-group">
                    <label for="" class="control-label">To</label>
                    <div class="input-group">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                    </div>
                    <button class="btn btn-warning" ng-click="searchIncomeDate(fromDate,toDate)">Search</button>
                </div>
            </div>

        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="text-primary">Incomes and details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="incomeTable" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Dr Account</th>
                            <th>Cr Account</th>
                            <th>Dr Amount</th>
                            <th>Cr Amount</th>
                            <th>Note</th>
                            <th ng-show="extra">User</th>
                            <th ng-show="extra">Created</th>
                            <th ng-show="extra">Updated</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="income in listCount  = (incomes | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                            <td>{{income.id}}</td>
                            <td>{{income.date | date:'dd-MMMM-yyyy'}}</td>
                            <td>{{income.drledger.name}}</td>
                            <td>{{income.crledger.name}}</td>
                            <td><i class="fa fa-inr"></i> {{income.dramount}}</td>
                            <td><i class="fa fa-inr"></i> {{income.cramount}}</td>
                            <td><p class="description" popover="{{income.note}}" popover-trigger="mouseenter">{{income.note}}</p></td>
                            <td ng-show="extra">{{income.user}}</td>
                            <td ng-show="extra">{{income.created_at}}</td>
                            <td ng-show="extra">{{income.updated_at}}</td>
                            <td>
                                <div ng-if="user.permissions.income.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                    <button type="button" class="btn btn-default" ng-click="editIncome(income);">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    <button type="button" class="btn btn-default" ng-click="deleteIncome(income); editmode = !editmode">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        <tr class="text-danger"><td colspan="4">Total</td><td style="text-align: center"><i class="fa fa-inr"></i> {{totalincome}}</td></tr>
                        </tbody>
        </tbody>
      </table>
                    </div>
                </div>
            </div>
      <div class="clearfix" ng-show="incomes.length > numPerPage">
        <pagination 
          ng-model="currentPage" 
          total-items="listCount.length" 
          max-size="maxSize" 
          items-per-page="numPerPage"
          boundary-links="true" 
          class="pagination-sm pull-right" 
          previous-text="&lsaquo;" 
          next-text="&rsaquo;" 
          first-text="&laquo;" 
          last-text="&raquo;"
        ></pagination>
      </div>
    </div>
  </div>
</div>