<!DOCTYPE html>
<html lang="en" ng-app="myApp">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Construction Management System Powered By Psybo Technologies</title>

    <link rel="stylesheet" href="[[ elixir("css/app.css")]]">

<style>
    .description{
      max-width: 250px;
      overflow: hidden;
      white-space: nowrap;
      -ms-text-overflow: ellipsis;
          text-overflow: ellipsis;
    }
    </style>
  </head>

  <body ng-controller="homecontroller" class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Construction MS</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img src="images/{{user.photo}}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{user.firstname+' '+user.lastname}}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu" click-anywhere-but-here="showMenu=false" ng-click="showMenu=true">
                  <li ><a><i class="fa fa-dashboard"></i> Dashboard <span class="fa fa-chevron-down"></span></a>
                    <ul ng-show="showMenu" class="nav child_menu">
                      <li ng-repeat="menu in user.menu" ng-if="menu.group=='General'"><a href="#{{menu.name}}"><i class="fa fa-{{menu.icon}}"></i> {{menu.title}}</a></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-bank"></i> Bank <span class="fa fa-chevron-down"></span></a>
                                      <ul ng-show="showMenu" class="nav child_menu">
                                        <li ng-repeat="menu in user.menu" ng-if="menu.group=='Bank'"><a href="#{{menu.name}}"><i class="fa fa-{{menu.icon}}"></i> {{menu.title}}</a></li>
                                      </ul>
                                    </li>
                   <li><a><i class="fa fa-users"></i> Employee <span class="fa fa-chevron-down"></span></a>
                                                         <ul ng-show="showMenu" class="nav child_menu">
                                                           <li ng-repeat="menu in user.menu" ng-if="menu.group=='Employee'"><a href="#{{menu.name}}"><i class="fa fa-{{menu.icon}}"></i> {{menu.title}}</a></li>
                                                         </ul>
                                                       </li>
                   <li><a><i class="fa fa-flickr"></i> Materials <span class="fa fa-chevron-down"></span></a>
                        <ul ng-show="showMenu" class="nav child_menu">
                             <li ng-repeat="menu in user.menu" ng-if="menu.group=='Materials'"><a href="#{{menu.name}}"><i class="fa fa-{{menu.icon}}"></i> {{menu.title}}</a></li>
                        </ul>
                   </li>
                    <li><a><i class="fa fa-dropbox"></i> Machinery <span class="fa fa-chevron-down"></span></a>
                                            <ul ng-show="showMenu" class="nav child_menu">
                                                <li ng-repeat="menu in user.menu" ng-if="menu.group=='Machinery'"><a href="#{{menu.name}}"><i class="fa fa-{{menu.icon}}"></i> {{menu.title}}</a></li>
                                            </ul>
                                      </li>
                   <li><a><i class="fa fa-sitemap"></i> Project <span class="fa fa-chevron-down"></span></a>
                         <ul ng-show="showMenu" class="nav child_menu">
                             <li ng-repeat="menu in user.menu" ng-if="menu.group=='Project'"><a href="#{{menu.name}}"><i class="fa fa-{{menu.icon}}"></i> {{menu.title}}</a></li>
                         </ul>
                   </li>
                    <li><a><i class="fa fa-money"></i> Income/Expense <span class="fa fa-chevron-down"></span></a>
                         <ul ng-show="showMenu" class="nav child_menu">
                             <li ng-repeat="menu in user.menu" ng-if="menu.group=='Cash'"><a href="#{{menu.name}}"><i class="fa fa-{{menu.icon}}"></i> {{menu.title}}</a></li>
                         </ul>
                    </li>
                      <li><a><i class="fa fa-book"></i> Account <span class="fa fa-chevron-down"></span></a>
                          <ul ng-show="showMenu" class="nav child_menu">
                               <li ng-repeat="menu in user.menu" ng-if="menu.group=='Account'"><a href="#{{menu.name}}"><i class="fa fa-{{menu.icon}}"></i> {{menu.title}}</a></li>

                                <li><a><i class="fa fa-edit"></i> Report <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li ng-repeat="menu in user.menu" ng-if="menu.group=='AccountReport'"><a href="#{{menu.name}}"><i class="fa fa-{{menu.icon}}"></i> {{menu.title}}</a></li>
                                    </ul>
                                </li>
                           </ul>
                      </li>
                       <li><a><i class="fa fa-gear"></i> Settings <span class="fa fa-chevron-down"></span></a>
                           <ul ng-show="showMenu" class="nav child_menu">
                               <li ng-repeat="menu in user.menu" ng-if="menu.group=='Settings'"><a href="#{{menu.name}}"><i class="fa fa-{{menu.icon}}"></i> {{menu.title}}</a></li>
                           </ul>

                       </li>

                  <!--<li><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="form.html">General Form</a>
                      </li>
                      <li><a href="form_advanced.html">Advanced Components</a>
                      </li>
                      <li><a href="form_validation.html">Form Validation</a>
                      </li>
                      <li><a href="form_wizards.html">Form Wizard</a>
                      </li>
                      <li><a href="form_upload.html">Form Upload</a>
                      </li>
                      <li><a href="form_buttons.html">Form Buttons</a>
                      </li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> UI Elements <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="general_elements.html">General Elements</a>
                      </li>
                      <li><a href="media_gallery.html">Media Gallery</a>
                      </li>
                      <li><a href="typography.html">Typography</a>
                      </li>
                      <li><a href="icons.html">Icons</a>
                      </li>
                      <li><a href="glyphicons.html">Glyphicons</a>
                      </li>
                      <li><a href="widgets.html">Widgets</a>
                      </li>
                      <li><a href="invoice.html">Invoice</a>
                      </li>
                      <li><a href="inbox.html">Inbox</a>
                      </li>
                      <li><a href="calendar.html">Calendar</a>
                      </li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="tables.html">Tables</a>
                      </li>
                      <li><a href="tables_dynamic.html">Table Dynamic</a>
                      </li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="chartjs.html">Chart JS</a>
                      </li>
                      <li><a href="chartjs2.html">Chart JS2</a>
                      </li>
                      <li><a href="morisjs.html">Moris JS</a>
                      </li>
                      <li><a href="echarts.html">ECharts </a>
                      </li>
                      <li><a href="other_charts.html">Other Charts </a>
                      </li>
                    </ul>
                  </li>-->
                </ul>
              </div>
             <!-- <div class="menu_section">
                <h3>Live On</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">E-commerce</a>
                      </li>
                      <li><a href="projects.html">Projects</a>
                      </li>
                      <li><a href="project_detail.html">Project Detail</a>
                      </li>
                      <li><a href="contacts.html">Contacts</a>
                      </li>
                      <li><a href="profile.html">Profile</a>
                      </li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="page_404.html">404 Error</a>
                      </li>
                      <li><a href="page_500.html">500 Error</a>
                      </li>
                      <li><a href="plain_page.html">Plain Page</a>
                      </li>
                      <li><a href="login.html">Login Page</a>
                      </li>
                      <li><a href="pricing_tables.html">Pricing Tables</a>
                      </li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="#level1_1">Level One</a>
                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>
                  </li>
                  <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a>
                  </li>
                </ul>
              </div>-->

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle" ng-click="showExtra();"><i class="fa fa-bars"></i></a>


              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt="">{{user.firstname}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="#profile">  Profile</a>
                    </li>
                    <li>
                      <a href="#settings">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li>
                      <a href="javascript:;">Help</a>
                    </li>
                    <li><a href="user/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image">
                                          <img src="images/img.jpg" alt="Profile Image" />
                                      </span>
                        <span>
                                          <span>John Smith</span>
                        <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                                          Film festivals used to be do-or-die moments for movie makers. They were where...
                                      </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image">
                                          <img src="images/img.jpg" alt="Profile Image" />
                                      </span>
                        <span>
                                          <span>John Smith</span>
                        <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                                          Film festivals used to be do-or-die moments for movie makers. They were where...
                                      </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image">
                                          <img src="images/img.jpg" alt="Profile Image" />
                                      </span>
                        <span>
                                          <span>John Smith</span>
                        <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                                          Film festivals used to be do-or-die moments for movie makers. They were where...
                                      </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image">
                                          <img src="images/img.jpg" alt="Profile Image" />
                                      </span>
                        <span>
                                          <span>John Smith</span>
                        <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                                          Film festivals used to be do-or-die moments for movie makers. They were where...
                                      </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a href="inbox.html">
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>

              </ul>
              </div>
            </nav>
          </div>

        </div>
        <!-- /top navigation -->


        <!-- page content -->
       <!--
        <!--<div class="container-fluid">-->

        <!-- </div>-->
          <div class="right_col" role="main">
            <div class="row" ng-view></div>
          <div>
            
        <!--</div>
         <div class="container-fluid">
                     <div ng-view></div>
                </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
         <div class="pull-left">
                    <span class="text-success">Construction Company Management System </span>
          </div>
          <div class="pull-right">
            <span class="text-primary">Powered By </span><a target="_blank" href="http://psybotechnologies.com/">Psybo Technologies</a>
          </div>
          <div class="clearfix1"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
</div>
   <script src="[[ elixir("js/angularjs.min.js") ]]"></script>
   <script src="[[ elixir("js/bootstrap.min.js") ]]"></script>
   <script src="[[ elixir("js/app.js") ]]"></script>
    <script language=Javascript>
    <!--
            function isNumberKey(evt)
            {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31
                        && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }
    //-->
    </script>
  </body>
</html>