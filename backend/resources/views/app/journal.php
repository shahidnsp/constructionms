<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Voucher</h2>
    </div>
</div>
<div class="box">
    <p class="text-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta corrupti ipsum quo, autem placeat assumenda repellat? Rem alias minima quas quis, aut quidem, labore amet accusantium ad earum officiis doloremque.</p>
</div>
<div class="row">
<tabset class="col-md-12">
    <tab>
        <tab-heading>Journal</tab-heading>
        <div class="row">
            <p class="text-success">Journal entry is an entry to the journal. Journal is a record that keeps accounting transactions in chronological order, i.e. as they occur. Ledger is a record that keeps accounting transactions by accounts. Account is a unit to record and summarize accounting transactions.</p>
        </div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.journal.write =='true'" ng-click="newJournal();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Journal</button>
            <form class="form-horizontal" ng-show="journaledit" ng-submit="addJournal();">
                <h3>New Journal</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Payed Date</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newjournal.date" is-open="rentpicker" show-button-bar="false" show-weeks="false" readonly>
                                <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="rentpicker=true"><i class="fa fa-calendar"></i></button>
                                </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Dr Account</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" ng-model="newjournal.drledgers_id" required>
                                <option  value="" disabled>Debit Account</option>
                                <option ng-repeat="ledger in ledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                            </select>
                            <span class="input-group-btn">
                                 <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                     <button type="button" class="btn btn-default" ng-click="createLedger('JournalDr');"><i class="fa fa-search"></i></button>
                                 </a>
                            </span>
                        </div>
                    </div>
                    <label for="" class="col-sm-2 control-label">Cr Account</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" ng-model="newjournal.crledgers_id" required>
                                <option  value="" disabled>Credit Account</option>
                                <option ng-repeat="ledger in ledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                            </select>
                            <span class="input-group-btn">
                                 <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                     <button type="button" class="btn btn-default" ng-click="createLedger('JournalCr');"><i class="fa fa-search"></i></button>
                                 </a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Dr Amount</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" placeholder="Debit Amount" ng-change="changeJournalAmountDr();" ng-model="newjournal.dramount" required>
                    </div>
                    <label for="" class="col-sm-2 control-label">Cr Amount</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" placeholder="Credit Amount" ng-change="changeJournalAmountCr();" ng-model="newjournal.cramount" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Narration</label>
                    <div class="col-sm-10">
                        <textarea ng-model="newjournal.note" class="form-control" placeholder="Narration"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelJournal();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="journaledit">Journal and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-sm btn-info" ng-click="exportToExcel('journalTable')">Export To Excel</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 text-right">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">From</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">To</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                        <button class="btn btn-warning" ng-click="searchJournalDate(fromDate,toDate)">Search</button>
                    </div>
                </div>

            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="text-success">Journals from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="journalTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Dr Account</th>
                    <th>Cr Account</th>
                    <th>Dr Amount</th>
                    <th>Cr Amount</th>
                    <th>Note</th>
                    <th ng-show="extra">Created</th>
                    <th ng-show="extra">Updated</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="journal in listCount  = (journals | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                    <td>{{journal.id}}</td>
                    <td>{{journal.date | date:'dd-MMMM-yyyy'}}</td>
                    <td>{{journal.drledger.name}}</td>
                    <td>{{journal.crledger.name}}</td>
                    <td><i class="fa fa-inr"></i>{{journal.dramount}}</td>
                    <td><i class="fa fa-inr"></i>{{journal.cramount}}</td>
                    <td><p class="description" popover="{{journal.note}}" popover-trigger="mouseenter">{{journal.note}}</p></td>
                    <td ng-show="extra">{{journal.created_at}}</td>
                    <td ng-show="extra">{{journal.updated_at}}</td>
                    <td>
                        <div ng-if="user.permissions.journal.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                            <button type="button" class="btn btn-default" ng-click="editJournal(journal);">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="deleteJournal(journal); editmode = !editmode">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr class="text-danger">
                    <td  colspan="4">Total</td><td style="text-align: center"><i class="fa fa-inr"></i>{{totalJournalDr}}</td><td style="text-align: center"><i class="fa fa-inr"></i>{{totalJournalCr}}</td>
                </tr>
                </tbody>
            </table>
                        </div>
                    </div>
                </div>
            <div class="clearfix" ng-show="journals.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>
        </tab>
    <tab>
        <tab-heading>Payment</tab-heading>
        <div class="row">
            <p class="text-success">This voucher is used to paid any type of expenses either direct or indirect expenses by cash/chque/dd to any party. You can also use this voucher to pay bill amount against credit purchases through this voucher.</p>
        </div>
        <!--<div class="box">-->
            <!--<div class="col-md-12">-->
                <div class="box">
                    <button ng-if="user.permissions.journal.write =='true'" ng-click="newPayment();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Payment</button>
                    <form class="form-horizontal" ng-show="paymentedit" ng-submit="addPayment();">
                        <h3>New Payment</h3><br>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Payed Date</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newpayment.date" is-open="paymentpicker" show-button-bar="false" show-weeks="false" readonly>
                                <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="paymentpicker=true"><i class="fa fa-calendar"></i></button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Dr Account</label>

                            <div class="col-sm-4">
                                <div class="input-group">
                                    <select class="form-control" ng-model="newpayment.drledgers_id" required>
                                        <option  value="" disabled>Debit Account</option>
                                        <option ng-repeat="ledger in ledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                                    </select>
                                    <span class="input-group-btn">
                                         <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                             <button type="button" class="btn btn-default" ng-click="createLedger('PaymentDr');"><i class="fa fa-search"></i></button>
                                         </a>
                                    </span>
                                </div>
                            </div>
                            <label for="" class="col-sm-2 control-label">Cr Account</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <select class="form-control" ng-model="newpayment.crledgers_id" required>
                                        <option  value="" disabled>Credit Account</option>
                                        <option ng-repeat="ledger in cashLedgers" value="{{ledger.id}}">{{ledger.name}}</option>
                                    </select>
                                    <span class="input-group-btn">
                                         <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                             <button type="button" class="btn btn-default" ng-click="createLedger('PaymentCr');"><i class="fa fa-search"></i></button>
                                         </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Dr Amount</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" placeholder="Debit Amount" ng-change="changePaymentAmountDr();" ng-model="newpayment.dramount" required>
                            </div>
                            <label for="" class="col-sm-2 control-label">Cr Amount</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" placeholder="Credit Amount" ng-change="changePaymentAmountCr();" ng-model="newpayment.cramount" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Narration</label>
                            <div class="col-sm-10">
                                <textarea ng-model="newpayment.note" class="form-control" placeholder="Narration"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12 text-right">
                                <button type="button" class="btn btn-default" ng-click="cancelPayment();">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                        <hr>
                    </form>
                    <h3 ng-hide="paymentedit">Payment and details</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">Show
                                <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                entries
                            </label>
                        </div>
                        <div class="col-sm-3 text-center">
                            <div>
                                <button class="btn btn-sm btn-info" ng-click="exportToExcel('paymentTable')">Export To Excel</button>
                            </div>
                        </div>
                        <div class="col-md-5 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 text-right">
                            <div class="form-inline form-group">
                                <label for="" class="control-label">From</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 text-left">
                            <div class="form-inline form-group">
                                <label for="" class="control-label">To</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                                </div>
                                <button class="btn btn-warning" ng-click="searchPaymentDate(fromDate,toDate)">Search</button>
                            </div>
                        </div>

                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="text-success">Payment from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="paymentTable" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Dr Account</th>
                            <th>Cr Account</th>
                            <th>Dr Amount</th>
                            <th>Cr Amount</th>
                            <th>Note</th>
                            <th ng-show="extra">Created</th>
                            <th ng-show="extra">Updated</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="payment in listCount  = (payments | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                            <td>{{payment.id}}</td>
                            <td>{{payment.date | date:'dd-MMMM-yyyy'}}</td>
                            <td>{{payment.drledger.name}}</td>
                            <td>{{payment.crledger.name}}</td>
                            <td><i class="fa fa-inr"></i>{{payment.dramount}}</td>
                            <td><i class="fa fa-inr"></i>{{payment.cramount}}</td>
                            <td><p class="description" popover="{{payment.note}}" popover-trigger="mouseenter">{{payment.note}}</p></td>
                            <td ng-show="extra">{{payment.created_at}}</td>
                            <td ng-show="extra">{{payment.updated_at}}</td>
                            <td>
                                <div ng-if="user.permissions.journal.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                    <button type="button" class="btn btn-default" ng-click="editPayment(payment);">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    <button type="button" class="btn btn-default" ng-click="deletePayment(payment); editmode = !editmode">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        <tr class="text-danger">
                            <td  colspan="4">Total</td><td style="text-align: center"><i class="fa fa-inr"></i>{{totalPaymentDr}}</td><td style="text-align: center"><i class="fa fa-inr"></i>{{totalPaymentCr}}</td>
                        </tr>
                        </tbody>
                    </table>
                                </div>
                                </div>
                                </div>
                    <div class="clearfix" ng-show="payments.length > numPerPage">
                        <pagination
                            ng-model="currentPage"
                            total-items="listCount.length"
                            max-size="maxSize"
                            items-per-page="numPerPage"
                            boundary-links="true"
                            class="pagination-sm pull-right"
                            previous-text="&lsaquo;"
                            next-text="&rsaquo;"
                            first-text="&laquo;"
                            last-text="&raquo;"
                            ></pagination>
                    </div>
                </div>
            <!--</div>-->
        <!--</div>-->
    </tab>
    <tab>
        <tab-heading>Receipt</tab-heading>
        <div class="row">
            <p class="text-success">This voucher is used to receive any income or received cash/chque/dd from any party. You can also make cash sales entries through this voucher.</p>
        </div>
        <!--<div class="box">-->
        <!--<div class="col-md-12">-->
        <div class="box">
            <button ng-if="user.permissions.journal.write =='true'" ng-click="newReceipt();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Receipt</button>
            <form class="form-horizontal" ng-show="receiptedit" ng-submit="addReceipt();">
                <h3>New Receipt</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Payed Date</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newreceipt.date" is-open="receiptpicker" show-button-bar="false" show-weeks="false" readonly>
                                <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="receiptpicker=true"><i class="fa fa-calendar"></i></button>
                                </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Dr Account</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" ng-model="newreceipt.drledgers_id" required>
                                <option  value="" disabled>Debit Account</option>
                                <option ng-repeat="ledger in cashLedgers" value="{{ledger.id}}">{{ledger.name}}</option>
                            </select>
                            <span class="input-group-btn">
                                 <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                     <button type="button" class="btn btn-default" ng-click="createLedger('ReceiptDr');"><i class="fa fa-search"></i></button>
                                 </a>
                            </span>
                        </div>
                    </div>
                    <label for="" class="col-sm-2 control-label">Cr Account</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" ng-model="newreceipt.crledgers_id" required>
                                <option  value="" disabled>Credit Account</option>
                                <option ng-repeat="ledger in ledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                            </select>
                            <span class="input-group-btn">
                                 <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                     <button type="button" class="btn btn-default" ng-click="createLedger('ReceiptCr');"><i class="fa fa-search"></i></button>
                                 </a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Dr Amount</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" placeholder="Debit Amount" ng-change="changeReceiptAmountDr();" ng-model="newreceipt.dramount" required>
                    </div>
                    <label for="" class="col-sm-2 control-label">Cr Amount</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" placeholder="Credit Amount" ng-change="changeReceiptAmountCr();" ng-model="newreceipt.cramount" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Narration</label>
                    <div class="col-sm-10">
                        <textarea ng-model="newreceipt.note" class="form-control" placeholder="Narration"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelReceipt();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="receiptedit">Receipt and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-sm btn-info" ng-click="exportToExcel('receiptTable')">Export To Excel</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 text-right">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">From</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">To</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                        <button class="btn btn-warning" ng-click="searchReceiptDate(fromDate,toDate)">Search</button>
                    </div>
                </div>

            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="text-success">Receipts from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="receiptTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Dr Account</th>
                    <th>Cr Account</th>
                    <th>Dr Amount</th>
                    <th>Cr Amount</th>
                    <th>Note</th>
                    <th ng-show="extra">Created</th>
                    <th ng-show="extra">Updated</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="receipt in listCount  = (receipts | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                    <td>{{receipt.id}}</td>
                    <td>{{receipt.date | date:'dd-MMMM-yyyy'}}</td>
                    <td>{{receipt.drledger.name}}</td>
                    <td>{{receipt.crledger.name}}</td>
                    <td><i class="fa fa-inr"></i>{{receipt.dramount}}</td>
                    <td><i class="fa fa-inr"></i>{{receipt.cramount}}</td>
                    <td><p class="description" popover="{{receipt.note}}" popover-trigger="mouseenter">{{receipt.note}}</p></td>
                    <td ng-show="extra">{{receipt.created_at}}</td>
                    <td ng-show="extra">{{receipt.updated_at}}</td>
                    <td>
                        <div ng-if="user.permissions.journal.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                            <button type="button" class="btn btn-default" ng-click="editReceipt(receipt);">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="deleteReceipt(receipt); editmode = !editmode">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr class="text-danger">
                    <td  colspan="4">Total</td><td style="text-align: center"><i class="fa fa-inr"></i>{{totalReceiptDr}}</td><td style="text-align: center"><i class="fa fa-inr"></i>{{totalReceiptCr}}</td>
                </tr>
                </tbody>
            </table>
                    </div>

                </div>
            </div>
            <div class="clearfix" ng-show="receipt.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
        <!--</div>-->
        <!--</div>-->
    </tab>

    <!--<tab>
        <tab-heading>Credit Note</tab-heading>
        <div class="row">
            <p class="text-success">Credit note voucher is used specially for sales return entry and you can also use this voucher to decrease the creditor amount due the reason of rate difference.</p>
        </div>
        <div class="box">
            <button ng-if="user.permissions.journal.write =='true'" ng-click="newCredit();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Credit Note</button>
            <form class="form-horizontal" ng-show="creditedit" ng-submit="addCredit();">
                <h3>New Credit Note</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Payed Date</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newcredit.date" is-open="creditpicker" show-button-bar="false" show-weeks="false" readonly>
                                <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="creditpicker=true"><i class="fa fa-calendar"></i></button>
                                </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Dr Account</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" ng-model="newcredit.drledgers_id" required>
                                <option  value="" disabled>Debit Account</option>
                                <option ng-repeat="ledger in ledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                            </select>
                            <span class="input-group-btn">
                                 <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                     <button type="button" class="btn btn-default" ng-click="createLedger('CreditDr');"><i class="fa fa-search"></i></button>
                                 </a>
                            </span>
                        </div>
                    </div>
                    <label for="" class="col-sm-2 control-label">Cr Account</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" ng-model="newcredit.crledgers_id" required>
                                <option  value="" disabled>Credit Account</option>
                                <option ng-repeat="ledger in ledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                            </select>
                            <span class="input-group-btn">
                                 <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                     <button type="button" class="btn btn-default" ng-click="createLedger('CreditCr');"><i class="fa fa-search"></i></button>
                                 </a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Dr Amount</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" placeholder="Debit Amount" ng-change="changeCreditAmountDr();" ng-model="newcredit.dramount" required>
                    </div>
                    <label for="" class="col-sm-2 control-label">Cr Amount</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" placeholder="Credit Amount" ng-change="changeCreditAmountCr();" ng-model="newcredit.cramount" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Narration</label>
                    <div class="col-sm-10">
                        <textarea ng-model="newcredit.note" class="form-control" placeholder="Narration"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelCredit();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="receiptedit">Credit Note and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-sm btn-info" ng-click="exportToExcel('creditTable')">Export To Excel</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 text-right">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">From</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">To</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                        <button class="btn btn-warning" ng-click="searchCreditDate(fromDate,toDate)">Search</button>
                    </div>
                </div>

            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="text-success">Credit Notes from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
            <table id="creditTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Dr Account</th>
                    <th>Cr Account</th>
                    <th>Dr Amount</th>
                    <th>Cr Amount</th>
                    <th>Note</th>
                    <th ng-show="extra">Created</th>
                    <th ng-show="extra">Updated</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="credit in listCount  = (credits | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                    <td>{{credit.id}}</td>
                    <td>{{credit.date | date:'dd-MMMM-yyyy'}}</td>
                    <td>{{credit.drledger.name}}</td>
                    <td>{{credit.crledger.name}}</td>
                    <td><i class="fa fa-inr"></i>{{credit.dramount}}</td>
                    <td><i class="fa fa-inr"></i>{{credit.cramount}}</td>
                    <td><p class="description" popover="{{credit.note}}" popover-trigger="mouseenter">{{credit.note}}</p></td>
                    <td ng-show="extra">{{credit.created_at}}</td>
                    <td ng-show="extra">{{credit.updated_at}}</td>
                    <td>
                        <div ng-if="user.permissions.journal.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                            <button type="button" class="btn btn-default" ng-click="editCredit(credit);">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="deleteCredit(credit); editmode = !editmode">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr class="text-danger">
                    <td  colspan="4">Total</td><td style="text-align: center"><i class="fa fa-inr"></i>{{totalCreditDr}}</td><td style="text-align: center"><i class="fa fa-inr"></i>{{totalCreditCr}}</td>
                </tr>
                </tbody>
            </table>
                    </div>

                </div>
            </div>
            <div class="clearfix" ng-show="credits.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </tab>
    <tab>
        <tab-heading>Debit Note</tab-heading>
        <div class="row">
            <p class="text-success">Debit Note voucher is used specially for purchases return entry and you can also use this voucher to increase any debtor amount due the reason of rate difference.</p>
        </div>
        <div class="box">
            <button ng-if="user.permissions.journal.write =='true'" ng-click="newDebit();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Debit Note</button>
            <form class="form-horizontal" ng-show="debitedit" ng-submit="addDebit();">
                <h3>New Debit Note</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Payed Date</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newdebit.date" is-open="debitpicker" show-button-bar="false" show-weeks="false" readonly>
                                <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="debitpicker=true"><i class="fa fa-calendar"></i></button>
                                </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Dr Account</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" ng-model="newdebit.drledgers_id" required>
                                <option  value="" disabled>Debit Account</option>
                                <option ng-repeat="ledger in ledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                            </select>
                            <span class="input-group-btn">
                                 <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                     <button type="button" class="btn btn-default" ng-click="createLedger('DebitDr');"><i class="fa fa-search"></i></button>
                                 </a>
                            </span>
                        </div>
                    </div>
                    <label for="" class="col-sm-2 control-label">Cr Account</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <select class="form-control" ng-model="newdebit.crledgers_id" required>
                                <option  value="" disabled>Credit Account</option>
                                <option ng-repeat="ledger in ledgers" value="{{ledger.id}}">{{ledger.name}}</option>
                            </select>
                            <span class="input-group-btn">
                                 <a data-toggle="tooltip" data-placement="left" title="Create a new Ledger Account">
                                     <button type="button" class="btn btn-default" ng-click="createLedger('DebitCr');"><i class="fa fa-search"></i></button>
                                 </a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Dr Amount</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" placeholder="Debit Amount" ng-change="changeDebitAmountDr();" ng-model="newdebit.dramount" required>
                    </div>
                    <label for="" class="col-sm-2 control-label">Cr Amount</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" placeholder="Credit Amount" ng-change="changeDebitAmountCr();" ng-model="newdebit.cramount" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Narration</label>
                    <div class="col-sm-10">
                        <textarea ng-model="newdebit.note" class="form-control" placeholder="Narration"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelDebit();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="receiptedit">Debit Note and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-sm btn-info" ng-click="exportToExcel('debitTable')">Export To Excel</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 text-right">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">From</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">To</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                        <button class="btn btn-warning" ng-click="searchDebitDate(fromDate,toDate)">Search</button>
                    </div>
                </div>

            </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="text-success">Debit Notes from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
            <table id="debitTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Dr Account</th>
                    <th>Cr Account</th>
                    <th>Dr Amount</th>
                    <th>Cr Amount</th>
                    <th>Note</th>
                    <th ng-show="extra">Created</th>
                    <th ng-show="extra">Updated</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="debit in listCount  = (debits | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                    <td>{{debit.id}}</td>
                    <td>{{debit.date | date:'dd-MMMM-yyyy'}}</td>
                    <td>{{debit.drledger.name}}</td>
                    <td>{{debit.crledger.name}}</td>
                    <td><i class="fa fa-inr"></i>{{debit.dramount}}</td>
                    <td><i class="fa fa-inr"></i>{{debit.cramount}}</td>
                    <td><p class="description" popover="{{debit.note}}" popover-trigger="mouseenter">{{debit.note}}</p></td>
                    <td ng-show="extra">{{credit.created_at}}</td>
                    <td ng-show="extra">{{credit.updated_at}}</td>
                    <td>
                        <div ng-if="user.permissions.journal.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                            <button type="button" class="btn btn-default" ng-click="editDebit(debit);">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="deleteDebit(debit); editmode = !editmode">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr class="text-danger">
                    <td  colspan="4">Total</td><td style="text-align: center"><i class="fa fa-inr"></i>{{totalDebitDr}}</td><td style="text-align: center"><i class="fa fa-inr"></i>{{totalDebitCr}}</td>
                </tr>
                </tbody>
            </table>
                        </div>

                    </div>
                </div>
            <div class="clearfix" ng-show="debits.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>

        </div>
    </tab>-->
    </tabset>

    </div>
