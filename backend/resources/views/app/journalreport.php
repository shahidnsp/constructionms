<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-credit-card"> </i>  Journal Entries </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <h3 ng-hide="incomeedit">Journal Entries and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-info" ng-click="exportToExcel('incomeTable')">Export To Excel</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 text-right">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">From</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">To</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                        <button class="btn btn-warning" ng-click="searchJournalDate(fromDate,toDate);">Search</button>
                    </div>
                </div>

            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Journal Entries and details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="incomeTable" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>SlNo</th>
                                <th>Date</th>
                                <th>Particular</th>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th ng-show="extra">Created</th>
                                <th ng-show="extra">Updated</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="journal in listCount = (journals | filter:filterlist) | pagination: currentPage : numPerPage | orderBy:'-date'">
                                <td>{{journal.id}}</td>
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td>{{journal.date | date:'dd-MMMM-yyyy'}}</td>
                                <td><span class="text-primary">{{journal.drledger.name}} Dr.</span><br>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-success">To {{journal.crledger.name}}</span><br>
                                    <p class="description" popover="{{journal.note}}" popover-trigger="mouseenter">{{journal.note}}</p>
                                </td>
                                <td><i class="fa fa-inr"></i>{{journal.dramount}}</td>
                                <td><br/><i class="fa fa-inr"></i>{{journal.cramount}}</td>
                                <td ng-show="extra">{{journal.created_at}}</td>
                                <td ng-show="extra">{{journal.updated_at}}</td>
                                <td>
                                    <div ng-if="user.permissions.journalreport.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                        <button type="button" class="btn btn-default" ng-click="deleteJournal(journal); editmode = !editmode">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix" ng-show="journals.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>