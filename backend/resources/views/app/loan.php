<div class="row">
	<div class="col-md-12">
		<h2><i class="fa fa-tags"></i> Loan</h2>
		<ol class="breadcrumb-cus">
			<li ng-repeat="breadCrumb in breadCrumbs" ng-class="{active: $last}" >
				<a ng-if="!$last" href ng-click="switchDiv($event);"><i class="fa fa-home" ng-show="$first"></i> {{breadCrumb}}</a>
				<span ng-if="$last">{{breadCrumb}}</span>
			</li>
		</ol>
	</div>
</div>
<div class="row" ng-switch on="openDiv">
	<div class="col-md-12" ng-switch-default>
		<div class="box">
			<button ng-if="user.permissions.loan.write =='true'" type="button" class="btn btn-primary pull-right" ng-hide="loanedit" ng-click="newLoan();">
				<i class="fa fa-plus"></i> Add loan
			</button>
			<form class="form-horizontal" ng-submit="addLoan()" ng-show="loanedit">
				<h3>New Loan</h3><br>
				<div class="form-group">
				  <label class="col-sm-2 control-label">Loan name</label>
				  <div class="col-sm-10">
				    <input type="text" class="form-control" ng-model="newloan.name" placeholder="Loan name" required>
				  </div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Bank</label>
					<div class="col-sm-2">
						<select ng-model="newloan.bank_id" ng-options="bank.id as bank.name for bank in banks" class="form-control" required></select>
					</div>
				  <label class="col-sm-1 control-label">Account</label>
				  <div class="col-sm-3">
				    <input type="text" class="form-control" ng-model="newloan.account.number" placeholder="Account Number">
				  </div>
				  <label class="col-sm-1 control-label">IBAN</label>
				  <div class="col-sm-3">
				    <input type="text" class="form-control" ng-model="newloan.account.iban" placeholder="IBAN Number">
				  </div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Amount</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" ng-model="newloan.amount" ng-change="loanBal()" placeholder="Amount" required>
					</div>
				  <label class="col-sm-1 control-label">Paid</label>
				  <div class="col-sm-3">
				    <input type="text" class="form-control" ng-model="newloan.paid" ng-change="loanBal()" placeholder="Paid" required>
				  </div>
				  <label class="col-sm-1 control-label">Due</label>
				  <div class="col-sm-3">
				    <input type="text" class="form-control" ng-model="newloan.balance" placeholder="Due" required>
				  </div>
				</div>
				<div class="form-group">
				  <label class="col-sm-2 control-label">Interest</label>
				  <div class="col-sm-2">
						<input type="text" ng-model="newloan.interest" class="form-control" placeholder="%">
				  </div>
					<label class="col-sm-1 control-label">Date</label>
					<div class="col-sm-3">
						<div class="input-group">
							<input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newloan.date" is-open="datepicker" show-button-bar="false" show-weeks="false" readonly>
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" ng-click="datepicker=true"><i class="fa fa-calendar"></i></button>
							</span>
						</div>
					</div>
					<label class="col-sm-1 control-label">Close date</label>
					<div class="col-sm-3">
						<div class="input-group">
							<input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newloan.closedate" is-open="closedatepicker" show-button-bar="false" show-weeks="false" placeholder="Close date" readonly>
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" ng-click="closedatepicker=true"><i class="fa fa-calendar"></i></button>
							</span>
						</div>
					</div>
			  </div>
			  <div class="form-group">
					<label class="col-sm-2 control-label">Description</label>
					<div class="col-sm-10">
						<textarea class="form-control" ng-model="newloan.description" placeholder="Description"></textarea>
					</div>
				</div>
				<div class="form-group">
				  <div class="col-md-12 text-right">
				  	<button type="button" class="btn btn-default" ng-click="cancelLoan();">Cancel</button>
					  <button type="submit" class="btn btn-primary">Save</button>
				  </div>
				</div>
				<hr>
			</form>
			<h3>Loans and details</h3>
			<div class="row">
			  <div class="col-md-4">
			    <label for="">Show 
			      <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
			      entries
			    </label>
			  </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn-info" ng-click="exportToExcel('loanTable')">Export To Excel</button>
                    </div>
                </div>
	        <div class="col-md-4 text-right">
	          <div class="form-inline form-group">
	            <label for="filter-list">Search </label>
	            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
	          </div>
	        </div>
			</div>
            <div class="panel panel-default">
            <div class="panel-heading">
                <span class="text-success">Loans and details </span>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
			<table id="loanTable" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
                        <th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Account</th>
						<th>Amount</th>
						<th>Interest</th>
						<th>Paid</th>
						<th>Due</th>
					<!--	<th>Date</th>-->
						<!--<th>Close date</th>-->
                        <th ng-show="extra">User</th>
                        <th ng-show="extra">Created</th>
                        <th ng-show="extra">Updated</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="loan in  listCount = (loans | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                        <td>{{loan.id}}</td>
						<td><a href ng-click="openEmi(loan);">{{loan.name}}</a></td>
						<td><p class="description" popover="{{loan.description}}" popover-trigger="mouseenter">{{loan.description}}</p></td>
						<td>{{loan.account.number}}</td>
						<td><i class="fa fa-inr"></i> {{loan.amount}}</td>
						<td>{{loan.interest}}</td>
						<td><i class="fa fa-inr"></i> {{loan.paid}}</td>
						<td><i class="fa fa-inr"></i> {{loan.balance}}</td>
					<!--	<td>{{loan.date | date:'dd-MMMM-yyyy'}}</td>-->
					<!--	<td>{{loan.closedate | date:'dd-MMMM-yyyy'}}</td>-->
                        <td ng-show="extra">{{loan.user}}</td>
                        <td ng-show="extra">{{loan.created_at}}</td>
                        <td ng-show="extra">{{loan.updated_at}}</td>
						<td>
							<div ng-if="user.permissions.loan.edit =='true'" class="btn-group btn-group-xs" role="group">
								<button type="button" class="btn btn-default" ng-hide="editmode" ng-click="editLoan(loan);">
									<i class="fa fa-pencil"></i>
								</button>
								<button type="button" class="btn btn-default" ng-hide="editmode" ng-click="deleteLoan(loan);">
									<i class="fa fa-trash-o"></i>
								</button>
							</div>
						</td>
					</tr>
                <tr class="text-danger">
                    <td colspan="4" >Total</td>
                    <td ><i class="fa fa-inr"></i> {{totalloan}}</td>
                    <td ></td>
                    <td ><i class="fa fa-inr"></i> {{totalpaid}}</td>
                    <td><i class="fa fa-inr"></i> {{totaldue}}</td>
                </tr>
				</tbody>
			</table>
                    </div>
                </div>
                </div>
			<div class="clearfix">
				<pagination 
					ng-model="currentPage" 
					total-items="listCount.length" 
					max-size="maxSize" 
					items-per-page="numPerPage"
					boundary-links="true" 
					class="pagination-sm pull-right" 
					previous-text="&lsaquo;" 
					next-text="&rsaquo;" 
					first-text="&laquo;" 
					last-text="&raquo;"
				></pagination>
			</div>
		</div>
	</div>

    <div ng-switch-when="emi">
    <tabset class="col-md-12">
        <tab>
            <tab-heading>Loan Emi</tab-heading>
            <div>
                <div class="box">
                    <button ng-if="user.permissions.loan.write =='true'" type="button" class="btn btn-primary pull-right" ng-hide="emiedit" ng-click="newEmi();">
                        <i class="fa fa-plus"></i> Add EMI
                    </button>
                    <form class="form-horizontal" ng-submit="addEmi()" ng-show="emiedit">
                        <h3>New EMI</h3>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Loan</label>
                            <div class="col-sm-1">
                                <p class="form-control-static">{{myLoan.name}}</p>
                            </div>
                            <label class="col-sm-2 control-label">Amount</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" ng-model="newemi.amount" placeholder="Amount" required>
                            </div>
                            <label class="col-sm-1 control-label">Date</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newemi.date" is-open="emipicker" show-button-bar="false" show-weeks="false" readonly>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="emipicker=true"><i class="fa fa-calendar"></i></button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" ng-model="newemi.description" placeholder="Description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 text-right">
                                <button type="button" class="btn btn-default" ng-click="cancelEmi();">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                        <hr>
                    </form>
                    <h3>Loan EMI</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">Show
                                <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                entries
                            </label>
                        </div>
                        <div class="col-sm-3 text-center">
                            <div>
                                <button class="btn-info" ng-click="exportToExcel('emiTable')">Export To Excel</button>
                            </div>
                        </div>
                        <div class="col-md-5 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 text-right">
                                <div class="form-inline form-group">
                                    <label for="" class="control-label">From</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-left">
                                <div class="form-inline form-group">
                                    <label for="" class="control-label">To</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
                                            </span>
                                    </div>
                                    <button class="btn-warning" >Search</button>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="text-success">Loans EMI and details </span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                        <table id="emiTable" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Amount</th>
                                <th ng-show="extra">User</th>
                                <th ng-show="extra">Created</th>
                                <th ng-show="extra">Updated</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="emi in listCount = (emis | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                <td>{{emi.date | date:'dd-MMMM-yyyy'}}</td>
                                <td>
                                    <p class="description" popover="{{emi.description}}" popover-trigger="mouseenter">{{emi.description}}</p>
                                </td>
                                <td><i class="fa fa-inr"></i> {{emi.amount}}</td>
                                <td ng-show="extra">{{emi.user}}</td>
                                <td ng-show="extra">{{emi.created_at}}</td>
                                <td ng-show="extra">{{emi.updated_at}}</td>
                                <td>
                                    <div ng-if="user.permissions.loan.edit =='true'" class="btn-group btn-group-xs" role="group">
                                        <button type="button" class="btn btn-default" ng-click="editEmi(emi);">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="button" class="btn btn-default" ng-click="deleteEmi(emi);">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <tr class="text-danger">
                                <td colspan="2" >Total Emi Paid</td><td><i class="fa fa-inr"></i> {{totalemi}}</td>
                            </tr>
                            </tbody>
                        </table>
                                    </div>
                                </div>
                            </div>
                        <div class="clearfix">
                            <pagination
                                ng-model="currentPage"
                                total-items="listCount.length"
                                max-size="maxSize"
                                items-per-page="numPerPage"
                                boundary-links="true"
                                class="pagination-sm pull-right"
                                previous-text="&lsaquo;"
                                next-text="&rsaquo;"
                                first-text="&laquo;"
                                last-text="&raquo;"
                                ></pagination>
                        </div>
                    </div>
                </div>
            </div>

        </tab>
        <tab>
            <tab-heading ng-click="openExpense()">Loan Expenses</tab-heading>
            <div>
                <div class="box">
                   <button class="btn btn-primary pull-right" type="button" ng-click="newLoanExp()"><i class="fa fa-plus"></i>Add Expense</button>
                    <form class="form-horizontal" ng-show="loanExpEdit" ng-submit="addLoanExp()">
                        <h3>New Loan Expense</h3>
                        <div class="form-group">
                            <label class="control-label col-sm-1" style="text-align: left">Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" placeholder="Name" ng-model="newloanExp.name"/>
                            </div>
                            <label for="" class="col-sm-1 control-label" style="text-align: left">Date</label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" class="form-control" datepicker-popup="dd-MM-yyyy" ng-model="newloanExp.date" iis-open="wagepicker" show-button-bar="false" show-weeks="false" readonly/>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="wagepicker=true"><i class="fa fa-calendar"></i></button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-sm-1"> Amount</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" placeholder="Amount" ng-model="newloanExp.amount"/>
                            </div>
                            <label for="" class="control-label col-sm-1">Description</label>
                            <div class="col-sm-5">
                                <textarea class="resizable_textarea form-control" placeholder="Narration" ng-model="newloanExp.description"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12 text-right">
                                <button class="btn btn-danger" ng-click="cancelLoanExp">Cancel</button>
                                <button class="btn btn-default">Save</button>
                            </div>
                        </div>
                    </form>
                    <h3 Loan expense details></h3>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Show
                                <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                entries
                            </label>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search</label>
                                <input type="text" class="form-control" placeholder="Search" ng-model="filterlist"/>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="text-success">Loans Expense and details </span>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <!--<th>Loan id</th>-->
                                <th>Name</th>
                                <th>Amount</th>
                                <th>Description</th>
                                <th ng-show="extra">Created</th>
                                <th ng-show="extra">Updated</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="loanexp in listCount = (loanExpenses | filter:filterlist) | orderBy:'-created_at' | pagination:currentPage : numPerPage">
                            <td>{{loanexp.id}}</td>
                            <!--<td>{{loanexp.loan_id}}</td>-->
                            <td>{{loanexp.name}}</td>
                            <td>{{loanexp.amount}}</td>
                            <td>{{loanexp.description}}</td>
                            <td ng-show="extra">{{loanexp.creted_at}}</td>
                            <td ng-show="extra">{{loanexp.updated_at}}</td>
                            <td>
                                <div class="btn-group btn-group-xs" role="group">
                                    <button type="button" class="btn btn-default" ng-click="editLoanExp(loanexp)"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-default" ng-click="deleteLoanExp(loanexp)"><i class="fa fa-trash-o"></i></button>
                                </div>
                            </td>
                        </tr>
                        <tr class="text-danger">
                            <td colspan="2" >Total Expense</td><td><i class="fa fa-inr"></i> {{totalExpense}}</td>
                        </tr>
                        </tbody>
                    </table>
                                </div>
                            </div>
                        </div>
                    <div class="clearfix">
                        <pagination
                            ng-model="currentPage"
                            total-items="listCount.length"
                            max-size="maxSize"
                            items-per-page="numPerPage"
                            boundary-links="true"
                            class="pagination-sm pull-right"
                            previous-text="&lsaquo;"
                            next-text="&rsaquo;"
                            first-text="&laquo;"
                            last-text="&raquo;"
                            ></pagination>
                    </div>
                </div>
            </div>
        </tab>
    </tabset>
    </div>
</div>
