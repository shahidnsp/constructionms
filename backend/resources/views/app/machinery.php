<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Machineries</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.machinery.write =='true'" ng-click="newMachinary();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Machinery</button>
            <form class="form-horizontal" ng-show="machinaryedit" ng-submit="addMachinary();">
                <h3>New Machinery</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" ng-model="newmachinary.name" placeholder="Name" required>
                    </div>
                    <label for="" class="col-sm-1 control-label">Rent</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" ng-model="newmachinary.wageperday" placeholder="Rent Per Day" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Qty</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" ng-model="newmachinary.qty" placeholder="Qty">
                    </div>
                    <label for="" class="col-sm-1 control-label">Supplier</label>
                    <div class="col-sm-5">
                        <input type="file" class="form-control"  file-model="newmachinary.photo" placeholder="Image">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Description</label>
                    <div class="col-sm-11">
                        <textarea class="form-control" ng-model="newmachinary.description" placeholder="Description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelMachinary();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="rentedit">Machinery and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-sm btn-info" ng-click="exportToExcel('machenaryTable')">Export To Excel</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Suppliers and details
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="machenaryTable" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <!--<th>#</th>-->
                                <th>SlNo</th>
                                <th>Name</th>
                                <th>Wage(Day)</th>
                                <th>Description</th>
                                <th>Qty</th>
                                <th>Photo</th>
                                <th ng-show="extra">Created</th>
                                <th ng-show="extra">Updated</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="machinary in listCount  = (machinaries | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                               <!-- <td>{{machinary.id}}</td>-->
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td><a ng-click="open(machinary);">{{machinary.name}}</a></td>
                                <td><i class="fa fa-inr"></i> {{machinary.wageperday}}</td>
                                <td><p class="description" popover="{{machinary.description}}" popover-trigger="mouseenter">{{machinary.description}}</p></td>
                                <td>{{machinary.qty}}</td>
                                <td><img ng-click="viewPhoto(machinary);" id="image"  ng-src="{{machinary.photo}}" width="60px" height="60px"></td>
                                <td ng-show="extra">{{machinary.created_at}}</td>
                                <td ng-show="extra">{{machinary.updated_at}}</td>
                                <td>
                                    <div ng-if="user.permissions.machinery.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                        <button type="button" class="btn btn-default" ng-click="editMachinary(machinary);">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="button" class="btn btn-default" ng-click="deleteMachinary(machinary); editmode = !editmode">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix" ng-show="machinaries.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>