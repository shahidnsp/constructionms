<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Material Returns </h2>
    </div>
</div>
<div class="row">
<div class="col-md-12">
<div cgitlass="box">
    <button ng-if="user.permissions.materialreturn.write==='true'" ng-click="newMaterialReturn();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Return</button>
    <form class="form-horizontal" ng-show="materialreturnedit" ng-submit="addMaterialReturn();">
        <h3>New MaterialReturn</h3><br>
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">Return Date</label>
            <div class="col-sm-10">
                <div class="input-group">
                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newmaterialreturn.date" is-open="materialreturn_datepicker" show-button-bar="false" show-weeks="false" readonly>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default" ng-click="materialreturn_datepicker=true"><i class="fa fa-calendar"></i></button>
								</span>
                </div>
            </div>

        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">Project</label>
            <div class="col-sm-10">
                <select class="form-control" ng-model="newmaterialreturn.projects_id" required>
                    <option value="">Select</option>
                    <option ng-repeat="project in projects" value="{{project.id}}">{{project.name}}</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
                <textarea class="form-control" ng-model="newmaterialreturn.description" placeholder="Description"></textarea>
            </div>
        </div>
        <div>
            <div class="col-lg-13">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bmaterialreturned table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SlNo</th>
                                    <th>Row Meterial</th>
                                    <th>Rate</th>
                                    <th>Qty</th>
                                    <th>Unit</th>
                                    <th>Net</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="newitem in newitems">
                                    <td><div>{{$index+1}}</div></td>
                                    <td>
                                        <div ng-hide="editingData[newitem.id]">{{newitem.name}}</div>
                                        <div ng-show="editingData[newitem.id]"><select class="form-control" ng-change="selectItem(newitem);" ng-model="newitem.products_id"> <option ng-repeat="product in products" value="{{product.id}}" ng-selected="newitem.products_id == product.id">{{product.name}}</option></select></div>
                                    </td>
                                    <td>
                                        <div ng-hide="editingData[newitem.id]">{{newitem.rate}}</div>
                                        <div ng-show="editingData[newitem.id]"><input type="text" onkeypress="return isNumberKey(event)" ng-change="changePrice(newitem);" class="form-control" ng-model="newitem.rate"></div>
                                    </td>
                                    <td>
                                        <div ng-hide="editingData[newitem.id]">{{newitem.qty}}</div>
                                        <div ng-show="editingData[newitem.id]"><input type="text" onkeypress="return isNumberKey(event)" ng-change="changePrice(newitem);" class="form-control" ng-model="newitem.qty"></td></div>
                        <td>
                            <div ng-hide="editingData[newitem.id]">{{newitem.unit}}</div>
                            <div ng-show="editingData[newitem.id]"><select class="form-control" ng-change="changePrice(newitem);" ng-model="newitem.units_id"> <option ng-repeat="unit in units" value="{{unit.id}}">{{unit.name}}</option></select></div>
                        </td>
                        <td>
                            <div ng-hide="editingData[newitem.id]">{{newitem.net}}</div>
                            <div ng-show="editingData[newitem.id]"><input type="text" onkeypress="return isNumberKey(event)" class="form-control" ng-model="newitem.net"></div>
                        </td>

                        <td ng-show="newitems.length>0">
                            <div ng-hide="editingData[newitem.id]" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button type="button" class="btn btn-success" ng-click="editItem(newitem);">Edit
                                </button>
                                <button type="button" class="btn btn-danger" ng-click="removeItem(newitem); editmode = !editmode">Remove
                                </button>
                                <button type="button" class="btn btn-default" ng-click="viewItem(newitem); editmode = !editmode">View
                                </button>
                            </div>
                            <div ng-show="editingData[newitem.id]"  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button type="button" class="btn btn-success" ng-click="updateItem(newitem);">Update
                                </button>
                                <button type="button" class="btn btn-danger" ng-click="removeItem(newitem); editmode = !editmode">Remove
                                </button>
                                <button type="button" class="btn btn-default" ng-click="viewItem(newitem); editmode = !editmode">View
                                </button>
                            </div>
                        </td>
                        </tr>
                        <tr class="text-danger" style="font-size: 16px;">
                            <td colspan="5">Total</td><td style="text-align: center">{{newmaterialreturn.amount}}</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="form-group">
    <div class="col-sm-12 text-right">
        <button type="button" class="btn btn-default" ng-click="addItem();">Add Item</button>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12 text-right">
        <button type="button" class="btn btn-default" ng-click="cancelMaterialReturn();">Cancel</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</div>
<hr>
</form>

<h3>Materials Return and details</h3>
<div class="row">
    <div class="col-md-4">
        <label for="">Show
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-sm-3 text-center">
        <div>
            <button class="btn btn-primary btn-sm" ng-click="exportToExcel('tableMaterialReturn');">Export</button>
        </div>
    </div>
    <div class="col-md-5 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-5 text-right">
        <div class="form-inline form-group">
            <label for="" class="control-label">From</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
        </div>
    </div>
    <div class="col-sm-6 text-left">
        <div class="form-inline form-group">
            <label for="" class="control-label">To</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
            <button class="btn btn-warning" ng-click="searchMaterialReturnDate(fromDate,toDate)">Search</button>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <p class="text-primary">Materials Returns and Details from {{fromDate | date:'dd-MMMM-yyyy'}} to {{toDate | date:'dd-MMMM-yyyy'}}</p>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="tableMaterialReturn">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Project</th>
                        <th>Description</th>
                        <th>Item</th>
                        <th>Amount</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="materialreturn in listCount  = (materialreturns | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{materialreturn.date | date:'dd-MMMM-yyyy'}}</td>
                        <td>{{materialreturn.project.name}}</td>
                        <td><p class="description" popover="{{materialreturn.description}}" popover-trigger="mouseenter">{{materialreturn.description}}</p></td>
                        <td> <button type="button" class="btn btn-default" ng-click="viewMaterialReturnItem(materialreturn);">
                                View MaterialReturn Items
                            </button>
                        </td>
                        <td>{{materialreturn.amount}}</td>
                        <td>
                            <div  ng-if="user.permissions.materialreturn.edit==='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button type="button" class="btn btn-default" ng-click="editMaterialReturn(materialreturn);">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-default" ng-click="deleteMaterialReturn(materialreturn); editmode = !editmode">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="clearfix" ng-show="materialreturns.length > numPerPage">
    <pagination
        ng-model="currentPage"
        total-items="listCount.length"
        max-size="maxSize"
        items-per-page="numPerPage"
        boundary-links="true"
        class="pagination-sm pull-right"
        previous-text="&lsaquo;"
        next-text="&rsaquo;"
        first-text="&laquo;"
        last-text="&raquo;"
        ></pagination>
</div>
</div>
</div>
</div>

