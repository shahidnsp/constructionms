<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-tags"></i> Office Expenses</h2>
    </div>
</div>
<div class="row">
    <div class="box">
        <button class="btn btn-primary pull-right" type="button" ng-click="newOfficeExp()"><i class="fa fa-plus"></i>Add Office Expense</button>
        <form class="form-horizontal" ng-show="officeExpEdit" ng-submit="addOfficeExp()" name="addForm">
            <h3>New Office Expense</h3>
            <div class="form-group">
                <label class="col-sm-1 control-label" style="text-align: left;">Amount</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" ng-model="newofficeExp.amount" placeholder="Amount" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" required />
                </div>
                <label for="" class="col-sm-1 control-label" style="text-align: left;">Date</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newofficeExp.date" is-open="wagepicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="wagepicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="" class="col-sm-1 control-label" style="text-align: left;">Description</label>
                <div class="col-sm-11">
                    <textarea class="resizable_textarea form-control" placeholder="Narration" ng-model="newofficeExp.description"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 text-right">
                    <button class="btn btn-danger" ng-click="cancel()">Cancel</button>
                    <button class="btn btn-default">Save</button>
                </div>
            </div>
        </form>
<!--        TODO -->
<!--        ng-hide-->
        <h3>Office Expense and Details</h3>
        <div class="row">
            <div class="col-md-6">
                <label for="">Show
                    <select class="form-control pagiantion" ng-model="numPerPage" ng-options="item for item in numsForPage"></select>
                    entries
                </label>
            </div>
            <div class="col-md-6 text-right">
                <div class="form-inline form-group">
                    <label for="filter-list">Search</label>
                    <input type="text" class="form-control" placeholder="Search" ng-model="filterlist"/>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Units and details
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="officeTable" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>SlNo</th>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Description</th>
                    <th ng-show="extra">Created at</th>
                    <th ng-show="extra">Updated at</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="expense in listCount = (officeExpenses | filter:filterlist) | orderBy:'-created_at' | pagination:currentPage : numPerPage">
                    <td>{{expense.id}}</td>
                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                    <td>{{expense.date}}</td>
                    <td><i class="fa fa-inr"></i>{{expense.amount}}</td>
                    <td><p class="description" popover="{{expense.description}}" popover-trigger="mouseenter">{{expense.description}}</p></td>
                    <td ng-show="extra">{{expense.created_at}}</td>
                    <td ng-show="extra">{{expense.updated_at}}</td>
                    <td>
                        <div class="btn-group btn-group-xs" role="group">
                            <button type="button" class="btn btn-default" ng-click="editOfficeExp(expense)"><i class="fa fa-pencil"></i></button>
                            <button type="button" class="btn btn-default" ng-click="deleteOfficeExp(expense)"><i class="fa fa-trash-o"></i></button>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
                    </div>
                </div>
            </div>
        <div class="clearfix" ng-show="officeExpenses.length > numPerPage">
            <pagination
                ng-model="currentPage"
                total-items="listCount.length"
                max-size="maxSize"
                items-per-page="numPerPage"
                boundary-links="true"
                class="pagination-sm pull-right"
                previous-text="&lsaquo;"
                next-text="&rsaquo;"
                first-text="&laquo;"
                last-text="&raquo;"
                ></pagination>
        </div>
    </div>
</div>