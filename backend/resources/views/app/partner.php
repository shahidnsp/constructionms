<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Partner</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.partner.write =='true'" ng-click="newPartner();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Partner</button>
            <form class="form-horizontal" ng-show="partneredit" ng-submit="addPartner();">
                <h3>New Partner</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Fisrt Name</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newpartner.firstname" placeholder="Fisrt Name">
                    </div>
                    <label for="" class="col-sm-1 control-label">Last Name</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newpartner.lastname" placeholder="Last Name">
                    </div>
                    <label for="" class="col-sm-1 control-label">Reg Date</label>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newpartner.regDate" is-open="partnerpicker" show-button-bar="false" show-weeks="false" readonly>
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="partnerpicker=true"><i class="fa fa-calendar"></i></button>
              </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Telephone</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newpartner.phone" placeholder="Telephone">
                    </div>
                    <label for="" class="col-sm-1 control-label">Mobile 1</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newpartner.mobile1" placeholder="Mobile 1">
                    </div>
                        <label for="" class="col-sm-1 control-label">Mobile 2</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newpartner.mobile2" placeholder="Mobile 2">
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Email</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" ng-model="newpartner.email" placeholder="Email">
                    </div>
                    <label for="" class="col-sm-1 control-label">Project</label>
                    <div class="col-sm-5">
                        <select class="form-control" ng-model="newpartner.project_id">
                            <option ng-repeat="project in projects" value="{{project.id}}">{{project.name}}</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Amount</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" ng-model="newpartner.amount" placeholder="Amount">
                    </div>
                    <label for="" class="col-sm-1 control-label">Share(%)</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" ng-model="newpartner.share" placeholder="Share">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelPartner();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="partneredit">Partners and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-sm btn-info" ng-click="exportToExcel('partners')">Export To Excel</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <table id="partners" class="table table-hover table-responsive">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Mobile1</th>
                  <!--  <th>Mobile2</th>
                    <th>Email</th>-->
                    <th>Project</th>
                    <th>Capital</th>
                    <th>Share(%)</th>
                    <th>Date</th>
                    <th ng-show="extra">Created</th>
                    <th ng-show="extra">Updated</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="partner in listCount  = (partners | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                    <td>{{partner.id}}</td>
                    <td>{{partner.firstname}} {{partner.lastname}}</td>
                    <td><p class="description" popover="{{partner.address}}" popover-trigger="mouseenter">{{partner.address}}</p></td>
                    <td>{{partner.phone}}</td>
                    <td>{{partner.mobile1}}</td>
                  <!--  <td>{{partner.mobile2}}</td>
                    <td>{{partner.email}}</td>-->
                    <td>{{partner.projects.name}}</td>
                    <td><i class="fa fa-inr"></i> {{partner.amount}}</td>
                    <td>{{partner.share}}%</td>
                    <td>{{partner.regDate | date:'dd-MMMM-yyyy'}}</td>
                    <td ng-show="extra">{{partner.created_at}}</td>
                    <td ng-show="extra">{{partner.updated_at}}</td>
                    <td>
                        <div ng-if="user.permissions.partner.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                            <button type="button" class="btn btn-default" ng-click="editPartner(partner);">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="deletePartner(partner); editmode = !editmode">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="clearfix" ng-show="partners.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>