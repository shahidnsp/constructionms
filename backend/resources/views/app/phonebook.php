<div class="container-fluid" ng-controller="phonebookController">
	<div class="row" ng-controller="phonebookController">
		<div class="col-md-12 box">
			<h2>Phonebook</h2>
			<p>Its like a Phone Book.You can enter details of people related to you.You can assign any of these people as employee</p>
		</div>
		<div class="col-md-3 contact-box">
			<form>
			<div class="input-group">
			  <input type="search" class="form-control" placeholder="Search contact" ng-model="searchContact">
			  <div class="input-group-addon"><i class="fa fa-search"></i></div>
			</div>
			</form>
			<div class="phonebook-box">
					<div class="contact-list" ng-repeat="phoneContact in phoneContacts | filter:searchContact">
						<figure class="pull-left">
							<a ng-click="getDeatils($index)">
								<img src="images/{{phoneContact.photo}}" class="img-thumbnail">
							</a>
						</figure>
						<ul class="list-unstyled">
							<li><a ng-click="getDeatils($index)"><strong>{{phoneContact.name + " " + phoneContact.lastname}}</strong></a></li>
							<li><i class="fa fa-phone"></i> {{phoneContact.phone[0].number}}</li>
							<li><i class="fa fa-envelope"></i> {{phoneContact.email1}}</li>
						</ul>
					</div>
			</div>		
		</div>
		<div class="col-md-9 contact-table phonebook-box" ng-hide="contactMode.editMode">
			<div class="btn-group pull-right">
				<button ng-if="user.permissions.phonebook.write =='true'" class="btn btn-primary" type="button" ng-click="newContact();"><i class="fa fa-plus"></i> Add</button>
				<!-- <button class="btn btn-primary" type="button" ng-click="contactMode.editMode = !contactMode.editMode"><i class="fa fa-plus"></i> Add</button> -->
				<button ng-if="user.permissions.phonebook.edit =='true'" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">
				  <span class="caret"></span>
				  <span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul role="menu" class="dropdown-menu">
				  <li><a ng-click="editContact(selectedContact);"><i class="fa fa-edit"></i> Edit</a></li>
				  <li class="divider"></li>
				  <li><a ng-click="deleteContact(selectedContact);"><i class="fa fa-trash-o"></i> Delete</a></li>
				</ul>
			</div>
			<table  class="table table-hover">
				<tr>
					<th>Title</th>
					<td>{{selectedContact.title}}</td>
				</tr>
				<tr>
					<th>First name</th>
					<td>{{selectedContact.name}}</td>
				</tr>
				<tr>
					<th>Last name</th>
					<td>{{selectedContact.lastname}}</td>
				</tr>
				<tr>
					<th>Address</th>
					<td>{{selectedContact.address}}</td>
				</tr>
				<tr>
					<th>Primary email</th>
					<td>{{selectedContact.email1}}</td>
				</tr>
				<tr>
					<th>Secondary email</th>
					<td>{{selectedContact.email2}}</td>
				</tr>
				<tr ng-repeat="contact in selectedContact.phone">
					<th>Contact number {{$index + 1 }}</th>
					<td>{{contact.number}}</td>
				</tr>

			</table>
		</div>
		<div class="col-md-9 contact-edit phonebook-box" ng-show="contactMode.editMode">
			<h2 ng-show="contactMode.newMode">New contact</h2>
			<h2 ng-hide="contactMode.newMode">{{selectedContact.name + " " + selectedContact.lastname}}</h2>
			<form class="form-horizontal" ng-submit="addContact();">
			  <div class="form-group">
				<label for="title" class="col-sm-3 control-label">Title</label>
				<div class="col-sm-9">
					<select name="title" id="title" class="form-control" ng-model="currentContact.title" ng-options="title for title in contactMode.titleList"></select>
				</div>
			</div>
				<div class="form-group">
				  <label for="first-name" class="col-sm-3 control-label">First name</label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" id="first-name" placeholder="First name" ng-model="currentContact.name" required>
				  </div>
				</div>
				<div class="form-group">
				  <label for="last-name" class="col-sm-3 control-label">Last name</label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" id="last-name" placeholder="Last name" ng-model="currentContact.lastname" >
				  </div>
				</div>
				<div class="form-group">
				  <label for="last-name" class="col-sm-3 control-label">Address</label>
				  <div class="col-sm-9">
					<input type="text" class="form-control" id="address" placeholder="Address" ng-model="currentContact.address" >
				  </div>
				</div>
				<div class="form-group">
				  <label for="primary-email" class="col-sm-3 control-label">Primary email</label>
				  <div class="col-sm-9">
					<input type="email" class="form-control" id="primary-email" placeholder="Primary email" ng-model="currentContact.email1" >
				  </div>
				</div>
				<div class="form-group">
				  <label for="secondary-email" class="col-sm-3 control-label">Secondary email</label>
				  <div class="col-sm-9">
					<input type="email" class="form-control" id="secondary-email" placeholder="Secondary email" ng-model="currentContact.email2">
				  </div>
				</div>
                <div class="form-group">
                    <label for="secondary-email" class="col-sm-3 control-label">Photo</label>
                    <div class="col-sm-9">
                        <input type="file" class="form-control"  placeholder="Photo" file-model="currentContact.photo">
                    </div>
                </div>
				<div class="form-group" ng-repeat="contact in currentContact.phone">
				  <label for="contact-number3" class="col-sm-3 control-label">Contact number {{$index + 1 }}</label>
				  <div class="col-sm-9">
					<div class="input-group">
							<input type="tel" class="form-control" placeholder="Contact number {{$index + 1 }}" ng-model="contact.number">
							<span class="input-group-btn">
							  <button class="btn btn-default" type="button" ng-init="plusSign = true" ng-click="plusSign = !plusSign; dynamicField($last, $index);">
								  <i class="fa" ng-class="$last ? 'fa-plus' : 'fa-minus'"></i>
							  </button>
							</span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-12 text-right">
						<button type="button" class="btn btn-default" ng-click="initialContact();">Cancel</button>
						<button type="submit" class="btn btn-primary">
							{{contactMode.newMode ? "Add new contact" : "Update contact"}}
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>