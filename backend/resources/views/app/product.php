<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Products</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.product.write =='true'" ng-click="newProduct();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Product</button>
            <form class="form-horizontal" ng-show="productedit" ng-submit="addProduct();">
                <h3>New Product</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newproduct.name" placeholder="Name" required>
                    </div>
                    <label for="" class="col-sm-1 control-label">P.Price</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" ng-model="newproduct.purchase_price" ng-change="salesPrice()" placeholder="Amount">
                    </div>
                    <label for="" class="col-sm-1 control-label">S.Price</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" ng-model="newproduct.sales_price" placeholder="Amount" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Qty</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newproduct.qty" placeholder="Qty">
                    </div>
                    <label for="" class="col-sm-1 control-label">Supplier</label>
                    <div class="col-sm-2">
                        <select class="form-control" ng-model="newproduct.supplier_id">
                            <option ng-repeat="supplier in suppliers" value="{{supplier.id}}">{{supplier.name}}</option>
                        </select>
                    </div>
                    <label for="" class="col-sm-1 control-label">Unit</label>
                    <div class="col-sm-2">
                        <select class="form-control" ng-model="newproduct.unit_id" required>
                            <option ng-repeat="unit in units" value="{{unit.id}}">{{unit.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Stock Value</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newproduct.stockvalue" placeholder="Stock Value">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" ng-model="newproduct.description" placeholder="Description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelProduct();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="rentedit">Product and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-sm btn-info" ng-click="exportToExcel('productTable')">Export To Excel</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Suppliers and details
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="productTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>SlNo</th>
                    <th>Name</th>
                    <th>S.Price</th>
                    <th>P.Price</th>
                    <th>Description</th>
                    <th>Qty</th>
                    <th>Unit</th>
                    <th>Supplier</th>
                    <th ng-show="extra">Created</th>
                    <th ng-show="extra">Updated</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="product in listCount  = (products | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                    <td>{{product.id}}</td>
                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                    <td><a ng-click="open(product);">{{product.name}}</a></td>
                    <td><i class="fa fa-inr"></i> {{product.sales_price}}</td>
                    <td><i class="fa fa-inr"></i> {{product.purchase_price}}</td>
                    <td><p class="description" popover="{{product.description}}" popover-trigger="mouseenter">{{product.description}}</p></td>
                    <td>{{product.qty}}</td>
                    <td>{{product.units.name}}</td>
                    <td>{{product.suppliers.name}}</td>
                    <td ng-show="extra">{{product.created_at}}</td>
                    <td ng-show="extra">{{product.updated_at}}</td>
                    <td>
                        <div ng-if="user.permissions.product.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                            <button type="button" class="btn btn-default" ng-click="editProduct(product);">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="deleteProduct(product); editmode = !editmode">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
                        </div>
                    </div>
                </div>
            <div class="clearfix" ng-show="products.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>