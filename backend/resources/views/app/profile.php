  <div class="container-fluid" ng-controller="profileController">
    <div class="row">
      <div class="col-md-12">
        <h3><i class="fa fa-user"></i> My details</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="profile-brief">
          <div class="col-md-3 profile-summary">
            <figure>
              <a><img class="img-circle" src="images/{{user.photo}}" width="160" height="160" alt="Photo"></a>
            </figure>
          </div>
          <div class="col-md-6 profile-summary">
            <h3>{{user.name}} {{user.lastname}}</h3>
              <address>
                  <strong>Address</strong><br>
                  <abbr title="Phone">{{user.address}}</abbr>
              </address>
            <address>
              <strong>Phone</strong><br>
              <abbr title="Phone">{{user.phone}}</abbr>
            </address>
            <address>
              <strong>Email</strong><br>
              <a href="mailto:{{user.email1}}">{{user.email1}}</a>
            </address>
          </div>
          <div id="purchase-summary" class="col-md-3">
              <address>
                  <strong>Post</strong><br>
                  <abbr title="Phone">{{user.post}}</abbr>
              </address>
              <address>
                  <strong>District</strong><br>
                  <abbr title="Phone">{{user.district}}</abbr>
              </address>
              <address>
                  <strong>State</strong><br>
                  <abbr title="Phone">{{user.state}}</abbr>
              </address>
              <address>
                  <strong>Pin</strong><br>
                  <abbr title="Phone">{{user.pin}}</abbr>
              </address>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <tabset class="col-md-12">
          <tab>
            <tab-heading><i class="fa fa-user"></i> Profile</a></tab-heading>
            <div class="row box">
              <div class="col-md-4 hide">
                <strong>My details</strong>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae, placeat, quos totam voluptatibus veniam rem amet itaque rerum quibusdam et ipsam sequi error temporibus autem doloremque laboriosam iusto facere aliquam.</p>
              </div>
              <div class="col-md-6">
                <h4>Basic information</h4>
                <dl class="dl-horizontal">
                  <dt>First Name</dt>
                    <dd>{{user.name}}</dd>
                  <dt>Last Name</dt>
                    <dd>{{user.lastname}}</dd>
                  <dt>Address</dt>
                    <dd>{{user.address}}</dd>
                  <dt>Post</dt>
                    <dd>{{user.post}}</dd>
                </dl>
              </div>
              <div class="col-md-6">
                <h4>Contact information</h4>
                <ul class="list-unstyled">
                  <li><i class="fa fa-map-marker"></i> {{user.district}}</li>
                  <li><a href=""><i class="fa fa-phone"></i> {{user.phone}}</a></li>
                  <li><a href="mailto:{{user.email1}}"><i class="fa fa-envelope"></i>{{user.email1}} </a></li>

                </ul>
                <h4>Social connections</h4>
                <ul class="list-inline">
                  <li><a><i class="fa fa-facebook-square fa-2x"></i></a></li>
                  <li><a><i class="fa fa-twitter-square fa-2x"></i></a></li>
                  <li><a><i class="fa fa-linkedin-square fa-2x"></i></a></li>
                  <li><a><i class="fa fa-skype fa-2x"></i></a></li>
                  <li><a><i class="fa fa-youtube-square fa-2x"></i></a></li>
                  <li><a><i class="fa fa-instagram fa-2x"></i></a></li>
                </ul>
              </div>
            </div>
          </tab>
      </tabset>     


  </div>