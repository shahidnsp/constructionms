<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Profit and Loss Account </h2>
    </div>
</div>
<div class="row text-center">
    <!--<div class="col-md-4">
        <label for="">Show
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>-->
    <!--  <div class="col-sm-3">-->
    <div>
        <button class="btn-info" ng-click="exportToExcel('profitandlossTable')">Export To Excel</button>
    </div>
    <!-- </div>-->
    <!--<div class="col-md-5 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>-->
</div>
<br>
<div class="row">
    <div class="col-sm-5 text-right">
        <div class="form-inline form-group">
            <label for="" class="control-label">From</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
        </div>
    </div>
    <div class="col-sm-6 text-left">
        <div class="form-inline form-group">
            <label for="" class="control-label">To</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
            <button class="btn btn-warning" ng-click="searchDate(fromDate,toDate)">Search</button>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <p class="text-primary"> Profit and Loss Account </p>
            <p class="text-primary">{{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}} </p>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table  class="table table-striped table-bordered table-hover" id="profitandlossTable">
                    <tr>
                        <td class="text-left">Dr.</td><td class="text-right">Cr.</td>
                    </tr>
                    <tr>
                        <td>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <th>Particular</th><th>Amount</th>
                                </thead>
                                <tbody>
                                <tr ng-repeat="journal in journals" ng-if="journal.side==='Dr'">
                                    <td ng-if="journal.type==='Status'">To {{journal.particular}} (from Trading Account)</td>
                                    <td class="text-danger text-center" ng-if="journal.type=='Status'"><i class="fa fa-inr"></i> {{journal.balance}}</td>

                                    <td ng-if="journal.type==='Other'">To {{journal.particular}}</td>
                                    <td class="text-danger text-center" ng-if="journal.type==='Other'"><i class="fa fa-inr"></i> {{journal.balance}}</td>
                                </tr>
                                <tr ng-show="balanceSide==='Dr'" class="text-right text-success">
                                    <td>(To Net Profit transfered to B/S)</td><td><i class="fa fa-inr"></i> {{balance}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <th>Particular</th><th>Amount</th>
                                </thead>
                                <tbody>
                                <tr ng-repeat="journal in journals" ng-if="journal.side==='Cr'">
                                    <td ng-if="journal.type==='Status'">To {{journal.particular}} (from Trading Account)</td>
                                    <td class="text-danger text-center" ng-if="journal.type==='Status'"><i class="fa fa-inr"></i> {{journal.balance}}</td>

                                    <td ng-if="journal.type==='Other'">To {{journal.particular}}</td>
                                    <td class="text-danger text-center" ng-if="journal.type==='Other'"><i class="fa fa-inr"></i> {{journal.balance}}</td>
                                </tr>
                                <tr ng-show="balanceSide==='Cr'" class="text-right text-danger">
                                    <td>(By Net Loss transfered to B/S)</td><td><i class="fa fa-inr"></i> {{balance}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="text-right text-danger">
                        <td><i class="fa fa-inr"></i> {{debitTotal}}</td><td><i class="fa fa-inr"></i> {{creditTotal}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

