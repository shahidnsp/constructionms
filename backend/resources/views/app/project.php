<div class="row">
  <div class="col-md-12">
	<h2><i class="fa fa-briefcase"> </i>  Projects</h2>
  </div>
</div>
<div class="row">
	<div class="col-md-12">
        <div class="row">
            <div class="box">
                <p>You Can add project like Build an office or making a House.It also used to enter project related expenses and wage paid.</p>
            </div>
        </div>
	</div>
    <div class="col-md-12">
        <label class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10">
            <select class="form-control" name="" id="" ng-model="projectstatus" ng-change="changeStatus(projectstatus);">
                <option value="all">All</option>
                <option value="0">Active</option>
                <option value="1">Completed</option>
            </select>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-md-3">
		<div class="box" style="margin-bottom: 0; border-bottom: none;">
			<h4 class="pull-left">Projects</h4>
			<button ng-if="user.permissions.project.write =='true'" class="btn btn-xs btn-primary pull-right" ng-hide="projectedit" ng-click="newProject();"><i class="fa fa-plus"></i> Add project</button>
			<div class="clearfix"></div>
		</div>
		<ul class="list-group project-list">
		  <li class="list-group-item" ng-repeat="project in projects | orderBy:'-enddate'" ng-class="{active: project.id == curProject.id}">
			  <button class="pull-right btn btn-primary btn-xs" ng-click="openProject(project)"><i class="fa fa-arrow-right"></i></button>
			<p><strong>{{project.name}}</strong></p>
			<ul class="list-unstyled">
				<li>Budget: <i class="fa fa-inr"></i> {{project.approxamount}}</li>
			  	<li>Close Date: {{project.enddate | date:'dd-MMM-yyyy'}}</li>
			</ul>
		  </li>
		</ul>
	</div>
	<div class="col-md-9" ng-init="initialProject(0);">
		<div class="box">
			<form class="form-horizontal" ng-submit="addProject();" ng-show="projectedit">
				<h3>New Project</h3>
				<div class="form-group">
					<label class="col-sm-2 control-label">Project Name</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" ng-model="newproject.name" placeholder="Project Name" required="">
					</div>
					<label class="col-sm-2 control-label">Approximate Amount</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" ng-model="newproject.approxamount" placeholder="Amount">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Start Date</label>
					<div class="col-sm-4">
						<div class="input-group">
							<input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newproject.startdate" is-open="startdatepicker" show-button-bar="false" show-weeks="false" readonly required="">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" ng-click="startdatepicker=true"><i class="fa fa-calendar"></i></button>
							</span>
						</div>
					</div>
					<label class="col-sm-2 control-label">End Date</label>
					<div class="col-sm-4">
						<div class="input-group">
							<input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newproject.enddate" is-open="enddatepicker" show-button-bar="false" show-weeks="false" readonly>
							<span class="input-group-btn">
								<button type="button" class="btn btn-default" ng-click="enddatepicker=true"><i class="fa fa-calendar"></i></button>
							</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Description</label>
					<div class="col-sm-10">
						<textarea class="form-control" ng-model="newproject.description" placeholder="Description"></textarea>
					</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Contact Person</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newproject.contactperson" placeholder="Contact Person Name">
                    </div>
                    <label class="col-sm-1 control-label">Address</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" ng-model="newproject.address" placeholder="Address">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newproject.phone" placeholder="Phone Number">
                    </div>
                    <label class="col-sm-1 control-label">Mobile</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" ng-model="newproject.mobile" placeholder="Mobile Number">
                    </div>
                </div>
				<div class="form-group">
				  <div class="col-sm-12 text-right">
					<button type="button" class="btn btn-default" ng-click="cancelProject();">Cancel</button>
					  <button type="submit" class="btn btn-primary">Save</button>
				  </div>
				</div>
			</form>
			<div class="row" ng-hide="projectedit">
				<div class="col-md-12">
					<div class="btn-group pull-right" dropdown>
					  <button ng-if="user.permissions.project.write =='true'" type="button" class="btn btn-primary" ng-click="newProject();"><i class="fa fa-plus"></i> Add</button>
					  <button ng-if="user.permissions.project.edit =='true'" type="button" class="btn btn-primary dropdown-toggle" dropdown-toggle>
						<span class="caret"></span>
						<span class="sr-only">Split button!</span>
					  </button>
					  <ul class="dropdown-menu" role="menu" ng-show="projectaction">
							<li><a href ng-click="editProject(curProject);"><i class="fa fa-edit"></i> Edit</a></li>
                            <li class="divider"></li>
                            <li><a href ng-click="deactivateProject(curProject);"><i class="fa fa-refresh"></i> Deactivate</a></li>
							<li class="divider"></li>
							<li><a href ng-click="deleteProject(curProject);"><i class="fa fa-trash-o"></i> Delete</a></li>
					  </ul>
					</div>
					<h3>{{curProject.name}}</h3>
				</div>
				<div class="col-md-5">
					<table class="table table-striped table-responsive">
						<tbody>
							<tr>
								<td class="text-muted">Approximate Amount</td>
								<td><i class="fa fa-inr"></i> {{curProject.approxamount}}</td>
							</tr>
							<tr>
								<td class="text-muted">Start Date</td>
								<td>{{curProject.startdate | date:'dd-MMMM-yyyy'}}</td>
							</tr>
							<tr>
								<td class="text-muted">End Date</td>
								<td>{{curProject.enddate | date:'dd-MMMM-yyyy'}}</td>
							</tr>
                            <tr>
                                <td class="text-muted">Contact</td>
                                <td>{{curProject.contactperson}}</td>
                            </tr>
                            <tr>
                                <td class="text-muted">Address</td>
                                <td>{{curProject.address}}</td>
                            </tr>
                            <tr>
                                <td class="text-muted">Phone</td>
                                <td>{{curProject.phone}}</td>
                            </tr>
                            <tr>
                                <td class="text-muted">Mobile</td>
                                <td>{{curProject.mobile}}</td>
                            </tr>
                            <tr>
                                <td class="text-muted">Created By</td>
                                <td>{{curProject.user}}</td>
                            </tr>
                            <tr>
                                <td class="text-muted">Created</td>
                                <td>{{curProject.created_at}}</td>
                            </tr>
                            <tr>
                                <td class="text-muted">Updated</td>
                                <td>{{curProject.updated_at}}</td>
                            </tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-7">
					<dl>
					  <dt>Description</dt>
					  <dd><p>{{curProject.description}}</p></dd>
					</dl>
				</div>
			</div>
			<tabset ng-class="col-smd-12" ng-if="projectactive" ng-hide="projectedit">
				<tab>
					<tab-heading>Wages</tab-heading>
					<div class="box">
			<form class="form-horizontal" ng-show="wageedit" ng-submit="addWage();">
				<h3>New wage</h3><br>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Name</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" ng-model="newwage.name" placeholder="Name">
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Employee</label>
					<div class="col-sm-3">
				<!--	<input type="text" class="form-control" ng-model="newwage.employee.name" placeholder="Employee" typeahead="employee as employee.name for employee in employees | filter:$viewValue | limitTo:8">-->
                        <select class="form-control" ng-model="newwage.employees_id" required>
                            <option value="">Select</option>
                            <option ng-repeat="employee in prjctemployees" value="{{employee.id}}">{{employee.name}}</option>
                        </select>
					</div>
					<label for="" class="col-sm-1 control-label">Amount</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" ng-model="newwage.amount" placeholder="Amount">
					</div>
					<label for="" class="col-sm-1 control-label">Date</label>
					<div class="col-sm-3">
									<div class="input-group">
										<input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newwage.payDate" is-open="wagepicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="wagepicker=true"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Description</label>
					<div class="col-sm-10">
						<textarea class="form-control" ng-model="newwage.description" placeholder="Description"></textarea>
					</div>
				</div>
				<div class="form-group">
				  <div class="col-sm-12 text-right">
					<button type="button" class="btn btn-default" ng-click="cancelWage();">Cancel</button>
					  <button type="submit" class="btn btn-primary">Save</button>
				  </div>
				</div>
				<hr>
			</form>
                        <button ng-if="user.permissions.project.write =='true'" ng-click="newWage();" ng-hide="wageedit" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Wage</button>
				<h3 ng-hide="projectedit">Wage and details</h3>
			      <div class="row">
                      <div class="form-group">
			            <div class="col-sm-4">
			                <label for="">Show
			                    <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
			                    entries
			                </label>
			            </div>
                      <div class="col-sm-3 text-center">
                          <div>
                              <button class="btn btn-sm btn-info" ng-click="exportToExcel('table1')">Export To Excel</button>
                          </div>
                      </div>
                          <div class="col-sm-5 text-right">
							  <div class="form-inline form-group">
							    <label for="filter-list">Search </label>
							    <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="wagefilter">
							  </div>
                          </div>
			            </div>
                  </div>
                        <div class="row">
                                <div class="col-sm-6 text-right">
                                    <div class="form-inline form-group">
                                       <label for="" class="control-label">From</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                                       </div>
                                    </div>
                                </div>
                            <div class="col-sm-6 text-left">
                                <div class="form-inline form-group">
                                    <label for="" class="control-label">To</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-warning" ng-click="searchWage(fromDate,toDate);">Search</button>
                            </div>

                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="text-primary">Wages and details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
				<table id="table1" class="table table-hover table-responsive">
					<thead>
						<tr>
                            <th>ID</th>
							<th>Name</th>
                            <th>Employee</th>
							<th>Description</th>
							<th>Amount</th>
                            <th>Advance</th>
							<th>Date</th>
                            <th ng-show="extra">Created</th>
                            <th ng-show="extra">Updated</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
								<tr ng-repeat="wage in listCount = (wages | filter:wagefilter) | orderBy:'-date' | pagination: currentPage : numPerPage ">
                                    <td>{{wage.id}}</td>
							<td>{{wage.name}}</td>
                                    <td><a ng-click="open(wage);">{{wage.employee.name}}</a></td>
							<td><p class="description" popover="{{wage.description}}" popover-trigger="mouseenter">{{wage.description}}</p></td>
							<td><i class="fa fa-inr"></i> {{wage.amount}}</td>
                                    <td><i class="fa fa-inr"></i> {{wage.advance}}</td>
									<td>{{wage.payDate | date:'dd-MMMM-yyyy'}}</td>
                                    <td ng-show="extra">{{wage.created_at}}</td>
                                    <td ng-show="extra">{{wage.updated_at}}</td>
							<td>
								<div ng-if="user.permissions.project.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
									<button type="button" class="btn btn-default" ng-click="editWage(wage);">
										<i class="fa fa-pencil"></i>
									</button>
									<button type="button" class="btn btn-default" ng-click="deleteWage(wage); editmode = !editmode">
										<i class="fa fa-trash-o"></i>
									</button>
								</div>
							</td>
						</tr>
                    <tr class="text-danger">
                        <td colspan="4"><h5>Total Wage:</h5></td><td><h5><i class="fa fa-inr"></i>{{totalwage}}/-</h5></td>
                    </tr>
					</tbody>
				</table>
                                    </div>
                                </div>
                            </div>
						<div class="clearfix">
					<pagination 
						ng-model="currentPage" 
								total-items="listCount.length" 
								max-size="maxSize" 
								items-per-page="numPerPage"
						boundary-links="true" 
						class="pagination-sm pull-right" 
						previous-text="&lsaquo;" 
						next-text="&rsaquo;" 
						first-text="&laquo;" 
						last-text="&raquo;"
					></pagination>
				</div>
			</div>
				</tab>
                <tab>
                    <tab-heading>Employees</tab-heading>
                    <div class="box">
                        <div class="row">
                        <div class="col-md-3">
                            <div class="box" style="margin-bottom: 0; border-bottom: none;">
                                <h4 class="pull-left">Leaders</h4>
                                <button ng-if="user.permissions.project.write =='true'" class="btn btn-xs btn-primary pull-right" ng-hide="projectedit" ng-click="newLeader();"><i class="fa fa-plus"></i> Add leader</button>
                                <div class="clearfix"></div>
                            </div>
                            <ul class="list-group project-list">
                                <li class="list-group-item" ng-repeat="leader in leaders" ng-class="{active: leader.id == curLeader.id}">
                                    <button class="pull-right btn btn-primary btn-xs" ng-click="openLeader(leader)"><i class="fa fa-arrow-right"></i></button>
                                    <button class="pull-right btn btn-primary btn-xs" ng-click="deleteLeader(leader)"><i class="fa fa-remove"></i></button>
                                    <p><strong>{{leader.employee.name}}</strong></p>
                                    <!--<ul class="list-unstyled">
                                        <li>Budget: <i class="fa fa-inr"></i> {{project.approxamount}}</li>
                                        <li>Close Date: {{project.enddate | date:'dd-MMM-yyyy'}}</li>
                                    </ul>-->
                                </li>
                            </ul>
                        </div>
                      <div ng-show="empedit" class="box col-sm-9">
                        <form class="form-horizontal" ng-submit="addLeader();">
                            <h3>New Leader</h3>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Employee Leader</label>
                                <div class="col-sm-8">
                                    <select class="form-control" ng-model="newleader.employee_id">
                                        <option ng-repeat="employee in employees" value="{{employee.id}}">{{employee.name}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 text-right">
                                    <button type="button" class="btn btn-default" ng-click="cancelLeader();">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                      </div>
                        <div ng-show="leaderactive" class="box col-sm-9">
                            <button ng-if="user.permissions.project.write =='true'" ng-click="newGroup();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Employee</button>
                            <form class="form-horizontal" ng-show="empaddedit" ng-submit="addGroup();">
                                <h3>New Employee</h3><br>
                                <label class="col-sm-3 control-label">Employee to Group</label>
                                <div class="col-sm-8">
                                    <select class="form-control" ng-model="newgroup.employee_id">
                                        <option ng-repeat="employee in employees" value="{{employee.id}}">{{employee.name}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <button type="button" class="btn btn-default" ng-click="cancelGroup();">Cancel</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                                <hr>
                            </form>
                            <div class="box col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <span class="text-primary">Employee Groups</span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                <table id="table2" class="table table-hover table-responsive">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                       <!-- <th>Created</th>-->
                                        <th>Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="group in listCount = (groups | filter:expensefilter) | pagination: currentPage : numPerPage ">
                                        <td>{{group.id}}</td>
                                        <td><a ng-click="openGroupEmployee(group);">{{group.employees.name}}</a></td>
                                        <td><p class="description" popover="{{group.employees.address}}" popover-trigger="mouseenter">{{group.employees.address}}</p></td>
                                       <!-- <td>{{group.created_at}}</td>-->
                                        <td>
                                            <div ng-if="user.permissions.project.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                <button type="button" class="btn btn-default" ng-click="deleteGroup(group); editmode = !editmode">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <div class="clearfix">
                                    <pagination
                                        ng-model="currentPage"
                                        total-items="listCount.length"
                                        max-size="maxSize"
                                        items-per-page="numPerPage"
                                        boundary-links="true"
                                        class="pagination-sm pull-right"
                                        previous-text="&lsaquo;"
                                        next-text="&rsaquo;"
                                        first-text="&laquo;"
                                        last-text="&raquo;"
                                        ></pagination>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                </tab>

				<tab>
					<tab-heading>Expenses</tab-heading>
					<div class="box">
						<button ng-if="user.permissions.project.write =='true'" ng-click="newExpense();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Expense</button>
						<form class="form-horizontal" ng-show="expenseedit" ng-submit="addExpense();">
							<h3>New Expense</h3><br>
							<div class="form-group">
								<label for="" class="col-sm-2 control-label">Name</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" ng-model="newexpense.name" placeholder="Name">
								</div>
								<label for="" class="col-sm-1 control-label">Amount</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" ng-model="newexpense.amount" placeholder="Amount">
								</div>
								<label for="" class="col-sm-1 control-label">Date</label>
								<div class="col-sm-3">
								<div class="input-group">
									<input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newexpense.payDate" is-open="expensepicker" show-button-bar="false" show-weeks="false" readonly>
									<span class="input-group-btn">
										<button type="button" class="btn btn-default" ng-click="expensepicker=true"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Description</label>
								<div class="col-sm-10">
									<textarea class="form-control" ng-model="newexpense.description" placeholder="Description"></textarea>
								</div>
							</div>
							<div class="form-group">
							  <div class="col-sm-12 text-right">
							  	<button type="button" class="btn btn-default" ng-click="cancelExpense();">Cancel</button>
								  <button type="submit" class="btn btn-primary">Save</button>
							  </div>
							</div>
							<hr>
						</form>
						<h3 ng-hide="expenseedit">Expense and details</h3>
			      <div class="row">
			        <div class="col-md-4">
			          <label for="">Show 
			            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
			            entries
			          </label>
			        </div>
                      <div class="col-sm-3 text-center">
                          <div>
                              <button class="btn btn-sm btn-info" ng-click="exportToExcel('projectExpense')">Export To Excel</button>
                          </div>
                      </div>
							<div class="col-md-5 text-right">
							  <div class="form-inline form-group">
							    <label for="filter-list">Search </label>
							    <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="expensefilter">
							  </div>
							</div>
			      </div>
                        <div class="row">
                            <div class="col-sm-6 text-right">
                                <div class="form-inline form-group">
                                    <label for="" class="control-label">From</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-left">
                                <div class="form-inline form-group">
                                    <label for="" class="control-label">To</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-warning" ng-click="searchExpense(fromDate,toDate);">Search</button>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="text-primary">Expense and details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
						<table id="projectExpense" class="table table-hover table-responsive">
							<thead>
								<tr>
                                    <th>ID</th>
									<th>Name</th>
									<th>Amount</th>
									<th>Date</th>
									<th>Description</th>
                                    <th ng-show="extra">User</th>
                                    <th ng-show="extra">Created</th>
                                    <th ng-show="extra">Updated</th>
									<th>Edit</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="expense in listCount = (expenses | filter:expensefilter) | orderBy:'-date' | pagination: currentPage : numPerPage ">
                                    <td>{{expense.id}}</td>
									<td>{{expense.name}}</td>
									<td><i class="fa fa-inr"></i> {{expense.amount}}</td>
									<td>{{expense.payDate | date:'dd-MMMM-yyyy'}}</td>
									<td><p class="description" popover="{{expense.description}}" popover-trigger="mouseenter">{{expense.description}}</p></td>
                                    <td ng-show="extra">{{expense.user}}</td>
                                    <td ng-show="extra">{{expense.created_at}}</td>
                                    <td ng-show="extra">{{expense.updated_at}}</td>
									<td>
										<div ng-if="user.permissions.project.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
											<button type="button" class="btn btn-default" ng-click="editExpense(expense);">
												<i class="fa fa-pencil"></i>
											</button>
											<button type="button" class="btn btn-default" ng-click="deleteExpense(expense); editmode = !editmode">
												<i class="fa fa-trash-o"></i>
											</button>
										</div>
									</td>
								</tr>
                                <tr class="text-danger">
                                    <td colspan="2"><h5>Total Expense:</h5></td><td><h5><i class="fa fa-inr"></i>{{totalexpense}}/-</h5></td>
                                </tr>
							</tbody>
						</table>
                                    </div>
                                </div>
                            </div>
						<div class="clearfix">
							<pagination 
								ng-model="currentPage" 
								total-items="listCount.length" 
								max-size="maxSize" 
								items-per-page="numPerPage" 
								boundary-links="true" 
								class="pagination-sm pull-right" 
								previous-text="&lsaquo;" 
								next-text="&rsaquo;" 
								first-text="&laquo;" 
								last-text="&raquo;"
							></pagination>
						</div>
					</div>
				</tab>

                <tab>
                    <tab-heading>Incomes</tab-heading>
                    <div class="box">
                        <button ng-if="user.permissions.project.write =='true'" ng-click="newIncome();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Income</button>
                        <form class="form-horizontal" ng-show="incomeedit" ng-submit="addIncome();">
                            <h3>New Income</h3><br>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" ng-model="newincome.name" placeholder="Name">
                                </div>
                                <label for="" class="col-sm-1 control-label">Amount</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" ng-model="newincome.amount" placeholder="Amount">
                                </div>
                                <label for="" class="col-sm-1 control-label">Date</label>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newincome.date" is-open="incomepicker" show-button-bar="false" show-weeks="false" readonly>
									<span class="input-group-btn">
										<button type="button" class="btn btn-default" ng-click="incomepicker=true"><i class="fa fa-calendar"></i></button>
									</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" ng-model="newincome.description" placeholder="Description"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 text-right">
                                    <button type="button" class="btn btn-default" ng-click="cancelIncome();">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                            <hr>
                        </form>
                        <h3 ng-hide="expenseedit">Income and details</h3>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Show
                                    <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                    entries
                                </label>
                            </div>
                            <div class="col-sm-3 text-center">
                                <div>
                                    <button class="btn btn-sm btn-info" ng-click="exportToExcel('projectIncome')">Export To Excel</button>
                                </div>
                            </div>
                            <div class="col-md-5 text-right">
                                <div class="form-inline form-group">
                                    <label for="filter-list">Search </label>
                                    <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="incomefilter">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 text-right">
                                <div class="form-inline form-group">
                                    <label for="" class="control-label">From</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-left">
                                <div class="form-inline form-group">
                                    <label for="" class="control-label">To</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-warning" ng-click="searchIncome(fromDate,toDate);">Search</button>
                            </div>

                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="text-primary">Incomes and details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                        <table id="projectIncome" class="table table-hover table-responsive">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th ng-show="extra">User</th>
                                <th ng-show="extra">Created</th>
                                <th ng-show="extra">Updated</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="income in listCount = (incomes | filter:incomefilter) | orderBy:'-date' | pagination: currentPage : numPerPage ">
                                <td>{{income.id}}</td>
                                <td><i class="fa fa-inr"></i> {{income.name}}</td>
                                <td>{{income.amount}}</td>
                                <td>{{income.date | date:'dd-MMMM-yyyy'}}</td>
                                <td><p class="description" popover="{{income.description}}" popover-trigger="mouseenter">{{income.description}}</p></td>
                                <td ng-show="extra">{{income.user}}</td>
                                <td ng-show="extra">{{income.created_at}}</td>
                                <td ng-show="extra">{{income.updated_at}}</td>
                                <td>
                                    <div ng-if="user.permissions.project.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                        <button type="button" class="btn btn-default" ng-click="editIncome(income);">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="button" class="btn btn-default" ng-click="deleteIncome(income); editmode = !editmode">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <tr class="text-danger">
                                <td colspan="2"><h5>Total Income:</h5></td><td><h5><i class="fa fa-inr"></i>{{totalincome}}/-</h5></td>
                            </tr>
                            </tbody>
                        </table>
                                    </div>
                                </div>
                            </div>
                        <div class="clearfix">
                            <pagination
                                ng-model="currentPage"
                                total-items="listCount.length"
                                max-size="maxSize"
                                items-per-page="numPerPage"
                                boundary-links="true"
                                class="pagination-sm pull-right"
                                previous-text="&lsaquo;"
                                next-text="&rsaquo;"
                                first-text="&laquo;"
                                last-text="&raquo;"
                                ></pagination>
                        </div>
                    </div>
                </tab>
                <tab>
                    <tab-heading ng-click="getProjectStatus(curProject.id)">Project Status</tab-heading>
                    <div class="box">
                        <div class="row">
                            <div class="col-lg-6 text-left text-primary">
                                Project Expense: <i class="fa fa-inr"></i> {{projectExpense}}
                            </div>
                            <div class="col-lg-6 text-right text-success">
                                Project Income: <i class="fa fa-inr"></i> {{projectIncome}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 text-center text-danger">
                                Project Profit: <i class="fa fa-inr"></i> {{projectProfit}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Real Time Traffic
                                    </div>
                                    <div class="demo-container">
                                        <canvas id="chart"
                                            data-ac-chart="'bar'"
                                            data-ac-data="data"
                                            data-ac-config="config"
                                            height="100px" width="100px">
                                        </canvas>
                                    </div>

                                </div>
                        </div>
                    </div>
                </tab>
			</tabset>

		</div>
	</div>
</div>