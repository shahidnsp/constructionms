<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Projects Documents </h2>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.projectdocument.write =='true'" ng-click="newDocument();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Document</button>
            <form class="form-horizontal" ng-show="documentedit" ng-submit="addDocument();">
                <h3>New Document</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" ng-model="newdocument.name" placeholder="Name" required>
                    </div>
                    <label for="" class="col-sm-1 control-label">Date</label>
                    <div class="col-sm-5">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newdocument.date" is-open="documentpicker" show-button-bar="false" show-weeks="false" readonly>
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="documentpicker=true"><i class="fa fa-calendar"></i></button>
              </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Description</label>
                    <div class="col-sm-11">
                        <textarea class="form-control" ng-model="newdocument.description" placeholder="Description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Project</label>
                    <div class="col-sm-5">
                       <select class="form-control" ng-model="newdocument.projects_id" required>
                           <option value="">Select</option>
                           <option ng-repeat="project in projects" value="{{project.id}}">{{project.name}}</option>
                       </select>
                    </div>
                    <label class="col-sm-1 control-label">Document</label>
                    <div class="col-sm-5">
                       <input type="file" name="file" onchange="angular.element(this).scope().getFileDetails(this)" class="form-control" multiple>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelDocument();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="documentedit">Documents and details</h3>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-md-6 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 text-right">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">From</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">To</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                        <button class="btn btn-warning" ng-click="searchDocumentDate(fromDate,toDate)">Search</button>
                    </div>
                </div>

            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="text-success">Document and details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="documentTable" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>SlNo</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Project</th>
                                <th>Date</th>
                                <th ng-show="extra">Created</th>
                                <th ng-show="extra">Updated</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="document in listCount  = (documents | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                <td>{{document.id}}</td>
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td>{{document.name}}</td>
                                <td><p class="description" popover="{{document.description}}" popover-trigger="mouseenter">{{document.description}}</p></td>
                                <td>{{document.project.name}}</td>
                                <td>{{document.date | date:'dd-MMMM-yyyy'}}</td>
                                <td ng-show="extra">{{document.created_at}}</td>
                                <td ng-show="extra">{{document.updated_at}}</td>
                                <td>
                                    <div ng-if="user.permissions.projectdocument.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                        <button type="button" class="btn btn-default" ng-click="editDocument(document);">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="button" class="btn btn-default" ng-click="deleteDocument(document); editmode = !editmode">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix" ng-show="documents.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>