<div class="row">
  <div class="col-md-12">
    <h2><i class="fa fa-building-o"> </i> Rent</h2>
  </div>
</div>
<div class="box">
    <p class="text-primary"> Office Rent entering section</p>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <button ng-if="user.permissions.rent.write =='true'" ng-click="newRent();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Rent</button>
      <form class="form-horizontal" ng-show="rentedit" ng-submit="addRent();">
        <h3>New Rent</h3><br>
        <div class="form-group">
          <label for="" class="col-sm-1 control-label">Amount</label>
          <div class="col-sm-5">
            <input type="text" class="form-control" ng-model="newrent.dramount" placeholder="Amount" required>
          </div>
          <label for="" class="col-sm-1 control-label">Date</label>
          <div class="col-sm-5">
            <div class="input-group">
              <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newrent.date" is-open="rentpicker" show-button-bar="false" show-weeks="false" readonly>
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="rentpicker=true"><i class="fa fa-calendar"></i></button>
              </span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-1 control-label">Description</label>
          <div class="col-sm-11">
            <textarea class="form-control" ng-model="newrent.note" placeholder="Description"></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12 text-right">
            <button type="button" class="btn btn-default" ng-click="cancelRent();">Cancel</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
        <hr>
      </form>
      <h3 ng-hide="rentedit">Rents and details</h3>
        <div class="row">
            <div class="col-md-4">
                <label for="">Show
                    <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                    entries
                </label>
            </div>
            <div class="col-sm-3 text-center">
                <div>
                    <button class="btn btn-info btn-sm" ng-click="exportToExcel('rentTable');">Export to Excel</button>
                </div>
            </div>
            <div class="col-md-5 text-right">
                <div class="form-inline form-group">
                    <label for="filter-list">Search </label>
                    <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5 text-right">
                <div class="form-inline form-group">
                    <label for="" class="control-label">From</label>
                    <div class="input-group">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-inline form-group">
                    <label for="" class="control-label">To</label>
                    <div class="input-group">
                        <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                    </div>
                    <button class="btn btn-warning" ng-click="searchRentDate(fromDate,toDate)">Search</button>
                </div>
            </div>

        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="text-success">Rent and details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="rentTable" class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
              <th>#</th>
              <th>SlNo</th>
            <th>Amount</th>
            <th>Date</th>
            <th>Description</th>
              <!--<th ng-show="extra">User</th>-->
              <th ng-show="extra">Created</th>
              <th ng-show="extra">Updated</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="rent in listCount  = (rents | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
              <td>{{rent.id}}</td>
              <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
            <td><i class="fa fa-inr"></i> {{rent.dramount}}</td>
            <td>{{rent.date | date:'dd-MMMM-yyyy'}}</td>
            <td><p class="description" popover="{{rent.note}}" popover-trigger="mouseenter">{{rent.note}}</p></td>
           <!--   <td ng-show="extra">{{rent.user}}</td>-->
              <td ng-show="extra">{{rent.created_at}}</td>
              <td ng-show="extra">{{rent.updated_at}}</td>
            <td>
              <div ng-if="user.permissions.rent.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                <button type="button" class="btn btn-default" ng-click="editRent(rent);">
                  <i class="fa fa-pencil"></i>
                </button>
                <button type="button" class="btn btn-default" ng-click="deleteRent(rent); editmode = !editmode">
                  <i class="fa fa-trash-o"></i>
                </button>
              </div>
            </td>
          </tr>
        <tr class="text-danger"><td colspan="2">Total</td><td style="text-align: center"><i class="fa fa-inr"></i> {{totalrent}}</td></tr>
        </tbody>
      </table>
                    </div>
                </div>
            </div>
      <div class="clearfix" ng-show="rents.length > numPerPage">
        <pagination 
          ng-model="currentPage" 
          total-items="listCount.length" 
          max-size="maxSize" 
          items-per-page="numPerPage"
          boundary-links="true" 
          class="pagination-sm pull-right" 
          previous-text="&lsaquo;" 
          next-text="&rsaquo;" 
          first-text="&laquo;" 
          last-text="&raquo;"
        ></pagination>
      </div>
    </div>
  </div>
</div>