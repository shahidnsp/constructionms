<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-money"></i> Employee Salary</h2>
    </div>
</div>
<div class="row">
<tabset class="col-md-12">
<tab>
<tab-heading>Staff Salary Payment</tab-heading>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <button ng-if="user.permissions.salary.write =='true'" class="btn btn-primary pull-right" type="button" ng-click="newEmpSalary()"><i class="fa fa-plus"></i> Add Salary</button>
                <form class="form-horizontal" ng-show="empsalaryedit" ng-submit="addEmpSalary()">
                    <h3>Salary</h3>
                    <div class="form-group">
                        <label for="" class="col-sm-1 control-label">Cash/Bank</label>
                        <div class="col-sm-11">
                            <select class="form-control" ng-model="newempsalary.ledgers_id" required="">
                                <option ng-repeat="item in cashLedgers" value="{{item.id}}">{{item.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-1 control-label">Employee</label>
                        <div class="col-sm-5">
                            <select class="form-control" ng-model="newempsalary.employees_id" ng-change="getSalary(newempsalary.employees_id);" required="">
                                <option selected value="">Select Employee</option>
                                <option ng-repeat="item in employees" value="{{item.id}}">{{item.name}}</option>
                            </select>
                        </div>
                        <label for="" class="col-sm-1 control-label">Date</label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newempsalary.date" is-open="wagepicker" show-button-bar="false" show-weeks="false" readonly required="">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="wagepicker=true"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-sm-1">Basic</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" placeholder="Basic Salary" ng-change="totalSalary()" ng-model="newempsalary.basic"/>
                        </div>
                        <label for="" class="control-label col-sm-1">TA</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" placeholder="Travelling allowance" ng-change="totalSalary()" ng-model="newempsalary.ta"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-sm-1">DA</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" placeholder="Dearness allowance" ng-change="totalSalary()" ng-model="newempsalary.da"/>
                        </div>
                        <label for="" class="control-label col-sm-1">HRA</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" placeholder="House Rent Allowance" ng-change="totalSalary()" ng-model="newempsalary.hra"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-sm-1">Medical</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" placeholder="Medical Allowance" ng-change="totalSalary()" ng-model="newempsalary.medical"/>
                        </div>
                        <label for="" class="control-label col-sm-1">Pre Paid</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" placeholder="Advance" ng-change="totalSalary()" ng-model="newempsalary.advance" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-sm-1">Total</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" placeholder="Total" ng-model="newempsalary.amount" disabled />
                        </div>
                        <label for="" class="control-label col-sm-2">Total-PrePaid</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="Total" ng-model="newempsalary.total" disabled />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-sm-1">Pre.Balance</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Previous Balance" ng-model="newempsalary.prebalance" />
                        </div>
                        <label for="" class="control-label col-sm-1">Paid</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control text-danger" placeholder="Paid Amount" ng-model="newempsalary.paid" ng-change="changePaid();"/>
                        </div>
                        <label for="" class="control-label col-sm-1">Balance</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" placeholder="Balance" ng-model="newempsalary.balance" ng-change="changeBalance();"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label col-sm-1">Description</label>
                        <div class="col-sm-11">
                           <textarea class="form-control" ng-model="newempsalary.description" placeholder="Description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button type="button" class="btn btn-default" ng-click="cancelEmployee();">Cancel</button>
                            <button type="submit" class="btn btn-primary" >Save</button>
                        </div>
                    </div>
                </form>
                <h3>Salary details</h3>
                <div class="row">
                    <div class="col-md-4">
                        <label for="">Show
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="item for item in numsForPage"></select>
                            Entries
                        </label>
                    </div>
                    <div class="col-sm-3 text-center">
                        <div>
                            <button class="btn btn-sm btn-info" ng-click="exportToExcel('salaryTable')">Export To Excel</button>
                        </div>
                    </div>
                    <div class="col-md-5 text-right">
                        <div class="form-inline form-group">
                            <label for="filter-list">Search</label>
                            <input type="text" class="form-control" placeholder="Search" ng-model="filterlist"/>
                        </div>
                    </div>
                </div>
                <div class="row form-horizontal">
                    <div class="form-group">
                        <label for="" class="control-label col-sm-1">Employee</label>
                        <div class="col-sm-3">
                            <select class="form-control" ng-model="employees_id"  required="" ng-change="getEmployeeJoinDate(employees_id);">
                                <option selected value="0">All</option>
                                <option ng-repeat="item in employees" value="{{item.id}}">{{item.name}}</option>
                            </select>
                        </div>
                        <label for="" class="control-label col-sm-1">From</label>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                            </div>
                        </div>
                        <label for="" class="control-label col-sm-1">To</label>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-warning" ng-click="searchSalary(fromDate,toDate);">Search</button>
                        </div>

                    </div>

                </div>
                <div class="box">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="text-success">Salary and Details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="salaryTable" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>SlNo</th>
                                    <th>Date</th>
                                    <th>Employee</th>
                                    <th>Paid</th>
                                    <th>Balance</th>
                                    <th>Total</th>
                                    <th ng-show="extra">User</th>
                                    <th ng-show="extra">Created</th>
                                    <th ng-show="extra">Updated</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="salary in listCount = (empsalaries | filter:filterlist) | orderBy:'-date'  | pagination:currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{salary.date | date:'dd-MM-yyyy'}}</td>
                                    <td><a ng-click="open(salary);">{{salary.employee.name}}</a></td>
                                    <td><i class="fa fa-inr"></i> {{salary.paid}}</td>
                                    <td ng-class="salary.balance<0 ? 'text-success' : 'text-danger'"><i class="fa fa-inr"></i> {{salary.balance}}</td>
                                    <td><i class="fa fa-inr"></i> {{salary.total}}</td>
                                    <td ng-show="extra">{{salary.user}}</td>
                                    <td ng-show="extra">{{salary.created_at}}</td>
                                    <td ng-show="extra">{{salary.updated_at}}</td>
                                    <td>
                                        <div ng-if="user.permissions.salary.edit =='true'" class="btn-group btn-group-xs" role="group">
                                            <!--<button type="button" class="btn btn-default" ng-click="editEmpSalary(salary)"><i class="fa fa-pencil"></i></button>-->
                                            <button type="button" class="btn btn-default" ng-click="deleteEmployee(salary)"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="text-danger">
                                    <td colspan="3">Total</td><td><i class="fa fa-inr"> {{totalPaid}}</td><td><i class="fa fa-inr"> {{totalBalance}}</td><td><i class="fa fa-inr"> {{totalTotal}}</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <div class="clearfix">
                            <pagination
                                ng-model="currentPage"
                                total-items="listCount.length"
                                max-size="maxSize"
                                items-per-page="numPerPage"
                                boundary-links="true"
                                class="pagination-sm pull-right"
                                previous-text="&lsaquo;"
                                next-text="&rsaquo;"
                                first-text="&laquo;"
                                last-text="&raquo;"
                                ></pagination>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </tab>
        <tab>
            <tab-heading>Salary Balance List</tab-heading>
            <div class="box">
                <h3>Salary Balance List</h3>
                <div class="row">
                    <div class="col-md-4">
                        <label for="">Show
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="item for item in numsForPage"></select>
                            Entries
                        </label>
                    </div>
                    <div class="col-sm-3 text-center">
                        <div>
                            <button class="btn btn-sm btn-info" ng-click="exportToExcel('salaryBalanceTable')">Export To Excel</button>
                        </div>
                    </div>
                    <div class="col-md-5 text-right">
                        <div class="form-inline form-group">
                            <label for="filter-list">Search</label>
                            <input type="text" class="form-control" placeholder="Search" ng-model="filterlists"/>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="text-success">Salary Balance and Details</span>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="salaryBalanceTable" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SlNo</th>
                                    <th>Employee</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="list in listCount = (lists | filter:filterlists) | pagination:currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td><a ng-click="open(list);">{{list.name}}</a></td>
                                    <td style="text-align: center"><i class="fa fa-inr"></i> {{list.balance}}</td>
                                </tr>
                                <tr class="text-danger">
                                    <td colspan="2">Total</td><td style="text-align: center"><i class="fa fa-inr"> {{totalPreBalance}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix">
                            <pagination
                                ng-model="currentPage"
                                total-items="listCount.length"
                                max-size="maxSize"
                                items-per-page="numPerPage"
                                boundary-links="true"
                                class="pagination-sm pull-right"
                                previous-text="&lsaquo;"
                                next-text="&rsaquo;"
                                first-text="&laquo;"
                                last-text="&raquo;"
                                ></pagination>
                        </div>
                    </div>
        </tab>
        </tabset>
</div>