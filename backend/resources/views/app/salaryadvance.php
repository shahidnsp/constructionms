<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-credit-card"></i> Salary advance</h2>
    </div>
</div>
<div class="row">
    <tabset class="col-md-12">
        <tab>
            <tab-heading>Salary Advance</tab-heading>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <button ng-if="user.permissions.salaryadvance.write =='true'" class="btn btn-primary pull-right" type="button" ng-click="newSalaryAdv()"><i class="fa fa-plus"></i> Salary Advance</button>
                <form class="form-horizontal" ng-show="salaryadvedit" ng-submit="addSalaryAdv()">
                       <h3>Salary advance</h3>
                                <div class="form-group">
                                    <label for="" class="col-sm-1 control-label">Employee</label>
                                    <div class="col-sm-5">
                                        <select class="form-control" ng-model="newsalaryadv.employees_id" ng-change="getAdvance(newsalaryadv.employees_id);">
                                            <option ng-repeat="employee in employees" value="{{employee.id}}">{{employee.name}}</option>
                                        </select>
                                    </div>
                                    <label for="" class="col-sm-1 control-label">Date</label>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newsalaryadv.date" is-open="wagepicker" show-button-bar="false" show-weeks="false" readonly>
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default" ng-click="wagepicker=true"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-1 control-label">Amount</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="Amount" ng-model="newsalaryadv.amount" />
                                    </div>
                                    <label for="" class="col-sm-4 control-label">Pre.Advance Amount:<span class="text-danger"> {{previous}}</span></label>


                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-1 control-label">Description</label>
                                    <div class="col-sm-11">
                                        <textarea class="form-control" placeholder="Description" ng-model="newsalaryadv.description" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <button type="button" class="btn btn-default" ng-click="cancelSalaryAdv()">Cancel</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                </form>


                <h3>Advance Salary Details</h3>
                <div class="row">
                    <div class="col-md-4">
                        <label for="">Show
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="item for item in numsForPage"></select>
                            Entries
                        </label>
                    </div>
                    <div class="col-sm-3 text-center">
                        <div>
                            <button class="btn btn-sm btn-info" ng-click="exportToExcel('salaryAdvanceTable')">Export To Excel</button>
                        </div>
                    </div>
                    <div class="col-md-5 text-right">
                        <div class="form-inline form-group">
                            <label for="filter-list">Search</label>
                            <input type="text" class="form-control" placeholder="Search" ng-model="filterlist"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5 text-right">
                        <div class="form-inline form-group">
                            <label for="" class="control-label">From</label>
                            <div class="input-group">
                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 text-left">
                        <div class="form-inline form-group">
                            <label for="" class="control-label">To</label>
                            <div class="input-group">
                                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                            </div>
                            <button class="btn btn-warning" ng-click="searchAdvance(fromDate,toDate);">Search</button>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="text-success">Salary Advance and details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                        </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="table-responsive">
                                <table id="salaryAdvanceTable" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Slno</th>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                            <th>Description</th>
                                            <th ng-show="extra">User</th>
                                            <th ng-show="extra">Created at</th>
                                            <th ng-show="extra">Updated at</th>
                                            <th>Edit</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr  ng-repeat="salaryadv in listCount = (salaryadvances | filter:filterlist) | orderBy:'-created_at' | pagination:currentPage : numPerPage">
                                            <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                            <td><a ng-click="open(salaryadv);">{{salaryadv.employee.name}}</a></td>
                                            <td>{{salaryadv.date | date:'dd-MMMM-yyyy'}}</td>
                                            <td><i class="fa fa-inr"></i> {{salaryadv.amount}}</td>
                                            <td><p class="description" popover="{{salaryadv.description}}" popover-trigger="mouseenter">{{salaryadv.description}}</p></td>
                                            <td ng-show="extra">{{salaryadv.user}}</td>
                                            <td ng-show="extra">{{salaryadv.created_at}}</td>
                                            <td ng-show="extra">{{salaryadv.updated_at}}</td>
                                            <td>
                                                <div ng-if="user.permissions.salaryadvance.edit =='true'" class="btn-group btn-group-xs" role="group">
                                                    <button type="button" class="btn btn-default" ng-click="editSalaryAdv(salaryadv)"><i class="fa fa-pencil"></i></button>
                                                    <button type="button" class="btn btn-default" ng-click="deleteSalaryAdv(salaryadv)"><i class="fa fa-trash-o"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="text-danger">
                                            <td colspan="3">Total</td><td><i class="fa fa-inr"></i> {{totalAdvance}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                        </div>
                    <div class="clearfix" ng-show="salaryadvances.length > numPerPage">
                        <pagination
                            ng-model="currentPage"
                            total-items="listCount.length"
                            max-size="maxSize"
                            items-per-page="numPerPage"
                            boundary-links="true"
                            class="pagination-sm pull-right"
                            previous-text="&lsaquo;"
                            next-text="&rsaquo;"
                            first-text="&laquo;"
                            last-text="&raquo;"
                            ></pagination>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </tab>
        <tab>
            <tab-heading>Salary Advance List</tab-heading>
            <h3>Salary Advance List</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="item for item in numsForPage"></select>
                        Entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-sm btn-info" ng-click="exportToExcel('salaryAdvanceListTable')">Export To Excel</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search</label>
                        <input type="text" class="form-control" placeholder="Search" ng-model="filterlists"/>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="text-success">Salary Advance and details</span>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="table-responsive">
                                <table id="salaryAdvanceListTable" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Slno</th>
                                        <th>Name</th>
                                        <th>Balance</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr   ng-repeat="list in listCount = (lists | filter:filterlists)  | pagination:currentPage : numPerPage">
                                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                        <td><a ng-click="open(list);">{{list.name}}</a></td>
                                        <td style="text-align: center"><i class="fa fa-inr"></i> {{list.balance}}</td>
                                    </tr>
                                    <tr class="text-danger">
                                        <td  colspan="2">Total</td><td style="text-align: center"><i class="fa fa-inr"></i> {{totalBalance}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix" ng-show="lists.length > numPerPage">
                    <pagination
                        ng-model="currentPage"
                        total-items="listCount.length"
                        max-size="maxSize"
                        items-per-page="numPerPage"
                        boundary-links="true"
                        class="pagination-sm pull-right"
                        previous-text="&lsaquo;"
                        next-text="&rsaquo;"
                        first-text="&laquo;"
                        last-text="&raquo;"
                        ></pagination>
                </div>
            </div>
        </tab>
       </tabset>
</div>
