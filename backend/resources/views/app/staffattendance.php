    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6 text-left">
                <h2><i class="fa fa-building-o"> </i> Staff Attendance Register </h2>
            </div>
            <div class="col-sm-3 text-right">
                <!--<div class="" ng-show="showreport" ng-model="curdate">
                    <select name="" id="" class="form-control">
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div>-->
                <div class="input-group">
                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="curdate" is-open="reportpicker" show-button-bar="false" show-weeks="false" readonly required="" placeholder="{{curdate | date:'dd-MMMM-yyyy'}}">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default" ng-click="reportpicker=true"><i class="fa fa-calendar" ></i></button>
                    </span>
                </div>

            </div>
            <div class="col-md-3 text-left">
                <button class="btn btn-info" ng-click="loadAttendance(curdate)" type="button">Report</button>
                <button class="btn btn-info" ng-click="cancelReport()" type="button" ng-show="showreport">Close</button>
            </div>
        </div>
    </div>
    <br>
    <div class="row" ng-show="showreport">
        <div class="col-md-12 text-center">
            <h3>Attendance Report</h3>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-1">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-md-4">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlistreport">
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <th>Name</th>
<!--                        <th ng-repeat="(key,i ) in getNumber(number) track by $index" ng-init="5" ng-data="{{key}}">{{$index+1}}</th>-->
                        <th ng-repeat="day in days">{{$index+startfrom}}</th>
                        </thead>
                        <tbody>
<!--                        <tr ng-repeat="(key,employee) in emprecords">-->
                        <tr ng-repeat="employee in listcount = (emprecords | filter:filterlistreport) | orderBy:'name' | pagination:currentPage : numPerPage">
                            <td>{{employee.name}}</td>
<!--                            <td ng-repeat="attendance in employee.attendance track by $index" ng-class="{'absent': attendance.present == 0}" ng-init="employee.day == $index+1 ? $index+1 : ''">{{attendance.present === '1' ? 'P ' : 'A '}}</td>-->
                            <td ng-repeat="a in temp track by $index" ng-class="{'absent' : employee.attendance[$index].present == '0', 'danger' : employee.attendance[$index].present == '2'}">{{employee.attendance[$index].present == '1' ? 'P' : employee.attendance[$index].present == '0' ? 'A' : ''}}</td>
<!--                            <td ng-repeat="a in temp" >{{$index}}</td>-->
<!--                            <td  ng-repeat="(key2,i2 ) in getNumber(number) track by $index" ng-data="{{key2}}" >{{(($index + 1) == employee.attendance[$index].day && employee.attendance[$index].present == "1") ? 'p' : 'a'}} </td>-->
<!--                            <td ng-repeat="attendance in employee.attendance track by $index">{{$index+1}}</td>-->
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix">
                    <pagination
                        ng-model="currentPage"
                        total-items="emprecords.length"
                        max-size="maxSize"
                        items-per-page="numPerPage"
                        boundary-links="true"
                        class="pagination-sm pull-right"
                        previous-text="&lsaquo;"
                        next-text="&rsaquo;"
                        first-text="&laquo;"
                        last-text="&raquo;"
                        ></pagination>
                </div>
            </div>
        </div>
    </div>
    <div class="row" ng-show="showaddform" >
        <div class="col-md-12">
            <div class="box">
                <form ng-submit="newAttendance()" name="addform">
                    <div class="row">
                        <div class="col-md-1">
                            <label for="">Show
                                <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                entries
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-8 text-right">
                                <div class="input-group">
                                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="curdate" is-open="wagepicker" show-button-bar="false" show-weeks="false" readonly required="" ng-disabled="true">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" ng-click="wagepicker=true"><i class="fa fa-calendar" ng-disabled="true"></i></button>
                                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <div class="col-md-3 text-center">
                            <button class="btn btn-primary" type="submit" ng-disabled="isDisabled" >Submit</button>
                            <button class="btn btn-danger" type="button" ng-click="editAttendance(curdate)">edit</button>
                        </div>
                    </div>
                    <div class="table-responsive" >
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <td>SlNo</td>
                                    <td>Employee Name</td>
                                    <td>Attendance</td>
                                    <td>Note</td>
                                </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="employee in listcount = (employees | filter:filterlist) | orderBy:'name' | pagination:currentPage : numPerPage ">
                                <td>{{employee.id}}</td>
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td>{{employee.name}}</td>
                                <td><input type="checkbox" ng-model="employee.present" ng-true-value="1" ng-false-value="0" ng-checked="employee.present == 1" ng-disabled="isDisabled" /></td>
                                <td><input type="text" name="note" class="form-control" ng-model="employee.note" ng-disabled="isDisabled" /></td>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix">
                        <pagination
                            ng-model="currentPage"
                            total-items="attendances.length"
                            max-size="maxSize"
                            items-per-page="numPerPage"
                            boundary-links="true"
                            class="pagination-sm pull-right"
                            previous-text="&lsaquo;"
                            next-text="&rsaquo;"
                            first-text="&laquo;"
                            last-text="&raquo;"
                            ></pagination>
                    </div>
               <form/>
                </div>
        </div>
    </div>

    <div class="row" ng-show="showeditform" >
        <div class="box">
            <div class="col-md-12 text-center">
                <h3>Edit Register</h3>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box">
                <form name="editform" >
                    <div class="row">
                        <div class="col-md-1">
                            <label for="">Show
                                <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                entries
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-8 text-right">
                                <div class="input-group">
                                    <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="curdate" is-open="wagepicker" show-button-bar="false" show-weeks="false" readonly required="" ng-change="editAttendance(curdate)">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" ng-click="wagepicker=true"><i class="fa fa-calendar" ></i></button>
                                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <div class="col-md-3 text-center">
                            <button class="btn btn-primary" type="button" ng-click="updateAttendance(curattendance)">Update</button>
                            <button class="btn btn-danger" type="button" ng-click="cancelEdit()">Cancel</button>
                        </div>
                    </div>
                    <div class="table-responsive" >
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <td>SlNo</td>
                                <td>Employee Name</td>
                                <td>Attendance</td>
                                <td>Note</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="employee in listcount = (curattendance | filter:filterlist) | orderBy:'name' | pagination:currentPage : numPerPage ">
                                <td>{{employee.id}}</td>
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
<!--                                <td>{{employee.employee.name}}</td>-->
                                <td>{{employee.employees_id}}</td>
<!--                                <td><input type="checkbox" ng-model="employee.present" ng-true-value="1" ng-false-value="0" ng-checked="employee.present == 1" /></td>-->
                                <td>
                                    <select class="form-control"  ng-model="employee.present" ng-class="{'absent': employee.present == 0}">
                                        <option value="1">present</option>
                                        <option value="0">absent</option>
                                    </select>
                                </td>
                                <td><input type="text" name="note" class="form-control" ng-model="employee.note" /></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix">
                        <pagination
                            ng-model="currentPage"
                            total-items="curattendance.length"
                            max-size="maxSize"
                            items-per-page="numPerPage"
                            boundary-links="true"
                            class="pagination-sm pull-right"
                            previous-text="&lsaquo;"
                            next-text="&rsaquo;"
                            first-text="&laquo;"
                            last-text="&raquo;"
                            ></pagination>
                    </div>
                    <form/>
            </div>
        </div>
    </div>