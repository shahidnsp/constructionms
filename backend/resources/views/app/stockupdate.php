<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Row Materials </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <br>
            <h3 ng-hide="rentedit">Row Materials and details</h3>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>

                <div class="col-md-6 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Row Materials and Details
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tableRow">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Photo</th>
                                    <th>Qty</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="row in listCount  = (products | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td><a ng-click="open(row)">{{row.name}}</a></td>
                                    <td><p class="description" popover="{{row.description}}" popover-trigger="mouseenter">{{row.description}}</p></td>
                                    <td><img id="image" ng-src="{{row.photo}}" ng-click="viewPhoto(row);" width="60px" height="60px"></td>
                                    <td><input onkeypress="return isNumberKey(event)" class="form-control" ng-model="row.qty"></td>
                                    <td><button ng-click="updateItem(row)" class=" btn btn-primary btn-sm">Update</button></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

            <div class="clearfix" ng-show="products.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
