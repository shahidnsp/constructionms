<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Sundry Creditor's List </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <label for="">Show
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-sm-3 text-center">
        <div>
            <button class="btn btn-sm btn-info" ng-click="exportToExcel('creditorTable')">Export To Excel</button>
        </div>
    </div>
    <div class="col-md-5 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <p class="text-primary">Sundry Creditor's List and Details</p>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="creditorTable" class="table table-striped table-bordered table-hover" id="tableTransfer">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Account Name</th>
                        <th>Opening Balance</th>
                        <th>Debit Amount</th>
                        <th>Credit Amount</th>
                        <th>Advance</th>
                        <th>Due</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="journal in listCount  = (journals | filter:filterlist) | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{journal.name}}</td>
                        <td style="text-align: center"><i class="fa fa-inr"></i> {{journal.openingBalance}}</td>
                        <td style="text-align: center"><i class="fa fa-inr"></i> {{journal.debit}}</td>
                        <td style="text-align: center"><i class="fa fa-inr"></i> {{journal.credit}}</td>
                        <td style="text-align: center" class="text-primary"><i ng-hide="journal.advance==''" class="fa fa-inr"></i> {{journal.advance}}</td>
                        <td style="text-align: center" class="text-danger"><i ng-hide="journal.due==''" class="fa fa-inr"></i> {{journal.due}}</td>
                    </tr>
                    <tr style="text-align: center">
                        <td colspan="5" class="text-danger">Total</td><td class="text-warning"><i class="fa fa-inr"></i> {{totalAdvance}}</td><td class="text-danger"><i class="fa fa-inr"></i> {{totalDue}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="clearfix" ng-show="journals.length > numPerPage">
    <pagination
        ng-model="currentPage"
        total-items="listCount.length"
        max-size="maxSize"
        items-per-page="numPerPage"
        boundary-links="true"
        class="pagination-sm pull-right"
        previous-text="&lsaquo;"
        next-text="&rsaquo;"
        first-text="&laquo;"
        last-text="&raquo;"
        ></pagination>
</div>


