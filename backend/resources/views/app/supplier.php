<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Supplier</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.supplier.write =='true'" ng-click="newSupplier();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Supplier</button>
            <form class="form-horizontal" ng-show="supplieredit" ng-submit="addSupplier();">
                <h3>New Supplier</h3><br>
<!--                <div class="form-group">-->
<!--                    <label for="" class="col-sm-2 control-label">Date</label>-->
<!--                    <div class="col-sm-9">-->
<!--                        <div class="input-group">-->
<!--                            <input type="text" class="form-control" datepicker-popup ng-model="newsupplier.date" is-open="ledgerpicker" show-button-bar="false" show-weeks="false" readonly>-->
<!--                            <span class="input-group-btn">-->
<!--                              <button type="button" class="btn btn-default" ng-click="ledgerpicker=true"><i class="fa fa-calendar"></i></button>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.name" placeholder="Name" required="">
                    </div>
                    <label for="" class="col-sm-1 control-label">Address</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.address" placeholder="Address">
                    </div>

                </div>
                <!--<div class="form-group">
                    <label for="" class="col-sm-2 control-label">City</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.city" placeholder="City">
                    </div>
                    <label for="" class="col-sm-1 control-label">State</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.state" placeholder="State">
                    </div>
                </div>-->
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Telephone</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.phone" placeholder="Telephone Number">
                    </div>
                    <label for="" class="col-sm-1 control-label">Mobile</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.mobile" placeholder="Mobile Number">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.email" placeholder="Email Address">
                    </div>
                    <label for="" class="col-sm-1 control-label">Credit Period</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.creditPeriod" placeholder="Credit Period">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Credit Limit</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.creditLimit" placeholder="Credit Limit">
                    </div>
                    <label for="" class="col-sm-1 control-label">TIN</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.tin" placeholder="TIN">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">CST</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.cst" placeholder="CST">
                    </div>
                    <label for="" class="col-sm-1 control-label">PAN</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.pan" placeholder="PAN">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Narration</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" ng-model="newsupplier.narration" placeholder="Narration"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Opening Balance</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" ng-model="newsupplier.openingBalance" placeholder="Opening Balance">
                    </div>
                    <label for="" class="col-sm-1 control-label">Dr/Cr</label>
                    <div class="col-sm-4">
                        <select class="form-control" ng-model="newsupplier.crOrDr">
                            <option value="Dr">Dr</option>
                            <option value="Cr">Cr</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelSupplier();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="supplieredit">Suppliers and details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-sm btn-info" ng-click="exportToExcel('supplierTable')">Export To Excel</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Suppliers and details
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="supplierTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>SlNo</th>
                    <th>Name</th>
                    <th>Address</th>
                  <!--  <th>City</th>-->
                   <!-- <th>State</th>-->
                    <th>Phone</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Open Bal</th>
                    <th>Description</th>
                    <th ng-show="extra">Created</th>
                    <th ng-show="extra">Updated</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="supplier in listCount  = (suppliers | filter:filterlist) | orderBy:'-date' | pagination: currentPage : numPerPage">
                    <td>{{supplier.id}}</td>
                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                    <td>{{supplier.name}}</td>
                    <td> {{supplier.address}}</td>
                  <!--  <td>{{supplier.city}}</td>-->
                   <!-- <td>{{supplier.state}}</td>-->
                    <td> {{supplier.phone}}</td>
                    <td>{{supplier.mobile}}</td>
                    <td>{{supplier.email}}</td>
                    <td>{{supplier.openingBalance}}</td>
                    <td><p class="description" popover="{{supplier.note}}" popover-trigger="mouseenter">{{supplier.note}}</p></td>
                    <td ng-show="extra">{{supplier.created_at}}</td>
                    <td ng-show="extra">{{supplier.updated_at}}</td>
                    <td>
                        <div ng-if="user.permissions.supplier.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                            <button type="button" class="btn btn-default" ng-click="editSupplier(supplier);">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="deleteSupplier(supplier); editmode = !editmode">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
                        </div>
                    </div>
                </div>
            <div class="clearfix" ng-show="suppliers.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>