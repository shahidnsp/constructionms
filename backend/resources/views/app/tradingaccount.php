<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Trading Account </h2>
    </div>
</div>
<div class="row text-center">
    <!--<div class="col-md-4">
        <label for="">Show
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>-->
  <!--  <div class="col-sm-3">-->
        <div>
            <button class="btn btn-sm btn-info" ng-click="exportToExcel('tradingAccountTable')">Export To Excel</button>
        </div>
   <!-- </div>-->
    <!--<div class="col-md-5 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>-->
</div>
<br>
<div class="row">
    <div class="col-sm-5 text-right">
        <div class="form-inline form-group">
            <label for="" class="control-label">From</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
        </div>
    </div>
    <div class="col-sm-6 text-left">
        <div class="form-inline form-group">
            <label for="" class="control-label">To</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
            <button class="btn btn-warning" ng-click="searchDate(fromDate,toDate)">Search</button>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <p class="text-primary"> Trading Account </p>
            <p class="text-primary">{{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}} </p>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="tradingAccountTable" class="table table-striped table-bordered table-hover" id="tableTransfer">
                    <tr>
                        <td class="text-left">Dr.</td><td class="text-right">Cr.</td>
                    </tr>
                    <tr>
                       <td>
                           <table class="table table-striped table-bordered table-hover">
                               <thead>
                                 <th>Particular</th><th></th><th>Amount</th>
                               </thead>
                               <tbody>
                                <tr ng-repeat="journal in journals" ng-if="journal.side=='Dr'">
                                   <td ng-if="journal.type=='Purchase'">To {{journal.name}}</td>
                                   <td class="text-warning" ng-if="journal.type=='Purchase'"><i class="fa fa-inr"></i> {{journal.balance}}</td>
                                   <td class="text-danger text-center" ng-if="journal.type=='Purchase'"><i class="fa fa-inr"></i> {{purchaseTotal}}</td>

                                   <td class="text-right" ng-if="journal.type=='Purchase Return'">Less {{journal.name}}</td>
                                   <td class="text-warning" ng-if="journal.type=='Purchase Return'"><i class="fa fa-inr"></i> {{journal.balance}}</td>
                                   <td ng-if="journal.type=='Purchase Return'"></td>

                                   <td ng-if="journal.type=='Other'">To {{journal.name}}</td>
                                   <td ng-if="journal.type=='Other'"></td>
                                   <td class="text-danger text-center" ng-if="journal.type=='Other'"><i class="fa fa-inr"></i> {{journal.balance}}</td>
                               </tr>
                                <tr ng-show="balanceSide=='Dr'" class="text-right text-success">
                                    <td>(To Gross profit c/d)</td><td></td><td><i class="fa fa-inr"></i> {{balance}}</td>
                                </tr>
                               </tbody>
                           </table>
                       </td>
                        <td>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <th>Particular</th><th></th><th>Amount</th>
                                </thead>
                                <tbody>
                                <tr ng-repeat="journal in journals" ng-if="journal.side=='Cr'">
                                    <td ng-if="journal.type=='Sale'">By {{journal.name}}</td>
                                    <td class="text-warning" ng-if="journal.type=='Sale'"><i class="fa fa-inr"></i> {{journal.balance}}</td>
                                    <td class="text-danger text-center" ng-if="journal.type=='Sale'"><i class="fa fa-inr"></i> {{saleTotal}}</td>

                                    <td ng-if="journal.type=='Sale Return'" class="text-right">Less {{journal.name}}</td>
                                    <td class="text-warning" ng-if="journal.type=='Sale Return'"><i class="fa fa-inr"></i> {{journal.balance}}</td>
                                    <td ng-if="journal.type=='Sale Return'"></td>

                                    <td ng-if="journal.type=='Other'">By {{journal.name}}</td>
                                    <td ng-if="journal.type=='Other'"></td>
                                    <td class="text-danger text-center" ng-if="journal.type=='Other'"><i class="fa fa-inr"></i> {{journal.balance}}</td>
                                </tr>
                                <tr ng-show="balanceSide=='Cr'" class="text-right text-danger">
                                    <td>(By Gross loss c/d)</td><td></td><td><i class="fa fa-inr"></i> {{balance}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr class="text-right text-danger">
                        <td><i class="fa fa-inr"></i> {{debitTotal}}</td><td><i class="fa fa-inr"></i> {{creditTotal}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="clearfix" ng-show="journals.length > numPerPage">
    <pagination
        ng-model="currentPage"
        total-items="listCount.length"
        max-size="maxSize"
        items-per-page="numPerPage"
        boundary-links="true"
        class="pagination-sm pull-right"
        previous-text="&lsaquo;"
        next-text="&rsaquo;"
        first-text="&laquo;"
        last-text="&raquo;"
        ></pagination>
</div>