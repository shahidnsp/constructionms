<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Trail Balance </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <label for="">Show
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-sm-3 text-center">
        <div>
            <button class="btn btn-sm btn-info" ng-click="exportToExcel('trailBalanceTable')">Export To Excel</button>
        </div>
    </div>
    <div class="col-md-5 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-5 text-right">
        <div class="form-inline form-group">
            <label for="" class="control-label">From</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
        </div>
    </div>
    <div class="col-sm-6 text-left">
        <div class="form-inline form-group">
            <label for="" class="control-label">To</label>
            <div class="input-group">
                <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
            </div>
            <button class="btn btn-warning" ng-click="searchDate(fromDate,toDate)">Search</button>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <p class="text-primary"> Trial Balance </p>
            <p class="text-primary">{{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}} </p>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="trailBalanceTable">
                    <thead>
                    <tr>
                        <th>Account Title</th>
                        <th>Debit</th>
                        <th>Credit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="journal in listCount  = (journals | filter:filterlist) | pagination: currentPage : numPerPage">
                        <td>{{journal.name}}</td>
                        <td style="text-align: center"><i ng-if="journal.debit != ''" class="fa fa-inr"></i> {{journal.debit}}</td>
                        <td style="text-align: center"><i ng-if="journal.credit != ''" class="fa fa-inr"></i> {{journal.credit}}</td>
                    </tr>
                    <tr class="text-center text-danger">
                        <td></td><td><h4><i class="fa fa-inr"></i> {{drTotal}}</h4></td><td><h4><i class="fa fa-inr"></i> {{crTotal}}</h4></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="clearfix" ng-show="journals.length > numPerPage">
    <pagination
        ng-model="currentPage"
        total-items="listCount.length"
        max-size="maxSize"
        items-per-page="numPerPage"
        boundary-links="true"
        class="pagination-sm pull-right"
        previous-text="&lsaquo;"
        next-text="&rsaquo;"
        first-text="&laquo;"
        last-text="&raquo;"
        ></pagination>
</div>