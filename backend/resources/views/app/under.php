<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-tags"></i> Account Group </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.under.write =='true'" class="btn btn-primary pull-right" type="button" ng-click="newUnder()"><i class="fa fa-plus"></i>Add Account Group</button>
            <form class="form-horizontal" ng-show="underedit" ng-submit="addUnder()">
                <h3>New Under</h3>
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" placeholder="Accounts Group Name" ng-model="newunder.name"/>
                    </div>
                    <label for="" class="col-sm-1 control-label">Under</label>
                    <div class="col-sm-5">
                        <select class="form-control" ng-model="newunder.groupUnder" required>
                            <option value="">Select</option>
                            <option ng-repeat="under in unders" value="{{under.id}}">{{under.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Nature</label>
                    <div class="col-sm-5">
                        <select class="form-control" ng-model="newunder.nature" required>
                            <option value="">Select</option>
                            <option value="Assets">Assets</option>
                            <option value="Expenses">Expenses</option>
                            <option value="Income">Income</option>
                            <option value="Liabilities">Liabilities</option>
                        </select>
                    </div>
                    <label for="" class="col-sm-2 control-label">Affect Gross Profit</label>
                    <div class="col-sm-4">
                        <select class="form-control" ng-model="newunder.affectGrossProfit">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Narration</label>
                    <div class="col-sm-11">
                        <textarea class="form-control" ng-model="newunder.narration" placeholder="Narration"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-danger" ng-click="cancelUnder()">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
            <h3>Account Groups and details</h3>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="items for items in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-md-6 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search</label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist"/>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Accounts Under
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
            <table class="table table-hover table-responsive">
                <thead>
                    <tr>
                        <th>SlNo</th>
                        <th>Name</th>
                        <th>Under</th>
                        <th ng-show="extra">created</th>
                        <th ng-show="extra">updated</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="under in listCount = (unders | filter:filterlist) | pagination: currentPage : numPerPage" ng-if="under.id != 1">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{under.name}}</td>
                        <td>{{under.parent.name}}</td>
                        <td ng-show="extra">{{under.created_at}}</td>
                        <td ng-show="extra">{{under.updated_at}}</td>
                        <td>
                            <div ng-if="user.permissions.under.edit =='true'" ng-hide="under.isedit==0" class="btn-group btn-group-xs" role="group">
                                <button type="button" class="btn btn-default" ng-click="editUnder(under)"><i class="fa fa-pencil" ></i></button>
                                <button type="button" class="btn btn-default" ng-click="deleteUnder(under); editmode = !editmode"><i class="fa fa-trash-o"></i></button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
                        </div>
                    </div>
                </div>
            <div class="clearfix" ng-show="unders.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>