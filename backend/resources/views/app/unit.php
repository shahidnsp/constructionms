<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-building-o"> </i> Unit</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.unit.write =='true'" ng-click="newUnit();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Unit</button>
            <form class="form-horizontal" ng-show="unitedit" ng-submit="addUnit();">
                <h3>New Unit</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" ng-model="newunit.name" placeholder="Name">
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <label for="chkunit" class="control-label">Is Multiunit
                            <input id="chkunit" type="checkbox" class="checkbox-inline"  ng-model="newunit.isunit">
                            </label>
                        </div>
                    </div>
                    <label ng-show="newunit.isunit" for="" class="col-sm-1 control-label">Rate</label>
                    <div ng-show="newunit.isunit" class="col-sm-2">
                        <input type="text" class="form-control" ng-model="newunit.rate" placeholder="Rate">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelUnit();">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <hr>
            </form>
            <h3 ng-hide="rentedit">Unit and details</h3>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        entries
                    </label>
                </div>
                <div class="col-md-6 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search </label>
                        <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                    </div>
                </div>
            </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Units and details
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="unitTable" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>SlNo</th>
                    <th>Name</th>
                    <th>Is Unit</th>
                    <th>Rate</th>
                    <th ng-show="extra">Created</th>
                    <th ng-show="extra">Updated</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="unit in listCount  = (units | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                    <td>{{unit.id}}</td>
                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                    <td>{{unit.name}}</td>
                    <td>{{unit.isunit}}</td>
                    <td>{{unit.rate}}</td>
                    <td ng-show="extra">{{unit.created_at}}</td>
                    <td ng-show="extra">{{unit.updated_at}}</td>
                    <td>
                        <div ng-if="user.permissions.unit.edit =='true'" class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                            <button type="button" class="btn btn-default" ng-click="editUnit(unit);">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-default" ng-click="deleteUnit(unit); editmode = !editmode">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
                            </div>
                        </div>
                    </div>

            <div class="clearfix" ng-show="units.length > numPerPage">
                <pagination
                    ng-model="currentPage"
                    total-items="listCount.length"
                    max-size="maxSize"
                    items-per-page="numPerPage"
                    boundary-links="true"
                    class="pagination-sm pull-right"
                    previous-text="&lsaquo;"
                    next-text="&rsaquo;"
                    first-text="&laquo;"
                    last-text="&raquo;"
                    ></pagination>
            </div>
        </div>
    </div>
</div>