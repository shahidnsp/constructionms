

<button type="button" class="close" ng-click="close();">
    <i class="fa fa-times-circle-o" style="margin:10px;color:blue;"></i>
</button>
<div class="modal-header">
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-credit-card"> </i> Account Group wise Balance </h2>
        </div>
    </div>
</div>
<div class="modal-body">
    <div class="container">
        <div class="row">
            <div class="toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Group wise Ledger Account Balance<span ng-if="account[0].unders_id=='29">Cash-in Hand</span><span ng-if="account[0].unders_id=='28">Bank Account</span></h3>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box">
                                <div>
                                    <h3>Ledger Account  and details</h3>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Ledger Account Balance
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table id="supplierTable" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>SlNo</th>
                                                        <th>Particular</th>
                                                        <th>Opening</th>
                                                        <th>Debit</th>
                                                        <th>Credit</th>
                                                        <th>Closing</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr ng-repeat="account in accounts" style="text-align: center;">
                                                        <td>{{$index+1}}</td>
<!--                                                        <td>{{account.ledgers_id}}</td>-->
                                                        <td><a ng-click="OpenLedgerDetails(account.ledgers_id);">{{account.name}}</a></td>
                                                        <td>{{account.openingBalance}} {{account.openingDrorCr}}</td>
                                                        <td><i class="fa fa-inr"></i> {{account.debit}}</td>
                                                        <td><i class="fa fa-inr"></i> {{account.credit}}</td>
                                                        <td class="text-danger"><i class="fa fa-inr"></i> {{account.balance}} {{account.crOrDr}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix" ng-show="suppliers.length > numPerPage">
                                        <pagination
                                            ng-model="currentPage"
                                            total-items="listCount.length"
                                            max-size="maxSize"
                                            items-per-page="numPerPage"
                                            boundary-links="true"
                                            class="pagination-sm pull-right"
                                            previous-text="&lsaquo;"
                                            next-text="&rsaquo;"
                                            first-text="&laquo;"
                                            last-text="&raquo;"
                                            ></pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <span class="pull-right">
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" ng-click="close();"><i class="fa fa-close"></i></a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



