<button type="button" class="close" ng-click="cancel();">
    <i class="fa fa-times-circle-o" style="margin:10px;color:blue;"></i>
</button>
<div class="modal-header">
    <h3 class="modal-title">Attendance List of :{{attendances[0].attendancegroup.leaders.employee.name}}</h3>
</div>
<div class="modal-body">
    <div class="container">
        <div class="row">
            <div class="toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Attendance List</h3>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Show
                                <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                                entries
                            </label>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlistGroup">
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class=" col-md-12 col-lg-12 ">
                                <table class="table table-user-information">
                                    <thead>
                                        <th>SlNo</th>
                                        <th>Employee</th>
                                        <th>Status</th>
                                        <th>Wage</th>
                                        <th>Hour</th>
                                        <th>Total</th>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="attendance in listCount  = (attendances | filter:filterlistGroup)  | pagination: currentPage : numPerPage" ng-class="attendance.status=='A' ? 'text-danger' : 'text-primary'">
                                            <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                            <td>{{attendance.employees.name}}</td>
                                            <td>{{attendance.status}}</td>
                                            <td ng-hide="attendance.status=='A'"><i class="fa fa-inr"></i> {{attendance.wage}}</td>
                                            <td ng-hide="attendance.status=='A'">{{attendance.hour}}</td>
                                            <td ng-hide="attendance.status=='A'"><i class="fa fa-inr"></i> {{attendance.total}}</td>
                                        </tr>
                                        <tr class="text-danger">
                                            <td colspan="5">Total</td><td><i class="fa fa-inr"></i> {{total}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="clearfix" ng-show="attendances.length > numPerPage">
                                    <pagination
                                        ng-model="currentPage"
                                        total-items="listCount.length"
                                        max-size="maxSize"
                                        items-per-page="numPerPage"
                                        boundary-links="true"
                                        class="pagination-sm pull-right"
                                        previous-text="&lsaquo;"
                                        next-text="&rsaquo;"
                                        first-text="&laquo;"
                                        last-text="&raquo;"
                                        ></pagination>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-right">
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" ng-click="cancel();"><i class="fa fa-close"></i></a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



