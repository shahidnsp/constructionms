

<button type="button" class="close" ng-click="close();">
    <i class="fa fa-times-circle-o" style="margin:10px;color:blue;"></i>
</button>
<div class="modal-header">
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-credit-card"> </i> Ledger Details </h2>
        </div>
    </div>
</div>
<div class="modal-body">
    <div class="container">
        <div class="row">
            <div class="toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Ledger Details from {{fromDate | date:'dd-MM-yyyy'}} to {{toDate | date:'dd-MM-yyyy'}}</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box">
                                <div>
                                    <h3>Ledger Account  and details</h3>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Ledger Account Balance
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table id="supplierTable" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>SlNo</th>
                                                        <th>Voucher Type</th>
                                                        <th>Date</th>
                                                        <th>Debit</th>
                                                        <th>Credit</th>
                                                        <th>Balance</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr ng-repeat="account in accounts | orderBy:'-date'" style="text-align: center;">
                                                        <td>{{$index+1}}</td>
                                                        <td>{{account.voucherType}}</td>
                                                        <td>{{account.date | date:'dd-MM-yyyy'}}</td>
                                                        <td><i class="fa fa-inr"></i> {{account.debit}}</td>
                                                        <td><i class="fa fa-inr"></i> {{account.credit}}</td>
                                                        <td class="text-danger text-right"><i class="fa fa-inr"></i> {{account.balance}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix" ng-show="suppliers.length > numPerPage">
                                        <pagination
                                            ng-model="currentPage"
                                            total-items="listCount.length"
                                            max-size="maxSize"
                                            items-per-page="numPerPage"
                                            boundary-links="true"
                                            class="pagination-sm pull-right"
                                            previous-text="&lsaquo;"
                                            next-text="&rsaquo;"
                                            first-text="&laquo;"
                                            last-text="&raquo;"
                                            ></pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <span class="pull-right">
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" ng-click="close();"><i class="fa fa-close"></i></a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



