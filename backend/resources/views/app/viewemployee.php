<button type="button" class="close" ng-click="cancel();">
    <i class="fa fa-times-circle-o" style="margin:10px;color:blue;"></i>
</button>
<div class="modal-header">
    <h3 class="modal-title">Employee Name:{{employee.name}}</h3>
</div>
<div class="modal-body">

    <div class="container">
        <div class="row">

            <div class="toppad" >

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{employee.name}} / Join Date:{{employee.joinDate | date:'dd-MMMM-yyyy'}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" ng-click="viewPhoto(employee);" src="images/{{employee.photo}}"  style="width: 500px;height: 120px" class="img-circle img-responsive"> </div>
                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>

                                    <tr>
                                        <td>Address:</td>
                                        <td>{{employee.address}}</td>
                                    </tr>
                                    <tr>
                                        <td>Phone:</td>
                                        <td>{{employee.phone}}</td>
                                    </tr>
                                    <tr>
                                        <td>Mobile</td>
                                        <td>{{employee.mobile}}</td>
                                    </tr>

                                    <tr>
                                    <tr>
                                        <td>Place:</td>
                                        <td>{{employee.place}}</td>
                                    </tr>
                                    <tr>
                                        <td>Basic:</td>
                                        <td><i class="fa fa-inr"></i> {{employee.basic}}</td> <td>TA:</td>
                                        <td><i class="fa fa-inr"></i> {{employee.ta}}</td>
                                    </tr>

                                    <tr>
                                        <td>DA:</td>
                                        <td><i class="fa fa-inr"></i> {{employee.da}}</td><td>HRA:</td>
                                        <td><i class="fa fa-inr"></i> {{employee.hra}}</td>
                                    </tr>

                                    <tr>
                                        <td>Medical:</td>
                                        <td><i class="fa fa-inr"></i> {{employee.medical}}</td> <td class="text-danger">Total:</td>
                                        <td class="text-danger"><i class="fa fa-inr"></i> {{employee.total}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-right">
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" ng-click="cancel();"><i class="fa fa-close"></i></a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



