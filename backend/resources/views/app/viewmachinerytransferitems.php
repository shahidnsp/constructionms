

<button type="button" class="close" ng-click="cancel();">
    <i class="fa fa-times-circle-o" style="margin:10px;color:blue;"></i>
</button>
<div class="modal-header">
    <h3 class="modal-title">Machinery Items</h3>
</div>
<div class="modal-body">

    <div class="container">
        <div class="row">

            <div class="toppad" >
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Transfer Date:{{items[0].machinerytransfer.date | date:'dd-MMMM-yyyy'}}</h3>
                    </div>

                    <div class="col-lg-12">
                        <br/>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Transfer and Details
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                        <tr>
                                            <th>SlNo</th>
                                            <th>Machinery</th>
                                            <th>Wage</th>
                                            <th>Qty</th>
                                            <th>Days</th>
                                            <th>Net</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="item in items">
                                            <td>{{$index+1}}</td>
                                            <td>{{item.machinery.name}}</td>
                                            <td><i class="fa fa-inr"></i>{{item.rate}}</td>
                                            <td>{{item.qty}}</td>
                                            <td>{{item.days}}</td>
                                            <th><i class="fa fa-inr"></i>{{item.net}}</th>
                                        </tr>
                                        <tr>
                                            <td colspan="5">Total</td><td><p class="text-danger"><i class="fa fa-inr"></i>{{total}}</p></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer">
                        <span class="pull-right">
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" ng-click="cancel();"><i class="fa fa-close"></i></a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



