<button type="button" class="close" ng-click="cancel();">
    <i class="fa fa-times-circle-o" style="margin:10px;color:blue;"></i>
</button>
<div class="modal-header">
    <h3 class="modal-title">Row Material Name:{{product.name}}</h3>
</div>
<div class="modal-body">

    <div class="container">
        <div class="row">

            <div class="toppad" >


                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{product.name}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" ng-click="viewPhoto(product);" ng-src="{{product.photo}}"  style="width: 500px;height: 120px" class="img-circle img-responsive"> </div>
                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>Description:</td>
                                        <td>{{product.description}}</td>
                                    </tr>
                                    <tr>
                                        <td>Sales Rate:</td>
                                        <td><i class="fa fa-inr"></i>{{product.sales_price}}</td>

                                    </tr>
                                    <tr>
                                        <td>Purchase Rate::</td>
                                        <td><i class="fa fa-inr"></i>{{product.purchase_price}}</td>
                                    </tr>

                                    <tr>
                                    <tr>
                                        <td>Quantity:</td>
                                        <td>{{product.qty}}</td>
                                    </tr>
                                    <tr>
                                        <td>Unit:</td>
                                        <td>{{product.units.name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Supplier:</td>
                                        <td>{{product.suppliers.name}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <span class="pull-right">
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger" ng-click="cancel();"><i class="fa fa-close"></i></a>
                        </span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



