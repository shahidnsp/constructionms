<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-credit-card"></i> Worker's Salary Advance</h2>
    </div>
</div>
<div class="row">
    <tabset class="col-md-12">
        <tab>
            <tab-heading>Wage Advance</tab-heading>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-if="user.permissions.wageadvance.write =='true'" class="btn btn-primary pull-right" type="button" ng-click="newAdvance()"><i class="fa fa-plus"></i> Wage Advance</button>
            <form class="form-horizontal" ng-show="advanceedit" ng-submit="addAdvance()">
                <h3>Salary advance</h3>
                <div class="form-group">
                    <label class="col-sm-1 control-label">Project</label>
                    <div class="col-sm-11">
                        <select class="form-control" ng-change="changeProject(newadvance.projects_id);" ng-model="newadvance.projects_id">
                            <option value="">Select</option>
                            <option ng-repeat="project in projects" value="{{project.id}}">{{project.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Employee</label>
                    <div class="col-sm-5">
                        <select class="form-control" ng-model="leaders_id">
                            <option value="">Select</option>
                            <option ng-repeat="leader in leaders" value="{{leader.id}}">{{leader.employee.name}}</option>
                        </select>
                    </div>
                    <label for="" class="col-sm-1 control-label">Date</label>
                    <div class="col-sm-5">
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="newadvance.date" is-open="wagepicker" show-button-bar="false" show-weeks="false" readonly>
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default" ng-click="wagepicker=true"><i class="fa fa-calendar"></i></button>
                                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Amount</label>
                    <div class="col-sm-11">
                        <input type="text" class="form-control" placeholder="Amount" ng-model="newadvance.amount" />
                    </div>

                </div>
                <div class="form-group">
                    <label for="" class="col-sm-1 control-label">Description</label>
                    <div class="col-sm-11">
                        <textarea class="form-control" placeholder="Description" ng-model="newadvance.description" ></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="button" class="btn btn-default" ng-click="cancelAdvance()">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>


            <h3>Wage Advance Details</h3>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Show
                        <select class="form-control pagiantion" ng-model="numPerPage" ng-options="item for item in numsForPage"></select>
                        Entries
                    </label>
                </div>
                <div class="col-sm-3 text-center">
                    <div>
                        <button class="btn btn-sm btn-info" ng-click="exportToExcel('wageAdvance')">Export To Excel</button>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline form-group">
                        <label for="filter-list">Search</label>
                        <input type="text" class="form-control" placeholder="Search" ng-model="filterlist"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5 text-right">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">From</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate"  is-open="frompicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="frompicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-inline form-group">
                        <label for="" class="control-label">To</label>
                        <div class="input-group">
                            <input type="text" class="form-control" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate"  is-open="topicker" show-button-bar="false" show-weeks="false" readonly>
										<span class="input-group-btn">
											<button type="button" class="btn btn-default" ng-click="topicker=true"><i class="fa fa-calendar"></i></button>
										</span>
                        </div>
                        <button class="btn btn-warning" ng-click="searchAdvance(fromDate,toDate);">Search</button>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="text-success">Salary Advance and details from {{fromDate | date:'dd-MMMM-yyyy'}} To {{toDate | date:'dd-MMMM-yyyy'}}</span>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="table-responsive">
                                <table id="wageAdvance" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Slno</th>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Description</th>
                                        <th ng-show="extra">Created at</th>
                                        <th ng-show="extra">Updated at</th>
                                        <th>Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr  ng-repeat="advance in listCount = (advances | filter:filterlist) | orderBy:'-created_at' | pagination:currentPage : numPerPage">
                                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                        <td><a ng-click="open(advance);">{{advance.employee.name}}</a></td>
                                        <td>{{advance.date | date:'dd-MMMM-yyyy'}}</td>
                                        <td><i class="fa fa-inr"></i> {{advance.amount}}</td>
                                        <td><p class="description" popover="{{advance.description}}" popover-trigger="mouseenter">{{advance.description}}</p></td>
                                        <td ng-show="extra">{{advance.created_at}}</td>
                                        <td ng-show="extra">{{advance.updated_at}}</td>
                                        <td>
                                            <div ng-if="user.permissions.wageadvance.edit =='true'" class="btn-group btn-group-xs" role="group">
                                                <button type="button" class="btn btn-default" ng-click="editAdvance(advance)"><i class="fa fa-pencil"></i></button>
                                                <button type="button" class="btn btn-default" ng-click="deleteAdvance(advance)"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="text-danger">
                                        <td colspan="3">Total</td><td><i class="fa fa-inr"></i> {{totalAdvance}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix" ng-show="advances.length > numPerPage">
                    <pagination
                        ng-model="currentPage"
                        total-items="listCount.length"
                        max-size="maxSize"
                        items-per-page="numPerPage"
                        boundary-links="true"
                        class="pagination-sm pull-right"
                        previous-text="&lsaquo;"
                        next-text="&rsaquo;"
                        first-text="&laquo;"
                        last-text="&raquo;"
                        ></pagination>
                </div>
            </div>
        </div>
        </div>
        </div>
    </tab>
        <tab>
            <tab-heading>Wage Advance List</tab-heading>
            <div class="box">
                <h3>Wage Advance List</h3>
                <div class="row">
                    <div class="col-md-4">
                        <label for="">Show
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="item for item in numsForPage"></select>
                            Entries
                        </label>
                    </div>
                    <div class="col-sm-3 text-center">
                        <div>
                            <button class="btn btn-sm btn-info" ng-click="exportToExcel('wageAdvanceList')">Export To Excel</button>
                        </div>
                    </div>
                    <div class="col-md-5 text-right">
                        <div class="form-inline form-group">
                            <label for="filter-list">Search</label>
                            <input type="text" class="form-control" placeholder="Search" ng-model="filterlists"/>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="text-success">Wage Advance and details </span>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <div class="table-responsive">
                                    <table id="wageAdvanceList" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>Slno</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr  ng-repeat="list in listCount = (lists | filter:filterlists) | pagination:currentPage : numPerPage">
                                            <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                            <td><a ng-click="open(list);">{{list.name}}</a></td>
                                            <td style="text-align: center"><i class="fa fa-inr"></i> {{list.balance}}</td>
                                        </tr>
                                        <tr class="text-danger">
                                            <td colspan="2">Total</td><td style="text-align: center"><i class="fa fa-inr"></i> {{totalBalance}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix" ng-show="lists.length > numPerPage">
                        <pagination
                            ng-model="currentPage"
                            total-items="listCount.length"
                            max-size="maxSize"
                            items-per-page="numPerPage"
                            boundary-links="true"
                            class="pagination-sm pull-right"
                            previous-text="&lsaquo;"
                            next-text="&rsaquo;"
                            first-text="&laquo;"
                            last-text="&raquo;"
                            ></pagination>
                    </div>
                </div>
            </div>
        </tab>
   </tabset>
</div>