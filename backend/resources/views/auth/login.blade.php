<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>

	<style>
		/*.login-form{
			position: relative;
		}
		.login-form i{
			color: #888;
			position: absolute;
			top: 10px;
			right: 10px;
		}
		.login-form i + .form-control{
			padding-right: 30px;
		}
		body { background: url(img/bg-login.jpg) !important; }*/
		body {
           background: url(/img/bg.jpg) no-repeat center center fixed !important;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        }

        .panel-default {
        opacity: 0.9;
        margin-top:30px;
        }
        .form-group.last { margin-bottom:0px; }
	</style>
	<link rel="stylesheet" href="[[ elixir("css/app.css") ]]">
</head>
<body>

	<div class="container">
	    <div class="row">
	     @if (count($errors) > 0)
         <div class="col-md-4 col-md-offset-7">
         	<div class="alert alert-danger">
         		@foreach ($errors->all() as $error)
         			<p>[[$error]] </p>
         		@endforeach
         	</div>
         </div>
         @endif
	    </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-lock"></span> Login</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="post" action="[[URL::route('login')]]">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">
                                Username</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" name="username" placeholder="Username" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">
                                Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"/>
                                        Remember me
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-success btn-sm">
                                    Sign in</button>
                                     <button type="reset" class="btn btn-default btn-sm">
                                    Reset</button>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                        Powered By <a href="http://www.psybotechnologies.com">Psybo Technologies</a></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>